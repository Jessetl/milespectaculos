window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {

    window.$ = window.jQuery = require('jquery');
    window.Popper = require('popper.js');
    window.toastr = require('toastr');
    window.Dropzone = require('./dropzone/dropzone');

    require('bootstrap');
    require('jquery-ui/ui/widgets/datepicker.js');
    require('./metisMenu/metisMenu');
    require('./lightslider/lightslider');
    require('datatables.net-bs4')();
    
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

$(function() {
    $('.datepicker').datepicker({
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        dateFormat: 'dd-mm-yy',
        minDate: 0,
        changeMonth: true,
        changeYear: true
    });
});

$(function () {
	$('#sidebar-menu, #customize-menu').metisMenu({
		activeClass: 'open'
	});


	$('#sidebar-collapse-btn').on('click', function(event){
		event.preventDefault();
		
		$("#app").toggleClass("sidebar-open");
	});

	$("#sidebar-overlay").on('click', function() {
		$("#app").removeClass("sidebar-open");
	});
});

$(function() {
	var $itemActions = $(".item-actions-dropdown");

	$(document).on('click',function(e) {
		if (!$(e.target).closest('.item-actions-dropdown').length) {
			$itemActions.removeClass('active');
		}
	});
	
	$('.item-actions-toggle-btn').on('click',function(e){
		e.preventDefault();

		var $thisActionList = $(this).closest('.item-actions-dropdown');

		$itemActions.not($thisActionList).removeClass('active');

		$thisActionList.toggleClass('active');	
	});
});

$(function() {

	$('#lightSlider').lightSlider({
        speed: 200,
        item: 6,
        useCSS: true,
        slideMove: 6,
        slideMargin: 0,
        loop: true,
        auto: true,
        pager: false,
        controls: true,
        adaptiveHeight: true,
        responsive : [
        	{
                breakpoint:800,
                settings: {
                    item: 2,
                    slideMove: 2,
                    slideMargin: 0,
                }
            },
        	{
                breakpoint:480,
                settings: {
                    item: 1,
                    slideMove: 1,
                }
            }
        ],
        onSliderLoad: function () {
            $('#lightSlider').removeClass('cS-hidden');
        }
    });
});

$(function() {

    $('#imageslider').lightSlider({
        item: 1,
        useCSS: true,
        slideMargin: 0,
        speed: 500,
        auto: false,
        autoWidth: true,
        loop: true,
        pager: false,
        controls: true,
        adaptiveHeight: true,
        onSliderLoad: function() {
            $('#imageslider').removeClass('cS-hidden');
        }  
    });

    $('.lightSliderPost').lightSlider({
        speed: 200,
        item: 1,
        slideMove: 1,
        slideMargin: 20,
        loop: false,
        auto: true,
        pager: true,
        controls: true,
        adaptiveHeight: true,
    });

    $('#GallerylightSlider').lightSlider({
        slideMove: 5,
        slideMargin: 0,
        loop: true,
        auto: false,
        pager: false,
        controls: true,
        adaptiveHeight: true,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item: 2,
                    slideMove: 2,
                    slideMargin: 0,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item: 1,
                    slideMove: 1,
                }
            }
        ],
        onSliderLoad: function () {
            $('#GallerylightSlider').removeClass('cS-hidden');
        }
    });
});

$(function() {

    $('ul .liClose').on('click', function () {

        if ($(this).hasClass('liOpen')) {

            $(this).removeClass('liOpen');
            $(this).addClass('liClose');

        } else {

            $(this).addClass('liOpen');
            $(this).removeClass("liClose");
        }

        if ($('li.liOpen i').hasClass('fa fa-plus-square-o')) {
            
            $('li.liOpen i').removeClass('fa-plus-square-o');
            $('li.liOpen i').addClass('fa-minus-square-o');

        } else {

            $('li i').removeClass('fa-minus-square-o');
            $('li i').addClass('fa-plus-square-o');
        } 
    });
});

$(function() {
    $('.popover-example').popover();
});

$(function() {
    $('.popover-dismiss').popover({
    trigger: 'focus'
    })
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});


$(function () {
    $('map[data-toggle="tooltip"]').tooltip({
        animated: 'fade',
        placement: 'right',
    })
});

$(function() {
    $('.carousel').carousel({
        interval: 2000
    })
});

// Disable auto discover for all elements:
Dropzone.autoDiscover = true;

$(function () {

    $('select[name=type_amount]').on('change', function(e) {
        
        var type = $('select[name=type_amount]').val();

        if ( type == 'Otro' ) {

            $('input[name=amount]').removeAttr('disabled');

        } else {

            $('input[name=amount]').attr('disabled', 'disabled');
        }
    });
});

$(function () {

    $('#category').on('change', function () {

        let category = $('select[name=category]').val();

        $.get(`/get/${category}`, function ( data ) {

            $('#subcategory').empty();

            for (let i = 0; i < data.length ; i++) {

                $('#subcategory').append('<option value="'+ data[i].id + '" class="text-uppercase">' + data[i].name +'</option>');
            }
        });
    });
});

$(function () {
    
    let count = 0;

    $('#add').on('click', function () {
        
        $('#div_clone').clone().appendTo('#clone').end();

        count++;

    });  

    $('#remove').on('click', function () {
        
        if (count != 0) {
            
            $('#div_clone:last').remove();

            count--;
        } 
    });
});

$(function () {

    $('select[name=type_post]').on('change', function () {

        let type = $('#type_post option:selected').html();

        if ( type == 'DEMANDA') 
        {
            alert('Para ver las demandas de contrataciones tiene que ser VIP');
        }
    });
});

$(function () {

    $('select[name=type_posts]').on('change', function () {
        
        let type = $('#type_posts option:selected').val();
        
        if ( type == 0 ) 
        {
            $('#lengthBlock').addClass('d-none');
            $('#lengthBlock').removeClass('d-block');

        } else {

            $('#lengthBlock').addClass('d-block');
            $('#lengthBlock').removeClass('d-none');
        }
    });

});

$(function() {

    $('.image').on('click', function(e) {

        let image = $(this).attr('rel');
        
        $('#image').hide();
        $('#image').fadeIn('slow');

        $('#image').html('<img src="' + image + '" width="600" height="400"/>');
           
        return false;
    });
});

// Ajax para guardar videos de anuncio.

$(function() {

    $('#formMovies').on('submit', function (e) {

        e.preventDefault(); 

        if (!formMovies.checkValidity()) {

            return false;
        }

        let id = $('input[name=post]').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let fields = {
            video: $('input[name=video]').val()
        }

        $.ajax({

            url: `/posts/${id}/movies`,
            type: 'PUT',
            dataType: 'json',
            data: fields,

            success: function (data) {

                if ( data.success ) {

                    toastr.success(
                    data.success, '', {
                        timeOut: 5000
                    });

                    document.getElementById('formMovies').reset();

                } else {

                    toastr.error(
                    data.error, '', {
                        timeOut: 5000
                    });
                }
            },

            error: function (data) {
                
                var errors = data.responseJSON;

                jvalidator(errors);
            }
        });

        function jvalidator (errors) {

            $.each(errors, function (index, value) {
                
                $("#"+index).addClass('is-invalid');

            });
        }
    });
});

// Ajax para guardar videos en cookies de anuncio.

$(function() {

    $('#formMoviesCookies').on('submit', function (e) {

        e.preventDefault(); 

        if (!formMoviesCookies.checkValidity()) {

            return false;
        }

        let id = $('input[name=post]').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let fields = {
            video: $('input[name=video]').val()
        }

        $.ajax({

            url: `/upload/videos`,
            type: 'POST',
            dataType: 'json',
            data: fields,

            success: function (data) {

                if ( data.success ) {

                    toastr.success(
                    data.success, '', {
                        timeOut: 5000
                    });

                    document.getElementById('formMoviesCookies').reset();

                } else {

                    toastr.error(
                    data.error, '', {
                        timeOut: 5000
                    });
                }
            },

            error: function (data) {
                
                var errors = data.responseJSON;

                jvalidator(errors);
            }
        });

        function jvalidator (errors) {

            $.each(errors, function (index, value) {
                
                $("#"+index).addClass('is-invalid');

            });
        }
    });
});

// Ajax para guardar audio en cookies de anuncios.

$(function() {

    $('#uploadSoundsCookies').on('submit', function (e) {

        e.preventDefault(); 

        if (!uploadSoundsCookies.checkValidity()) {

            return false;
        }

        let id = $('input[name=post]').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let fields = {
            audio: $('input[name=audio]').val()
        }

        $.ajax({

            url: `/upload/audios`,
            type: 'POST',
            dataType: 'json',
            data: fields,

            success: function (data) {

                if ( data.success ) {

                    toastr.success(
                    data.success, '', {
                        timeOut: 5000
                    });

                } else {

                    toastr.error(
                    data.error, '', {
                        timeOut: 5000
                    });
                }
                
                document.getElementById('uploadSoundsCookies').reset();
            },

            error: function (data) {
                
                var errors = data.responseJSON;

                jvalidator(errors);
            }
        });

        function jvalidator (errors) {

            $.each(errors, function (index, value) {
                
                $("#"+index).addClass('is-invalid');

            });
        }
    });
});


// Ajax para guardar audio de anuncios.

$(function() {

    $('#uploadSounds').on('submit', function (e) {

        e.preventDefault(); 

        if (!uploadSounds.checkValidity()) {

            return false;
        }

        let id = $('input[name=post]').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let fields = {
            audio: $('input[name=audio]').val()
        }

        $.ajax({

            url: `/posts/${id}/sounds`,
            type: 'PUT',
            dataType: 'json',
            data: fields,

            success: function (data) {

                if ( data.success ) {

                    toastr.success(
                    data.success, '', {
                        timeOut: 5000
                    });

                } else {

                    toastr.error(
                    data.error, '', {
                        timeOut: 5000
                    });
                }
                
                document.getElementById('uploadSounds').reset();
            },

            error: function (data) {
                
                var errors = data.responseJSON;

                jvalidator(errors);
            }
        });

        function jvalidator (errors) {

            $.each(errors, function (index, value) {
                
                $("#"+index).addClass('is-invalid');

            });
        }
    });
});

$(function() {

    $('#postEvents').on('submit', function (e) {

        e.preventDefault(); 

        if (!postEvents.checkValidity()) {

            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let fields = $( this ).serialize();

        $.ajax({

            url: `/my-events`,
            type: 'POST',
            dataType: 'json',
            data: fields,

            success: function (data) {

                if ( data.success ) {

                    toastr.success(
                    data.success, '', {
                        timeOut: 5000
                    });

                    document.getElementById('formMovies').reset();

                } else {

                    toastr.error(
                    data.error, '', {
                        timeOut: 5000
                    });
                }
                
                document.getElementById('postEvents').reset();
            },

            error: function (data) {
                
                var errors = data.responseJSON;

                jvalidator(errors);
            }
        });

        function jvalidator (errors) {

            $.each(errors, function (index, value) {
                
                $("#"+index).addClass('is-invalid');

            });
        }

    });
});

$(function() {

    $('#append').on('click', function () {

        $.get(`/get/districts`, function (response) {
            
            $('#appendNew').append(
            '<div class="row react-id">'+
                '<div class="col">'+
                    '<div class="form-group">'+
                        '<label for="circuit">Provincias <span class="text-danger">(*)</span></label>'+
                        '<select id="districts" class="form-control districts" popover-dismiss" name="districts[]" data-toggle="popover" data-placement="top" data-content="Selecciona la provincia donde deseas publicar tu oferta/demanda" required>'+
                        '</select>'+
                    '</div>'+
                '</div>'+
                '<div class="col">'+
                    '<div class="form-group">'+
                        '<label for="circuit">Población <span class="text-danger">(*)</span></label>'+
                        '<input type="text" id="circuit" class="form-control" popover-dismiss" name="circuits[]" data-toggle="popover" data-placement="right" data-content="Escribe la localidad donde deseas publicar tu oferta/demanda"/>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-1 pl-1 pt-4">'+
                    '<button type="button" class="btn btn-link remove">Borrar</button>'+
                '</div>'+
            '</div>');

            $(document).on('click', '.remove', function(e){ 
                $(this).closest('div').parent('div').remove();
            });
            
            $.each(response, function(index, typeObj) {
                $('.districts').append('<option value="'+ typeObj.id +'"> '+ typeObj.name +' </option>');
            });
        });
    });

    $('#service').on('click', function () {

        let chek = $('input[name=service]').prop('checked');
        
        if ( chek == true ) {
            $('select[name="districts[]"]').prop('disabled', 'disabled');
            $('input[name="circuits[]"]').prop('disabled', 'disabled');
            $('#append').hide();
        } else {
            $('select[name="districts[]"]').removeAttr('disabled');
            $('input[name="circuits[]"]').removeAttr('disabled');
            $('#append').show();
        }
    });
});

// Ajax para guardar favoritos

$(function () {

    $('.js__favorite').on('click', function (e) {

        e.preventDefault(); 

        let id = this.id;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let fields = {
            slug: id
        };

        $.ajax({

            url: `/favorite/add`,
            type: 'POST',
            dataType: 'json',
            data: fields,

            success: function (data) {

                toastr.success(
                    data.success, '', {
                    timeOut: 5000
                });

                if ( $('[data-ref="'+id+'"]').hasClass('text-danger') ) {
                $('[data-ref="'+id+'"]').removeClass('text-danger');
                } else {
                $('[data-ref="'+id+'"]').addClass('text-danger');
                }
                
            },

            error: function (data) {
                
                toastr.error(
                    data.error, '', {
                    timeOut: 5000
                });
            }
        });
    });
});

// Ajax para guardar favoritos en cookies

$(function () {

    $('.js__fav').on('click', function (e) {

        e.preventDefault(); 

        let id = this.id;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let fields = {
            slug: id
        };

        $.ajax({

            url: `/my-favorites/store`,
            type: 'POST',
            dataType: 'json',
            data: fields,

            success: function (data) {

                toastr.success(
                    data.success, '', {
                    timeOut: 5000
                });

                if ( $('[data-ref="'+id+'"]').hasClass('text-danger') ) {
                $('[data-ref="'+id+'"]').removeClass('text-danger');
                } else {
                $('[data-ref="'+id+'"]').addClass('text-danger');
                }
                
            },

            error: function (data) {
                
                toastr.error(
                    data.error, '', {
                    timeOut: 5000
                });
            }
        });
    });
});

$(function() {
    
    $('.form-check-input').on('click', function (e) {
        
        let id = this.value;
        
        if( $(this).prop('checked') ) {

            $.get(`/get/${id}/district`, function (response) {

                $('#ajax__dt').append(
                '<div class="form-group" id="'+ response.id +'">'+
                    '<label for="circuit"> '+ response.name +' </label>'+
                    '<input type="text" id="circuit" class="form-control" popover-dismiss" name="circuits[' + response.id + '][]" data-toggle="popover" data-placement="right" data-content="Escribe la localidad donde deseas publicar tu oferta/demanda"/>'+
                '</div>');

            });

        } else {

            $("#" + id).remove();
        }
    });
});

// Jquery para mostrar el monto y costo de publicación.

$(function () {

    let ct = 0;

    $.get(`/get/payment`, function (response) {

        let amount = response.amount;
        let total = 0;
        let last_amount = 0;

        $('#amount').text(response.amount);

        $('.form-check-input').on('click', function (e) {

            if( $(this).prop('checked') ) {

                
                ct++;

                total += 1;
                total += amount * 0.21;

                last_amount = 1 + amount * 0.21;

            } else {

                ct--;

                total -= last_amount;
            }

            $('#payment_ct').text(ct);
            $('#total').text(total.toFixed(2));

            
        }); 

    });
});

$(function() {

    $('button.renewPostsJs').on('click', function () {

        $('#renewPosts').modal('show');

        $('a#renewJqueryPost').data('slug', $(this).data('post'));
    });

    $('a#renewJqueryPost').on('click', function () {

        var slug = $(this).data('slug');

        $.ajax({

            url: `/my-posts/renew/${slug}`,
            type: 'GET',
            dataType: 'json',
   

            success: function (data) {

                toastr.success(
                    data.success, '', {
                    timeOut: 5000
                });
            },

            error: function (data) {

                if( data.status === 422 ) {
                    toastr.error(
                        data.responseJSON.error, '', {
                        timeOut: 5000
                    });
                }
            }
        });
    });

});

$(function () {

    $('button.js__file').on('click', function () {

        $('#modalFiles').modal('show');
        $( 'div#js__files' ).empty();

        var slug = $(this).data('id');

        $.ajax({

            url: `/post/${slug}/files`,
            type: 'GET',
            dataType: 'json',
   

            success: function (data) {
                
                var i = 0;

                $.each(data.files, function( key, files ) {
                    
                    if ( data.files.length === 0 ) {

                        $( 'div#js__files' ).append('<div class="text-center border mb-2"><strong>NO HAY NINGÚN MP3 CARGADO</strong></div>');

                    } else {
                        
                        if ( files.meta_key == 'Video' ) {

                            $( 'div#js__files' ).append('<iframe width="466" height="205" src="'+files.meta_url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');

                            i++; 

                        } else if ( files.meta_key == 'Audio' ) {

                            $( 'div#js__files' ).append('<iframe width="100%" scrolling="no" height="100" frameborder="no" src="https://w.soundcloud.com/player/?url='+files.meta_url+'"></iframe>');
                            
                            i++;    
                        }
                    }

                });

                if ( i == 0 ) {

                    $( 'div#js__files' ).append('<div class="text-center border mb-2"><strong>NO HAY NINGÚN MP3 CARGADO</strong></div>');
                } 
            },

            error: function (data) {

            }
        });
    });
});

$(function () {

    $('select#category').on('change', function () {
        
        let category = $('#subcategory').val();

        if ( category !== '' ) {

            $('div#well').css('display', 'block');

        } else {

            $('div#well').css('display', 'none');
        }

    });
});
