
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('Publication', require('./components/Publication.vue'));
Vue.component('LoadImages', require('./components/LoadImages.vue'));
Vue.component('LoadData', require('./components/LoadData.vue'));
Vue.component('Followers', require('./components/Followers.vue'));

Vue.component('Profile', require('./components/profile/edit.vue'));
Vue.component('To', require('./components/to/transfer.vue'));
Vue.component('budgets-events', require('./components/budgets/events.vue'));
Vue.component('budgets-posts', require('./components/budgets/posts.vue'));
Vue.component('Events', require('./components/events/create.vue'));

const app = new Vue({
  el: '#app'
 
});
