<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Mil Espectaculos') }}</title>

    @include('layouts.partials.htmlheader')
</head>
<body class="loaded">
    <div class="main-wrapper">
        <div class="app" id="app">
            
            @include('layouts.partials.header')
            
            @include('layouts.partials.sidebar-admin')

            <article class="content items-list-page">
               
                @yield('content')
            
            </article>
            @include('layouts.partials.footer')
        </div>
    </div>

    @include('layouts.partials.scripts')

</body>
</html>
