<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="description" content="MIL ESPECTÁCULOS es tu Guía personalizada de Artistas y Empresas de eventos a nivel nacional, donde podras encontrar todo lo que puedas imaginar para tus Fiestas u Eventos ¿Si eres Artista o Empresa organizadora de Eventos  ¿A que esperas para crear tu perfil Gratis?">
		<title>{{ config('app.name', 'Laravel') }}</title>

		{!! NoCaptcha::renderJs() !!}

		@include('layouts.partials.htmlheader')
	</head>
<body>
	<!-- Cookies -->
	<div id="cookie-message" data-cookie-expiry="60">
		<p>
		<br>
			Mil Espectáculos Usamos cookies para mejorar tu navegación en nuestros sitio web. Al navegar en este sitio web, estás de acuerdo con nuestro uso de las cookies.
		</p>
		<p>
			<button class="btn btn-primary" onclick="setAgreeCookie()">Aceptar</button>
		</p>
	</div>
	<script>
		// Detect JS support
		document.body.className = document.body.className + "js_enabled";

	</script>
	<nav class="navbar navbar-expand-lg navbar-light bg-light {{ isset($header) ? 'transparent' : '' }}">
		<div class="container-fluid">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    	<span class="navbar-toggler-icon"></span>
		  	</button>

		  	<div class="collapse navbar-collapse {{ isset($header) ? 'font-weight-normal' : '' }}" id="menu">
				<ul class="navbar-nav mr-auto">
					@if (Auth::guest())
					  	<li class="nav-item">
					    	<a class="nav-link" href="{{ route('home') }}"> INICIO </a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="{{ route('register') }}"> REGISTRATE GRATIS </a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="{{ route('login') }}"> INICIAR SESIÓN </a>
					  	</li>
					@else
						@if (Auth::user()->role == 2 OR Auth::user()->role == 1)
							<li class="nav-item">
						    	<a class="nav-link" href="#"> Bienvenido, {{ Auth::user()->name }}</a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('home') }}"> INICIO </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('user.profile') }}"> PERFIL </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('mails.index') }}"> NOTIFICACIONES </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('master') }}"> C-PANEL </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('netw') }}"> RED SOCIAL </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('services.demands') }}"> DEMANDA DE SERVICIOS</a>
						  	</li>
						  	<li class="nav-item">
						    	<a href="{{ route('logout') }}"
				                onclick="event.preventDefault();
				                document.getElementById('logout-form').submit();" class="nav-link">
				                CERRAR SESIÓN
				            	</a>
						  	</li>
						  	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				                {{ csrf_field() }}
				            </form>
						@else
							<li class="nav-item">
						    	<a class="nav-link" href="#"> Bienvenido, <span class="text-uppercase">{{ Auth::user()->name }}</span></a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('home') }}"> INICIO </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('user.profile') }}"> PERFIL </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{ route('mails.index') }}"> NOTIFICACIONES </a>
						  	</li>
						  	@if (Auth::user()->premium)
							  	<li class="nav-item">
							    	<a class="nav-link" href="{{ route('netw') }}"> RED SOCIAL </a>
							  	</li>
							  	<li class="nav-item">
							    	<a class="nav-link" href="{{ route('services.demands') }}"> DEMANDA DE SERVICIOS</a>
							  	</li>
						  	@endif
						  	<li class="nav-item">
						    	<a href="{{ route('logout') }}"
				                onclick="event.preventDefault();
				                document.getElementById('logout-form').submit();" class="nav-link">
				                CERRAR SESIÓN
				            	</a>
						  	</li>
				            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				                {{ csrf_field() }}
				            </form>
			            @endif
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('logo')

	@yield('header')

	<div id="app">
		@yield('content')
	</div>

	@include('layouts.partials.footerfrontend')

	@include('layouts.partials.scripts')
</body>
</html>
