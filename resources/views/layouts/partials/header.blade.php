<header class="header">
    <div class="header-block header-block-collapse d-lg-none d-xl-none">
        <button class="collapse-btn" id="sidebar-collapse-btn"><i class="fa fa-bars"></i></button>
    </div>
    <div class="header-block header-block-nav">
        <ul class="nav-profile">
            <li class="profile dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img src="{{ empty(\Auth::user()->url) ? asset('img/avatar.png') : asset('storage/' . \Auth::user()->url) }}" width="40" height="40">
                    <span class="name"> {{ is_null(\Auth::user()->name) ? 'Account' : \Auth::user()->name }} </span>
                </a>
                <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                    <a class="dropdown-item" href="{{ route('profile.index') }}">
                        <i class="fa fa-user icon"></i> Perfil 
                    </a>
                    <a class="dropdown-item" href="#">
                        <i class="fa fa-bell icon"></i> Notificaciones 
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off icon"></i> Salir </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
        </ul>
    </div>
</header>
