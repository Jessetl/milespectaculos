<footer class="main-footer mt-5">
	<div class="container-fluid">
		<div class="row justify-content-around">
			<div class="col-md-3">
				<div class="text-center">
					<img src="{{ asset('/img/logo.png') }}" width="310" class="img-fluid">
				</div>
			</div>
			<div class="font-italic col-md-3">
				<div class="pt-4 text-white">
					LA EMPRESA
				</div>
				<div class="pt-3">
					<a href="{{ route('about_us') }}" class="text-white d-block">¿QUIENES SOMOS?</a>
					<a href="{{ route('prices') }}" class="text-white d-block">Blog</a>
					<a href="{{ route('help') }}" class="text-white d-block">¿Como funciona?</a>
					<a href="{{ route('questions') }}" class="text-white d-block">Preguntas frecuentes</a>
				</div>
			</div>
			<div class="font-italic col-md-3">
				<div class="pt-4 text-white">
					INFORMACIÓN
				</div>
				<div class="pt-3">
					<a href="{{ url('legal') }}" class="text-white d-block">Aviso legal</a>
					<a href="{{ url('legal') }}" class="text-white d-block">Terminos de Uso</a>
					<a href="{{ url('legal') }}" class="text-white d-block">Politica de Cookies</a>
				</div>
			</div>
			<div class="font-italic col-md-3">
				<div class="pt-4 text-white">
					SOPORTE
				</div>
				<div class="pt-3">
					<a href="{{ url('contact/us') }}" class="text-white d-block">Contacto</a>
					<a href="{{ url('login') }}" class="text-white d-block">Acceso</a>
					<a href="#" class="text-white d-block">Mapa del sitio</a>
					<a href="https://web.whatsapp.com/send?phone=34633774008&text=" target="_blank" class="text-white d-block">Whatsapp <img src="{{ asset('img/Whatsapp_37229.png') }}"></a>
					<a href="{{ url('contact/us') }}" class="text-white d-block">Buzón de sugerencias</a>
					<a href="{{ url('contact/us') }}" class="text-white d-block">Soporte Tecnico</a>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="container-fluid">
		<div class="footer-logo">
			<img src="{{ asset('/img/artistas.png') }}">
		</div>
	</div> -->
</footer>
