<div class="mobile-menu-handle"></div>
<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-container">
            <div class="sidebar-header">
                <div class="brand">
                    <div class="logo">
                        <span class="l l1"></span>
                        <span class="l l2"></span>
                        <span class="l l3"></span>
                        <span class="l l4"></span>
                        <span class="l l5"></span>
                    </div>
                    {{ \Auth::user()->rol->name }}
                </div>
            </div>
            <nav class="menu">
                <ul class="sidebar-menu metismenu" id="sidebar-menu">
                    <li>
                        <a href="{{ url('/') }}"> / </a>
                    </li>
                    <li>
                        <a href="#" aria-expanded="false">
                            Headers
                            <i class="fa arrow"></i>
                        </a>
                        <ul class="sidebar-nav collapse" aria-expanded="false">
                            <li>
                                <a href="{{ route('headers_pages') }}"> Páginas </a>
                            </li>
                            <li>
                                <a href="{{ route('headers.index') }}"> Categorías </a>
                            </li>
                            <li>
                                <a href="{{ route('headers.categories-type') }}"> Subcategorías </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('carousel.index') }}"> Carrusel </a>
                    </li>
                    <li>
                        <a href="{{ route('encrypted.index') }}"> Códigos </a>
                    </li>
                    <li>
                        <a href="{{ route('mailbox.index') }}"> Contactos </a>
                    </li>
                    <li>
                        <a href="#" aria-expanded="false">
                            Gestiones
                            <i class="fa arrow"></i>
                        </a>
                        <ul class="sidebar-nav collapse" aria-expanded="false">
                            <li>
                                <a href="{{ route('budgets.index') }}"> Eventos </a>
                            </li>
                            <li>
                                <a href="{{ route('budgetsPost.index') }}"> Anuncios </a>
                            </li>
                            <li>
                                <a href="{{ route('manages.index') }}"> Comisiones </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" aria-expanded="false">
                            Facturaciones
                            <i class="fa arrow"></i>
                        </a>
                        <ul class="sidebar-nav collapse" aria-expanded="false">
                            <li>
                                <a href="{{ route('advertisements.index') }}"> Anuncios </a>
                            </li>
                            <li>
                                <a href="{{ route('subscriptions.index') }}"> Suscripciones </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" aria-expanded="false">
                            Servicios
                            <i class="fa arrow"></i>
                        </a>
                        <ul class="sidebar-nav collapse" aria-expanded="false">
                            <li>
                                <a href="{{ route('categories.index') }}"> Categorías </a>
                            </li>
                            <li>
                                <a href="{{ route('categories-type.index') }}"> Subcategorías </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" aria-expanded="false">
                            Anuncios
                            <i class="fa arrow"></i>
                        </a>
                        <ul class="sidebar-nav collapse" aria-expanded="false">
                            <li>
                                <a href="{{ route('published.index') }}"> Publicados </a>
                            </li>
                            <li>
                                <a href="{{ route('complaints.index') }}"> Denunciados </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('users.index') }}"> Usuarios </a>
                    </li>
                    <li>
                        <a href="{{ route('events.index') }}"> Eventos </a>
                    </li>
                    <li>
                        <a href="{{ route('config.index') }}"> Configuraciones </a>
                    </li>
                    <li>
                        <a href="{{ route('paypal.index') }}"> Paypal </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</aside>
<div class="sidebar-overlay" id="sidebar-overlay"></div>
<div class="sidebar-mobile-menu-handle" id="sidebar-mobile-menu-handle"></div>