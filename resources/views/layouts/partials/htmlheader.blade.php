<!-- Icon -->
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/icons/icon.ico') }}">
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_TRACKING_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109155667-1');
</script>
