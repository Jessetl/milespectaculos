<header class="wrapper header-frontend {{ isset($header) ? 'hasHeader' : '' }}" @if( isset($header) ) style="background-image: url({{ asset('storage/headers/'. $header) }}); background-size: cover; background-position: center center; background-repeat: no-repeat;" @endif>
    <div class="container">
        <div class="text-center">
            <div class="wrapper-logo">
                <a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" class="img-fluid mt-4"></a>
            </div>
            <div class="wrapper-text">
                <h1 class="text-uppercase">COMIENZA EL ESPECTÁCULO</h1>
            </div>
        </div>
        <div class="row search justify-content-center">
            <div class="col-md-6">
                <div class="search-form">
                {!! Form::open(['route' => 'search.index','method' => 'GET']) !!}
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Qué buscas?">
                        <div class="input-group-prepend">
                            <input type="submit" class="input-group-text primary" value="BUSCAR">
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
                <div class="boxmap">
                    @include('layouts.partials.map')
                </div>
            </div>
        </div>
    </div>
</header>
