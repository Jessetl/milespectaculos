<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/cookies-message.js') }}"></script>

@yield('scripts')