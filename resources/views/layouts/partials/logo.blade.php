<header class="wrapper header-frontend" 
	@if( isset($header) ) 
		style="
		background-image: url({{ asset('storage/headers/'. $header) }}); 
		background-size: cover;
        background-repeat: no-repeat;
        margin-bottom: 0.1rem;
        background-position-y: center;
		height: 468px;"
	@endif
>

    @if( isset($logo) == (FALSE) )
        <div class="container">
            <div class="text-center">
                <div class="wrapper-logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('img/logo.png') }}" class="img-fluid mt-4">
                    </a>
                </div>
            </div>
        </div>
    @endif
</header>