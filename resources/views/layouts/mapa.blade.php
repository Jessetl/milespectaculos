<img src="{{ asset('./img/mapa.png') }}" alt="" usemap="#Map" style="margin: -70px 0px -60px -13px;" />
<map name="Map" id="Map">
    <area alt="" title="León" href="{{ url('leon/posts') }}" shape="poly" 
    coords="45,103,45,100,43,96,43,91,46,88,50,88,55,87,58,87,75,85,76,91,75,94,73,98,71,102,63,105" />
    <area alt="" title="Asturias" href="{{ url('asturias/posts') }}" shape="poly" 
    coords="73,82,81,79,69,77,65,76,61,75,57,75,54,77,44,76,43,80,45,83,48,85,52,85,56,85" />
    <area alt="" title="A Coruña" href="{{ url('a-coruna/posts') }}" shape="poly"
    coords="8,88,5,85,5,82,9,81,13,79,16,79,21,79,25,74,29,72,25,90,19,90,15,92,12,92,10,93" />
    <area alt="" title="Lugo" href="{{ url('lugo/posts') }}" shape="poly" 
    coords="30,97,30,89,29,82,30,80,34,75,39,78,39,83,39,85,40,89,40,92,40,94,40,97,39,101,37,100" />
    <area alt="" title="Pontevedra" href="{{ url('pontevedra/posts') }}" shape="poly" 
    coords="9,110,11,106,11,103,11,100,12,95,18,92,21,92,27,90,25,92,25,94,22,95,19,96,19,99,19,102,21,103,20,105" />
    <area alt="" title="Ourense" href="{{ url('ourense/posts') }}" shape="poly" 
    coords="23,112,24,109,26,107,23,104,21,100,21,97,27,95,31,100,34,102,43,101,40,104,38,107,37,110,34,110" />
    <area alt="" title="Zamora" href="{{ url('zamora/posts') }}" shape="poly" 
    coords="43,108,47,106,50,105,54,106,57,107,59,108,63,109,65,111,67,119,67,123,67,125,59,123,55,124,52,117" />
    <area alt="" title="Valladolid" href="{{ url('valladolid/posts') }}" shape="poly" 
    coords="68,108,71,106,74,106,75,109,75,111,79,114,88,115,90,117,90,119,90,121,84,123,82,124,80,125,78,128,70,125" />
    <area alt="" title="Palencia" href="{{ url('palencia/posts') }}" shape="poly" 
    coords="75,102,78,93,80,89,84,89,87,92,88,94,88,96,88,98,87,101,87,104,91,108,93,110,91,112,87,112,84,111,81,110,79,108,77,105" />
    <area alt="" title="Salamanca" href="{{ url('salamanca/posts') }}" shape="poly" 
    coords="45,132,49,129,53,127,56,128,60,129,65,128,71,128,73,130,61,145,57,146,50,142,46,145,43,146,44,140,45,135" />
    <area alt="" title="Cantabria" href="{{ url('cantabria/posts') }}" shape="poly" 
    coords="79,86,79,83,83,81,87,80,90,78,93,77,96,77,100,77,103,78,103,80,99,81,97,82,93,85,93,88,93,90,86,86" />
    <area alt="" title="Burgos" href="{{ url('burgos/posts') }}" shape="poly" 
    coords="91,93,95,93,96,89,96,86,100,84,105,86,105,89,105,93,106,97,108,102,108,106,107,109,105,112,102,118,99,118,95,117,92,116,94,112,93,107,92,104,91,99,92,96" />
    <area alt="" title="Vizcaya" href="{{ url('vizcaya/posts') }}" shape="poly" 
    coords="106,84,107,82,109,83,111,80,114,80,115,83,113,87" />
    <area alt="" title="Avila" href="{{ url('avila/posts') }}" shape="poly" 
    coords="65,146,71,140,72,138,72,135,72,132,76,128,87,139,84,143,81,147,68,148,71,151,74,149" />
    <area alt="" title="Caceres" href="{{ url('caceres/posts') }}" shape="poly" 
    coords="53,173,56,173,58,173,62,173,67,171,70,169,71,164,71,161,67,157,53,149,50,149,45,158,38,163,47,151,45,151,38,166,44,167" />
    <area alt="" title="Toledo" href="{{ url('toledo/posts') }}" shape="poly" 
    coords="75,152,77,152,79,151,82,150,84,149,87,149,90,150,92,150,96,151,98,154,107,155,108,160,109,162,103,166,101,167,98,168,93,167,91,162,76,163,73,157,71,156" />
    <area alt="" title="Segovia" href="{{ url('segovia/posts') }}" shape="poly" 
    coords="82,129,85,126,87,123,93,121,101,121,102,125,93,133,91,136,87,136" />
    <area alt="" title="Alava" href="{{ url('alava/posts') }}" shape="poly" 
    coords="110,90,112,88,116,88,118,90,118,92,118,94,118,96,116,96,113,96" />
    <area alt="" title="Badajoz" href="{{ url('badajoz/posts') }}" shape="poly" 
    coords="40,172,39,169,48,172,52,174,54,174,58,174,65,174,68,174,71,172,75,167,77,167,73,178,70,181,67,183,64,185,63,189,63,192,59,193,55,194,53,196,49,195,45,194,38,189,40,181,43,177" />
    <area alt="" title="Huelva" href="{{ url('huelva/posts') }}" shape="poly" 
    coords="34,214,33,208,42,195,52,202,49,203,47,207,49,211,49,220,47,220,41,215" />
    <area alt="" title="Cadiz" href="{{ url('cadiz/posts') }}" shape="poly" 
    coords="49,228,49,225,53,225,56,225,59,226,61,224,64,223,66,222,69,222,67,225,66,228,65,231,67,235,67,239,63,241" />
    <area alt="" title="Sevilla" href="{{ url('sevilla/posts') }}" shape="poly" 
    coords="55,200,58,198,61,197,71,210,77,213,76,219,71,221,65,220,59,222,52,219,51,215" />
    <area alt="" title="Malaga" href="{{ url('malaga/posts') }}" shape="poly" 
    coords="71,226,74,221,79,216,81,216,83,216,87,217,94,225,85,228,82,229,78,231,74,233,70,234,68,232,69,227" />
    <area alt="" title="Granada" href="{{ url('granada/posts') }}" shape="poly" 
    coords="96,214,102,209,117,203,123,199,117,211,107,229,103,229,96,225,93,220" />
    <area alt="" title="Almeria" href="{{ url('almeria/posts') }}" shape="poly" 
    coords="113,225,117,226,121,225,125,225,129,221,133,209,130,208,128,207,125,207,114,221" />
    <area alt="" title="Córdoba" href="{{ url('cordoba/posts') }}" shape="poly" 
    coords="85,215,81,211,77,210,75,205,66,187,69,186,73,184,75,184,77,185,81,187,84,190,87,192" />
    <area alt="" title="Ciudad Real" href="{{ url('ciudad-real/posts') }}" shape="poly" 
    coords="77,179,79,175,82,172,83,170,87,169,89,171,94,171,99,171,102,171,106,171,110,171,112,174,112,179,113,182,113,185,95,186,93,187,91,187,85,187" />
    <area alt="" title="Madrid" href="{{ url('madrid/posts') }}" shape="poly" 
    coords="88,145,100,133,101,139,103,143,105,146,106,149,103,151,100,151,95,147,92,146" />
    <area alt="" title="Guipuzcoa" href="{{ url('guipuzcoa/posts') }}" shape="poly" 
    coords="118,80,127,78,120,90" />
    <area alt="" title="Navarra" href="{{ url('navarra/posts') }}" shape="poly" 
    coords="120,97,131,82,145,89,137,97,136,101,135,103,135,105,135,107,133,109" />
    <area alt="" title="Soria" href="{{ url('soria/posts') }}" shape="poly" 
    coords="109,112,121,108,126,113,127,115,127,119,124,123,122,125,122,127,117,127,105,123" />
    <area alt="" title="Jaen" href="{{ url('jaen/posts') }}" shape="poly" 
    coords="90,189,117,188,115,201,112,203,97,209,94,210,91,209" />
    <area alt="" title="Albacete" href="{{ url('albacete/posts') }}" shape="poly" 
    coords="115,170,138,167,146,181,138,183,136,187,133,189,130,191,125,194,123,195,119,184,116,181" />
    <area alt="" title="Murcia" href="{{ url('murcia/posts') }}" shape="poly" 
    coords="126,198,138,211,153,206,145,184" />
    <area alt="" title="Alicante" href="{{ url('alicante/posts') }}" shape="poly" 
    coords="151,181,168,179,153,196,152,191" />
    <area alt="" title="Guadalajara" href="{{ url('guadalajara/posts') }}" shape="poly" 
    coords="104,127,133,132,128,142,117,140,109,145,107,144,106,139" />
    <area alt="" title="Cuenca" href="{{ url('cuenca/posts') }}" shape="poly" 
    coords="109,151,120,143,140,154,140,157,137,160,134,164,131,166,127,166,121,167,119,168,115,168,115,166,115,164,113,161" />
    <area alt="" title="Valencia" href="{{ url('valencia/posts') }}" shape="poly" 
    coords="146,154,157,158,161,178,154,177,152,178,147,177,143,169,142,160,142,158" />
    <area alt="" title="Teruel" href="{{ url('teruel/posts') }}" shape="poly" 
    coords="132,142,148,152,166,131,148,128,139,129,137,133" />
    <area alt="" title="Castellon" href="{{ url('castellon/posts') }}" shape="poly" 
    coords="165,136,171,140,160,156,155,154,151,152" />
    <area alt="" title="Santa Cruz de Tenerife" href="{{ url('santa-cruz-de-tenerife/posts') }}" shape="poly" 
    coords="151,236,179,226,181,242,151,241" />
    <area alt="" title="Las Palmas" href="{{ url('las-palmas/posts') }}" shape="poly" 
    coords="193,217,204,218,202,242,194,241" />
    <area alt="" title="Huesca" href="{{ url('huesca/posts') }}" shape="poly" 
    coords="147,90,168,90,166,120,150,108" />
    <area alt="" title="Llerida" href="{{ url('llerida/posts') }}" shape="poly" 
    coords="172,90,189,97,183,114,179,117,177,119,173,120,171,121,169,120" />
    <area alt="" title="Zaragoza" href="{{ url('zaragoza/posts') }}" shape="poly" 
    coords="141,99,158,123,135,130,133,129,131,124,128,121" />
    <area alt="" title="Tarragona" href="{{ url('tarragona/posts') }}" shape="poly" 
    coords="169,124,185,119,174,139,169,136" />
    <area alt="" title="Barcelona" href="{{ url('barcelona/posts') }}" shape="poly" 
    coords="191,98,204,111,190,123,188,119,188,115,188,112" />
    <area alt="" title="Girona" href="{{ url('girona/posts') }}" shape="poly" 
    coords="194,95,214,96,207,110" />
    <area alt="" title="Baleares" href="{{ url('baleares/posts') }}" shape="poly" 
    coords="195,175,211,190,237,166,225,154" />
    <area  alt="" title="La Rioja" href="{{ url('la-rioja/posts') }}" shape="poly" 
    coords="109,98,126,104,125,107,111,109" />
</map>