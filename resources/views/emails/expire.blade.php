@component('mail::message')

Hola {{ $user->name }}, 

No respondas a este correo electrónico, ya que no se verá. Para asistencia, vaya a http://www.milespectaculos.com/

Nos gustaría recordarle que su anuncio sera eliminado si no es renovado en los próximos 7 días. Recuerde, cualquier anuncio que se vuelva inactivo se considerará en abandonado y sera eliminado.

** Te estás perdiendo una gran oportunidad: ** Donde podras encontrar todo lo que puedas imaginar para tus Fiestas u Eventos.

@component('mail::button', ['url' => \URL::route('my-posts.publish', $post->slug), 'color' => 'blue'])
ANUNCIO
@endcomponent

¡Gracias por usar MilEspectáculos!<br>
{{ config('app.name') }}
@endcomponent
