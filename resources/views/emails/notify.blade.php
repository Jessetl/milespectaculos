@component('mail::message')

# Hola,

@component('mail::panel')

Un usuario esta interesado en la contratación de sus servicios, revisa tu buzón de notificaciones en nuestra Web.

@endcomponent

@component('mail::button', ['url' => \URL::route('notify.show', $notify->id), 'color' => 'blue'])
	VER MÁS
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
