@component('mail::message')

Hola, {{ $mailbox->departament }}

** Mensaje **

{{ $mailbox->suggest }}

** Información de contacto **

Nombre: {{ $mailbox->name }}<br>
Teléfono: {{ $mailbox->phone }}

@endcomponent
