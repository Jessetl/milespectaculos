@component('mail::message')

# Saludos,

@component('mail::panel')

Un usuario se ha contactado contigo, revisa tu buzón de notificaciones en nuestra Web.

@endcomponent

@component('mail::button', ['url' => \URL::route('mail.show', $mail->id), 'color' => 'blue'])
	NOTIFICACIÓN
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
