@component('mail::message')

# Saludos,

@component('mail::panel')

Un usuario te ha respondido,

{{ $answer->answer }}

@endcomponent

@component('mail::button', ['url' => \URL::route('mail__answer', $answer->mails->id), 'color' => 'blue'])
	RESPONDER
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
