@component('mail::message')

Hola {{ $user->name }}, bienvenido a **Milespectáculos** 

No respondas a este correo electrónico, ya que no se verá. Para asistencia, vaya a http://www.milespectaculos.com/

Este mensaje ha sido enviado para validar su dirección de correo electrónico  {{ $user->email }} en la Web MilEspectáculos.

@component('mail::button', ['url' => \URL::route('profile.confirmed', $user->confirmation_code), 'color' => 'blue'])
CONFIRMAR MI CUENTA
@endcomponent

¡Gracias por usar Mil Espectáculos!<br>
{{ config('app.name') }}

@endcomponent
