<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label">Configuración</label>
			<input type="text" name="meta_value" value="{{ $configuration->meta_value }}" class="form-control {{ $errors->has('meta_value') ? ' is-invalid' : '' }} boxed" required>
			@if ($errors->has('meta_value'))
            	<div class="invalid-feedback">{{ $errors->first('meta_value') }}</div>
        	@endif
		</div>
	</div>
</div>