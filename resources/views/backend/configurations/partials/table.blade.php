<table class="table table-striped table-bordered table-hover" id="configurations">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Configuración</th>
			<th scope="col">Valor</th>
			<th scope="col">Publicado</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($configurations as $key => $configuration)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $configuration->meta_key }}</td>
			<td>{{ $configuration->meta_value }}</td>
			<td class="item-list">
				<div class="item-col item-col-date">
					<div class="no-overflow"> {{ $configuration->created_at }} </div>
				</div>
			</td>
			<td class="text-center">
				<a href="{{ route('config.edit', [$configuration]) }}" class="btn btn-primary">
					<i class="fa fa-pencil"></i>
				</a>				
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay configuraciones agregadas recientemente.</td>
		</tr>
		@endforelse
	</tbody>
</table>