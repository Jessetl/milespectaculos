@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Formulario de Header </h3>
			    <p class="title-description">Editar header de categoría de servicios.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-md-12">
		
		{!! Form::model($category, ['route' => ['headers.update-category_type', $category->id], 'method' => 'PUT', 'role' => 'form', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}

			<div class="card card-block">
				<div class="card-body">
					
					@include('backend.headers.partials.fields-edit')
					
					<div class="row">
						<div class="col">
							<div class="form-group">
			               		<button type="submit" class="btn btn-primary"> Guardar </button>
			            	</div>
						</div>
					</div>
				</div>
			</div>

		{!! Form::close() !!}

		</div>
	</div>
</section>

@endsection