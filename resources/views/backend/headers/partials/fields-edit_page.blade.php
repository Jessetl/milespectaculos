<div class="row">
	<div class="col-md">
		<div class="form-group">
			<div class="text-center">
				@if ( !empty($page->url) )
					<img src="{{ asset('storage/headers/'. $page->url) }}" width="100%" height="auto">
				@endif
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md">
		<label for="file">Página <span class="text-uppercase">"{{ $page->page }}"</span></label>
		<div class="form-group">
			<input type="file" name="file" class="form-control-file" value="{{ $page->url }}" required/>
			@if ($errors->has('file'))
	        	<div class="invalid-feedback">{{ $errors->first('file') }}</div>
	    	@endif
		</div>
	</div>
</div>