<table class="table table-striped table-bordered table-hover" id="pages">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Imagen</th>
			<th scope="col">Página</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($pages as $key => $page)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td class="text-center">{{ empty($page->url) ? 'INACTIVO' : 'ACTIVO' }}</td>
			<td class="font-weight-light">{{ $page->page }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($page, ['route' => ['delete.headers_page', $page->id], 'files' => 'true', 'method' => 'DELETE']) !!}
										
										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
								<li>
									<a href="{{ route('edit.headers_pages', [$page->id]) }}" class="edit"><i class="fa fa-pencil"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay categorías creadas</td>
		</tr>
		@endforelse
	</tbody>
</table>