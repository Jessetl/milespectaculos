<div class="row">
	<div class="col">
		<div class="form-group">
			<div class="text-center">
				@if ( !empty($category->header) )
					<img src="{{ asset('storage/headers/'. $category->header) }}" width="100%" height="auto">
				@endif
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="file">Header</label>
			<input type="file" name="file" class="form-control" value="{{ $category->url }}" required/>
    	</div>
		@if ($errors->has('file'))
        	<div class="invalid-feedback">{{ $errors->first('file') }}</div>
    	@endif
	</div>
</div>