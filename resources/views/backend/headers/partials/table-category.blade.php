<table class="table table-striped table-bordered table-hover" id="headerCategory">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Categoría</th>
			<th scope="col">Header</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($categories as $key => $category)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td class="font-weight-light">{{ $category->name }}</td>
			<td class="text-center">{{ empty($category->header) ? 'INACTIVO' : 'ACTIVO' }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($category, ['route' => ['headers.delete-category', $category->id], 'files' => 'true', 'method' => 'DELETE', 'enctype' => 'multipart/form-data']) !!}
										
										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
								<li>
									<a href="{{ route('headers.edit', [$category]) }}" class="edit"><i class="fa fa-pencil"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay categorías creadas</td>
		</tr>
		@endforelse
	</tbody>
</table>