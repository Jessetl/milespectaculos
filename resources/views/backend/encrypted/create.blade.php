@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Formulario de Códigos </h3>
			    <p class="title-description">Crear nuevos códigos.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">

	@include('backend.messages.request')
	
	<div class="row">
		<div class="col-md-12">

			{!! Form::open(['route' => 'encrypted.store', 'method' => 'POST', 'role' => 'form']) !!}

			<div class="card card-block">
				<div class="title-block"></div>
					
				@include('backend.encrypted.partials.fields')
				
				<div class="form-group pt-2">
	           	 	<button type="submit" class="btn btn-primary"> Guardar </button>
	        	</div>
			</div>

			{!! Form::close() !!}
			
		</div>
	</div>
</section>

@endsection