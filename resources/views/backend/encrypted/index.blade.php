@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Códigos de suscripciones <a href="{{ route('encrypted.create') }}" class="btn btn-primary btn-sm rounded-s"> Nuevos códigos </a></h3>
			    <p class="title-description">Códigos gratuitos para suscripciones VIP de usuarios.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	
	@include('backend.messages.request')

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
						<h3 class="title">Códigos</h3>
					</div>
					<section class="example">
						<div class="table-responsive">
							@include('backend.encrypted.partials.table')
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('scripts')

<script>
	$(document).ready(function() {
		$('#encrypted').DataTable();
	});
</script>

@endsection