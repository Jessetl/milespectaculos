<table class="table table-striped table-bordered table-hover" id="encrypted">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Código</th>
			<th scope="col">Estatus</th>
			<th scope="col">Expira</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($encrypteds as $key => $encrypted)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $encrypted->code }}</td>
			<td>{{ $encrypted->enabled ? 'DISPONIBLE' : 'NO DISPONIBLE' }}</td>
			<td>{{ $encrypted->ends_at }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($encrypted, ['route' => ['encrypted.destroy', $encrypted->id], 'method' => 'DELETE']) !!}
										
										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay códigos registrados.</td>
		</tr>
		@endforelse
	</tbody>
</table>