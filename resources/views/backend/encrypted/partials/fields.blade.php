<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="number">Cuantos códigos desea generar?</label>
			<input id="number" type="number" min="5" max="50" name="number" value="{{ old('number') }}" class="form-control {{ $errors->has('number') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="right" data-content="Escribe la cantidad de códigos" value="1" required>
			@if ($errors->has('number'))
	        	<div class="invalid-feedback">{{ $errors->first('number') }}</div>
	    	@endif
    	</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="ends_at">Fecha de expiración</label>
			<input id="ends_at" type="date" name="ends_at" value="{{ old('ends_at') }}" class="form-control {{ $errors->has('ends_at') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="right" data-content="Coloca la fecha de expiración de los códigos" required>
			@if ($errors->has('ends_at'))
	        	<div class="invalid-feedback">{{ $errors->first('ends_at') }}</div>
	    	@endif
    	</div>
	</div>
</div>