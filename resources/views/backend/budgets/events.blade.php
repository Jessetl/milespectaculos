@extends('layouts.main')

@section('content')

 <div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Presupestos </h3>
			    <p class="title-description">Presupuestos para eventos.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	
	@include('backend.messages.request')

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
						<h3 class="title">Eventos</h3>
					</div>
					<section class="example">
						<div class="table-responsive">
							@include('backend.budgets.partials.table-event')
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('scripts')

<script>
	$(document).ready(function() {
		$('#budgets').DataTable();
	});
</script>

@endsection