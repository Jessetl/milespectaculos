<table class="table table-striped table-bordered table-hover" id="budgetsPost">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Título</th>
			<th scope="col">Categoría</th>
			<th scope="col">E-mail</th>
			<th scope="col">Teléfono</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($posts as $key => $post)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td class="font-weight-light">{{ $post->name }}</td>
			<td>{{ $post->category->name }}</td>
			<td>{{ $post->email }}</td>
			<td>{{ $post->phone1 }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									<a href="{{ route('budgetsPost.edit', [$post]) }}" class="edit"><i class="fa fa-pencil"></i></a>
								</li>
								<li>
									<a href="{{ route('budgetsPost.gallery', [$post]) }}" class="edit"><i class="fa fa-file-image-o"></i></a>
								</li>
								<li>
									<a href="{{ route('budgetsPost.movies', [$post]) }}" class="edit"><i class="fa fa-file-video-o"></i></a>
								</li>
								<li>
									<a href="{{ route('budgetsPost.sounds', [$post]) }}" class="edit"><i class="fa fa-file-audio-o"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="6">No hay propuestas registradas.</td>
		</tr>
		@endforelse
	</tbody>
</table>