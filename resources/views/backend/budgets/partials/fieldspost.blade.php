<div class="row">
	<div class="col">
		<div class="form-group">
			<div class="text-center">
				@if ( !empty($post->header) )
					<img src="{{ asset('storage/headers/'. $post->header) }}" width="100%" height="auto">
				@endif
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col">
        <label for="file">Portada</label>
        <div class="form-group">
            <input id="file" type="file" class="form-control-input {{ $errors->has('file') ? ' is-invalid' : '' }}" name="file" value="{{ old('file') }}"/>
            @if ($errors->has('file'))
                <div class="invalid-feedback">{{ $errors->first('file') }}</div>
            @endif
            <small class="form-text my-2">
                La imagen debe ser rectangular de forma panorámica. 
            </small>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md">
		<label for="status">Estado</label>
		<div class="form-group">
			<select id="status" class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" required>
				<option value="draft" {{ $post->status == 'draft' ? 'selected' : '' }}>Borrador</option>
				<option value="published" {{ $post->status == 'published' ? 'selected' : '' }}>Publicado</option>
			</select>
			@if ($errors->has('status'))
                <div class="invalid-feedback">{{ $errors->first('status') }}</div>
            @endif
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md">
        <label for="title">Título</label>
        <div class="form-group">
            <input type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $post->name }}" required/>
            @if ($errors->has('title'))
                <div class="invalid-feedback">{{ $errors->first('title') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md">
        <label for="content">Descripción <span class="text-danger">(*)</span></label>
        <div class="form-group">
            <textarea id="content" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" rows="6" required>{{ $post->content }}</textarea>
            @if ($errors->has('content'))
                <div class="invalid-feedback">{{ $errors->first('content') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md">
        <label for="one">Télefono 1 <span class="text-danger">(*)</span></label>
        <div class="form-group">
            <input type="text" id="one" class="form-control {{ $errors->has('one') ? ' is-invalid' : '' }}" name="one" value="{{ $post->phone1 }}" required/>
            @if ($errors->has('one'))
                <div class="invalid-feedback">{{ $errors->first('one') }}</div>
            @endif
        </div>
    </div>
    <div class="col-md">
        <label for="two">Télefono 2</label>
        <div class="form-group">
            <input type="text" id="two" class="form-control {{ $errors->has('two') ? ' is-invalid' : '' }}" name="two" value="{{ is_null($post->phone2) ? old('two') : $post->phone2 }}"/>
            @if ($errors->has('two'))
                <div class="invalid-feedback">{{ $errors->first('two') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <label for="email">E-mail <span class="text-danger">(*)</span></label>
        <div class="form-group">
            <input type="text" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $post->email }}" placeholder="Dirección de correo Electrónico" rows="3" required/>
            @if ($errors->has('email'))
                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
            @endif
        </div>
    </div>
    <div class="col">
        <label for="url">Sitio Web</label>
        <div class="form-group">
            <input type="text" class="form-control {{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" value="{{ $post->url }}" placeholder="Ingresa tu página Web"/>
            @if ($errors->has('url'))
                <div class="invalid-feedback">{{ $errors->first('url') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <label for="address">Lugar</label>
        <div class="form-group">
            <div class="input-group">
                <input class="form-control" id="address" name="directions" value="{{ old('directions') }}" type="text"/>
                <span class="input-group-append">
                    <input class="btn btn-primary" id="look" type="button" value="BUSCAR">
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md">    
        <div id="map" style="height:400px;"></div>
        <div class="form-group">
            <input id="id_lat" type="text" style="display:none;" name="latInput" value="{{ $post->lat }}"/>
            <input id="id_lng" type="text" style="display:none;" name="lngInput" value="{{ $post->lng }}"/>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <label for="type_amount">Precio del servicio <span class="text-danger">(*)</span></label>
        <select class="form-control {{ $errors->has('type_amount') ? ' is-invalid' : '' }}" name="type_amount" required>
            <option value="A Convenir" {{ ($post->type_amount == 'A Convenir') ? 'selected' : '' }}>A CONVENIR</option>
            <option value="A Consultar" {{ ($post->type_amount == 'A Consultar') ? 'selected' : '' }}>A CONSULTAR</option>
            <option value="Otro" {{ ($post->type_amount == 'Otro') ? 'selected' : '' }}>OTRO</option>
        </select>
        @if ($errors->has('type_amount'))
            <div class="invalid-feedback">{{ $errors->first('type_amount') }}</div>
        @endif
    </div>
    <div class="col">
        <div class="form-group">
            <label for="discount">Descuento Especial <span class="text-danger">%</span></label>
            <input type="number" id="discount" class="form-control {{ $errors->has('discount') ? ' is-invalid' : '' }} popover-dismiss" name="discount" data-toggle="popover" value="{{ $post->discount }}" data-placement="top" data-content="Descuento especial para profesionales"/>
            @if ($errors->has('discount'))
                <div class="invalid-feedback">{{ $errors->first('discount') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="amount">¿Cuánto quieres que se enumere? </label>
            <input type="number" class="form-control {{ $errors->has('amount') ? ' is-invalid' : '' }}" step=".01" name="amount" value="{{ $post->amount }}" {{ $post->type_amount == 'Otro' ? '' : 'disabled'}}/>
            @if ($errors->has('amount'))
                <div class="invalid-feedback">{{ $errors->first('amount') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
	<div class="col-md">
		<div class="form-group">
			<button type="submit" class="btn btn-primary float-right">Guardar</button>
		</div>
	</div>
</div>