<div class="row justify-content-between">
	<div class="col-md">
		<div class="card border">
			<div class="card-body">
				<div class="row">
					<div class="col-md text-center">
						@if ( empty($budget->usr->url) )
				    		<img src="{{ asset('img/avatar.png') }}" class="img-fluid" width="220" height="220">
						@else
							<img src="{{ asset('storage/' . $budget->usr->url) }}" class="img-fluid" width="220" height="220">
						@endif
					</div>
					<div class="col-md">
						<p>
							<strong>Nombres</strong>: 
							<br>
							<span class="text-uppercase">{{ $budget->usr->name }}</span>
						</p>
						<p>
							<strong>Apellidos</strong>: 
							<br>
							<span class="text-uppercase">{{ $budget->usr->surname }}</span>
						</p>
						<p>
							<strong>Teléfono</strong>: 
							<br>
							<span class="text-uppercase">{{ empty($budget->usr->phone) ? 'NO TIENE' : $budget->usr->phone }}</span>
						</p>
						<p>
							<strong>E-mail</strong>:
							<br>
							<span>{{ $budget->usr->email }}</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md">
		
		@include('backend.messages.request')

		<h5 class="card-title">
			¿QUÉ ESTOY ORGANIZANDO?
		</h5>
		<div class="row">
			<div class="col-md">
				<label for="type">Estoy organizando</label>
				<div class="form-group">
					<input type="text" id="type" value="{{ $budget->type }}" class="form-control" readonly/>
				</div>
			</div>
			<div class="col-md">
				<label for="country">Ciudad</label>
				<div class="form-group">
					<input type="text" id="country" value="{{ $budget->country }}" class="form-control" readonly/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md">
				<label for="postal">Código Postal</label>
				<div class="form-group">
					<input type="text" id="postal" value="{{ $budget->postal }}" class="form-control" readonly/>
				</div>
			</div>
			<div class="col-md">
				<label for="day">Día del evento</label>
				<div class="form-group">
					<input type="text" id="dat" value="{{ $budget->dayevent }}" class="form-control" readonly/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md">
				<label for="hour">Hora del evento</label>
				<div class="form-group">
					<input type="text" id="hour" value="{{ $budget->hourevent }}" class="form-control" readonly/>
				</div>
			</div>
			<div class="col-md">
				<label for="duration">Duración</label>
				<div class="form-group">
					<input type="text" id="duration" value="{{ $budget->duration }}" class="form-control" readonly/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md">
				<label for="budget">Presupuesto máximo</label>
				<div class="form-group">
					<input type="text" id="budget" value="{{ $budget->budget }}" class="form-control" readonly/>
				</div>
			</div>
			<div class="col-md">
				<label for="place">Lugar</label>
				<div class="form-group">
					<input type="text" id="place" value="{{ $budget->place }}" class="form-control" readonly/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md">
				<label for="detail">Detalles de tu fiesta o evento</label>
				<div class="form-group">
					<textarea id="detail" rows="5" class="form-control" readonly>{{ $budget->detail }}</textarea>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md">
				@if($budget->status == 0)
					<a href="{{ route('budgets.success', $budget) }}" class="btn btn-success float-right mx-2">Terminado</a>
				@endif
				<a href="{{ route('budgets.index') }}" class="btn btn-primary float-right">Ir atras</a>
			</div>
		</div>
	</div>
</div>