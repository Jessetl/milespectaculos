<table class="table table-striped table-bordered table-hover" id="budgets">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Usuario</th>
			<th scope="col">Categoría</th>
			<th scope="col">Ciudad</th>
			<th scope="col">Fecha</th>
			<th scope="col">Estado</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($budgets as $key => $budget)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td class="font-weight-light">{{ $budget->usr->name }}</td>
			<td>{{ $budget->type }}</td>
			<td>{{ $budget->country }}</td>
			<td>{{ $budget->dayevent }}</td>
			<td>{{ $budget->status ? 'TERMINADO' : 'EN ESPERA' }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									<a href="{{ route('budgets.show', [$budget]) }}" class="edit"><i class="fa fa-eye"></i></a>
								</li>
								<li>
									{!! Form::model($budget, ['route' => ['budgets.destroy', $budget->id], 'method' => 'DELETE']) !!}

										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="7">No hay propuestas registradas.</td>
		</tr>
		@endforelse
	</tbody>
</table>