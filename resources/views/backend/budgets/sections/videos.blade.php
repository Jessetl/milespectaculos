@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Videos de anuncio </h3>
			    <p class="title-description">Videos de anuncio cargadas.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-md-12">

			@include('backend.messages.request')
			
			<div class="card card-block">
				<div class="title-block"></div>

				{!! Form::model($post, ['route' => ['budgetsPost.updateMovies', $post->id], 'method' => 'PUT']) !!}
					
					<div class="row">
						<div class="col-md">
							<label for="movie">Video</label>
							<div class="form-group">
								<div class="input-group">
			                    	<input id="movie" type="text" name="video" class="form-control" required/>
			                    	<button type="submit" class="btn">SUBIR VIDEO</button>
			                    </div>
							</div>
						</div>
					</div>

                {!! Form::close() !!}
					
				@foreach($post->videos as $key => $video)
				
					<div class="row">
						<div class="col-md">
							<div class="form-group">
								<div class="input-group">
						            <input class="form-control" name="video" value="{{ $video->meta_url }}" type="text" readonly />
						            <a href="{{ route('budgetsPost.deleteMovies', $video->id) }}" class="input-group-append">
						                <input class="btn btn-primary" type="button" value="Eiminar">
						            </a>
						        </div>
	                            
							</div>
	                    </div>
					</div>

                @endforeach

			</div>
		</div>
	</div>
</section>

@endsection