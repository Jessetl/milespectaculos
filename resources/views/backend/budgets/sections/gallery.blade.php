@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Imagenes de anuncio </h3>
			    <p class="title-description">Imagenes de anuncio cargadas.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-md-12">

			@include('backend.messages.request')
			
			<div class="card card-block">
				<div class="title-block"></div>

				{!! Form::model($post, ['route' => ['budgetsPost.updateGallery', $post->id], 'files' => 'true', 'id' => 'budgetsFileUpload', 'class' => 'dropzone', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
					
					<div class="row">
						<div class="form-group">
		                    <div class="fallback">
		                        <input name="file" type="file" required/>
		                    </div>
						</div>
					</div>

                {!! Form::close() !!}

					
					<div class="row my-4">
						@foreach($post->images as $key => $image)
						<div class="col-md">
							<div class="form-group">
                                <img src="{{ asset('storage/posts/'. $image->meta_url) }}" class="img-fluid"alt="Image">
                                <a href="{{ route('budgetsPost.deleteGallery', $image->id) }}" class="btn btn-danger my-2"> Eliminar </a>
							</div>
	                    </div>
                        @endforeach
					</div>

			</div>
		</div>
	</div>
</section>

@endsection

@section('scripts')

<script type="text/javascript">
Dropzone.options.budgetsFileUpload = {

    dictDefaultMessage: 'PINCHE AQUÍ PARA SUBIR ARCHIVOS',
    dictRemoveFile: 'Remover',
    dictMaxFilesExceeded: 'No puedes subir más archivos.',

    maxFilesize: 5,
    addRemoveLinks: true,
    acceptedFiles: ".jpg,.png",

    error: function(file, message) {
        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.message);
    }
};
</script>

@endsection