<!-- Modal -->
<div class="modal fade" id="uploadAudios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">SUBIR AUDIOS</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

         	{!! Form::open(['method' => 'PUT', 'name' => 'formSounds', 'id' => 'uploadSounds']) !!}

			<div class="modal-body">
				<div class="row p-2">
                	<input type="text" class="form-control popover-dismiss" name="audio" data-toggle="popover" data-placement="top" data-content="Pega aquí la URL de tu audio de SoundCloud" required/>
                	<span id="response" class="text-danger"></span>
                </div>    
			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="uploadMovies">Enviar</button>
			</div>

            {!! Form::close() !!}
		</div>
	</div>
</div>