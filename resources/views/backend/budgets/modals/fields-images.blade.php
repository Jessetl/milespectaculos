<!-- Modal -->
<div class="modal fade" id="uploadImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">SUBIR IMAGENES</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="dropzone">
                    {!! Form::model($post, ['route' => ['posts.upload', $post->id], 'files' => 'true', 'id' => 'myAwesomeDropzone', 'class' => 'dropzone', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}

                        <div class="fallback">
                            <input name="file" type="file" required/>
                        </div>

                    {!! Form::close() !!}

                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>