@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Anuncio </h3>
			    <p class="title-description">Datos de anuncio para usuario.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-md-12">

			@include('backend.messages.request')
			
			<div class="card card-block">
				<div class="title-block"></div>

				{!! Form::model($post, ['route' => ['budgetsPost.update', $post], 'method' => 'PUT', 'role' => 'form', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}
					
					@include('backend.budgets.partials.fieldspost')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="lat" value="{{ $post->lat }}">
<input type="hidden" name="lng" value="{{ $post->lng }}">

@endsection

@section('scripts')
<!-- Google Maps -->
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbapNm5FNXk2Db4pJ47Fr7lKXEsd8jtsA&callback=initMap">
</script>


<script>

var map;
var marker;
var coords;

let lat = $('input[name=lat]').val();
let lng = $('input[name=lng]').val();

function initMap() {
        
    var myLatlng = new google.maps.LatLng(lat,lng);

    var mapOptions = {
        zoom: 12,
        center: myLatlng
    }

    var geocoder = new google.maps.Geocoder();

    document.getElementById('look').addEventListener('click', function() {
        geocodeAddress(geocoder, map);
    });
          
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Drag me!"
    });
        
    marker.addListener('click', function(){
        placeMarker(marker.getPosition());
    });

    marker.addListener('drag', handleEvent);
    marker.addListener('dragend', handleEvent);

}

function handleEvent(event) {

    document.getElementById('id_lat').value = event.latLng.lat();
    document.getElementById('id_lng').value = event.latLng.lng();
}

function placeMarker(cords) {
    
    marker.setPosition(cords);
    document.getElementById("id_lat").value = cords.lat();
    document.getElementById("id_lng").value = cords.lng();
}

function geocodeAddress(geocoder, resultsMap) {

    var address = document.getElementById('address').value;
    
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            placeMarker(results[0].geometry.location);
            
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
    });
}
    
</script>


@endsection