@extends('layouts.main')

@section('content')


<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Formulario de Usuario </h3>
			    <p class="title-description">Editar usuarios registrados.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">

	@include('backend.messages.request')
	
	<div class="row">
		<div class="col-md-12">
			<div class="card card-block">
				<div class="title-block"></div>
				{!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT', 'role' => 'form']) !!}
					
					@include('backend.users.partials.fields-edit')

					<div class="form-group">
                        <button type="submit" class="btn btn-primary"> Guardar </button>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</section>

@endsection
