<table class="table table-striped table-bordered table-hover" id="users">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Nombres y Apellidos</th>
			<th scope="col">Correo electrónico</th>
			<th scope="col">Rol</th>
			<th scope="col">VIP</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($users as $key => $user)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ is_null($user->name) ? 'Free Account' : $user->name .' '. $user->surname }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->rol->name }}</td>
			<td>{{ $user->premium ? 'SI' : 'NO' }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($user, ['route' => ['users.destroy', $user->id], 'method' => 'DELETE']) !!}
										
										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
								<li>
									<a href="{{ route('users.edit', [$user]) }}" class="edit"><i class="fa fa-pencil"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay usuarios registrados</td>
		</tr>
		@endforelse
	</tbody>
</table>