<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Username</label>
			<input type="text" name="username" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe un nombre de usuario" value="{{ old('username') }}" required/>
			@if ($errors->has('username'))
	        	<div class="invalid-feedback">{{ $errors->first('username') }}</div>
	    	@endif
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<label class="control-label">Email</label>
			<input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe un correo electronico valido para el nuevo usuario" value="{{ old('email') }}" required/>
			@if ($errors->has('email'))
	        	<div class="invalid-feedback">{{ $errors->first('email') }}</div>
	    	@endif
    	</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Nombres</label>
			<input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe el nombre del nuevo usuario" value="{{ old('name') }}" required/>
			@if ($errors->has('name'))
	        	<div class="invalid-feedback">{{ $errors->first('name') }}</div>
	    	@endif
    	</div>
	</div>
	<div class="col">
		<div class="form-group">
			<label class="control-label">Apellidos</label>
			<input type="text" name="surname" class="form-control {{ $errors->has('surname') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe el apellido del nuevo usuario" value="{{ old('surname') }}" required/>
			@if ($errors->has('surname'))
	        	<div class="invalid-feedback">{{ $errors->first('surname') }}</div>
	    	@endif
   		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Roles</label>
			<select class="form-control {{ $errors->has('role') ? ' is-invalid' : '' }} boxed popover-dismiss" name="role" data-toggle="popover" data-placement="top" data-content="Seleccione el rol que tendra el nuevo usuario dentro del sistema" required>
				@forelse($roles as $key => $role)
				<option value="{{ $role->id }}" {{ (old('role') == $role->id) ? 'selected': '' }}>{{ $role->name }}</option>
				@empty
				<option value="" selected></option>
				@endforelse
			</select>
			@if ($errors->has('role'))
	        	<div class="invalid-feedback">{{ $errors->first('role') }}</div>
	    	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Contraseña</label>
			<input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Incluir letras, numeros y simbolos en tu contraseña la hará mas segura" required>
			@if ($errors->has('password'))
	        	<div class="invalid-feedback">{{ $errors->first('password') }}</div>
	    	@endif
    	</div>
	</div>
	<div class="col">
		<div class="form-group">
			<label class="control-label">Repetir Contraseña</label>
			<input type="password" name="password_confirmation" class="form-control boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Repite exactamente la contraseña anterior"required>
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Confirmado</label>
			<select class="form-control {{ $errors->has('confirmed') ? ' is-invalid' : '' }} boxed popover-dismiss" name="confirmed" data-toggle="popover" data-placement="top" data-content="¿Desea que se confirme automaticamente el correo electronico?" required>
				<option value="0" {{ (old('confirmed') == 0) ? 'selected' : '' }}> NO </option>
				<option value="1" {{ (old('confirmed') == 1) ? 'selected' : '' }}> SI </option>
			</select>
			@if ($errors->has('confirmed'))
	        	<div class="invalid-feedback">{{ $errors->first('confirmed') }}</div>
	    	@endif
    	</div>
	</div>
	<div class="col">
		<div class="form-group">
			<label class="control-label">Premium</label>
			<select class="form-control {{ $errors->has('premium') ? ' is-invalid' : '' }} boxed popover-dismiss" name="premium"  data-toggle="popover" data-placement="top" data-content="¿El nuevo usuario sera VIP?" required>
				<option value="0" {{ (old('premium') == 0) ? 'selected' : '' }}> NO </option>
				<option value="1" {{ (old('premium') == 1) ? 'selected' : '' }}> SI </option>
			</select>
			@if ($errors->has('premium'))
	        	<div class="invalid-feedback">{{ $errors->first('premium') }}</div>
	    	@endif
    	</div>
	</div>
</div>