<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Roles</label>
			<select class="form-control {{ $errors->has('role') ? ' is-invalid' : '' }} boxed popover-dismiss" name="role" data-toggle="popover" data-placement="top" data-content="Selecciona el nuevo rol que tendra el usuario dentro del sistema" required>
				@forelse($roles as $key => $role)
				<option value="{{ $role->id }}" {{ (strcmp($role->id, $user->role) == 0) ? 'selected' : '' }}>{{ $role->name }}</option>
				@empty
				<option value="" selected></option>
				@endforelse
			</select>
			@if ($errors->has('role'))
            	<div class="invalid-feedback">{{ $errors->first('role') }}</div>
        	@endif
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<label class="control-label">Nombres</label>
			<input type="text" name="name" value="{{ $user->name }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe el nuevo nombre del usuario" required>
			@if ($errors->has('name'))
            	<div class="invalid-feedback">{{ $errors->first('name') }}</div>
        	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Apellidos</label>
			<input type="text" name="surname" value="{{ $user->surname }}" class="form-control {{ $errors->has('surname') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe el nuevo apellido del usuario" required>
			@if ($errors->has('surname'))
            	<div class="invalid-feedback">{{ $errors->first('surname') }}</div>
        	@endif
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<label class="control-label">Email</label>
			<input type="email" name="email" value="{{ $user->email }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe un nuevo correo electronico valido para el usuario" required>
			@if ($errors->has('email'))
            	<div class="invalid-feedback">{{ $errors->first('email') }}</div>
        	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Confirmado</label>
			<select class="form-control {{ $errors->has('confirmed') ? ' is-invalid' : '' }} boxed popover-dismiss" name="confirmed" data-toggle="popover" data-placement="top" data-content="¿Desea que se el correo electronico se mantenga confirmado?" required>
				<option value="0"> NO </option>
				<option value="1" {{ $user->confirmed ? 'selected' : '' }}> SI </option>
			</select>
			@if ($errors->has('confirmed'))
            	<div class="invalid-feedback">{{ $errors->first('confirmed') }}</div>
        	@endif
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<label class="control-label">Premium</label>
			<select class="form-control {{ $errors->has('premium') ? ' is-invalid' : '' }} boxed popover-dismiss" name="premium" data-toggle="popover" data-placement="top" data-content="¿Desea que el usuario sea VIP?" required>
				<option value="0"> NO </option>
				<option value="1" {{ $user->premium ? 'selected' : '' }}> SI </option>
			</select>
			@if ($errors->has('premium'))
            	<div class="invalid-feedback">{{ $errors->first('premium') }}</div>
        	@endif
		</div>
	</div>
</div>