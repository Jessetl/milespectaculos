<table class="table table-striped table-bordered table-hover" id="notifications">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Anuncio</th>
			<th scope="col">Interesado</th>
			<th scope="col">Correo electrónico</th>
			<th scope="col">Teléfono</th>
		</tr>
	</thead>
	<tbody>
		@forelse($notifications as $key => $notify)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $notify->pts->name }}</td>
			<td>{{ $notify->usr->name }}</td>
			<td>{{ $notify->usr->email }}</td>
			<td>{{ empty($notify->usr->phone) ? 'NO POSEE' : $notify->usr->phone }}</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay notificaciones por gestionar</td>
		</tr>
		@endforelse
	</tbody>
</table>