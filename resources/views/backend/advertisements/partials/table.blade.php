<table class="table table-striped table-bordered table-hover" id="subscriptions">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Descripción</th>
			<th scope="col">Usuario</th>
			<th scope="col">PayPal ID</th>
			<th scope="col">Estado</th>
			<th scope="col">Precio</th>
			<th scope="col">Total</th>
			<th scope="col">Iva</th>
			<th scope="col">Fecha de Pago</th>
		</tr>
	</thead>
	<tbody>
		@forelse($payments as $key => $payment)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $payment->name  }}</td>
			<td>{{ $payment->posts->usr->name }}</td>
			<td>{{ $payment->payment }}</td>
			<td>{{ $payment->status ? 'CONFIRMADA' : 'RECHAZADA' }}</td>
			<td>{{ $payment->price }} €</td>
			<td>{{ $payment->total }} €</td>
			<td>{{ $payment->tax }}</td>
			<td>{{ $payment->created_at }}</td>
		</tr>
		@empty
		<tr>
			<td colspan="8">No hay facturaciones agregadas recientemente.</td>
		</tr>
		@endforelse
	</tbody>
</table>
