<table class="table table-bordered">
	<thead>
		<tr>
			<th colspan="2" class="text-center"> Pedido de gestión de anuncio </th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				{{ $payment->name }}
			</td>
			<td>
				<strong> Precio </strong>
				<br>EUR {{ $payment->price }}
				<br><strong>Iva</strong>
				<br>EUR {{ $payment->tax }}
				<br><strong>Total</strong>
				<br>EUR {{ $payment->total }}
			</td>
		</tr>
	</tbody>
</table>

<table class="table table-bordered">
	<thead>
		<tr>
			<th colspan="2" class="text-center"> Información sobre el pago </th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				Datos de Facturación
			</td>
			<td class="text-right">
				<span class="font-weight-bold">
					{{ $payment->posts->usr->name }} {{ $payment->posts->usr->surname }}
				</span>
				<br>{{ $payment->posts->usr->phone }}
			</td>
		</tr>
		<tr>
			<td>
				Detalles de Transacción
			</td>
			<td class="text-center">
				<span class="{{ $payment->status ? 'text-success' : 'text-danger' }}">
					{{ $payment->status ? 'APROBADA' : 'RECHAZADA' }}
				</span>
			</td>
		</tr>
	</tbody>
</table>