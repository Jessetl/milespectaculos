@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Facturación </h3>
			</div>
		</div>
	</div>
</div>

<section class="section">

	@include('backend.messages.request')
	
	<div class="row ">
		<div class="col-md-12">
			<div class="card card-block">
				<div class="card-body">
					<div class="title-block font-weight-bold">{{ $payment->name }}</div>
					<p class="float-right">
					 	{{ $payment->payment }}
					</p>

					@include('backend.advertisements.partials.datashow')

					<p class="text-center">
						Volver al <a href="{{ route('advertisements.index') }}"> resumen de pedidos </a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection