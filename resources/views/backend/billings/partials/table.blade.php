<table class="table table-striped table-bordered table-hover" id="suscriptions">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Transacción paypal</th>
			<th scope="col">Estatus</th>
			<th scope="col">Precio de membresía</th>
			<th scope="col">Total (12) meses</th>
			<th scope="col">Iva</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($payments as $key => $payment)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $payment->paypal }}</td>
			<td>{{ $payment->status ? 'CONFIRMADA' : 'CANCELADA' }}</td>
			<td>{{ $payment->amount }} €</td>
			<td>{{ $payment->amount_total }} €</td>
			<td>{{ $payment->iva }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									<a href="#" class="edit"><i class="fa fa-eye"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="7">No hay facturaciones agregadas recientemente.</td>
		</tr>
		@endforelse
	</tbody>
</table>