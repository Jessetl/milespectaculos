<table class="table table-striped table-bordered table-hover" id="billings">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Transacción paypal</th>
			<th scope="col">Estatus</th>
			<th scope="col">Total</th>
			<th scope="col">Iva</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($shoppings as $key => $shopping)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ empty($shopping->paypal_payment_id) ? 'SIN PROCESAR' : $shopping->paypal_payment_id }}</td>
			<td>{{ $shopping->status }}</td>
			<td>{{ $shopping->amount_total }}</td>
			<td>{{ $shopping->iva }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									<a href="#" class="edit"><i class="fa fa-eye"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="6">No hay facturaciones agregadas recientemente.</td>
		</tr>
		@endforelse
	</tbody>
</table>
