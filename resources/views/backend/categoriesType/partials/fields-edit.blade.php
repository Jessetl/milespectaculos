<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Categoría</label>
			<select class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }} boxed popover-dismiss" name="category" data-toggle="popover" data-placement="top" data-content="Seleccione la categoría para la nueva subcategoría" required>
				@forelse($categories as $key => $category)
				<option value="{{ $category->id }}" {{ (strcmp($category->id, $categories_type->category) == 0) ? 'selected' : '' }}>{{ $category->name }}</option>
				@empty
				<option value="" selected></option>
				@endforelse
			</select>
			@if ($errors->has('category'))
            	<div class="invalid-feedback">{{ $errors->first('category') }}</div>
        	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label class="control-label">Subcategoría</label>
			<input id="name" type="text" name="name" value="{{ $categories_type->name }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="top" data-content="Sustituye por el nuevo nombre de la subcategoría" required>
			@if ($errors->has('name'))
            	<div class="invalid-feedback">{{ $errors->first('name') }}</div>
        	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="slug">Slug</label>
			<input id="slug" type="text" maxlength="75" name="slug" value="{{ $categories_type->slug }}" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}" required>
			@if ($errors->has('slug'))
	        	<div class="invalid-feedback">{{ $errors->first('slug') }}</div>
	    	@else
				<small class="form-text text-muted">
					El “slug” es la versión amigable de la URL del nombre. Suele estar en minúsculas y contiene solo letras, números y guiones.
				</small>
	    	@endif
    	</div>
	</div>
</div>
