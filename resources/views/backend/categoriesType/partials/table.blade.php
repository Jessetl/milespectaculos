<table class="table table-striped table-bordered table-hover" id="categoriesType">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Categoría</th>
			<th scope="col">Nombre</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($categories_type as $key => $type)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $type->categorie->name }}</td>
			<td>{{ $type->name }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($type, ['route' => ['categories-type.destroy', $type->id], 'method' => 'DELETE']) !!}

									<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
								<li>
									<a href="{{ route('categories-type.edit', [$type->id]) }}" class="edit"><i class="fa fa-pencil"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay Subcategorías creadas</td>
		</tr>
		@endforelse
	</tbody>
</table>