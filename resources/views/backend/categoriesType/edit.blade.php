@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Formulario de Categorías </h3>
			    <p class="title-description">Editar SubCategorías para publicaciones de anuncios.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-block">
				<div class="title-block"></div>
				{!! Form::model($categories_type, ['route' => ['categories-type.update', $categories_type->id], 'method' => 'PUT', 'role' => 'form']) !!}
					
					@include('backend.categoriesType.partials.fields-edit')

					<div class="form-group">
                        <button type="submit" class="btn btn-primary"> Guardar </button>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</section>

@endsection

@section('scripts')

<script>
	$(document).ready(function () {
        $("#name").keyup(function (value) {
            var text = $(this).val();
            $("#slug").val(text.replace(/\s/g, "-").toLowerCase());
        });
    });
</script>

@endsection