<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Evento</label>
			<input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} boxed" required>
			@if ($errors->has('name'))
            	<div class="invalid-feedback">{{ $errors->first('name') }}</div>
        	@endif
		</div>
	</div>
</div>