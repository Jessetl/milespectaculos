<table class="table table-striped table-bordered table-hover" id="events">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Patrocinador</th>
			<th scope="col">Provincia</th>
			<th scope="col">Localidad</th>
			<th scope="col">Título</th>
			<th scope="col">Fecha</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($events as $key => $event)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ empty($event->user_event->name) ? 'NO POSEE' : $event->user_event->name }}</td>
			<td class="font-weight-light">{{ $event->districts->name }}</td>
			<td>{{ $event->circuit }}</td>
			<td>{{ $event->title }}</td>
			<td>{{ $event->date }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($event, ['route' => ['events.destroy', $event->id], 'method' => 'DELETE']) !!}

										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="7">No hay eventos registrados.</td>
		</tr>
		@endforelse
	</tbody>
</table>