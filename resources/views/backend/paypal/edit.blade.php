@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Formulario de Configuración Paypal </h3>
			    <p class="title-description">Editar configuración paypal para pagos de usuarios.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-block">
				<div class="title-block"></div>
				{!! Form::model($paypal, ['route' => ['paypal.update', $paypal->id], 'method' => 'PUT', 'role' => 'form']) !!}
					
					@include('backend.paypal.partials.fields')

					<div class="form-group">
                        <button type="submit" class="btn btn-primary"> Guardar </button>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</section>

@endsection