<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="name">Nombre</label>
			<input type="text" id="name" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $paypal->name }}" required/>
			@if ($errors->has('name'))
            	<div class="invalid-feedback">{{ $errors->first('name') }}</div>
        	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">	
			<label for="status">Estatus</label>
			<select id="status" name="status" class="form-control" required>
				<option value="ACTIVO" selected="{{ $paypal->status === 'ACTIVO' ? 'selected' : '' }}"> ACTIVO </option>
				<option value="INACTIVO" selected="{{ $paypal->status === 'INACTIVO' ? 'selected' : '' }}"> INACTIVO </option>
			</select>
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="slug">Slug</label>
			<input id="slug" type="text" maxlength="75" step=".99" name="slug" value="{{ $paypal->slug }}" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}" required>
			@if ($errors->has('slug'))
	        	<div class="invalid-feedback">{{ $errors->first('slug') }}</div>
	    	@else
				<small class="form-text text-muted">
					El “slug” es la versión amigable de la URL del nombre. Suele estar en minúsculas y contiene solo letras, números y guiones.
				</small>
	    	@endif
    	</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="amount">Monto</label>
			<input type="number" id="amount" name="amount" min="1" step=".01" value="{{ $paypal->amount }}" class="form-control {{ $errors->has('amount') ? ' is-invalid' : '' }}" required/>
			@if ($errors->has('amount'))
            	<div class="invalid-feedback">{{ $errors->first('amount') }}</div>
        	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="description">Descripción</label>
			<textarea id="description" name="description" class="form-control" rows="3" required>{{ $paypal->description }}</textarea>
			@if ($errors->has('description'))
            	<div class="invalid-feedback">{{ $errors->first('description') }}</div>
        	@endif
		</div>
	</div>
</div>