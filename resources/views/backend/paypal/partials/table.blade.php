<table class="table table-striped table-bordered table-hover" id="paypal">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Nombre</th>
			<th scope="col">Precio</th>
			<th scope="col">Publicado</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($paypals as $key => $paypal)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td class="font-weight-light">{{ $paypal->name }}</td>
			<td>{{ $paypal->amount }}</td>
			<td class="item-list">
				<div class="item-col item-col-date">
					<div class="no-overflow"> {{ $paypal->created_at }} </div>
				</div>
			</td>
			<td class="text-center">
				<a href="{{ route('paypal.edit', [$paypal]) }}" class="btn btn-primary">
					<i class="fa fa-pencil"></i>
				</a>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay configuraciones agregadas recientemente.</td>
		</tr>
		@endforelse
	</tbody>
</table>