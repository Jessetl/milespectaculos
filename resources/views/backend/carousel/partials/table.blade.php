<table class="table table-striped table-bordered table-hover" id="carrusel">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Nombre</th>
			<th scope="col">Estatus</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($carousels as $key => $carousel)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $carousel->name }}</td>
			<td>{{ $carousel->status ? 'ACTIVO' : 'INACTIVO' }}</td>
			<td class="text-center">
				{!! Form::model($carousel, ['route' => ['carousel.store', $carousel->id], 'method' => 'POST']) !!}
					<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa {{ $carousel->status ? 'fa-toggle-on' : 'fa-toggle-off' }}"></i></a>
				{!! Form::close() !!}
								
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay categorías creadas</td>
		</tr>
		@endforelse
	</tbody>
</table>
