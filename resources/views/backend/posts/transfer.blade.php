@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Transferencia de anuncio </h3>
			    <p class="title-description">Transferir anuncio a otro usuario.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-block">
				<div class="title-block"></div>
				
				<to :post="{{ $post }}"></to>
			</div>
		</div>
	</div>
</section>

@endsection