<table class="table table-striped table-bordered table-hover" id="posts">
	<thead>
		<tr>
			<th>#</th>
			<th scope="col">Código</th>
			<th scope="col">Título</th>
			<th scope="col">Propietario</th>
			<th scope="col">Correo electrónico</th>
			<th scope="col">Estatus</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($posts as $key => $post)
		<tr>
			<td>{{ $key +1 }}</td>
			<td>{{ $post->code }}</td>
			<th class="font-weight-light">{{ empty($post->name) ? 'NO POSEE' : $post->name }}</th>
			<td>{{ $post->usr->name }} {{ $post->usr->surname }}</td>
			<td>{{ $post->usr->email }}</td>
			<td>{{ $post->status == 'draft' ? 'Borrador' : 'Publicado' }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
								{!! Form::open(['route' => ['published.destroy', $post->id], 'method' => 'DELETE', 'name' => 'formName', 'id' => 'formName']) !!}
									<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>
								{!! Form::close() !!}
								</li>
								<li>
									<a href="{{ route('toTransfer', $post) }}" class="edit">
										<i class="fa fa-random"></i>
									</a>
								</li>
								<li>
									<a href="{{ route('view.post', $post) }}" class="edit"><i class="fa fa-eye"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="7">No hay anuncios publicados</td>
		</tr>
		@endforelse
	</tbody>
</table>
