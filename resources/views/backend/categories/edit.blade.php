@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Formulario de Categorías </h3>
			    <p class="title-description">Editar categorías de servicios.</p>
			</div>
		</div>
	</div>
</div>

<section class="section">
	
	@include('backend.messages.request')

	<div class="row">
		<div class="col-md-12">

		{!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PUT', 'role' => 'form', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}

			<div class="card card-block">
				<div class="title-block"></div>
					
				@include('backend.categories.partials.fields-edit')
				
				<div class="form-group pt-2">
               		<button type="submit" class="btn btn-primary"> Guardar </button>
            	</div>
			</div>

		{!! Form::close() !!}
		
		</div>
	</div>
</section>

@endsection

@section('scripts')

<script>
	$(document).ready(function () {
        $("#name").keyup(function (value) {
            var text = $(this).val();
            $("#slug").val(text.replace(/\s/g, "-").toLowerCase());
        });
    });
</script>

@endsection