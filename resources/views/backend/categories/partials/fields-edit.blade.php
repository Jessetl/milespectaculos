<div class="row">
	<div class="col-md">
		<div class="form-group">
			@if ( !is_null($category->url) )
				<img src="{{ asset('storage/categories/'. $category->url) }}" class="img-fluid" />
			@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md">
		<label for="file">Logo</label>
		<div class="form-group">
			<input type="file" id="file" name="file" class="form-control-file" value="{{ $category->url }}"/>
			@if ($errors->has('file'))
	        	<div class="invalid-feedback">{{ $errors->first('file') }}</div>
	    	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md">
		<label for="name">Categorías</label>
		<div class="form-group">		
			<input type="text" id="name" name="name" value="{{ $category->name }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="right" data-content="Sustituye por el nombre de la nueva categoría" required>
			@if ($errors->has('name'))
	        	<div class="invalid-feedback">{{ $errors->first('name') }}</div>
	    	@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md">
		<label for="slug">Slug</label>
		<div class="form-group">
			<input type="text" id="slug" maxlength="75" name="slug" value="{{ $category->slug }}" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}" required>
			@if ($errors->has('slug'))
	        	<div class="invalid-feedback">{{ $errors->first('slug') }}</div>
	    	@else
				<small class="form-text text-muted">
					El “slug” es la versión amigable de la URL del nombre. Suele estar en minúsculas y contiene solo letras, números y guiones.
				</small>
	    	@endif
    	</div>
	</div>
</div>