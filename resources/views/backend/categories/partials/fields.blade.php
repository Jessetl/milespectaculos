<div class="row pt-2">
	<div class="col">
		<div class="form-group">
			<label for="file">Logo</label>
			<input type="file" name="file" class="form-control {{ $errors->has('file') ? ' is-invalid' : '' }}"/>
			@if ($errors->has('file'))
	        	<div class="invalid-feedback">{{ $errors->first('file') }}</div>
	    	@else
				<small id="file" class="form-text text-muted">
					La imagen debe contener un máximo de 120 x 120px. 
				</small>
	    	@endif
    	</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="name">Categoría</label>
			<input id="name" type="text" maxlength="50" name="name" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} boxed popover-dismiss" data-toggle="popover" data-placement="right" data-content="Escribe el nombre de la nueva categoría" required/>

			@if ($errors->has('name'))
	        	<div class="invalid-feedback">{{ $errors->first('name') }}</div>
	    	@endif
    	</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="form-group">
			<label for="slug">Slug</label>
			<input id="slug" type="text" maxlength="75" name="slug" value="{{ old('slug') }}" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}" required>

			@if ($errors->has('slug'))
	        	<div class="invalid-feedback">{{ $errors->first('slug') }}</div>
	    	@else
				<small class="form-text text-muted">
					El “slug” es la versión amigable de la URL del nombre. Suele estar en minúsculas y contiene solo letras, números y guiones.
				</small>
	    	@endif
    	</div>
	</div>
</div>