<table class="table table-striped table-bordered table-hover" id="mails">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Departamento</th>
			<th scope="col">Nombre del contacto</th>
			<th scope="col">Teléfono</th>
			<th scope="col">Fecha</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($mails as $key => $mail)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $mail->departament }}</td>
			<td class="font-weight-light">{{ $mail->name }}</td>
			<td>{{ $mail->phone }}</td>
			<td>{{ $mail->created_at }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($mail, ['route' => ['mailbox.destroy', $mail->id], 'files' => 'true', 'method' => 'DELETE']) !!}
										
										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
								<li>
									<a href="{{ route('mailbox.show', [$mail->id]) }}" class="edit"><i class="fa fa-eye"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay emails recibidos</td>
		</tr>
		@endforelse
	</tbody>
</table>