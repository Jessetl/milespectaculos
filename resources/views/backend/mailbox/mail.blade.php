@extends('layouts.main')

@section('content')

 <div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> {{ $mail->name }}, {{ $mail->departament }} </h3>
			</div>
		</div>
	</div>
</div>

<section class="section">
	
	@include('backend.messages.request')

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<section class="example">
						<div class="row my-3">
							<div class="col col-md">
								<strong class="d-block mb-3">Datos del contacto</strong>
								Correo electrónico: {{ $mail->email }}<br>
								Teléfono: {{ $mail->phone }}<br>
							</div>
						</div>
						<div class="row">
							<div class="col col-md">
								<strong class="d-block mb-3">Mensaje</strong>
								{{ $mail->suggest }}
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection