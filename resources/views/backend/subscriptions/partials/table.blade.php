<table class="table table-striped table-bordered table-hover" id="subscriptions">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Descripción</th>
			<th scope="col">Usuario</th>
			<th scope="col">PayPal ID</th>
			<th scope="col">Estado</th>
			<th scope="col">Precio</th>
			<th scope="col">Total</th>
			<th scope="col">Iva</th>
			<th scope="col">Fecha de Pago</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($payments as $key => $payment)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $payment->name }}</td>
			<td>{{ $payment->users->name }}</td>
			<td>{{ $payment->payment }}</td>
			<td>{{ $payment->status ? 'CONFIRMADA' : 'CANCELADA' }}</td>
			<td>{{ $payment->price }} €</td>
			<td>{{ $payment->total }} €</td>
			<td>{{ $payment->tax }}</td>
			<td>{{ $payment->created_at }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									<a href="{{ route('subscriptions.show', $payment) }}" class="edit"><i class="fa fa-eye"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="8">No hay facturaciones agregadas recientemente.</td>
		</tr>
		@endforelse
	</tbody>
</table>
