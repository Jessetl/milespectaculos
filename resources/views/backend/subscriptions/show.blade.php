@extends('layouts.main')

@section('content')

<div class="title-search-block">
    <div class="title-block">
        <div class="row">
			<div class="col-md-6">
			    <h3 class="title"> Facturación </h3>
			</div>
		</div>
	</div>
</div>

<section class="section">

	@include('backend.messages.request')
	
	<div class="row ">
		<div class="col-md-12">
			<div class="card card-block">
				<div class="card-body">
					<div class="title-block font-weight-bold">{{ $payment->name }}</div>
					<p class="float-right">
					 	{{ $payment->payment }}
					</p>

					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="2" class="text-center"> Pedido de suscripción </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									{{ $payment->name }}
								</td>
								<td>
									<strong> Precio </strong>
									<br>EUR {{ $payment->price }}
									<br><strong>Iva</strong>
									<br>EUR {{ $payment->tax }}
									<br><strong>Total</strong>
									<br>EUR {{ $payment->total }}
								</td>
							</tr>
						</tbody>
					</table>

					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="2" class="text-center"> Información sobre el pago </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									Datos de Facturación
								</td>
								<td class="text-right">
									<span class="font-weight-bold">
										{{ $payment->users->name }} {{ $payment->users->surname }}
									</span>
									<br>{{ $payment->users->phone }}
								</td>
							</tr>
							<tr>
								<td>
									Detalles de Transacción
								</td>
								<td class="text-center">
									<span class="{{ $payment->status ? 'text-success' : 'text-danger' }}">
										{{ $payment->status ? 'APROBADA' : 'RECHAZADA' }}
									</span>
								</td>
							</tr>
						</tbody>
					</table>

					<p class="text-center">
						Volver al <a href="{{ route('subscriptions.index') }}"> resumen de pedidos </a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection