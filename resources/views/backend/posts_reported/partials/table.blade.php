<table class="table table-striped table-bordered table-hover" id="posts">
	<thead>
		<tr>
			<th scope="col">Código</th>
			<th scope="col">Título</th>
			<th scope="col">Razón</th>
			<th scope="col">Propietario</th>
			<th scope="col">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@forelse($posts as $key => $post)
		<tr>
			<td>{{ $post->posts->code }}</td>
			<td>{{ $post->posts->name }}</td>
			<td>{{ $post->reason }}</td>
			<td>{{ $post->posts->usr->name }}</td>
			<td class="item-list">
				<div class="item-col fixed item-col-actions-dropdown">
					<div class="item-actions-dropdown">
						<a class="item-actions-toggle-btn">
							<span class="inactive">
								<i class="fa fa-cog"></i>
							</span>
							<span class="active">
								<i class="fa fa-chevron-circle-right"></i>
							</span>
						</a>
						<div class="item-actions-block">
							<ul class="item-actions-list">
								<li>
									{!! Form::model($post->posts, ['route' => ['complaints.destroy', $post->posts->id], 'method' => 'DELETE']) !!}

										<a href="#" class="remove" onclick="this.parentNode.submit()"><i class="fa fa-trash-o"></i></a>

									{!! Form::close() !!}
								</li>
								<li>
									<a href="#" class="edit"><i class="fa fa-eye"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="5">No hay anuncios denunciados</td>
		</tr>
		@endforelse
	</tbody>
</table>