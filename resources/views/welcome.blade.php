@extends('layouts.app')

@section('header')

    @include('layouts.partials.headerfrontend')

@endsection

@section('content')

<div class="container">
    <div class="row justify-content-around my-4">
        <div class="col-md text-center my-1">
            <a href="{{ url('posts/create') }}">
                <img src="{{ asset('/img/buttons/publicar-anuncio.png') }}">
            </a>
        </div>
        <div class="col-md text-center my-1">
            <a href="{{ url('my-posts') }}">
                <img src="{{ asset('/img/buttons/modificar-anuncio.png') }}">
            </a>
        </div>
        <div class="col-md text-center my-1">
            @if(auth()->guest())
            <a href="{{ route('my_favorites') }}">
                <img src="{{ asset('/img/buttons/anuncios-favoritos.png') }}">
            </a>
            @else
            <a href="{{ route('favorites.index') }}">
                <img src="{{ asset('/img/buttons/anuncios-favoritos.png') }}">
            </a>
            @endif
        </div>
        <div class="col-md text-center my-1">
            <a href="{{ url('events') }}">
                <img src="{{ asset('/img/buttons/arte-eventos.png') }}">
            </a>
        </div>
        <div class="col-md text-center my-1">
            <a href="{{ route('zone-vip.index') }}">
                <img src="{{ asset('/img/buttons/zona-vip.png') }}">
            </a>
        </div>
    </div>
</div>
<div class="container-fluid pb-5">
    <div class="row">
        @foreach($categories as $key => $category)

        <div class="col-md-3">
            <div class="p-3 text-center">
                @if (env('APP_DEBUG'))
                    <img src="{{ asset('/img/icons/circulos.png') }}">
                @else
                    <img src="{{ asset('storage/categories/'. $category->url) }}" height="120">
                @endif
            </div>
            <div class="pt-2 text-center">
                <span class="text-primary name-category text-uppercase">{{ $category->name }}</span>
            </div>

            {!! Form::open(['route' => 'category.search', 'method' => 'GET', 'class' => 'pt-2']) !!}

            <div class="input-group">
                <select class="custom-select" name="subcategory" {{ ($category->category_type->count() == 0) ? 'disabled' : '' }} onchange="this.form.submit();">
                    <option value="" selected disabled>Subcategorias</option>

                    <option value="{{ $category->slug }}">Todas las subcategorías</option>

                    @foreach($category->category_type as $key => $subcategory)

                        @if ( $subcategory->name !== 'Otros' AND $subcategory->name !== 'otros' )
                        <option value="{{ $subcategory->slug }}">{{ $subcategory->name }}</option>
                        @endif

                    @endforeach

                    <option value="otros">Otros</option>
                </select>
            </div>

            {!! Form::close() !!}
        </div>

        @endforeach

        <div class="col-md-3">
            <div class="p-3 text-center">
                <img src="{{ asset('img/icons/6c20e7be-d77f-40b9-8fcd-b0e532878ab4.jpg') }}" width="180" height="120">
            </div>
            <div class="pt-2 text-center">
                @if(Auth::guest())
                    <a href="{{ url('login') }}" class="text-primary name-category text-uppercase pointer-none">
                        SOLO ARTISTAS Y EMPRESAS ASOCIADAS
                    </a>
                @else
                    <a href="{{ url(Auth::user()->username . '/page') }}" class="text-primary name-category text-uppercase pointer-none">
                        SOLO ARTISTAS Y EMPRESAS ASOCIADAS
                    </a>
                @endif
            </div>
        </div>
        <div class="col-md-3">
             <div class="p-3 text-center">
                 <img src="{{ asset('img/icons/7M7M48s5ZYjajmUO284T.jpg') }}" width="180" height="120">
             </div>
             <div class="pt-2 text-center">
                 <a href="{{ url('contact/us') }}" class="text-danger pointer-none">
                    BUZON DE SUGERENCIAS<br><span class="text-primary name-category text-uppercase">AYUDANOS A MEJORAR POR TI</span></a>
             </div>
        </div>
    </div>
</div>

<hr>

<div class="container-fluid pb-5">
    <div class="row">
        <div class="col-md">
            <div class="text-center">
                <h3 class="font-weight-light my-3">
                    ¿Falta de tiempo para crear tu anuncio?
                </h3>
                <a href="{{ route('budgets.ad') }}" class="btn btn-primary btn-block">
                    DÉJALO DE NUESTRA MANO
                </a>
            </div>
        </div>
        <div class="col-md">
            <div class="text-center">
                <h3 class="font-weight-light my-3">
                    ¿Falta de tiempo para organizar tu evento?
                </h3>
                <a href="{{ route('budgets.event') }}" class="btn btn-primary btn-block">
                    TE LO ORGANIZAMOS
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md my-5 text-center">
            <iframe width="50%" height="315" src="https://www.youtube.com/embed/eHKWPl9gEkI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="content-slider pt-3">
                <ul id="lightSlider" class="cS-hidden">
                    @foreach($posts as $key => $post)
                        <li class="lslide">
                            <a href="{{ route('view.post', $post) }}">
                                <img src="{{ is_null($post->image) ? asset('img/slider.png') : asset('storage/posts/' . $post->image) }}" width="210" height="120" align="Artista">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>


@endsection
