@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">RED SOCIAL INTERNA.</h5>
        </header>

        <div class="col-md-9">

        	@forelse($posts as $key => $post)

        		<div class="card border mb-4">
			        <div class="card-header">
			            {{ $post->name }} <span class="float-right">{{ $post->created_at }}</span>
			        </div>
				    <div class="card-body">
			            <div class="row">
			                <div class="col-md-8">
			                    <p class="text-justify">
			                        {{ $post->content }}
			                    </p>
			                </div>
			                <div class="col-md-4 pb-5">
			                	<div class="image pointer" style="background-image: url({{ postLoadImage($post) }})"></div>
			                </div>
			                <div class="p-3">
				                <h3 class="text-danger"> DEMANDA </h3>
				            </div>
			                <div class="p-3">
				                @if ( $post->type )
				                    <button class="btn btn-border btn-success"> PROFESIONAL </button>
				                @else 
				                    <button class="btn btn-border btn-warning text-white"> PARTICULAR </button>
				                @endif
				            </div>
			            </div>
			        </div>
			        <div class="card-footer">
			        	<div class="p-1">
                            <button type="button" class="btn btn-border btn-primary btn-sm" 
			                    data-toggle="modal" data-target="#ModalContact{{ $key }}">
			                    <i class="fa fa-radius fa-volume-control-phone text-danger"></i> 
			                    CONTACTAR 
			                </button>
                        </div>
			        </div>
		        </div>

		        @include('frontend.search.modals.contact')

		    @empty

		    	<div class="card border mb-4">
				    <div class="card-body">
				    	<h5 class="card-title">No hay demandas de servicios disponibles.</h5>
				    </div>
				</div>
		    @endforelse

        	<span>
        		{!! $posts->render() !!}
        	</span>
        </div>
	    <div class="col-md-3">
	        
	        @include('frontend.posts.partials.menu-vertical')
	        
	    </div>
    </div>
</div>

@endsection