@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">SUBIR VIDEO A YOUTUBE.</h5>
        </header>

        <div class="col-md-9">

            @include('frontend.session.session')
            
            {!! Form::model($post, ['route' => ['upload.youtube', $post], 'method' => 'PUT', 'name' => 'formUpload', 'enctype' => 'multipart/form-data']) !!}

            <div class="card border p-2">
            	<div class="card-body">
            		<h4 class="card-title font-weight-light"> Sube tu video con nostros. </h4>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="title">Título <span class="text-danger">(*)</span></label>
                                <input type="text" name="title" value="{{ old('title') }}" class="form-control" required/>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="category">Categoría</label>
                                <select id="category" class="form-control" name="category" required>
                                    <option value="1">Cine y animación</option>
                                    <option value="2">Motor</option>
                                    <option value="10">Música</option>
                                    <option value="15">Mascotas y animales</option>
                                    <option value="17">Deportes</option>
                                    <option value="19">Viajes y eventos</option>
                                    <option value="20">Juegos</option>
                                    <option value="22">Gente y blogs</option>
                                    <option value="23">Comedia</option>
                                    <option value="24">Entretenimiento</option>
                                    <option value="25">Noticias y política</option>
                                    <option value="26">Consejos y estilo</option>
                                    <option value="27">Formación</option>
                                    <option value="28">Ciencia y tecnología</option>
                                    <option value="29">ONG y activismo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="description">Descripción</label>
                                <textarea value="{{ old('description') }}" class="form-control" rows="5" name="description" maxlength="200" required></textarea>
                            </div>
                        </div>
                    </div>
            		<div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="video"> Video </label>
                    			<input id="video" type="file" class="form-control" name="file" accept="video/*" required/>
                            </div>   
                        </div>
            		</div>
	            	<div class="row">
                        <div class="col">
                            <div class="form-group">
	            		        <button type="submit" class="btn btn-primary float-right">Subir</button>
                            </div>
                        </div>
	            	</div>
            	</div>
            </div>

            {!! Form::close() !!}

        </div>
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

@endsection