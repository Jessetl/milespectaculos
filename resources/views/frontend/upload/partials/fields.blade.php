@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">SUBE TU VIDEO A YOUTUBE.</h5>
        </header>

        <div class="col-md-9">
            <div class="card border p-2">
            	<div class="card-body">
            		<h4 class="font-weight-light"> Suscríbete ahora </h4>
            		<div class="ml-draft__info">
            			<h6 class="font-weight-bold"></h6>
            		</div>
	            	<div class="ml-action__button">
	            		<button type="submit" class="btn btn-border btn-primary">Continuar</button>
	            		<a class="ml-action-button__secondary" href="{{ route('posts.new') }}">Iniciar una nueva publicación</a>
	            	</div>
            	</div>
            </div>
        </div>
    </div>
</div>

@endsection