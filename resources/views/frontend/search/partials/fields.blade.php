<div class="row pt-2 pb-4">
    <div class="col">
		<select class="form-control text-uppercase" name="category" id="category">
			<option value="">TODAS LAS CATEGORIAS</option>
			@forelse($categories as $key => $category)
				<option value="{{ $category->id }}">{{ $category->name }}</option>
			@empty
				<option value="" selected disabled></option>
			@endforelse
		</select>
    </div>
    <div class="col">
		<select class="form-control text-uppercase" name="subcategory" id="subcategory">
			<option value="">SUBCATEGORÍA</option>
		</select>
    </div>
    <div class="col">
		<select class="form-control text-uppercase" name="districts">
			<option value="" selected>TODA ESPAÑA</option>
			@forelse($districts as $key => $district)
				<option value="{{ $district->id }}" 
					@if ( isset($slug) ) 
						{{ ($district->slug == $slug) ? 'selected' : '' }}
					@endif>
					{{ $district->name }}</option>
			@empty
				<option value="" selected disabled></option>
			@endforelse
		</select>
    </div>
</div>
<div class="row pt-2 pb-4">
	<div class="col">
		<select class="form-control" name="date">
			<option value="" selected>ORDENAR POR FECHA</option>
			<option value="ASC">ASCENDENTE</option>
			<option value="DESC">DESCENDENTE</option>
		</select>
	</div>
	<div class="col">
		<select class="form-control" name="type_post" id="type_post">
			<option value="">OFERTA/DEMANDA</option>
			<option value="">DEMANDA</option>
			<option value="1">OFERTA</option>
		</select>
	</div>
	<div class="col">
		<input type="number" step="0.1" class="form-control" name="amount_min" value="" placeholder="€ DESDE">
	</div>
	<div class="col">
		<input type="number" step="0.1" class="form-control" name="amount_max" value="" placeholder="€ HASTA">
	</div>
</div>
<div class="row pt-2 pb-4">
	<div class="col">
		<select class="form-control" name="type">
			<option value="">PARTICULAR/PROFESIONAL</option>
			<option value="0">PARTICULAR</option>
			<option value="1">PROFESIONAL</option>
		</select>
	</div>
	<div class="col">
		<input type="text" class="form-control" name="search" placeholder="¿Que buscas?" />
	</div>
</div>
