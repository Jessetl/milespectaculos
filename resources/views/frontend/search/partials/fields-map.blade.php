@forelse($posts as $key => $post)

<div class="card border mb-4">
    <div class="card-header">
        {{ $post->name }} <span class="float-right">{{ $post->code }} - {{ toDateString($post->updated_at) }}</span>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <p class="text-justify title-description-ad">
                    {{ $post->content }}
                </p>
            </div>
            <div class="col-md-4">
                <div class="image pointer" style="background-image: url({{ postLoadImage($post) }})"></div>
                <load-images :post="{{ $post }}"></load-images>
            </div>

            @if($post->type_posts == 0)

            <div class="col-3">
                <h4 class="text-danger d-inline text-uppercase">
                    {{ strcmp($post->type_amount, 'Otro') ? $post->type_amount : $post->amount . ' € ' }} 
                </h4>
            </div>

            <div class="col">
                @if ( $post->type )
                    <span class="btn btn-border btn-success"> PROFESIONAL </span>
                @else 
                    <span class="btn btn-border btn-warning text-white"> PARTICULAR </span>
                @endif

                @if(Auth::guest())
                    <a href="{{ route('login') }}" class="btn btn-border btn-warning text-white">
                        <i class="fa fa-thumbs-o-up"></i> FANS {{ $post->usr->followers()->count() }}</span>
                    </a>
                @else

                    <followers :post="{{ $post }}" :fans="{{ $post->usr->followers()->count() }}"></followers>

                @endif
            </div>

            @endif

            @if ($post->isPremium($post->user))
            <div class="col text-right">
                <img src="{{ asset('img/vip.png') }}" width="60" height="50">
            </div>
            @endif
        </div>
    </div>
    <div class="card-footer">
        <div class="d-flex flex-wrap justify-content-start">
            
            <div class="p-1">
                <button type="button" class="btn btn-border btn-primary btn-sm" 
                    data-toggle="modal" data-target="#ModalContact{{ $key }}">
                    <i class="fa fa-radius fa-volume-control-phone text-danger"></i> 
                    CONTACTAR 
                </button>
            </div>
            
            <div class="p-1">
                <button type="button" class="btn btn-border btn-primary btn-sm" 
                    data-toggle="modal" data-target="#ModalShare{{ $key }}">
                    <i class="fa fa-radius fa-share-alt text-danger"></i> 
                    COMPARTIR 
                </button>
            </div>
           
           <div class="p-1">

                @if(auth()->guest())

                    @if (\Cookie::get('favorites'))
                        <button type="submit" class="btn btn-border btn-primary btn-sm js__fav" id="{{ $post->slug }}">
                            <i data-ref="{{ $post->slug }}" class="fa fa-radius fa-heart {{ in_array($post->slug, unserialize(\Cookie::get('favorites'))) ? 'text-danger' : '' }}"></i> 
                            FAVORITO 
                        </button>
                    @else
                        <button type="submit" class="btn btn-border btn-primary btn-sm js__fav" id="{{ $post->slug }}">
                            <i data-ref="{{ $post->slug }}" class="fa fa-radius fa-heart"></i> 
                            FAVORITO 
                        </button>
                    @endif

                @else

                    <button type="submit" class="btn btn-border btn-primary btn-sm js__favorite" id="{{ $post->slug }}">
                        <i data-ref="{{ $post->slug }}" class="fa fa-radius fa-heart {{ Auth::user()->isFavorite($post) ? 'text-danger' : '' }}"></i> 
                        FAVORITO 
                    </button>

                @endif

            </div>

            <div class="p-1">
                <a href="{{ route('reports.create', $post) }}" class="btn btn-border btn-primary btn-sm" 
                    target="_blank">
                    <i class="fa fa-radius fa-bullhorn text-danger"></i> 
                    DENUNCIAR 
                </a>
            </div>

            <div class="p-1">
                <button type="button" class="btn btn-border btn-primary btn-sm js__file" data-id="{{ $post->slug }}">
                    MP3 
                    <i class="fa fa-camera fa-radius text-danger"></i>
                    <i class="fa fa-headphones fa-radius text-danger"></i> 
                </button>
            </div>

            <div class="p-1">
                @if ($post->isPremium($post->user))
                    
                    <a href="{{ route('view.post', $post) }}" class="btn btn-border btn-primary btn-sm">
                        <i class="fa fa-radius fa-user text-danger"></i> VER PERFIL 
                    </a>

                @else 
                    <a href="javascript:void(0)" class="btn btn-border btn-primary btn-sm disabled">
                        <i class="fa fa-radius fa-user"></i>  VER PERFIL 
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
    
    @include('frontend.search.modals.contact')
    @include('frontend.search.modals.share')

@empty

<div class="card border mb-4 pb-4">
    <div class="card-body">
        <h5 class="card-title">No hay anuncios en esta categoría</h5>
    </div>
</div>

@endforelse

<span>
    {{ $posts->appends(request()->input())->links() }}
</span>
