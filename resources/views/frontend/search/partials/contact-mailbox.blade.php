@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ENVIAR UN EMAIL</h5>
        </header>

        <div class="col-md-9">
            
            @include('frontend.session.session')

            {!! Form::open(['route' => 'contact.post', 'method' => 'POST', 'autocomplete' => 'off']) !!}
            
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="nombre"> Nombre <span class="text-danger">(*)</span></label>
                            <input type="text" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} popover-dismiss" name="name" data-toggle="popover" data-placement="top" data-content="Escribe tu nombre completo" required>
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <input type="hidden" name="username" value="{{ $user->username }}">
                        <div class="form-group col-md-6">
                            <label for="email"> Correo electronico <span class="text-danger">(*)</span></label>
                            <input type="text" autocomplete="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} popover-dismiss" name="email" data-toggle="popover" data-placement="top" data-content="Escribe tu Correo electronico" required>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="suggest"> Mensaje <span class="text-danger">(*)</span></label>
                            <textarea id="suggest" class="form-control {{ $errors->has('suggest') ? ' is-invalid' : '' }} popover-dismiss" name="suggest" data-toggle="popover" data-placement="top" data-content="Escribe tu mensaje de contacto detalladamente." required rows="4">
                            </textarea>
                            @if($errors->has('suggest'))
                                <div class="invalid-feedback">{{ $errors->first('suggest') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Enviar">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    Los campos con <span class="text-danger">(*)</span> son obligatorios
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>


</div>

@endsection