@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="modal fade" tabindex="-1" role="dialog" id="modalFiles" aria-labelledby="modalFiles" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalFiles">MP3</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="js__files"></div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalImages" aria-labelledby="modalImages" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalImages">IMAGENES DE PUBLICACIÓN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                    <div id="js__images"></div>
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">BÚSQUEDA.</h5>
        </header>

        <div class="col-md-9">
            {!! Form::open(['route' => 'advanced.search', 'method' => 'GET', 'autocomplete' => 'off']) !!}
                <div class="card border mb-3">
                    <div class="card-body">

                        @include('frontend.search.partials.fields')

                        <span class="text-black">Encontrados {{ count($posts) }} anuncios.</span>
                    
                        <div class="row pt-3">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-primary float-right" value="Buscar">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-center">
                        ¿ Quieres conocer las ventajas de pertenecer a nuestro Club de asociados VIP ? <a href="{{ url('zone-vip') }}" target="_blank" class="text-danger">Pincha aquí</a>
                    </div>
                </div>

                @include('frontend.search.partials.fields-advance')

            {!! Form::close() !!}
        </div>
	    <div class="col-md-3">
	        
            @include('frontend.posts.partials.menu-vertical')

            <div class="card mt-3">
                <div class="card-header menu-vertical">
                    <div class="text-center">CATEGORÍAS</div>
                </div>
        
                <div class="card-body no__padding">
                    @foreach($categories as $key => $category)
                    <div class="item__menu">
                       <div class="category__item bg-purple"> 
                            {{ $category->name }} 
                        </div>
                        <div class="category-item__main">
                            @foreach($category->category_type as $key => $type)
                                <div class="category-item__item">
                                    <a href="{{ route('category.return', $type) }}"> {{  $type->name }}</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            
            <div class="card border mt-3">
                <div class="card-header menu-vertical">
                    <div class="text-center">BUSCAR POR PROVINCIA</div>
                </div>
                <div class="card-body">
                    @include('frontend.search.layouts.mapa')
                </div>
            </div>
	    </div>
    </div>
</div>

@endsection