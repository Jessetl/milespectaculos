<div class="modal fade" id="ModalFiles{{ '['.$key.']' }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">MP3</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

			@forelse($post->posts_meta()->where('meta_key', 'Video')->get() as $key => $meta)

				@if ( !empty($meta->meta_url) )
					<iframe width="466" height="205" src="{{ $meta->meta_url }}" 
	                    frameborder="0" allow="autoplay; encrypted-media" 
	                    allowfullscreen>
	                </iframe>
                @endif

            @empty

            	<div class="text-center border mb-2">
            		<strong>NO HAY VIDEO CARGADOS</strong>
            	</div>

			@endforelse

			@forelse($post->posts_meta()->where('meta_key', 'Audio')->get() as $key => $meta)

				@if ( !empty($meta->meta_url) )
					<iframe width="100%" scrolling="no" height="100" frameborder="no" 
	                    src="https://w.soundcloud.com/player/?url={{ $meta->meta_url }}">   
	                </iframe>
                @endif

            @empty

            	<div class="text-center border mb-2">
            		<strong>NO HAY AUDIOS CARGADOS</strong>
            	</div>

			@endforelse

			</div>
		</div>
	</div>
</div>

