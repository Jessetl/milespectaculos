<div class="modal fade" id="ModalShare{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div>
					<strong>
						<a href="mailto:name@milespectaculos.com" class="text-primary"> 
							<h4><i class="fa fa-envelope-open-o"></i>
							Enviar por email</h4>
						</a>
					</strong>
				</div>
				<div>
					<strong>
						<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
							<h4><i class="fa fa-facebook-official"></i>
							Facebook</h4>
						</a>
					</strong>
				</div>
				<div>
					<strong> 
						<a target="_blank" href="http://twitter.com/share?url=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
							<h4><i class="fa fa-twitter-square"></i>
							Twitter</h4>
						</a>
					</strong>
				</div>
				<div>
					<strong>
						<a target="_blank" href="http://pinterest.com/pin/create/button/?url=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
							<h4><i class="fa fa-pinterest-square"></i>
							Pinterest</h4>
						</a>
					</strong>
				</div>
				<div>
					<strong>
						<a target="_blank" href="https://plus.google.com/share?url=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
							<h4><i class="fa fa-google-plus-square "></i>
							Google+</h4>
						</a>
					</strong>
				</div>
			</div>
		</div>
	</div>
</div>

