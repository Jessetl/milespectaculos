<div class="modal fade" id="ModalContact{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="ModalContact" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">DATOS DE CONTACTO</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="p-2">
					<strong>
						{{ $post->usr->name }}  ({{ $post->type ? 'PROFESIONAL' : 'PARTICULAR' }}) 
					</strong>
				</div>
				@if ( $post->usr->premium == (TRUE))
					<div class="p-2">
						<strong>
							<i class="fa fa-radius fa-phone text-danger"></i> {{ $post->phone1 }}
						</strong>
					</div>
					@if( !empty($post->phone2) )
					<div class="p-2">
						<strong>
							<i class="fa fa-radius fa-phone text-danger"></i> {{ $post->phone2 }}
						</strong>
					</div>
					@endif
				@else
					<div class="p-2">
						<strong>
							<i class="fa fa-radius fa-phone text-danger"></i> (+ 34) 633 77 40 08
						</strong>
					</div>
					<div class="p-2">
						<strong>
							<i class="fa fa-radius fa-phone text-danger"></i> (+ 34) 622 94 76 77
						</strong>
					</div>
					<div class="p-2">
						<strong>
							<i class="fa fa-radius fa-phone text-danger"></i> (+ 34) 952 07 31 57
						</strong>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>

