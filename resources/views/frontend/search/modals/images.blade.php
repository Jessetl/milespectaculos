<div class="modal fade" id="images{{ '['.$key.']' }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">IMAGENES DE PUBLICACIÓN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div id="carousel{{ $key }}" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
				
					@foreach (postImage($post) as $meta => $img)

						<div class="carousel-item {{ $meta == 0 ? 'active' : '' }} ">
							<img class="d-block w-100" src="{{ asset('storage/posts/' . $img->meta_url) }}" alt="First slide">
						</div>

					@endforeach    

					<a class="carousel-control-prev" href="#carousel{{ $key }}" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carousel{{ $key }}" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>