<div class="row pt-2">
    <div class="col">
        <label for="title">Título <span class="text-danger">(*)</span></label>
        <input type="text" id="title" name="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}" placeholder="" required/>
        @if ($errors->has('title'))
            <div class="invalid-feedback">{{ $errors->first('title') }}</div>
        @endif
    </div>
    <div class="col">
        <label for="date">Fecha del Evento <span class="text-danger">(*)</span></label>
        <input type="date" id="date" name="date" class="form-control {{ $errors->has('date') ? ' is-invalid' : '' }}" value="{{ old('date') }}" placeholder="" required/>
        @if ($errors->has('date'))
            <div class="invalid-feedback">{{ $errors->first('date') }}</div>
        @endif
    </div>
</div>

<div class="row pt-2">
    <div class="col">
    	<label for="districts">Provincias <span class="text-danger">(*)</span></label>
        <select id="districts" class="form-control {{ $errors->has('districts') ? ' is-invalid' : '' }}" name="districts" required>
            @foreach($districts as $key => $district)
                <option value="{{ $district->id }}" >{{ $district->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('districts'))
            <div class="invalid-feedback">{{ $errors->first('districts') }}</div>
        @endif
    </div>
    <div class="col">
        <label for="circuit">Localidad <span class="text-danger">(*)</span></label>
        <input type="text" id="circuit" class="form-control {{ $errors->has('circuit') ? ' is-invalid' : '' }}" name="circuit" value="{{ old('circuit') }}" required/>
        @if ($errors->has('circuit'))
            <div class="invalid-feedback">{{ $errors->first('circuit') }}</div>
        @endif
    </div>
</div>

<div class="row pt-2">
	<div class="col">
        <label for="address">Dirección <span class="text-danger">(*)</span></label>
        <textarea class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" name="address" placeholder="" rows="3">{{ old('address') }}</textarea>
        @if ($errors->has('address'))
            <div class="invalid-feedback">{{ $errors->first('address') }}</div>
        @endif
    </div>
</div>