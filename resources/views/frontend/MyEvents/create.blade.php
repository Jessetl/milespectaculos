@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">AÑADIR NUEVO EVENTO</h5>
        </header>

        <div class="col-md-9">
    		{!! Form::open(['route' => 'my-events.store', 'method' => 'POST', 'autocomplete' => 'off']) !!}
            <div class="card p-2">
                <div class="card-body">
                <h6 class="card-title">Publica tus proximos eventos.</span></h6>
                    
        			@include('frontend.MyEvents.partials.fields')

        			<span class="text-danger">Los usuarios gratuitos solo permiten anunciar 3 eventos.</span>
                    
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Publicar">
                        </div>
                    </div>
                </div>
            </div>
    		{!! Form::close() !!}
        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>
</div>

@endsection