@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">SUSCRIPCIÓN {{ $paypal->name }}</h5>
        </header>

        <div class="col-md-4">

        	{!! Form::open(['route' => ['paypal.subscription', $paypal->id], 'method' => 'GET']) !!}

        	<div class="card border">
        		<div class="card-body">
                    <img src="{{ asset('img/vip.png') }}" class="img-fluid my-2">
        			<h5 class="card-title">
        				{{ $paypal->name }}
        			</h5>
        			<p>
        				{{ $paypal->description }}
        			</p>
        			<p>
        				Precio: <span class="text-danger"> {{ $paypal->amount }} € </span> + <span class="text-danger"> 21% </span> IVA 
        			</p>
                    <p>
                        Total: <span class="text-danger"> {{  ($paypal->amount * 0.21) + $paypal->amount }} €</span>
                    </p>
        		</div>
        		<div class="card-footer">
        			<button type="submit" class="btn btn-danger mx-2"> Suscribite ahora</button>
        			<a href="{{ route('prices') }}" target="_blank">Ver más características</a>
        		</div>
        	</div>

        	{!! Form::close() !!}
    	
        </div>

        @if( !empty($paypalT) )
        <div class="col-md-4">
        	
        	{!! Form::open(['route' => ['paypal.subscription', $paypal->id], 'method' => 'GET']) !!}

        	<div class="card border">
        		<div class="card-body">
        			<h5 class="card-title">
        				{{ $paypal->name }}
        			</h5>
        			<p>
        				{{ $paypal->description }}
        			</p>
        			<p>
        				Precio: <span class="text-danger"> {{ $paypal->amount }} € </span> + <span class="text-danger"> 21% </span> IVA 
        			</p>
        			<p>
	        			Total: <span class="text-danger"> {{ ($paypal->amount + 0.21) * 12 }} </span>
        			</p>
        		</div>
        		<div class="card-footer">
        			<button type="submit" class="btn btn-danger mx-2"> Suscribirse ahora</button>
        			<a href="{{ route('prices') }}" target="_blank">Ver más caracteristicas</a>
        		</div>
        	</div>

        	{!! Form::close() !!}

        </div>
        @endif
    </div>
</div>

@endsection