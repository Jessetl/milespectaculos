@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">CARACTERÍSTICAS Y PRECIOS.</h5>
        </header>

        <div class="col-md-12">
        	<table class="table table-responsive">
        		<thead class="bg-white">
        			<tr class="text-center">
        				<th class="col-md-3 text-uppercase text-center">
        					CARACTERÍSTICAS
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">ARTISTAS PROFESIONALES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">EMPRESAS PROFESIONALES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">Oferta</span>
        					<span class="text-danger d-block">especial solo</span>
        					<span class="text-danger d-block">este mes</span>
        				</th>
        			</tr>
        		</thead>
        		<tbody class="bg-white">
        			<tr class="text-center">
        				<th>
        					RED SOCIAL INTERNA
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th class="text-center">
                            <span class="text-danger d-block">ARTISTAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
        				</th>
        			</tr>
        			<tr>
        				<td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/red social.png') }}">
        					Podrás establecer contacto con Artistas y Empresas  del sector, intercambiar experiencias, ideas, intercambiar documentos, midis,karaokes,partituras, etc. buscar sustitutos para tus galas, hacer negocios, comprar y vender lo que ya no necesitas, etc.
        				</td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">          
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">               
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
        			</tr>
                    <tr class="text-center">
                        <th>
                            OFERTAS Y DESCUENTOS ESPECIALES ENTRE EMPRESAS Y ARTISTAS
                        </th>
                        <th>
                            <span class="text-danger d-block">GRATIS</span>
                            <span>ARTISTAS Y EMPRESAS</span>
                        </th>
                        <th>
                            <span class="text-danger d-block">GRATIS</span>
                            <span>USUARIOS PARTICULARES</span>
                        </th>
                        <th>
                            <span class="text-danger d-block">ARTISTAS</span>
                        </th>
                        <th>
                            <span class="text-danger d-block">EMPRESAS</span>
                        </th>
                        <th>
                            <span class="d-block">ARTISTAS</span>
                            <span class="d-block">Y</span>
                            <span class="d-block">EMPRESAS</span>
                        </th>
                    </tr>
                    <tr>
                        <td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/descuentos especiales.png') }}">
                            El Lugar donde podrás presentar tus Ofertas Especiales y te podrás beneficiar de los descuentos de las Empresas Asociadas, estas Ofertas y Descuentos es solo para Profesionales Asociados 
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
                    </tr>
        			<tr class="text-center">
        				<th>
        					PROMOCIÓN DE TUS EVENTOS
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
        				</th>
        			</tr>
        			<tr>
        				<td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/arte y eventos.png') }}">
        					Presenta tus Eventos en tu perfil, a muchos clientes les gusta ver antes de Contratar la Actuación o Servicio, Más aún si organizas conciertos, ferias, Exposiciones, etc. También sirve para que automáticamente se comparta a tus FANS de redes sociales y a nuestro departamento de Marketing para promocionaros con otras técnicas a nivel online. 
        				</td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					DIFUSIÓN DE TU PERFIL EN LAS REDES CLUB FANS
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
        				</th>
        			</tr>
        			<tr>
        				<td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/Facebook-Fans.png') }}">
        					Para que tus contactos se hagan tus FANS. A más FANS y seguidores aparezcan en tu perfil, mayor Garantía percibiránlos Clientes sobre tu Empresa o Espectáculo y también cada vez que expongas un evento en tu perfil les llegará a su perfil o móvil haciéndote una Gran publicidad de forma automática 
        				</td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					MEJOR POSICIONAMIENTO
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
        				</th>
        			</tr>
        			<tr>
        				<td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/mejor posicionamiento.png') }}">
        					Nuestros Socios Vip gozan de un mejor posicionamiento no solo en su categoría y en el panel superior de la pagina principal, donde aparecen de forma animada, por orden de perfiles mas actualizados. Cada vez que actualizas tuperfil, te posicionas en primer lugar y sales en el panel superior de destacados
        				</td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					MARKETING ONLINE
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
        				</th>
        			</tr>
        			<tr>
        				<td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/servicio de marketing online.png') }}">
        					Tener a vuestra disposición el mejor Equipo de Marketing que se encargara no solo a través de MIL ESPECTÁCULOS si no también mediante otrastécnicas como, E-mail Marketing, Tele Marketing, Social Network, Social Media, etc. para llegar a organismos públicos y privados, como ayuntamientos, agencias, representantes, Hoteles, etc. y así ser lo mas efectivos y rentables a nuestros socios Vip.
        				</td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					ZONA DE SOCIOS
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
        				</th>
        			</tr>
        			<tr>
        				<td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/zona negocios2.png') }}">
        					MIL ESPECTÁCULOS no solo es un portal de ofertas, donde nuestros profesionales exponen sus servicios, también somos un portal de demanda de servicios donde Particulares y Empresas, demandan profesionales, pero estas demandas solo aparecen en el panel interno, donde tan solo tienen acceso nuestros asociados VIP
        				</td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					MAS BENEFICIOS VIP
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
        				</th>
        			</tr>
        			<tr>
        				<td class="text-justify">
                            <img class="img-fluid" src="{{ asset('img/prices/mas beneficios vip.png') }}">
        					Nuestros Socios No poseen limites; al insertar fotos, videos, MP3, anuncios de ofertas y demandas, sin limites exponiendo todos los servicios que ofrece y en cada provincia de España. Esta ventaja es ideal para representantes artísticos que poseen muchos Artistas. o Empresas y Espectáculos con cobertura Nacional. URL Propia dentro de Mil Espectáculos, perfil web dentro de Mil Espectáculos. etc.....
        				</td>
        				<td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/DESVENTAJA.png') }}" width="90" height="90">             
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">               
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
                        <td>
                            <img class="img-fluid" src="{{ asset('img/prices/VENTAJA.png') }}" width="90" height="90">              
                        </td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					PUBLICACIÓN Y CONTACTO
        				</th>
        				<th>
        					Art. Emp.
        				</th>
        				<th>
        					Artistas Empresas
        				</th>
        				<th>
        					Artistas
        				</th>
        				<th>
        					Empresas
        				</th>
        				<th>
        					Art. Emp.
        				</th>
        			</tr>
        			<tr>
        				<td> Publicación de anuncio </td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        			</tr>
        			<tr>
        				<td> Formulario de contacto </td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        				<td>SÍ</td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					Listado y ficha artística
        				</th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        			</tr>
        			<tr>
        				<td>Mejora en la posición de listados (*)</td>
        				<td>
        					<span class="d-block">Si</span> <span class="text-danger">Manualmente</span>
        				</td>
        				<td>
        					<span class="d-block">Si</span> <span class="text-danger">Manualmente</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Perfil web ficha artística Personal</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Anuncio destacado en listados Logo VIP</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Datos de contacto visibles en listado</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Video destacado y visible en listado</td>
        				<td>
        					Si (UNO)
        				</td>
        				<td>
        					Si (UNO)
        				</td>
        				<td>
        					ILIMITADOS
        				</td>
        				<td>
        					ILIMITADOS
        				</td>
        				<td>
        					ILIMITADOS
        				</td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					Contenido del anuncio
        				</th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        			</tr>
        			<tr>
        				<td>Anuncios por Provincias</td>
        				<td>1</td>
        				<td>1</td>
        				<td>Toda España</td>
        				<td>Toda España</td>
        				<td>Toda España</td>
        			</tr>
        			<tr>
        				<td>Anunciar tus Eventos</td>
        				<td>3</td>
        				<td>3</td>
        				<td>ILIMITADOS</td>
        				<td>ILIMITADOS</td>
        				<td>ILIMITADOS</td>
        			</tr>
        			<tr>
        				<td>Etiquetas</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Ilimitado
        				</td>
        				<td>
        					Ilimitado
        				</td>
        				<td>
        					Ilimitado
        				</td>
        			</tr>
    				<tr>
    					<td>Promoción Fans</td>
    					<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Ilimitado
        				</td>
        				<td>
        					Ilimitado
        				</td>
        				<td>
        					Ilimitado
        				</td>
    				</tr>
        			<tr>
        				<td>Gestión de opiniones</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>URL propia dentro de Mil Espectáculos</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Peticiones  de presupuesto Panel Interno (DEMANDA)</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Acceso a Red Social Interna</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Equipo de Marketing Online</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>	
        			</tr>
        			<tr class="text-center">
        				<th>
        					Recursos multimedia
        				</th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        			</tr>
        			<tr>
        				<td>Fotos (mínimo 1)</td>
        				<td>4</td>
        				<td>4</td>
        				<td>Ilimitadas</td>
        				<td>Ilimitadas</td>
        				<td>Ilimitadas</td>
        			</tr>
        			<tr>
        				<td>MP3 (mínima 1, si aplica)</td>
        				<td class="text-danger">NO</td>
        				<td>2</td>
        				<td>Ilimitadas</td>
        				<td>Ilimitadas</td>
        				<td>Ilimitadas</td>
        			</tr>
        			<tr>
        				<td>Vídeos (mínimo 1, si aplica)</td>
        				<td>1</td>
        				<td>1</td>
        				<td>Ilimitadas</td>
        				<td>Ilimitadas</td>
        				<td>Ilimitadas</td>
        			</tr>
        			<tr>
        				<td>Actualización automática del anuncio</td>
        				<td class="text-danger">Actualización (manual)</td>
        				<td class="text-danger">Actualización (manual)</td>
        				<td>Si (AUTOMÁTICA)</td>
        				<td>Si (AUTOMÁTICA)</td>
        				<td>Si (AUTOMÁTICA)</td>
        			</tr>
        			<tr>
        				<td>Mapa de localización</td>
        				<td class="text-danger">NO</td>
        				<td class="text-danger">NO</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        			</tr>
        			<tr>
        				<td>Experiencia de usuario UX geográficamente localizado para móvi</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        			</tr>
        			<tr>
        				<td>notificaciones push</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        			</tr>
        			<tr>
        				<td>posibilidad al usuario de que pongan su enlace de youtube y si no lo poseen que tengan la posibilidad de subirlo desde su perfil a youtube de milespectáculos </td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        			</tr>
        			<tr>
        				<td>Duración del anuncio para renovación</td>
        				<td>30, 60 o 90 días a elegir</td>
        				<td>30, 60 o 90 días a elegir</td>
        				<td>Anual</td>
        				<td>Anual</td>
        				<td>Anual</td>
        			</tr>
        			<tr class="text-center">
        				<th>
        					RECURSOS PROFESIONALES DE MARKETING MEDIANTE DIFUSIÓN ONLINE
        				</th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        				<th></th>
        			</tr>
        			<tr>
        				<td>Marketing promocionales a Ayuntamientos, Agencias, Empresas, particulares etc.</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>aplicación Móvil para la web desde Play Store pensando en AP</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        				<td>SI</td>
        			</tr>
        			<tr>
        				<td>Acceso a Foro Red Social interno</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Poder compartir archivos Internos con otros Asociados, Mídis, Karaokes, Pdf, Videos, etc.</td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr>
        				<td>Tienda Online interna donde poder comprar y vender artículos de 2ª mano totalmente Gratis. </td>
        				<td>
        					<span class="text-danger">No</span>
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        				<td>
        					Sí
        				</td>
        			</tr>
        			<tr class="text-center">
        				<th class="col-md-3 text-uppercase text-center">
        					CARACTERÍSTICAS
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">ARTISTAS PROFESIONALES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">EMPRESAS PROFESIONALES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">Oferta</span>
        					<span class="text-danger d-block">espcial solo</span>
        					<span class="text-danger d-block">este mes</span>
        				</th>
        			</tr>
        			<tr class="text-center">
        				<th>
        					<span class="text-danger">PRECIOS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
        				</th>
        			</tr>
        			<tr class="text-center">
        				<td>Precio (**)</td>
        				<td class="text-danger">Gratis</td>
        				<td class="text-danger">Gratis</td>
        				<td><span class="text-danger">{{ $payments['ar']->amount }}</span> € / mes + IVA (Contratación mínima  12 meses) Total Anual <span class="text-danger">{{ $payments['ar']->amount * 12 }}</span> € + IVA</td>
        				<td><span class="text-danger">{{ $payments['em']->amount }}</span> € / mes + IVA (Contratación mínima  12 meses) Total Anual <span class="text-danger">{{ $payments['em']->amount * 12 }}</span> € + IVA</td>
        				<td>
        					<strong>ARTISTAS</strong> <span class="text-danger">1,00</span> € / mes + IVA Contratación mínima 12 meses) Total Anual 12,00 € + IVA <br>
        					<strong>Empresas</strong> <span class="text-danger">12,50€</span> /mes (Contratación mínima 12 meses) Total Anual 150,00 € + IVA
        				</td>
        			</tr>
        		</tbody>
        	</table>
        </div>
    </div>
</div>
<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">OTROS SERVICIOS ADICIONALES DE PAGO.</h5>
        </header>

        <div class="col-md-12">
        	<table class="table table-responsive">
        		<thead class="bg-white">
        			<tr class="text-center">
        				<th class="col-md-3 text-uppercase text-center">
        					<span class="text-danger d-block">MODALIDAD</span>
        					<strong>POR PROVINCIAS</strong>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">ARTISTAS PROFESIONALES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">EMPRESAS PROFESIONALES</span>
        				</th>
        				<th class="text-uppercase">
        					<span class="text-danger d-block">Oferta</span>
        					<span class="text-danger d-block">espcial solo</span>
        					<span class="text-danger d-block">este mes</span>
        				</th>
        			</tr>
        		</thead>
        		<tbody class="bg-white">
        			<tr class="text-center">
        				<th>
        					<span class="text-danger">PRECIOS POR PROVINCIAS ADICIONALES</span>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>ARTISTAS Y EMPRESAS</span>
                            <strong class="d-block">1 PROVINCIA</strong>
                            <strong class="text-danger">GRATIS</strong>
        				</th>
        				<th>
        					<span class="text-danger d-block">GRATIS</span>
        					<span>USUARIOS PARTICULARES</span>
                            <strong class="d-block">1 PROVINCIA</strong>
                            <strong class="text-danger">GRATIS</strong>
        				</th>
        				<th>
        					<span class="text-danger d-block">ARTISTAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
                            <strong class="d-block">ILIMITADO</strong>
                            <strong class="text-danger">GRATIS</strong>
        				</th>
        				<th>
        					<span class="text-danger d-block">EMPRESAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
                            <strong class="d-block">ILIMITADO</strong>
                            <strong class="text-danger">GRATIS</strong>
        				</th>
        				<th>
        					<span class="d-block">ARTISTAS</span>
        					<span class="d-block">Y</span>
        					<span class="d-block">EMPRESAS</span>
                            <img class="img-fluid" src="{{ asset('img/prices/vip2.png') }}" width="90" height="90">
                            <strong class="d-block">ILIMITADO</strong>
                            <strong class="text-danger">GRATIS</strong>
        				</th>
        			</tr>
        			<tr class="text-center">
        				<td></td>
        				<td>
        					<span class="text-danger">{{ $payments['pr']->amount }}</span> € POR PROVINCIA ADICIONAL MENSUAL + 21% IVA
        				</td>
        				<td>
        					<span class="text-danger">{{ $payments['pr']->amount }}</span> € POR PROVINCIA ADICIONAL MENSUAL + 21% IVA
        				</td>
        				<td>
        					<strong class="d-block">ARTISTAS VIP</strong>
        					ILIMITADO<br>
        					<span class="text-danger">Gratis</span>
        				</td>
        				<td>
        					<strong class="d-block">EMPRESAS VIP</strong>
        					ILIMITADO<br>
        					<span class="text-danger">Gratis</span>
        				</td>
        				<td>
        					<strong class="d-block">ARTISTAS VIP</strong>
        					ILIMITADO<br>
        					<span class="text-danger">Gratis</span>
        					<br>
        					<strong class="d-block">EMPRESAS VIP</strong>
        					ILIMITADO<br>
        					<span class="text-danger">Gratis</span>
        				</td>
        			</tr>
        			<tr>
        				<td colspan="6" class="text-center">Tarifas y promociones al 952 07 31 57 o comercial@milespectaculos.com <br> ¿Quieres que te llamemos?</td>
        			</tr>
        		</tbody>
        	</table>
        </div>
    </div>
</div>
@endsection