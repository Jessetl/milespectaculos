@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ORGANIZAMOS TU EVENTO</h5>
        </header>
        <div class="col-md-9">
        	
        	@include('frontend.session.session')

        	{!! Form::open(['route' => 'budgets.storeEvent', 'method' => 'POST']) !!}

            <div class="card-body">
                <h4 class="card-title">
                    ¿QUÉ ESTÁS ORGANIZANDO?
                </h4>
                <div class="row">
                    <div class="col-md">
                        <label for="category"> Estoy organizando </label>
                        <div class="form-group">
                            <select id="category" name="type" class="form-control {{ $errors->has('type') ? ' is-invalid' : '' }}" required>
                                <option value="" selected disabled>Elige un tipo de evento</option>
                                <optgroup label="FIESTAS POPULARES">
                                    <option value="Verbenas">Verbenas</option>
                                    <option value="Fallas">Fallas</option>
                                    <option value="Ferias">Ferias</option>
                                    <option value="Fiesta del Pilar">Fiesta del Pilar</option>
                                    <option value="Fiestas locales y patronales">Fiestas locales y patronales</option>
                                    <option value="Hoguera de San Juan">Hoguera de San Juan</option>
                                    <option value="Moros y Cristianos">Moros y Cristianos</option>
                                    <option value="San Fermín">San Fermín</option>
                                    <option value="San Isidro">San Isidro</option>
                                    <option value="Sant Jordi">Sant Jordi</option>
                                    <option value="Semana Grande de Bilbao">Semana Grande de Bilbao</option>
                                    <option value="Semana Santa">Semana Santa</option>
                                    <option value="Tomatina de Buñol">Tomatina de Buñol</option>
                                </optgroup>
                                <optgroup label="BODAS">
                                    <option value="Aniversario de Bodas">Aniversario de Bodas</option>
                                    <option value="Banquetes de Boda">Banquetes de Boda</option>
                                    <option value="Boda Civil">Boda Civil</option>
                                    <option value="Boda Religiosa">Boda Religiosa</option>
                                    <option value="Bodas">Bodas</option>
                                    <option value="Bodas de Oro">Bodas de Oro</option>
                                    <option value="Bodas de Plata">Bodas de Plata</option>
                                    <option value="Bodas en la Playa">Bodas en la Playa</option>
                                    <option value="Ceremonia de Bodas">Ceremonia de Bodas</option>
                                    <option value="Cóctel de Bienvenida">Cóctel de Bienvenida</option>
                                    <option value="Despedida de Soltero">Despedida de Soltero</option>
                                    <option value="Despedida de Soltera">Despedida de Soltera</option>
                                    <option value="Recepción de Bodas">Recepción de Bodas</option>
                                </optgroup>
                                <optgroup label="CUMPLEAÑOS Y ANIVERSARIOS">
                                    <option value="Aniversario de Boda">Aniversario de Boda</option>
                                    <option value="Cumpleaños">Cumpleaños</option>
                                    <option value="Cumpleaños Adulto">Cumpleaños Adulto</option>
                                    <option value="Cumpleaños Infantil">Cumpleaños Infantil</option>
                                    <option value="Fiesta de Aniversario">Fiesta de Aniversario</option>
                                    <option value="Fiesta de Jubilación">Fiesta de Jubilación</option>
                                </optgroup>
                                <optgroup label="EVENTOS">
                                    <option value="Cabalgatas de Reyes">Cabalgatas de Reyes</option>
                                    <option value="Cenas">Cenas</option>
                                    <option value="Certámenes">Certámenes</option>
                                    <option value="Cóctel">Cóctel</option>
                                    <option value="Concentración de Coches">Concentración de Coches</option>
                                    <option value="Concentración de Motos">Concentración de Motos</option>
                                    <option value="Conciertos">Conciertos</option>
                                    <option value="Convenciones">Convenciones</option>
                                    <option value="Cruceros">Cruceros</option>
                                    <option value="Entrega de Premios">Entrega de Premios</option>
                                    <option value="Evento Corporativo">Evento Corporativo</option>
                                    <option value="Exposiciones">Exposiciones</option>
                                    <option value="Festivales">Festivales</option>
                                    <option value="Inauguraciones">Inauguraciones</option>
                                    <option value="Pasacalles">Pasacalles</option>
                                    <option value="Quedadas">Quedadas</option>
                                    <option value="Reuniones Familiares">Reuniones Familiares</option>
                                    <option value="Seminarios">Seminarios</option>
                                </optgroup>
                                <optgroup label="DÍAS ESPECIALES">
                                    <option value="Año Nuevo Chino">Año Nuevo Chino</option>
                                    <option value="Bautizos">Bautizos</option>
                                    <option value="Carnavales">Carnavales</option>
                                    <option value="Comuniones">Comuniones</option>
                                    <option value="Día de la Madre">Día de la Madre</option>
                                    <option value="Día de todos los Santos">Día de todos los Santos</option>
                                    <option value="Día del Padre">Día del Padre</option>
                                    <option value="Día del Trabajador">Día del Trabajador</option>
                                    <option value="Funerales">Funerales</option>
                                    <option value="Noche Buena">Noche Buena</option>
                                    <option value="Noche Vieja">Noche Vieja</option>
                                    <option value="Pedidas de Mano">Pedidas de Mano</option>
                                    <option value="San Valentín">San Valentín</option>
                                </optgroup>
                                <optgroup label="FIESTAS">
                                    <option value="Fiesta de Disfraces">Fiesta de Disfraces</option>
                                    <option value="Fiesta de Empresas">Fiesta de Empresas</option>
                                    <option value="Fiesta de Fin de Curso">Fiesta de Fin de Curso</option>
                                    <option value="Fiesta de Graduación">Fiesta de Graduación</option>
                                    <option value="Fiesta de halloween">Fiesta de halloween</option>
                                    <option value="Fiesta de los 50">Fiesta de los 50</option>
                                    <option value="Fiesta de los 60">Fiesta de los 60</option>
                                    <option value="Fiesta de los 70">Fiesta de los 70</option>
                                    <option value="Fiesta de los 80">Fiesta de los 80</option>
                                    <option value="Fiesta en Barco">Fiesta en Barco</option>
                                    <option value="Fiesta en la Playa">Fiesta en la Playa</option>
                                    <option value="Fiesta Gay">Fiesta Gay</option>
                                    <option value="Fiestas Hawaianas">Fiestas Hawaianas</option>
                                    <option value="Fiesta Étnicas">Fiesta Étnicas</option>
                                    <option value="Fiesta Fin de Año">Fiesta Fin de Año</option>
                                    <option value="Fiestas Navideñas">Fiestas Navideñas</option>
                                    <option value="Fiesta para la 3ª Edad">Fiesta para la 3ª Edad</option>
                                    <option value="Fiesta Privadas">Fiesta Privadas</option>
                                    <option value="Fiestas Single">Fiestas Single</option>
                                    <option value="Fiesta Sorpresa">Fiesta Sorpresa</option>
                                    <option value="Fiesta Temáticas">Fiesta Temáticas</option>
                                    <option value="Ectoberfest">Ectoberfest</option>
                                    <option value="Otros">Otros</option>
                                </optgroup>
                            </select>
                            @if ($errors->has('type'))
                                <div class="invalid-feedback">{{ $errors->first('type') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="country"> Ciudad </label>
                        <div class="form-group">
                            <input type="text" id="country" class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" placeholder="Empieza a escribir" value="{{ old('country') }}" required/>
                            @if ($errors->has('country'))
                                <div class="invalid-feedback">{{ $errors->first('country') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="postal_code"> Código Postal </label>
                        <div class="form-group">
                            <input type="text" id="postal_code" class="form-control {{ $errors->has('postal_code') ? ' is-invalid' : '' }}" name="postal_code" value="{{ old('postal_code') }}" required/>
                            @if ($errors->has('postal_code'))
                                <div class="invalid-feedback">{{ $errors->first('postal_code') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <label for="day"> Día del evento </label>
                        <div class="form-group">
                            <input type="text" id="day" class="form-control {{ $errors->has('day') ? ' is-invalid' : '' }} datepicker" name="day" data-required placeholder="Click para elegir" value="{{ old('day') }}" autocomplete="off" required/>
                            @if ($errors->has('day'))
                                <div class="invalid-feedback">{{ $errors->first('day') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="hour">Hora del evento</label>
                        <div class="form-group">
                            <select id="hour" name="hour" data-required="" data-error="Debes especificar la hora aproximada de inicio del evento" class="form-control">
                                <option value="" selected disabled>Hora</option>
                                <option value="00:00">0:00</option>
                                <option value="00:30">0:30</option>
                                <option value="01:00">1:00</option>
                                <option value="01:30">1:30</option>
                                <option value="01:30">2:00</option>
                                <option value="02:00">2:30</option>
                                <option value="03:00">3:00</option>
                                <option value="03:30">3:30</option>
                                <option value="04:00">4:00</option>
                                <option value="04:30">4:30</option>
                                <option value="05:00">5:00</option>
                                <option value="05:30">5:30</option>
                                <option value="06:00">6:00</option>
                                <option value="06:30">6:30</option>
                                <option value="07:00">7:00</option>
                                <option value="07:30">7:30</option>
                                <option value="08:00">8:00</option>
                                <option value="08:30">8:30</option>
                                <option value="09:00">9:00</option>
                                <option value="09:30">9:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                                <option value="13:00">13:00</option>
                                <option value="13:30">13:30</option>
                                <option value="14:00">14:00</option>
                                <option value="14:30">14:30</option>
                                <option value="15:00">15:00</option>
                                <option value="15:30">15:30</option>
                                <option value="16:00">16:00</option>
                                <option value="16:30">16:30</option>
                                <option value="17:00">17:00</option>
                                <option value="17:30">17:30</option>
                                <option value="18:00">18:00</option>
                                <option value="18:30">18:30</option>
                                <option value="19:00">19:00</option>
                                <option value="19:30">19:30</option>
                                <option value="20:00">20:00</option>
                                <option value="20:30">20:30</option>
                                <option value="21:00">21:00</option>
                                <option value="21:30">21:30</option>
                                <option value="22:00">22:00</option>
                                <option value="22:30">22:30</option>
                                <option value="23:00">23:00</option>
                                <option value="23:30">23:30</option>
                            </select>
                            @if ($errors->has('hour'))
                                <div class="invalid-feedback">{{ $errors->first('hour') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="duration">Duración del evento</label>
                        <div class="form-group">
                            <select id="duration" name="duration" class="form-control {{ $errors->has('duration') ? ' is-invalid' : '' }}" required>
                                <option value="" selected disabled>Duración</option>
                                <option value="No lo sé">No lo sé</option>
                                <option value="30 minutos">30 minutos</option>
                                <option value="45 minutos">45 minutos</option>
                                <option value="1 hora">1 hora</option>
                                <option value="2 horas">2 horas</option>
                                <option value="3 horas">3 horas</option>
                                <option value="4 horas">4 horas</option>
                                <option value="5 horas">5 horas</option>
                                <option value="6 horas">6 horas</option>
                                <option value="7 horas">7 horas</option>
                                <option value="8 horas">8 horas</option>
                            </select>
                            @if ($errors->has('duration'))
                                <div class="invalid-feedback">{{ $errors->first('duration') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="budget">Presupuesto máximo</label>
                        <div class="form-group">
                            <input type="text" id="budget" maxlength="75" name="budget" class="form-control {{ $errors->has('budget') ? ' is-invalid' : '' }}" placeholder="Opcional" autocomplete="off"/>
                            @if ($errors->has('budget'))
                                <div class="invalid-feedback">{{ $errors->first('budget') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="place">Lugar</label>
                        <div class="form-group">
                            <input type="text" id="place" maxlength="75" name="place" class="form-control {{ $errors->has('place') ? ' is-invalid' : '' }}" placeholder="Opcional"/>
                            @if ($errors->has('place'))
                                <div class="invalid-feedback">{{ $errors->first('place') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <label for="detail">Detalles de tu fiesta o evento</label>
                        <div class="form-group">
                            <textarea id="detail" rows="3" cols="30" name="detail" class="form-control {{ $errors->has('detail') ? ' is-invalid' : '' }}" placeholder="Por favor, cuéntanos todo lo que necesitas para tu fiesta o evento" required>{{ old('detail') }}</textarea>
                            @if ($errors->has('detail'))
                                <div class="invalid-feedback">{{ $errors->first('detail') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                @guest
                <h4 class="card-title">
                    ¿QUIÉN ERES?
                    <a href="{{ route('login') }}" class="float-right">¿Ya estás registrado?</a>
                </h4>
                <div class="row">
                    <div class="col-md">
                        <label for="role">Yo soy</label>
                        <div class="form-group">
                            <select id="role" name="role" class="form-control {{ $errors->has('role') ? ' is-invalid' : '' }}" required>
                                <option value="" selected disabled>Selecciona</option>
                                <option value="user" {{ old('role') == 3 ? 'selected' : '' }}>Usuario Particular</option>
                                <option value="artist" {{ old('role') == 'artist' ? 'selected' : '' }}>Artista</option>
                                <option value="company" {{ old('role') == 'company' ? 'selected' : '' }}>Empresa</option>
                            </select>
                            @if ($errors->has('role'))
                                <div class="invalid-feedback">{{ $errors->first('role') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="name">Nombres</label>
                        <div class="form-group">
                            <input type="text" maxlength="45" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" required/>
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="surname">Apellidos</label>
                        <div class="form-group">
                            <input type="text" maxlength="45" name="surname" class="form-control {{ $errors->has('surname') ? ' is-invalid' : '' }}" value="{{ old('surname') }}" required/>
                            @if ($errors->has('surname'))
                                <div class="invalid-feedback">{{ $errors->first('surname') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <label for="email">Tu e-mail</label>
                        <div class="form-group">
                            <input type="email" id="email" maxlength="45" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required/>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="email_confirmation">Confirma tu e-mail</label>
                        <div class="form-group">
                            <input type="email" id="email_confirmation" maxlength="45" name="email_confirmation" class="form-control {{ $errors->has('email_confirmation') ? ' is-invalid' : '' }}" value="{{ old('email_confirmation') }}" required/>
                            @if ($errors->has('email_confirmation'))
                                <div class="invalid-feedback">{{ $errors->first('email_confirmation') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="phone">Teléfono</label>
                        <div class="form-group">
                            <input type="text" id="phone" maxlength="16" name="phone" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" />
                            @if ($errors->has('phone'))
                                <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <label for="password">Tu contraseña</label>
                        <div class="form-group">
                            <input type="password" id="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ old('password') }}" required/>
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="password_confirmation">Repite tu contraseña</label>
                        <div class="form-group">
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" value="{{ old('password_confirmation') }}" required/>
                            @if ($errors->has('password_confirmation'))
                                <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md">
                        <label for="company">Empresa</label>
                        <div class="form-group">
                            <input type="text" id="company" maxlength="75" name="company" class="form-control {{ $errors->has('company') ? ' is-invalid' : '' }}" value="{{ old('company') }}" placeholder="Opcional"/>
                            @if ($errors->has('company'))
                                <div class="invalid-feedback">{{ $errors->first('company') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                @endguest
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <div class="checkbox">    
                                <label>
                                    <input type="checkbox" name="politics" required> He leído y acepto el aviso legal y la politica de privacidad
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">
                                Continuar
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <div class="p-2">
                                        <div class="font-weight-bold">
                                            Responsable: <span class="font-weight-light">Francisco Carvajal Pérez</span>
                                        </div>
                                        <div class="font-weight-bold">
                                            Finalidad: <span class="font-weight-light">Responder solicitudes y dar respuesta a las dudas relativas al funcionamiento del sitio web</span>
                                        </div>
                                        <div class="font-weight-bold">
                                            Legitimización: <span class="font-weight-light">Consentimiento del interesado</span>
                                        </div>
                                        <div class="font-weight-bold">
                                            Destinatarios: <span class="font-weight-light">Se cederán datos a terceros por obligación legal y para la ejecución del servicio que nos has solicitado</span>
                                        </div>
                                        <div class="font-weight-bold">
                                            Derechos: <span class="font-weight-light">Acceder, rectificar y suprimir los datos, asi como otros derechos, como se explica en la información adicional</span>
                                        </div>
                                        <div class="font-weight-bold">
                                            Información adicional: <span class="font-weight-light">Consulta la información completa en la <a href="">politica de privacidad.</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        	
        </div>
        <div class="col-md-3">
        	<div class="card-body">
        		<p class="font-weight-bold">
        			1.- Cuéntanos qué estás organizando
        		</p>
        		<p class="font-weight-bold">
        			2.- Recibe propuestas GRATIS
        		</p>
        		<p class="font-weight-bold">
        			2.- Compara precios, elige al mejor y consulta...
        		</p>
        		<p>
        			<a href="{{ url('contact/us') }}">Contacta con nostros</a>
        			<a href="https://web.whatsapp.com/send?phone=34633774008&text=">
        				Whatsapp
        			</a>
        		</p>
        		<p>
                    <span class="text-primary d-block"><i class="fa fa-phone-square fa-2x"></i> (+ 34) 633 77 40 08</span>
                    <span class="text-primary d-block"><i class="fa fa-phone-square fa-2x"></i> (+ 34) 622 94 76 77</span>
                    <span class="text-primary d-block"><i class="fa fa-phone-square fa-2x"></i> (+ 34) 952 07 31 57</span>
                </p>
        	</div>
        </div>
    </div>
</div>

@endsection
