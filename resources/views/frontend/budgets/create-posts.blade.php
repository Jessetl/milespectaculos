@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">¿FALTA DE TIEMPO PARA CREAR TU ANUNCIO?</h5>
        </header>
        <div class="col-md-9">
        	
        	@include('frontend.session.session')
        	
        	{!! Form::open(['route' => ['budgets.storePost'], 'method' => 'POST', 'autocomplete' => 'off', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}

            <div class="card border">
                <div class="card-body">
                    <div class="card-title">
                        <h6 class="text-primary">
                            Selecciona en que Categoría y Subcategoría deseas aparecer
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="category">Categoría</label>
                            <div class="form-group">
                                <select id="category" name="category" class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}" required>
                                    <option value="" selected disabled>Seleccione</option>
                                    
                                    @foreach($categories as $key => $category)

                                    <option value="{{ $category->id }}">{{ $category->name }}</option>

                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                    <div class="invalid-feedback">{{ $errors->first('category') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md">
                            <label for="subcategory">Subcategoría</label>
                            <div class="form-group">
                                <select id="subcategory" name="subcategory" class="form-control {{ $errors->has('subcategory') ? ' is-invalid' : '' }}" required>
                                    <option value="" selected disabled>Selecione</option>
                                </select>
                                @if ($errors->has('subcategory'))
                                    <div class="invalid-feedback">{{ $errors->first('subcategory') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <div id="well" class="well" style="max-height: 300px;overflow: auto; display: none;">
                                    <ul class="list-group checked-list-box">
                                        @foreach($districts as $key => $district)

                                            <li class="list-group-item">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="district" value="{{ $district->id }}">
                                                    <label class="form-check-label" for="defaultCheck1">
                                                        {{ $district->name }}
                                                    </label>
                                                </div>
                                            </li>

                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md" id="ajax__dt"></div>
                    </div>

                    <hr>

                    <div class="card-title">
                        <h6 class="text-primary">
                            Seleccione la foto que desea que aparezca en su portada.
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="file">Portada</label>
                            <div class="form-group">
                                <input id="file" type="file" class="form-control-file {{ $errors->has('file') ? ' is-invalid' : '' }}" name="file" value="{{ old('file') }}"/>
                                @if ($errors->has('file'))
                                    <div class="invalid-feedback">{{ $errors->first('file') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-title">
                        <h6 class="text-primary">
                            Rellene sus datos de contacto y detalles del servicio que ofrece
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="title">Título <span class="text-danger">(*)</span></label>
                                <input type="text" id="title" name="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }} popover-dismiss" 
                                value="{{ old('title') }}" data-toggle="popover" data-placement="top" data-content="Agrega un titulo que facilite la búsqueda de tu anuncio." maxlength="75" required/>
                                @if ($errors->has('title'))
                                    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="content">Descripción <span class="text-danger">(*)</span></label>
                                <textarea id="content" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }} popover-dismiss" name="content" rows="6" data-toggle="popover" data-placement="top" data-content="Describe tu oferta de la manera mas clara y exacta posible." required>{{ old('content') }}</textarea>
                                @if ($errors->has('content'))
                                    <div class="invalid-feedback">{{ $errors->first('content') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="one">Teléfono 1 <span class="text-danger">(*)</span></label>
                            <div class="form-group">
                                <input type="text" id="one" class="form-control {{ $errors->has('one') ? ' is-invalid' : '' }} popover-dismiss" name="one" data-toggle="popover" data-placement="top" data-content="Escribe un telefóno de contacto sin espacios, guiones ni puntos. ejemplo: 916341234" 
                                value="{{ old('one') }}" maxlength="15" required/>
                                @if ($errors->has('one'))
                                    <div class="invalid-feedback">{{ $errors->first('one') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col">
                            <label for="two">Teléfono 2</label>
                            <div class="form-group">
                                <input type="text" id="two" class="form-control {{ $errors->has('two') ? ' is-invalid' : '' }}" 
                                name="tow" value="{{ old('two') }}" maxlength="15"/>
                                @if ($errors->has('two'))
                                    <div class="invalid-feedback">{{ $errors->first('two') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="email">E-mail <span class="text-danger">(*)</span></label>
                            <div class="form-group">
                                <input type="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} popover-dismiss" name="email" data-toggle="popover" data-placement="top" data-content="Escribe un E-Mail valido con el formato: ejemplo@ejemplo.com" value="{{ old('email') }}" maxlength="75" required/>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col">
                            <label for="url">Sitio Web</label>
                            <div class="form-group">
                                <input type="text" class="form-control {{ $errors->has('url') ? ' is-invalid' : '' }} popover-dismiss" name="url" data-toggle="popover" data-placement="top" data-content="Si posees página Web, escribe aquí la URL de la página, Ejemplo: www.milespectaculos.com" value="{{ old('url') }}"/>
                                @if ($errors->has('url'))
                                    <div class="invalid-feedback">{{ $errors->first('url') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="address">Lugar</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control" id="address" name="directions" value="{{ old('directions') }}" type="text"/>
                                    <span class="input-group-append">
                                        <input class="btn btn-primary" id="look" type="button" value="BUSCAR">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">  
                            <div class="form-group">  
                                <div id="map" style="height:400px;"></div>
                                <input id="id_lat" type="text" style="display:none;" name="latInput" value="{{ old('latInput') }}"/>
                                <input id="id_lng" type="text" style="display:none;" name="lngInput" value="{{ old('lngInput') }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="card-title">
                        <h6 class="text-primary">
                            Cargue las imágenes que desea que aparezca en su anuncio.
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#uploadImage">CARGAR IMÁGENES</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-title">
                        <h6 class="text-primary">
                            Cargue los enlaces de videos de YouTube que desea que aparezcan en su anuncio
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#uploadVideo">CARGAR VIDEOS</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-title">
                        <h6 class="text-primary">
                            Ponga precio a su servicio o seleccione entre; a convenir, a consultar o otros.
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="type_amount">Precio del servicio <span class="text-danger">(*)</span></label>
                            <div class="form-group">
                                <select id="type_amount" class="form-control {{ $errors->has('type_amount') ? ' is-invalid' : '' }} popover-dismiss" name="type_amount" required>
                                    <option value="A Convenir">A CONVENIR</option>
                                    <option value="A Consultar">A CONSULTAR</option>
                                    <option value="Otro">OTRO</option>
                                </select>
                                @if ($errors->has('type_amount'))
                                    <div class="invalid-feedback">{{ $errors->first('type_amount') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md">
                            <label for="amount">¿Cuánto quieres que se enumere?</label>
                            <div class="form-group">
                                <input type="number" id="amount" class="form-control {{ $errors->has('amount') ? ' is-invalid' : '' }} popover-dismiss" step=".01" name="amount" value="{{ old('amount') }}" data-toggle="popover" data-placement="top" data-content="Coloca una suma aproximada del costo de tu servicio, agrega solo dos decimales en caso de necesitarlo. Ejemplo: 1485,02" disabled required/>
                                @if ($errors->has('amount'))
                                    <div class="invalid-feedback">{{ $errors->first('amount') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md">
                            <label for="discount">Descuento Especial <span class="text-danger">%</span></label>
                            <div class="form-group">
                                <input type="number" id="discount" class="form-control {{ $errors->has('discount') ? ' is-invalid' : '' }} popover-dismiss" name="discount" data-toggle="popover" data-placement="top" data-content="Descuento especial para profesionales" value="{{ old('discount') }}"/>
                                @if ($errors->has('discount'))
                                    <div class="invalid-feedback">{{ $errors->first('discount') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-title">
                        <h6 class="text-primary">
                            Seleccione todos sus enlaces de MP3 de SoundCloud que desee que aparezcan
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#uploadAudios">CARGAR AUDIOS</button>
                            </div>
                        </div>
                    </div>  
                    <div class="card-title">
                        <h6 class="text-primary">
                            Añada sus proximos Eventos si lo desea
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#registerEvents">AÑADIR UN EVENTO</button>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="registerEvents" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <Events :districts="{{ $districts }}"></Events>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <span class="text-primary float-right">Su anuncio se publicara antes de 24 hrs</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-check"> 
                                <div class="form-group">  
                                    <input type="checkbox" class="form-check-input position-static" name="politics" required/> <span class="font-weight-light"> He leído y acepto el <a href="{{ url('legal#aviso') }}"> aviso legal </a> y la <a href="{{ url('legal#politica') }}"> política de privacidad </a> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer pl-3">
                    Los campos con <span class="text-danger">(*)</span> son obligatorios
                </div>
            </div>

            {!! Form::close() !!}

        </div>
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

@include('frontend.Modals.uploadFiles')
@include('frontend.Modals.uploadVideos')
@include('frontend.Modals.uploadAudios')
@include('frontend.Modals.uploadEvents')

@endsection

@section('scripts')

<script type="text/javascript">
    
Dropzone.options.myUploadFiles  = {

    dictDefaultMessage: 'PINCHE AQUÍ PARA SUBIR ARCHIVOS',
    dictRemoveFile: 'Remover',
    dictMaxFilesExceeded: 'No puedes subir más archivos.',

    maxFilesize: 5,
    addRemoveLinks: true,
    acceptedFiles: ".jpg,.png",

    error: function(file, message) {
        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.message);
    }
};

</script>

<!-- Google Maps -->
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbapNm5FNXk2Db4pJ47Fr7lKXEsd8jtsA&callback=initMap">
</script>

<script>

var map;
var marker;
var coords;

function initMap() {
        
    var myLatlng = new google.maps.LatLng(-25.363882,131.044922);

    var mapOptions = {
        zoom: 12,
        center: myLatlng
    }

    var geocoder = new google.maps.Geocoder();

    document.getElementById('look').addEventListener('click', function() {
        geocodeAddress(geocoder, map);
    });
          
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Drag me!"
    });
        
    marker.addListener('click', function(){
        placeMarker(marker.getPosition());
    });

    marker.addListener('drag', handleEvent);
    marker.addListener('dragend', handleEvent);

}

function handleEvent(event) {

    document.getElementById('id_lat').value = event.latLng.lat();
    document.getElementById('id_lng').value = event.latLng.lng();
}

function placeMarker(cords) {
    
    marker.setPosition(cords);
    document.getElementById("id_lat").value = cords.lat();
    document.getElementById("id_lng").value = cords.lng();
}

function geocodeAddress(geocoder, resultsMap) {

    var address = document.getElementById('address').value;
    
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            placeMarker(results[0].geometry.location);
            
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
    });
}
    
</script>

@endsection