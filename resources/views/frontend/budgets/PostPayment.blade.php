@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">DATOS DE PAGO</h5>
        </header>
        <div class="col-md-9">
        	
        	@include('frontend.session.session')

        	<div class="card-body">
        		<p class="text-justify">
        			Si por falta de tiempo no puedes crear tus propios anuncios te lo creamos nosotros por un módico
					precio con nuestro equipo de profesionales
				</p>
				<p class="text-justify">
					Le insertamos las características de su servicio, fotos, videos, mp3, próximos eventos, datos de
					contacto en cada una de las categorías y provincias que desee a nivel nacional, para ello solo tiene
					que rellenar este cuestionario y mandarnos, tus fotos, enlaces de videos de youtube y mp3 de
					sonudcloud, las fechas y lugares de tus próximos eventos, a exponer por cada anuncio, categoría
					donde deseas aparecer. Y nosotros te creamos cada anuncio por tan solo <h5 class="text-danger"> 5 € + 21% de IVA = 6,05 € </h5>
        		</p>
        		<p>
        			<a href="{{ route('payment.budgetPost', $post) }}" class="btn btn-primary">Aceptar</a>
        		</p>
            </div>
        </div>
    </div>
</div>

@endsection