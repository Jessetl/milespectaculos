@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')


<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ESCRIBE TU SUGERENCIA.</h5>
        </header>

        
        <div class="col-md-8">

        @include('frontend.session.session')
        
        {!! Form::open(['route' => 'mailbox.store', 'method' => 'POST']) !!}

            <div class="card border">
                <div class="card-header">
                    Formulario 
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="nombre"> Nombre <span class="text-danger">(*)</span></label>
                            <input type="text" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} popover-dismiss" name="name" data-toggle="popover" data-placement="top" data-content="Escribe tu nombre completo" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    
                        <div class="form-group col-md-6">
                            <label for="email"> Correo electrónico <span class="text-danger">(*)</span></label>
                            <input type="text" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} popover-dismiss" name="email" data-toggle="popover" data-placement="top" data-content="Escribe tu Correo electronico" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="suggest"> Sugerencia <span class="text-danger">(*)</span></label>
                            <textarea id="suggest" class="form-control {{ $errors->has('suggest') ? ' is-invalid' : '' }} popover-dismiss" name="suggest" rows="4" data-toggle="popover" data-placement="top" data-content="Escribe tu sugerencia detalladamente." required>{{ old('suggest') }}
                            </textarea>
                            @if ($errors->has('suggest'))
                                <div class="invalid-feedback">{{ $errors->first('suggest') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Enviar">
                        </div>
                    </div>
                </div>
                <div class="card-footer p-2">
                    <span>Los campos marcados con <span class="text-danger">(*)</span> son obligatorios.</span>
                </div>
            </div>

        {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

