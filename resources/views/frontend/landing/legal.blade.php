@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">AVISO LEGAL, CONDICIONES DE USO Y POLITICA DE PRIVACIDAD Y COOKIES</h5>
        </header>
        <div class="col">
            <div class="card">
                <div class="card-body text-justify">
                    <p align="center" style="font-variant: normal; letter-spacing: normal; font-style: normal">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><u><b>AVISO
                        LEGAL, CONDICIONES DE USO Y POLITICA DE PRIVACIDAD Y COOKIES</b></u></font></font></font></p>
                        <h2 class="western"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><span style="letter-spacing: normal"><span style="font-style: normal"><b id="#aviso">Aviso</b></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><span style="letter-spacing: normal"><span style="font-style: normal"><b>
                        </b></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><span style="letter-spacing: normal"><span style="font-style: normal"> <b> L</b></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><span style="letter-spacing: normal"><span style="font-style: normal"><b>egal
                        de </b></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><span style="letter-spacing: normal"><span style="font-style: normal"><b>Mil
                        Espect&aacute;culos </b></span></span></font></font></font></span>
                        </h2>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">1.
                        DATOS IDENTIFICATIVOS: En cumplimiento con el deber de informaci&oacute;n
                        recogido en art&iacute;culo 10 de la Ley 34/2002, de 11 de julio, de
                        Servicios de la Sociedad de la Informaci&oacute;n y del Comercio
                        Electr&oacute;nico, a continuaci&oacute;n, se reflejan los siguientes
                        datos: el titular del dominio web es Francisco Carvajal P&eacute;rez
                        (en adelante MILESPECTACULOS)  con domicilio a estos efectos en
                        Camino de los Carabineros, 37 29004 M&Aacute;LAGA con C.I.F.:
                        33354508 - T Correo electr&oacute;nico de contacto:
                        info@milespectaculos.com del sitio web. </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">2.
                        USUARIOS: El acceso y/o uso de este portal de MILESPECTACULOS
                        propietaria del sitio web atribuye la condici&oacute;n de USUARIO,
                        que acepta, desde dicho acceso y/o uso, las Condiciones Generales de
                        Uso aqu&iacute; reflejadas. Las citadas Condiciones ser&aacute;n de
                        aplicaci&oacute;n independientemente de las Condiciones Generales de
                        Contrataci&oacute;n que en su caso resulten de obligado cumplimiento.
                        </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">3.
                        USO DEL PORTAL: http://www.milespectaculos.com proporciona el acceso
                        a multitud de informaciones, servicios, programas o datos (en
                        adelante, &quot;los contenidos&quot;) en Internet pertenecientes a
                        MILESPECTACULOS o a sus licenciantes a los que el USUARIO pueda tener
                        acceso. El USUARIO asume la responsabilidad del uso del portal. </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%"><a name="_GoBack"></a>
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">4.
                        PROTECCI&Oacute;N DE DATOS: MILESPECTACULOS cumple con las
                        directrices del Reglamento Europeo de Protecci&oacute;n de Datos
                        2016/679 del 27 de abril de 2016 y dem&aacute;s normativa que
                        garantiza un correcto uso y tratamiento de los datos personales del
                        usuario. Puede ejercer sus derechos sus derechos de acceso,
                        rectificaci&oacute;n, supresi&oacute;n, limitaci&oacute;n del
                        tratamiento, portabilidad u oposici&oacute;n, mediante correo
                        electr&oacute;nico a &ldquo;info@milespectaculos.com&rdquo;, la
                        finalidad del tratamiento ser&aacute; contactar con los usuarios para
                        facilitarle informaci&oacute;n de los servicios ofrecidos por la
                        empresa, as&iacute; como su tratamiento como clientes. Mediante la
                        presente cl&aacute;usula le informamos que puede realizar cualquier
                        tipo de reclamaci&oacute;n ante la autoridad de control, la Agencia
                        Espa&ntilde;ola de Protecci&oacute;n de Datos. Asimismo,
                        MILESPECTACULOS informa que conservar&aacute; sus datos personales
                        siempre que la finalidad de la cesi&oacute;n siga vigente y mientras
                        el usuario no haya indicado lo contrario.</font></font></font></p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">5.
                        PROPIEDAD INTELECTUAL E INDUSTRIAL MILESPECTACULOS por s&iacute; o
                        como cesionaria, es titular de todos los derechos de propiedad
                        intelectual e industrial de su p&aacute;gina web, as&iacute; como de
                        los elementos contenidos en la misma (a t&iacute;tulo enunciativo,
                        im&aacute;genes, sonido, audio, v&iacute;deo, software o textos;
                        marcas o logotipos, combinaciones de colores, estructura y dise&ntilde;o,
                        selecci&oacute;n de materiales usados, programas de ordenador
                        necesarios para su funcionamiento, acceso y uso, etc.), titularidad
                        de MILESPECTACULOS o bien de sus licenciantes. Todos los derechos
                        reservados. En virtud de lo dispuesto en los art&iacute;culos 8 y
                        32.1, p&aacute;rrafo segundo, de la Ley de Propiedad Intelectual,
                        quedan expresamente prohibidas la reproducci&oacute;n, la
                        distribuci&oacute;n y la comunicaci&oacute;n p&uacute;blica, incluida
                        su modalidad de puesta a disposici&oacute;n, de la totalidad o parte
                        de los contenidos de esta p&aacute;gina web, con fines comerciales,
                        en cualquier soporte y por cualquier medio t&eacute;cnico, sin la
                        autorizaci&oacute;n de MILESPECTACULOS El USUARIO se compromete a
                        respetar los derechos de Propiedad Intelectual e Industrial
                        titularidad de MILESPECTACULOS Podr&aacute; visualizar los elementos
                        del portal e incluso imprimirlos, copiarlos y almacenarlos en el
                        disco duro de su ordenador o en cualquier otro soporte f&iacute;sico
                        siempre y cuando sea, &uacute;nica y exclusivamente, para su uso
                        personal y privado. El USUARIO deber&aacute; abstenerse de suprimir,
                        alterar, eludir o manipular cualquier dispositivo de protecci&oacute;n
                        o sistema de seguridad que estuviera instalado en las p&aacute;ginas
                        de MILESPECTACULOS.</font></font></font></p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"> </font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">6.
                        EXCLUSI&Oacute;N DE GARANT&Iacute;AS Y RESPONSABILIDAD:
                        MILESPECTACULOS no se hace responsable, en ning&uacute;n caso, de los
                        da&ntilde;os y perjuicios de cualquier naturaleza que pudieran
                        ocasionar, a t&iacute;tulo enunciativo: errores u omisiones en los
                        contenidos, falta de disponibilidad del portal o la transmisi&oacute;n
                        de virus o programas maliciosos o lesivos en los contenidos, a pesar
                        de haber adoptado todas las medidas tecnol&oacute;gicas necesarias
                        para evitarlo. </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">7.
                        MODIFICACIONES: MILESPECTACULOS se reserva el derecho de efectuar sin
                        previo aviso las modificaciones que considere oportunas en su portal,
                        pudiendo cambiar, suprimir o a&ntilde;adir tanto los contenidos y
                        servicios que se presten a trav&eacute;s de la misma como la forma en
                        la que &eacute;stos aparezcan presentados o localizados en su portal.
                        </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">8.
                        ENLACES: En el caso de que en nombre del dominio se dispusiesen
                        enlaces o hiperv&iacute;nculos hac&iacute;a otros sitios de Internet,
                        MILESPECTACULOS no ejercer&aacute; ning&uacute;n tipo de control
                        sobre dichos sitios y contenidos. En ning&uacute;n caso
                        MILESPECTACULOS asumir&aacute; responsabilidad alguna por los
                        contenidos de alg&uacute;n enlace perteneciente a un sitio web ajeno,
                        ni garantizar&aacute; la disponibilidad t&eacute;cnica, calidad,
                        fiabilidad, exactitud, amplitud, veracidad, validez y
                        constitucionalidad de cualquier material o informaci&oacute;n
                        contenida en ninguno de dichos hiperv&iacute;nculos u otros sitios de
                        Internet. Igualmente, la inclusi&oacute;n de estas conexiones
                        externas no implicar&aacute; ning&uacute;n tipo de asociaci&oacute;n,
                        fusi&oacute;n o participaci&oacute;n con las entidades conectadas. </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">9.
                        DERECHO DE EXCLUSI&Oacute;N: MILESPECTACULOS se reserva el derecho a
                        denegar o retirar el acceso a portal y/o los servicios ofrecidos sin
                        necesidad de preaviso, a instancia propia o de un tercero, a aquellos
                        usuarios que incumplan las presentes Condiciones Generales de Uso. </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">10.
                        GENERALIDADES: MILESPECTACULOS perseguir&aacute; el incumplimiento de
                        las presentes condiciones, as&iacute; como cualquier utilizaci&oacute;n
                        indebida de su portal ejerciendo todas las acciones civiles y penales
                        que le puedan corresponder en derecho. </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <br/>

                        </p>
                        <p align="left" style="margin-bottom: 0cm; font-weight: normal; line-height: 100%">
                        <font color="#666666"><font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">11.
                        MODIFICACI&Oacute;N DE LAS PRESENTES CONDICIONES Y DURACI&Oacute;N:
                        MILESPECTACULOS podr&aacute; modificar en cualquier momento las
                        condiciones aqu&iacute; determinadas, siendo debidamente publicadas
                        como aqu&iacute; aparecen. La vigencia de las citadas condiciones ir&aacute;
                        en funci&oacute;n de su exposici&oacute;n y estar&aacute;n vigentes
                        hasta que sean modificadas por otras debidamente publicadas. </font></font></font>
                        </p>
                        <p align="left" style="margin-bottom: 0cm; line-height: 100%"> 
                        </p>
                        <p><font color="#666666"> <font face="Raleway, arial, sans-seriff, sans-serif"><font size="2" style="font-size: 10pt">12.
                        LEGISLACI&Oacute;N APLICABLE Y JURISDICCI&Oacute;N: La relaci&oacute;n
                        entre MILESPECTACULOS y el USUARIO se regir&aacute; por la normativa
                        espa&ntilde;ola vigente y cualquier controversia se someter&aacute; a
                        los Juzgados y tribunales de la ciudad de M&aacute;laga. </font></font></font>
                        </p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2"><a name="terminos-de-uso"></a>
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>T&eacute;rminos
                        de uso de MIL ESPECT&Aacute;CULOS </b></font></font></font>
                        </h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos pone a disposici&oacute;n de <font face="Raleway, arial, sans-seriff, sans-serif">los</font>
                        usuarios de Internet (en adelante EL USUARIO) el Sitio Web
                        milespectaculos.com (en adelante MIL ESPECT&Aacute;CULOS) de forma
                        gratuita.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Los
                        presentes T&eacute;rminos de Uso regulan el acceso y uso de nuestro
                        portal web.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">La
                        utilizaci&oacute;n de este portal Web implica la aceptaci&oacute;n
                        plena de los T&eacute;rminos de Uso, as&iacute; como de nuestras
                        </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Condiciones
                        Legales</span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">
                        </span></span></span></span></font></font></span></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">en
                        el momento en que el Usuario acceda al mismo. </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Mil
                        Espect&aacute;culos</span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">
                        podr&aacute; modificar estos T&eacute;rminos de Uso sin notificaci&oacute;n
                        previa, por tanto, el usuario deber&aacute; leerlos atentamente cada
                        vez que utilice este servicio. Estas condiciones estar&aacute;n
                        plenamente accesibles en el Sitio web a trav&eacute;s del enlace
                        &quot;T&eacute;rminos de Uso&quot;.</span></span></span></font></font></font></span></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Objeto</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">La
                        finalidad de Mil Espect&aacute;culos es la de ser un punto de
                        referencia para los interesados en la contrataci&oacute;n de artistas
                        y espect&aacute;culos as&iacute; como para los profesionales del
                        sector Eventos.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Ofrecemos
                        los siguientes servicios a los Usuarios que se registren a trav&eacute;s
                        de nuestro formulario de alta:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Publicaci&oacute;n
                            de anuncios relacionados con el sector del espect&aacute;culo o
                            Eventos</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Creaci&oacute;n
                            de una ficha art&iacute;stica personal del usuario</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Promoci&oacute;n
                            y difusi&oacute;n de la ficha art&iacute;sticas del usuario a trav&eacute;s
                            de Internet</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Bolsa
                            de trabajo para los usuarios</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Informaci&oacute;n
                            y &uacute;ltimas novedades sobre el sector del espect&aacute;culo y
                            artistas</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Remisi&oacute;n
                            de comunicaciones electr&oacute;nicas promocionales e informativas
                            sobre el sector del espect&aacute;culo y otros sectores de su
                            inter&eacute;s</font></font></font></p>
                        </ul>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Mil
                        </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Espect&aacute;culos</span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">
                        facilitar&aacute; los datos y/o informaci&oacute;n proporcionados por
                        el Usuario a los dem&aacute;s usuarios de la web siempre que dicha
                        informaci&oacute;n no vulnere disposiciones legales o nuestras </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">
                        </span></span></span></span></font></font></span></font></span><span style="font-variant: normal"><font color="#4d4d4d"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Condiciones
                        Legales</span></span></span></span></font></font></span></font></span></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos se esforzar&aacute; al m&aacute;ximo por mantener
                        disponible el Sitio Web en todo momento, pero el USUARIO reconoce que
                        t&eacute;cnicamente es imposible mantener la disponibilidad al 100%.
                        El servicio puede presentar anomal&iacute;as o la suspensi&oacute;n
                        pasajera del mismo por razones de mantenimiento o seguridad, as&iacute;
                        como por causas no imputables a Mil Espect&aacute;culos (cortes de
                        electricidad, anomal&iacute;as en los servicios de comunicaci&oacute;n,
                        etc&hellip;)</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos se reserva el derecho a eliminar del Sitio Web
                        cualquier contenido ilegal sin previo aviso.</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Obligaciones
                        de los Usuarios en Mil Espect&aacute;culos</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Para
                        el acceso a los servicios ofertados en este sitio web, el Usuario
                        debe ser mayor de edad o disponer de la autorizaci&oacute;n de sus
                        padres o tutores legales.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                        Usuario se compromete a usar la plataforma sujet&aacute;ndose a la
                        Ley, a las buenas costumbres y a los presentes T&eacute;rminos de
                        Uso, manteniendo el debido respeto a los dem&aacute;s usuarios.
                        Asimismo, el USUARIO est&aacute; obligado a respetar las leyes
                        aplicables y los derechos de terceros al utilizar los contenidos y
                        servicios de Mil Espect&aacute;culos.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Queda
                        prohibida la reproducci&oacute;n, transmisi&oacute;n, distribuci&oacute;n,
                        adaptaci&oacute;n, o modificaci&oacute;n de los contenidos del Sitio
                        Web (textos, dise&ntilde;os, gr&aacute;ficos, informaciones, bases de
                        datos, archivos de sonido y/o imagen, logos) y cualquier elemento de
                        este Sitio Web, salvo autorizaci&oacute;n previa de los leg&iacute;timos
                        titulares o cuando resulte as&iacute; permitido por la ley.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Al
                        USUARIO le estar&aacute; prohibido: utilizar contenidos injuriosos o
                        calumniosos, utilizar contenidos pornogr&aacute;ficos o que vulneren
                        la leyes de protecci&oacute;n de menores as&iacute; como distribuir
                        productos pornogr&aacute;ficos o que vulneren las leyes de protecci&oacute;n
                        de menores, molestar a otros usuarios utilizando SPAM o de cualquier
                        otra forma, utilizar contenidos protegidos legalmente sin tener
                        derecho a ello, o hacer publicidad, ofrecer o distribuir bienes o
                        servicios protegidos legalmente, as&iacute; como realizar acciones
                        contrarias a la libre competencia, incluidas las encaminadas a la
                        captaci&oacute;n de clientes progresiva.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Algunas
                        funcionalidades o servicios del Sitio Web no exigen la previa
                        suscripci&oacute;n o registro de los usuarios. No obstante, la
                        utilizaci&oacute;n de algunos servicios est&aacute; condicionada a la
                        previa cumplimentaci&oacute;n del registro de usuario, seleccionando
                        el identificador y la contrase&ntilde;a que el usuario se compromete
                        a conservar y a usar con diligencia.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">La
                        contrase&ntilde;a de cada usuario es personal e intransferible. No se
                        permite la cesi&oacute;n de la contrase&ntilde;a a terceros, ni tan
                        siquiera de manera temporal. En este sentido, cada usuario deber&aacute;
                        adoptar las medidas necesarias para la custodia de la contrase&ntilde;a
                        seleccionada, evitando el uso de la misma por terceros. el Usuario es
                        el &uacute;nico responsable de la utilizaci&oacute;n que se haga con
                        su contrase&ntilde;a, con completa indemnidad para Mil Espect&aacute;culos.
                        Si el usuario sospecha que su contrase&ntilde;a est&aacute; siendo
                        utilizada por terceros deber&aacute; comunicarlo a Mil Espect&aacute;culos
                        con la mayor brevedad.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                        Usuario garantiza la autenticidad de todos aquellos datos que
                        comunique como consecuencia de la cumplimentaci&oacute;n de los
                        formularios necesarios para la utilizaci&oacute;n de las
                        funcionalidades. Es responsabilidad del usuario mantener toda la
                        informaci&oacute;n permanentemente actualizada, de forma que responda
                        en cada momento a la situaci&oacute;n real del usuario. En cualquier
                        caso, el Usuario ser&aacute; el &uacute;nico responsable de las
                        manifestaciones falsas o inexactas que realice y de los perjuicios
                        que cause a Mil Espect&aacute;culos o a terceros por la informaci&oacute;n
                        facilitada.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Seg&uacute;n
                        nuestra pol&iacute;tica anti-spamming de Mil Espect&aacute;culos, el
                        Usuario est&aacute; obligado a no recabar y utilizar datos a partir
                        de los datos de otros usuarios a los que se pueda acceder a trav&eacute;s
                        de los contenidos y servicios ofrecidos en Mil Espect&aacute;culos
                        para la realizaci&oacute;n de actividades con fines promocionales o
                        publicitarios as&iacute; como de remitir comunicaciones comerciales
                        de cualquier tipo y a trav&eacute;s de cualquier soporte no
                        solicitado ni previamente consentido por Mil Espect&aacute;culos y/o
                        los interesados.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">El
                        usuario es consciente y acepta voluntariamente, que el servicio tiene
                        lugar, en todo caso, bajo su &uacute;nica responsabilidad. El usuario
                        responder&aacute; de los da&ntilde;os y perjuicios de toda naturaleza
                        que </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Mil
                        </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Espect&aacute;culos</span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">
                        pueda sufrir como consecuencia del incumplimiento intencional o
                        culpablemente de cualquiera de las obligaciones expuestas en estos
                        T&eacute;rminos de Uso, en nuestra </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Informaci&oacute;n
                        legal </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">o
                        dictadas por la ley en relaci&oacute;n con este servicio.</span></span></span></font></font></font></span></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Publicaci&oacute;n
                        de contenidos en Mil Espect&aacute;culos</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                        Usuario se declara titular leg&iacute;timo de los derechos de
                        propiedad intelectual e industrial del contenido para la
                        reproducci&oacute;n, distribuci&oacute;n y comunicaci&oacute;n
                        p&uacute;blica a trav&eacute;s de cualquier medio electr&oacute;nico
                        para todo el mundo y con tiempo ilimitado</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Esta
                        prohibida la publicaci&oacute;n de contenidos que deterioren la
                        calidad del servicio.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Esta
                        prohibida la publicaci&oacute;n de contenidos que:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Sean
                            presuntamente il&iacute;citos con la normativa nacional, comunitaria
                            o internacional o que realicen actividades presuntamente il&iacute;citas
                            o contravengan los principio de la buena fe.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Puedan
                            da&ntilde;ar el buen nombre y reputaci&oacute;n de Mil Espect&aacute;culos.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">No
                            re&uacute;nan los par&aacute;metros de calidad establecidos por Mil
                            Espect&aacute;culos.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Sean
                            enga&ntilde;osos, de dudosa eficacia o que puedan causar perjuicios
                            contra las personas.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Que
                            apoyen o justifiquen el racismo, la violencia y el odio.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Que
                            atenten contra los derechos fundamentales de las personas, puedan
                            buscar la debilidad del usuario, falten a la cortes&iacute;a,
                            molesten o puedan generar opiniones negativas en nuestros usuarios o
                            terceros.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Que
                            contravengan los principios de legalidad, honradez, responsabilidad,
                            protecci&oacute;n de la dignidad humana, protecci&oacute;n de
                            menores, protecci&oacute;n de orden p&uacute;blico, protecci&oacute;n
                            de la vida privada, protecci&oacute;n del consumidor y los derechos
                            de la propiedad intelectual e industrial</font></font></font></p>
                        </ul>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                        Usuario que publique contenidos en Mil Espect&aacute;culos se
                        compromete a cumplir con los requisitos y pautas detallados a
                        continuaci&oacute;n:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Los
                            datos requeridos tanto en el alta de usuario como en la inserci&oacute;n
                            de un anuncio deben ser completados con informaci&oacute;n veraz.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Los
                            espect&aacute;culos ofertados deben ser reales, y la informaci&oacute;n
                            sobre ellos debe ser clara, precisa y tiene que ajustarse a la
                            realidad</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">No
                            se aceptan anuncios de espect&aacute;culos que puedan ser
                            clasificados como &quot;s&oacute;lo para adultos&quot;</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Se
                            debe utilizar un lenguaje profesional en la descripci&oacute;n del
                            espect&aacute;culo.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Est&aacute;
                            prohibida la publicaci&oacute;n de anuncios que puedan deteriorar la
                            calidad de nuestro servicio. Est&aacute; prohibida la inserci&oacute;n
                            de anuncios duplicados, con datos de contacto err&oacute;neos o con
                            un nombre de empresa irreal</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Que
                            atenten contra los derechos fundamentales de las personas, puedan
                            buscar la debilidad del usuario, falten a la cortes&iacute;a,
                            molesten o puedan generar opiniones negativas en nuestros usuarios o
                            terceros.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Si
                            encuentras alg&uacute;n anuncio o contenido inapropiado en </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Mil
                            </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Espect&aacute;culos</span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">,
                            por favor contacta con nosotros a trav&eacute;s de<a href="https://www.milespectaculos.com/contact/us">
                            </a></span></span></span></font></font></font></span><a href="https://www.milespectaculos.com/contact/us"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">nuestro
                            formulario de contacto</span></span></span></span></font></font></span></font></span></a></p>
                        </ul>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos se reserva el derecho a eliminar las fichas
                        art&iacute;sticas de miembros por los siguientes motivos:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Comportamiento
                            inadecuado o poco &eacute;tico que generen quejas por parte de
                            nuestros clientes y/o usuarios.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Uso
                            inadecuado o fraudulento de nuestro sistema de reservas y
                            contrataci&oacute;n de espect&aacute;culos y servicios.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">No
                            cumplir los requisitos m&iacute;nimos de calidad y profesionalidad,
                            tanto en el contenido de las fichas art&iacute;sticas como en los
                            eventos contratados a trav&eacute;s de Mil Espect&aacute;culos.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Es
                            necesario completar todos los campos obligatorios del formulario
                            PUBLICAR ANUNCIO.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Hay
                            que respetar las categor&iacute;as y las secciones (demanda vs.
                            Oferta).</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><font color="#3c3c3c"><font face="Verdana, arial">No
                            est&aacute; permitido publicar un anuncio id&eacute;ntico o similar
                            a otro ya publicado </font></font><font color="#3c3c3c"><font face="Verdana, arial">en
                            la misma provincia</font></font><font color="#3c3c3c"><font face="Verdana, arial">.</font></font></font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido utilizar correos ficticios para publicar un
                            anuncio.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido escribir el anuncio en may&uacute;sculas.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido publicar servicios de publicaci&oacute;n de
                            anuncios.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido anunciar algo gen&eacute;rico.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            esta permitido utilizar palabras clave no relacionados al anuncio
                            (&ldquo;Keyword Spamming&rdquo;).</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><font color="#3c3c3c"><font face="Verdana, arial">No
                            est&aacute; permitido utilizar sistemas o herramientas para la
                            publicaci&oacute;n o renovaci&oacute;n masiva y/o autom&aacute;tica
                            de anuncios, sin el previo consentimiento escrito por Mil
                            </font></font><font color="#3c3c3c"><font face="Verdana, arial">Espect&aacute;culos.</font></font></font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido copiar, procesar o distribuir el texto y/o las
                            fotos o im&aacute;genes de terceros sin su previa autorizaci&oacute;n.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido incluir el n&uacute;mero de tel&eacute;fono o
                            el correo electr&oacute;nico fuera de los campos expl&iacute;citamente
                            previstos para incluir esta informaci&oacute;n o URL en el campo
                            nombre del formulario PUBLICAR ANUNCIO.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido publicar anuncios con contenido er&oacute;tico,
                            sexual o pornogr&aacute;fico.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Solo
                            se puede renovar un anuncio de forma gratuita una vez cada 24h.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitida la publicaci&oacute;n de n&uacute;meros de
                            tarificaci&oacute;n especial (905, 806, etc) en ninguna categor&iacute;a
                            </font></font></font>
                            </p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><font color="#3c3c3c"><font face="Verdana, arial">En
                            anuncios v&aacute;lidos para toda Espa&ntilde;a, se tiene que
                            indicar la localidad en la que se encuentra el </font></font><font color="#3c3c3c"><font face="Verdana, arial">Artista
                            o Empresa del sector Eventos.</font></font></font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido anunciar servicios de videncia </font></font></font>
                            </p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">No
                            est&aacute; permitido poner un precio ficticio para mejorar las
                            posiciones en las b&uacute;squedas.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><font color="#3c3c3c"><font face="Verdana, arial">No
                            est&aacute; permitido publicar anuncios que promocionen negocios que
                            de alg&uacute;n modo puedan ser competidores de </font></font><font color="#3c3c3c"><font face="Verdana, arial">M</font></font><font color="#3c3c3c"><font face="Verdana, arial">il
                            </font></font><font color="#3c3c3c"><font face="Verdana, arial">Espect&aacute;culos.</font></font></font></font></font></p>
                        </ul>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 100%; orphans: 2; widows: 2">
                        <br/>
                        <br/>

                        </p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <br/>
                        <br/>

                        </p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">La
                        eliminaci&oacute;n de una cuenta por incumplimiento de cualquiera de
                        nuestros t&eacute;rminos de uso implica la no devoluci&oacute;n de
                        los importes abonados en caso de tratarse de una cuenta de pago.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">La
                        determinaci&oacute;n de cualquiera de estos supuestos es
                        responsabilidad exclusiva de Mil Espect&aacute;culos atendiendo a su
                        buen juicio a las demandas de terceras personas o instituciones</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                        incumplimiento de dicha norma puede significar la cancelaci&oacute;n
                        inmediata de la cuenta de usuario y la destrucci&oacute;n de todos
                        los contenidos asociados al mismo sin posibilidad de recuperaci&oacute;n
                        de los mismos</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Hacemos
                        revisiones peri&oacute;dicas de los anuncios y demandas para asegurar
                        que nuestros T&eacute;rminos de Uso se cumplan pero no podemos
                        controlar todos y cada uno de los anuncios que se publican, por tanto
                        no podemos asumir la responsabilidad de los contenidos.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Asimismo,
                        Mil Espect&aacute;culos se reserva la potestad de retirar del sitio
                        web aquellos contenidos que se consideren no apropiados a las
                        caracter&iacute;sticas y finalidades de Milespectaculos.com</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cualquier
                        Usuario que inserte un contenido que contravenga la legalidad vigente
                        asumir&aacute; la responsabilidad exclusiva de los perjuicios y
                        consecuencias derivadas de la misma, eximiendo a Mil Espect&aacute;culos
                        de cualquier responsabilidad.</font></font></font></p>
                        <ol>
                            <ol>
                                <ul>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Todos
                                    los precios incluyen impuestos indirectos.</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Los
                                    anuncios no son reembolsables. Una vez realizado el pago, &eacute;ste
                                    se cobrar&aacute; &iacute;ntegramente sin opci&oacute;n a
                                    devoluci&oacute;n.</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Los
                                    anuncios adquiridos no son transferibles. Los anuncios se asocian
                                    a la cuenta de correo electr&oacute;nico que hay que indicar a la
                                    hora de comprar los cr&eacute;ditos. Los anuncios no se pueden
                                    transferir entre cuentas (es decir de un correo electr&oacute;nico
                                    a otro) aunque el due&ntilde;o de las cuentas sea el mismo.</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">En
                                    caso de que milespectaculos.com borrase un anuncio porque
                                    considera, a su absoluta discreci&oacute;n, que dicho anuncio
                                    infrinja las Condiciones de Uso o las Normas de Publicaci&oacute;n,
                                    milaespectaculos.com no devolver&aacute; los servicios de
                                    contratados por dicho anuncio. Este anuncio simplemente sera
                                    eliminado de la web.</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">En
                                    caso de que, por alg&uacute;n problema t&eacute;cnico o de otra
                                    &iacute;ndole, no se cumpliera con este servicio, nuestra
                                    responsabilidad se limitar&aacute; &uacute;nicamente a volver a
                                    subir tu anuncio sin coste adicional y a la mayor brevedad.</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Los
                                    datos recabados por este servicio en la p&aacute;gina de
                                    milespectaculos.com se rigen por nuestra pol&iacute;tica de
                                    protecci&oacute;n de datos.</font></font></font></p>
                                </ul>
                                <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">En
                                Mil espect&aacute;culos nos tomamos muy en serio cualquier uso
                                fraudulento con tarjetas de cr&eacute;dito, d&eacute;bito u
                                operaciones bancarias realizadas por nuestros usuarios. Por ello,
                                los usuarios responsables de este tipo de acciones ser&aacute;n
                                bloqueados permanentemente de nuestra p&aacute;gina y sus datos
                                podr&aacute;n ser comunicados a las autoridades pertinentes.</font></font></font></p>
                                <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Te
                                recordamos que seg&uacute;n las Condiciones de Uso, Mil
                                espect&aacute;culos se reserva el derecho, ejercitable en cualquier
                                momento y de modo discrecional a rechazar cualquier anuncio o
                                compromiso de ubicaci&oacute;n de un anuncio en una categor&iacute;a
                                o localidad determinada. Mil espect&aacute;culos tambi&eacute;n se
                                reserva el derecho de eliminar cualquier anuncio del Portal sin
                                necesidad de avisar previamente a los usuarios y/o anunciantes.</font></font></font></p>
                                <p align="left" style="margin-bottom: 0.26cm; border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Mil
                                espect&aacute;culos puede denegar o poner fin a su servicio y
                                adoptar medidas t&eacute;cnicas y legales para mantener a los
                                usuarios alejados del Portal si creemos que est&aacute;n creando
                                problemas o actuando de forma contraria al esp&iacute;ritu o la
                                forma de nuestras normas y condiciones de uso, todo ello con
                                independencia de cualquier pago realizado por el uso del Portal o
                                servicios complementarios. Sin embargo, decidamos o no retirar el
                                acceso al sitio web de un usuario, no aceptamos ninguna
                                responsabilidad por el uso no autorizado o ilegal del sitio web por
                                los usuarios, tal y como se describe en los p&aacute;rrafos
                                anteriores.</font></font></font></p>
                            </ol>
                        </ol>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Exclusi&oacute;n
                        de garant&iacute;as y responsabilidades</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos no garantiza la disponibilidad, continuidad ni la
                        infalibilidad del funcionamiento del Sitio Web, y en consecuencia,
                        excluye, en la m&aacute;xima medida permitida por la legislaci&oacute;n
                        vigente, cualquier responsabilidad por los da&ntilde;os y perjuicios
                        de toda naturaleza que puedan deberse a la falta de disponibilidad o
                        de continuidad del funcionamiento del Sitio Web y de los servicios
                        habilitados en el mismo, as&iacute; como a los errores en el acceso a
                        las distintas p&aacute;ginas web o aquellas desde las que, en su
                        caso, se presten dichos servicios.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos excluye cualquier responsabilidad por los da&ntilde;os
                        y perjuicios de toda &iacute;ndole que pudieran deberse a los
                        servicios prestados por terceros a trav&eacute;s de este Portal Web
                        as&iacute; como a los medios que estos habilitan para gestionar las
                        solicitudes de servicio, y concretamente, a modo enunciativo y no
                        limitativo: por los actos de competencia desleal y publicidad il&iacute;cita
                        como consecuencia de la prestaci&oacute;n de servicios por terceros a
                        trav&eacute;s del Sitio Web, as&iacute; como a la falta de veracidad,
                        exactitud, exhaustividad, vicios, defectos, pertinencia y/o
                        actualidad de los contenidos transmitidos, difundidos, almacenados,
                        recibidos, obtenidos, puestos a disposici&oacute;n o accesibles
                        mediante los servicios prestados por terceros a trav&eacute;s de este
                        Sitio Web.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                        usuario tiene garantizado el derecho a darse de baja del portal de
                        manera simple y eficaz y a dejar de recibir mensajes con origen en el
                        mismo. Mil Espect&aacute;culos no puede garantizar que otros usuarios
                        que ya cuentan con la direcci&oacute;n de correo del usuario a trav&eacute;s
                        de su contacto en el site puedan volver a contactar en el futuro.</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Datos
                        personales</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos le informa que tratar&aacute; los datos de
                        car&aacute;cter personal en los t&eacute;rminos de la Pol&iacute;tica
                        de Protecci&oacute;n de Datos de Car&aacute;cter Personal del Sitio
                        Web Milespectaculos.com, que el usuario podr&aacute; encontrar en la
                        Informaci&oacute;n Legal del Sitio Web.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Asimismo,
                        el usuario consiente expresamente que los contenidos que introduzca
                        sean encontrados y accesibles a trav&eacute;s de los buscadores de
                        Internet. Los Usuarios garantizan la veracidad, exactitud, vigencia y
                        autenticidad de los Datos Personales facilitados, y se comprometen a
                        mantenerlos debidamente actualizados.</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Pago
                        de comisiones o tasas</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Mil
                        </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Espect&aacute;culos</span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">
                        ofrece su plataforma de promoci&oacute;n de artistas y profesionales
                        del espect&aacute;culo de forma totalmente gratuita. No obstante,
                        existen planes de promoci&oacute;n que ofrecen ventajas a los
                        usuarios que paguen una cuota mensual, semestral o anual. Los
                        diferentes tipos de planes est&aacute;n publicados aqu&iacute;: </span></span></span></font></font></font></span><a href="https://www.milespectaculos.com/prices"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Ver
                        planes y cuotas.</span></span></span></span></font></font></span></font></span></a></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">No
                        se admiten devoluciones de pago de nuestros planes de promoci&oacute;n.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">As&iacute;
                        mismo, Mil Espect&aacute;culos puede cerrar contratos con los
                        usuarios de la web a trav&eacute;s de nuestra red de comerciales (JOB
                        CENTRE). Los usuarios que contraten este servicio deber&aacute;n
                        pagar una comisi&oacute;n del 10% del importe final del evento
                        organizado</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Seguridad</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos no puede garantizar que terceros, autorizados o
                        no, puedan tener conocimiento de la clase, condiciones,
                        caracter&iacute;sticas y circunstancias del uso que los Usuarios
                        hacen de las funcionalidades o que puedan acceder y, en su caso,
                        acceder, interceptar, eliminar, alterar, modificar o manipular los
                        mensajes, comunicaciones y contenidos de cualquier clase que los
                        Usuarios difundan o pongan a disposici&oacute;n de terceros a trav&eacute;s
                        de dichas funcionalidades, debido a que que las medidas de seguridad
                        en Internet no son inexpugnables, respecto a la privacidad y
                        seguridad.</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Enlaces
                        a p&aacute;ginas de terceros</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos incluye dentro de sus contenidos enlaces con
                        sitios pertenecientes y/o gestionados por terceros con el objeto de
                        facilitar el acceso a informaci&oacute;n disponible a trav&eacute;s
                        de Internet. La Mil Espect&aacute;culos no asume ninguna
                        responsabilidad derivada de la existencia de enlaces entre los
                        contenidos de este sitio y contenidos situados fuera del mismo o de
                        cualquier otra menci&oacute;n de contenidos externos a este sitio.
                        Tales enlaces o menciones tienen una finalidad exclusivamente
                        informativa y, en ning&uacute;n caso, implican el apoyo, aprobaci&oacute;n,
                        comercializaci&oacute;n o relaci&oacute;n alguna entre Mil
                        Espect&aacute;culos y las personas o entidades autoras y/o gestoras
                        de tales contenidos o titulares de los sitios donde se encuentren.</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>Legislaci&oacute;n
                        aplicable y Jurisdicci&oacute;n</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Estas
                        Condiciones Generales se rigen por la ley espa&ntilde;ola. &ldquo;Las
                        partes se someten, a su elecci&oacute;n, para la resoluci&oacute;n de
                        los conflictos y con renuncia a cualquier otro fuero, a los juzgados
                        y tribunales del domicilio del usuario. Asimismo, como entidad
                        adherida a CONFIANZA ONLINE y en los t&eacute;rminos de su C&oacute;digo
                        &Eacute;tico, en caso de controversias relativas a la contrataci&oacute;n
                        y publicidad online, protecci&oacute;n de datos y protecci&oacute;n
                        de menores, el usuario podr&aacute; acudir al sistema de resoluci&oacute;n
                        extrajudicial de controversias de CONFIANZA ONLINE
                        (</span></span></span></font></font></font></span><a href="http://www.confianzaonline.es/" target="_blank"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">www.confianzaonline.es</span></span></span></span></font></font></span></font></span></a><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">)</span></span></span></font></font></font></span></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2"><a name="politica-de-cookies"></a>
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>&iquest;Qu&eacute;
                        son las cookies?</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Las
                        cookies son un conjunto de datos que un servidor deposita en el
                        navegador del Usuario para recoger la informaci&oacute;n de registro
                        est&aacute;ndar de Internet y la informaci&oacute;n del
                        comportamiento de los visitantes en un sitio web. Es decir, se trata
                        de peque&ntilde;os archivos de texto que quedan almacenados en el
                        disco duro del ordenador y que sirven para identificar al Usuario
                        cuando se conecta nuevamente al sitio web. Su objetivo es registrar
                        la visita del Usuario y guardar cierta informaci&oacute;n. Su uso es
                        com&uacute;n y frecuente en la web ya que permite a las p&aacute;ginas
                        funcionar de manera m&aacute;s eficiente y conseguir una mayor
                        personalizaci&oacute;n y an&aacute;lisis sobre el comportamiento del
                        Usuario.</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>&iquest;Qu&eacute;
                        tipos de cookies utilizamos?</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Las
                        cookies de este sitio web se usan para personalizar el contenido y
                        los anuncios, ofrecer funciones de redes sociales y analizar el
                        tr&aacute;fico. Adem&aacute;s, compartimos informaci&oacute;n sobre
                        el uso que haga del sitio web con nuestros partners de redes
                        sociales, publicidad y an&aacute;lisis web, quienes pueden combinarla
                        con otra informaci&oacute;n que les haya proporcionado o que hayan
                        recopilado a partir del uso que haya hecho de sus servicios.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mil
                        Espect&aacute;culos puede utilizar los siguientes tipos de cookies:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cookies
                            propias: Son aqu&eacute;llas que se env&iacute;an al equipo terminal
                            del usuario desde un equipo o dominio gestionado por el propio
                            editor y desde el que se presta el servicio solicitado por el
                            usuario.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cookies
                            de terceros: Son aqu&eacute;llas que se env&iacute;an al equipo
                            terminal del usuario desde un equipo o dominio que no es gestionado
                            por el editor, sino por otra entidad que trata los datos obtenidos
                            trav&eacute;s de las cookies.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cookies
                            de sesi&oacute;n: son las que desaparecen del equipo del Usuario
                            cuando &eacute;ste abandona el sitio web visitado o cierra su
                            navegador. Normalmente se almacenan en la memoria cach&eacute; del
                            equipo.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cookies
                            persistentes: son las que se almacenan en el disco duro del equipo
                            del Usuario de forma permanente o prolongada, de modo que el sitio
                            web que las ha lanzado puede leerlas cada vez que el usuario lo
                            visita de nuevo. La fecha de caducidad de este tipo de cookies puede
                            variar entre unos minutos hasta varios a&ntilde;os.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cookies
                            de an&aacute;lisis: recogen informaci&oacute;n del uso que se
                            realiza del sitio web &uacute;nicamente con fines estad&iacute;sticos.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cookies
                            publicitarias y de publicidad comportamental: Son todas aquellas que
                            tienen como objetivo mejorar la eficacia de los espacios
                            publicitarios recogiendo informaci&oacute;n sobre las preferencias y
                            elecciones personales del Usuario.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cookies
                            de afiliados: permiten hacer un seguimiento de las visitas
                            procedentes de otras webs, con las que el sitio web establece un
                            contrato de afiliaci&oacute;n (empresas de afiliaci&oacute;n).</font></font></font></p>
                        </ul>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">En
                        concreto Mil Espect&aacute;culos utiliza cookies de los siguientes
                        proveedores de servicios:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><a href="http://www.google.com/intl/es/policies/terms"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">&quot;Google
                            Analytics&quot; de Google Inc.</span></span></span></span></font></font></span></font></span></a><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">:
                            Herramienta de an&aacute;lisis que permite recoger datos sobre el
                            comportamiento de los usuarios en nuestro portal web, como fecha de
                            la primera visita, n&uacute;mero de visitas, fecha de la &uacute;ltima
                            visita, n&uacute;mero de p&aacute;ginas vistas, etc&hellip; El
                            usuario puede inhabilitar en cualquier momento el uso de las cookies
                            de Google, desactiv&aacute;ndolas desde su navegador.</span></span></span></font></font></font></span></p>
                        </ul>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Este
                        sitio tambi&eacute;n puede albergar publicidad propia, de afiliados,
                        o de redes publicitarias. Esta publicidad se muestra mediante
                        servidores publicitarios que tambi&eacute;n utilizan cookies para
                        mostrar contenidos publicitarios afines a los usuarios. Cada uno de
                        estos servidores publicitarios dispone de su propia pol&iacute;tica
                        de privacidad, que puede ser consultada en sus propias p&aacute;ginas
                        web. Las cookies son ficheros creados en el navegador del usuario
                        para registrar su actividad en el Sitio Web y permitirle una
                        navegaci&oacute;n m&aacute;s fluida y personalizada. Para utilizar
                        este Sitio Web no resulta necesaria la instalaci&oacute;n de cookies.
                        El usuario puede no aceptarlas o configurar su navegador para
                        bloquearlas y, en su caso, eliminarlas. Actualmente este sitio
                        alberga publicidad de:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Google
                            Adsense:&nbsp;</span></span></span></font></font></font></span><a href="https://policies.google.com/privacy?gl=es"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Pol&iacute;tica
                            de privacidad de Google Adsense</span></span></span></span></font></font></span></font></span></a><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">&nbsp;&ndash;&nbsp;</span></span></span></font></font></font></span><a href="https://www.google.com/adsense/new/localized-terms"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">T&eacute;rminos
                            y condiciones</span></span></span></span></font></font></span></font></span></a><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">&nbsp;&ndash;&nbsp;</span></span></span></font></font></font></span><a href="https://support.google.com/adsense/answer/48182?sourceid=aso&amp;subid=ww-ww-et-asui&amp;medium=link"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Pol&iacute;ticas
                            del programa</span></span></span></span></font></font></span></font></span></a></p>
                        </ul>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                        tratamiento de los datos de car&aacute;cter personal, as&iacute; como
                        el env&iacute;o de comunicaciones comerciales realizadas por medios
                        electr&oacute;nicos, son conformes a la Ley Org&aacute;nica 15/1999,
                        de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter
                        Personal y a la Ley 34/2002, de 11 de julio, de servicios de la
                        Sociedad de Informaci&oacute;n y de Comercio Electr&oacute;nico.</font></font></font></p>
                        <h2 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b>&iquest;C&oacute;mo
                        puedo deshabilitar las cookies en mi navegador?</b></font></font></font></h2>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Se
                        pueden configurar los diferentes navegadores para avisar al Usuario
                        de la recepci&oacute;n de cookies y, si se desea, impedir su
                        instalaci&oacute;n en el equipo. Asimismo, el Usuario puede revisar
                        en su navegador qu&eacute; cookies tiene instaladas y cu&aacute;l es
                        el plazo de caducidad de las mismas, pudiendo eliminarlas.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Para
                        ampliar esta informaci&oacute;n consulte las instrucciones y manuales
                        de su navegador:</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Para
                        m&aacute;s informaci&oacute;n sobre la administraci&oacute;n de las
                        cookies en Google Chrome: </span></span></span></font></font></font></span><a href="https://support.google.com/chrome/answer/95647?hl=es" target="blank_"><span style="font-variant: normal"><font color="#23527c"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><u><span style="font-weight: normal"><span style="background: transparent">Informaci&oacute;n
                        cookies Google Chrome</span></span></u></span></span></font></font></font></span></a></p>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Para
                        m&aacute;s informaci&oacute;n sobre la administraci&oacute;n de las
                        cookies en Internet Explorer: </span></span></span></font></font></font></span><a href="http://windows.microsoft.com/es-es/windows-vista/cookies-frequently-asked-questions" target="blank_"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Informaci&oacute;n
                        cookies Internet Explorer</span></span></span></span></font></font></span></font></span></a></p>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Para
                        m&aacute;s informaci&oacute;n sobre la administraci&oacute;n de las
                        cookies en Mozilla Firefox: </span></span></span></font></font></font></span><a href="http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="blank_"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Informaci&oacute;n
                        cookies Mozilla Firefox</span></span></span></span></font></font></span></font></span></a></p>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Para
                        m&aacute;s informaci&oacute;n sobre la administraci&oacute;n de las
                        cookies en Safari: </span></span></span></font></font></font></span><a href="http://www.apple.com/es/privacy/use-of-cookies/" target="blank_"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Informaci&oacute;n
                        cookies Safari</span></span></span></span></font></font></span></font></span></a></p>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">Para
                        m&aacute;s informaci&oacute;n sobre la administraci&oacute;n de las
                        cookies en Opera: </span></span></span></font></font></font></span><a href="http://help.opera.com/Windows/11.50/es-ES/cookies.html" target="blank_"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Informaci&oacute;n
                        cookies Opera</span></span></span></span></font></font></span></font></span></a></p>
                        <h2 class="western" style="font-variant: normal; letter-spacing: normal; font-style: normal">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="6" style="font-size: 22pt"><b id="politica">POL&Iacute;TICA
                        DE PRIVACIDAD Y PROTECCI&Oacute;N DE DATOS</b></font></font></font></h2>
                        <h3 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="5" style="font-size: 18pt"><b>1.
                        RESPONSABLE</b></font></font></font></h3>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>&iquest;Qui&eacute;n
                        es el responsable del tratamiento de tus datos?</b></font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>Identidad:
                            </b>Francisco Carvajal P&eacute;rez. &ndash; NIF: 33354508 - T</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>Dir.
                            Postal: </b>Camino de los Carabineros, 37 29004 (M&aacute;laga)</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><b>Tel&eacute;fono:</b></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">
                            </span></span></span></font></font></font></span><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">622
                            94 76 77</span></span></span></span></font></font></span></font></span></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><b>Correo
                            electr&oacute;nico:</b></span></span></font></font></font></span><a href="mailto:info@lafactoriadelshow.com"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">info@</span></span></span></span></font></font></span></font></span></a><a href="mailto:info@lafactoriadelshow.com"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">milespectaculos</span></span></span></span></font></font></span></font></span></a><a href="mailto:info@lafactoriadelshow.com"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">.com</span></span></span></span></font></font></span></font></span></a></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; orphans: 2; widows: 2"><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><b>Contacto
                            Delegado Protecci&oacute;n de Datos:</b></span></span></font></font></font></span><span style="font-variant: normal"><font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal">
                            </span></span></span></font></font></font></span><a href="mailto:info@lafactoriadelshow.com"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">info@</span></span></span></span></font></font></span></font></span></a><a href="mailto:info@lafactoriadelshow.com"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">milespectaculos</span></span></span></span></font></font></span></font></span></a><a href="mailto:info@lafactoriadelshow.com"><span style="font-variant: normal"><font color="#337ab7"><span style="text-decoration: none"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><span style="letter-spacing: normal"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">.com</span></span></span></span></font></font></span></font></span></a></p>
                        </ul>
                        <h3 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="5" style="font-size: 18pt"><b>2.
                        FINALIDADES</b></font></font></font></h3>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>&iquest;Con
                        qu&eacute; finalidad tratamos tus datos personales?</b></font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>ARTISTAS
                        Y PROVEEDORES:</b></font></font></font></p>
                        <ol>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Crear
                            un perfil profesional visible en internet. Crearemos un enlace para
                            que tu perfil profesional aparezca en las b&uacute;squedas de
                            internet. Este perfil se puede desactivar desde los ajustes de tu
                            panel de control.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Realizar
                            una gesti&oacute;n correcta de los servicios solicitados. En este
                            sentido, los datos que nos proporciones tanto en el momento de crear
                            tu cuenta con nosotros como para crear tu perfil ser&aacute;n
                            tratados para:</font></font></font></p>
                            <ul>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Usar
                                el sistema de mensajer&iacute;a instant&aacute;nea. Los usuarios
                                podr&aacute;n iniciar contigo una conversaci&oacute;n a trav&eacute;s
                                de nuestro servicio de mensajer&iacute;a instant&aacute;nea. Si
                                detectamos una actividad o comportamiento sospechoso o ilegal,
                                nuestro equipo de seguridad podr&aacute; revisar los mensajes
                                enviados y recibidos. Del mismo modo, podremos acceder a los
                                mensajes enviados y recibidos para mejorar y desarrollar nuestros
                                servicios.</font></font></font></p>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Permitir
                                que los usuarios de la web vean tus datos de contacto desde tu
                                perfil profesional. Si una empresa o persona se interesa en tu
                                perfil, es posible que contacte contigo para comprobar si est&aacute;s
                                interesado en su oferta de trabajo. Recuerda que siempre podr&aacute;s
                                cambiar esta configuraci&oacute;n en los ajustes de tu panel de
                                control.</font></font></font></p>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Permitir
                                a Mil Espect&aacute;culos incluir tus servicios en presupuestos a
                                clientes potenciales seg&uacute;n las tarifas que publiques en tu
                                panel de control.</font></font></font></p>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Gestionar
                                los servicios solicitados. Te enviaremos comunicados, avisos de
                                mensajes nuevos, recordatorios, confirmaciones, cambios de estado,
                                avisos t&eacute;cnicos, entre otros.</font></font></font></p>
                            </ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Enviarte
                            ofertas y alertas de empleo. Trataremos tus datos para avisarte de
                            las ofertas de empleo que se adapten mejor a tu perfil.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mostrarte
                            publicidad inteligente. En base a los datos de tu navegaci&oacute;n
                            que recojamos mediante el uso de cookies, te mostraremos publicidad
                            personalizada adaptada a tus gustos y preferencias. No se tomar&aacute;n
                            decisiones automatizadas en base a dicho perfil que produzcan
                            efectos jur&iacute;dicos o significativos para ti. El perfil
                            comercial que se elabore a trav&eacute;s de las cookies podr&aacute;
                            ser utilizado por sites de terceros para mostrarte publicidad
                            personalizada.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Enviarte
                            mensajes de Mil Espect&aacute;culos y de terceros. Mediante estos
                            env&iacute;os te informaremos de los productos, servicios o
                            novedades de Mil Espect&aacute;culos as&iacute; como de terceras
                            empresas.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mejorar
                            nuestros servicios y productos mediante el estudio de tu
                            comportamiento para adaptarlos a tus necesidades y gustos.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Prevenir
                            abusos y fraudes en el uso de nuestros servicios (por ejemplo,
                            actividades fraudulentas, ataques de denegaci&oacute;n de servicios,
                            env&iacute;o de spam, entre otros).</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cesi&oacute;n
                            de datos a organismos y autoridades p&uacute;blicas, siempre y
                            cuando sean requeridos de conformidad con las disposiciones legales
                            y reglamentarias.</font></font></font></p>
                        </ol>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>ORGANIZADORES
                        DE FIESTAS Y EVENTOS:</b></font></font></font></p>
                        <ol>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Enviar
                            mensajes y solicitudes de presupuesto a artistas y proveedores
                            registrados en Mil Espect&aacute;culos o directamente a nuestro
                            equipo comercial.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Realizar
                            una gesti&oacute;n correcta de los servicios solicitados. En este
                            sentido, los datos que nos proporciones en el momento de crear tu
                            cuenta con nosotros ser&aacute;n tratados para:</font></font></font></p>
                            <ul>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Usar
                                el sistema de mensajer&iacute;a instant&aacute;nea. Puedes iniciar
                                una conversaci&oacute;n con artistas y/o proveedores a trav&eacute;s
                                de nuestro servicio de mensajer&iacute;a instant&aacute;nea. Si
                                detectamos una actividad o comportamiento sospechoso o ilegal,
                                nuestro equipo de seguridad podr&aacute; revisar los mensajes
                                enviados y recibidos. Del mismo modo, podremos acceder a los
                                mensajes enviados y recibidos para mejorar y desarrollar nuestros
                                servicios.</font></font></font></p>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Permitir
                                que los artistas y/o proveedores registrados vean tus datos
                                personales y de contacto. Si contactas con un artista o proveedor,
                                este contactar&aacute; contigo para darte una respuesta.</font></font></font></p>
                            </ul>
                        </ol>
                        <ol>
                            <ul>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Permitir
                                a Mil Espect&aacute;culos enviarte informaci&oacute;n y
                                presupuestos de los servicios o productos solicitados.</font></font></font></p>
                            </ul>
                        </ol>
                        <ol>
                            <ul>
                                <li/>
                        <p style="margin-bottom: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                                <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Gestionar
                                los servicios solicitados. Te enviaremos comunicados, avisos de
                                mensajes nuevos, recordatorios, confirmaciones, cambios de estado,
                                avisos t&eacute;cnicos, entre otros.</font></font></font></p>
                            </ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mostrarte
                            publicidad inteligente. En base a los datos de tu navegaci&oacute;n
                            que recojamos mediante el uso de cookies, te mostraremos publicidad
                            personalizada adaptada a tus gustos y preferencias. No se tomar&aacute;n
                            decisiones automatizadas en base a dicho perfil que produzcan
                            efectos jur&iacute;dicos o significativos para ti. El perfil
                            comercial que se elabore a trav&eacute;s de las cookies podr&aacute;
                            ser utilizado por sites de terceros para mostrarte publicidad
                            personalizada.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Enviarte
                            mensajes de Mil Espect&aacute;culos y de terceros. Mediante estos
                            env&iacute;os te informaremos de los productos, servicios o
                            novedades de Mil Espect&aacute;culos as&iacute; como de terceras
                            empresas.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Mejorar
                            nuestros servicios y productos mediante el estudio de tu
                            comportamiento para adaptarlos a tus necesidades y gustos.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Prevenir
                            abusos y fraudes en el uso de nuestros servicios (por ejemplo,
                            actividades fraudulentas, ataques de denegaci&oacute;n de servicios,
                            env&iacute;o de spam, entre otros).</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cesi&oacute;n
                            de datos a organismos y autoridades p&uacute;blicas, siempre y
                            cuando sean requeridos de conformidad con las disposiciones legales
                            y reglamentarias.</font></font></font></p>
                        </ol>
                        <h3 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="5" style="font-size: 18pt"><b>3.
                        PLAZO DE CONSERVACI&Oacute;N DE LOS DATOS</b></font></font></font></h3>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>&iquest;Por
                        cu&aacute;nto tiempo conservaremos tus datos?</b></font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Tus
                        datos ser&aacute;n conservados mientras dure la relaci&oacute;n
                        contractual y comercial con nosotros, solicites su supresi&oacute;n,
                        as&iacute; como el tiempo necesario para cumplir las obligaciones
                        legales.</font></font></font></p>
                        <h3 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="5" style="font-size: 18pt"><b>4.
                        LEGITIMACI&Oacute;N</b></font></font></font></h3>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>&iquest;Cu&aacute;l
                        es la legitimaci&oacute;n para el tratamiento de sus datos?</b></font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">La
                        base legal para el tratamiento de tus datos radica en:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">La
                            ejecuci&oacute;n de un contrato en relaci&oacute;n con la finalidad
                            indicada en el apartado 2 anterior.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                            consentimiento del usuario en relaci&oacute;n con la finalidad
                            indicada en los apartados 1 a 6 anteriores.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                            inter&eacute;s leg&iacute;timo de Mil Espect&aacute;culos en
                            relaci&oacute;n con la finalidad indicada en el apartado 7 anterior.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">El
                            cumplimiento de obligaciones legales aplicables a Mil Espect&aacute;culos
                            para la finalidad indicada en el apartado 8 anterior.</font></font></font></p>
                        </ul>
                        <h3 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="5" style="font-size: 18pt"><b>5.
                        DESTINATARIOS</b></font></font></font></h3>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>&iquest;A
                        qu&eacute; destinatarios se comunicar&aacute;n tus datos?</b></font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Tus
                        datos personales ser&aacute;n comunicados a terceros en los
                        siguientes supuestos:</font></font></font></p>
                        <ul>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Cuando
                            existan clientes interesados en contratar tus servicios en
                            presupuestos enviados y gestionados por nuestro departamento
                            comercial.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Tus
                            datos podr&aacute;n ser accedidos por aquellos proveedores que
                            prestan servicios a Mil Espect&aacute;culos, tales como servicios de
                            alojamiento de contenido, de mensajer&iacute;a instant&aacute;nea,
                            de env&iacute;o de notificaciones, servicios publicitarios, etc. Mil
                            Espect&aacute;culos .ha suscrito los correspondientes contratos de
                            encargo de tratamiento con cada uno de los proveedores que prestan
                            servicios a Mil Espect&aacute;culos con el objetivo de garantizar
                            que dichos proveedores tratar&aacute;n tus datos de conformidad con
                            lo establecido en la legislaci&oacute;n vigente.</font></font></font></p>
                            <li/>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                            <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Tus
                            datos personales tambi&eacute;n podr&aacute;n ser cedidos a las
                            autoridades competentes en los casos que exista una obligaci&oacute;n
                            legal.</font></font></font></p>
                        </ul>
                        <h3 class="western" style="margin-top: 0.53cm; margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 110%; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="5" style="font-size: 18pt"><b>6.
                        DERECHOS</b></font></font></font></h3>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt"><b>&iquest;Cu&aacute;les
                        son tus derechos cuando nos facilitas tus datos y c&oacute;mo puedes
                        ejercerlos?</b></font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Tienes
                        derecho a obtener confirmaci&oacute;n sobre si en Mil Espect&aacute;culos.
                        estamos tratando datos personales que te conciernan, o no.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Asimismo,
                        tienes derecho a acceder a tus datos personales, as&iacute; como a
                        solicitar la rectificaci&oacute;n de los datos inexactos o, en su
                        caso, solicitar su supresi&oacute;n cuando, entre otros motivos, los
                        datos ya no sean necesarios para los fines que fueron recogidos.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">En
                        determinadas circunstancias, podr&aacute;s solicitar la limitaci&oacute;n
                        del tratamiento de tus datos, en cuyo caso &uacute;nicamente los
                        conservaremos para el ejercicio o la defensa de reclamaciones.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">En
                        determinadas circunstancias y por motivos relacionados con tu
                        situaci&oacute;n particular, podr&aacute;s oponerte al tratamiento de
                        tus datos. Mil Espect&aacute;culos dejar&aacute; de tratar los datos,
                        salvo por motivos leg&iacute;timos imperiosos, o el ejercicio o la
                        defensa de posibles reclamaciones.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Asimismo,
                        puedes ejercer el derecho a la portabilidad de los datos, as&iacute;
                        como retirar los consentimientos facilitados en cualquier momento,
                        sin que ello afecte a la licitud del tratamiento basado en el
                        consentimiento previo a su retirada.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Si
                        deseas hacer uso de cualquiera de tus derechos puedes dirigirte a
                        nosotros a trav&eacute;s de los enlaces habilitados que encontrar&aacute;s
                        en los correos electr&oacute;nicos y comunicaciones de Mil
                        Espect&aacute;culos o en tu cuenta de usuario.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Alternativamente,
                        tambi&eacute;n puedes dirigirte a nosotros mediante correo postal en
                        la siguiente direcci&oacute;n: Mil Espect&aacute;culos, Camino de los
                        Carabineros, 37. 29004 (M&aacute;laga), Espa&ntilde;a. Dirigido a
                        Atenci&oacute;n al Usuario e indicando en el sobre &ldquo;Protecci&oacute;n
                        de Datos&rdquo;. Recuerda facilitar la mayor informaci&oacute;n
                        posible sobre tu solicitud: Nombre y apellidos, direcci&oacute;n de
                        correo electr&oacute;nico que utilizas para la cuenta o portal objeto
                        de tu solicitud.</font></font></font></p>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2">
                        <font color="#4d4d4d"><font face="Raleway, arial, sans-seriff"><font size="2" style="font-size: 10pt">Por
                        &uacute;ltimo, te informamos que puedes dirigirte ante la Agencia
                        Espa&ntilde;ola de Protecci&oacute;n de Datos y dem&aacute;s
                        organismos p&uacute;blicos competentes para cualquier reclamaci&oacute;n
                        derivada del tratamiento de tus datos personales.</font></font></font></p>
                        <ol>
                            <ol>
                                <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                <font color="#3c3c3c"><font face="Verdana, arial"><font size="4" style="font-size: 14pt"><b>Aspectos
                                a considerar para proteger sus datos de login:</b></font></font></font></p>
                                <ul>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Nunca
                                    comparta una cuenta de ordenador</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Nunca
                                    utilice la misma contrase&ntilde;a en m&aacute;s de una cuenta</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Nunca
                                    mencione su contrase&ntilde;a a nadie, ni siquiera a personas que
                                    afirman ser del servicio al usuario de mil espect&aacute;culos</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Nunca
                                    apunte una contrase&ntilde;a</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Nunca
                                    comunique su contrase&ntilde;a por tel&eacute;fono, e-mail o
                                    mensajer&iacute;a instant&aacute;neo</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Aseg&uacute;rese
                                    siempre de hacer el log-out antes de dejar su ordenador
                                    desatendido</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Pide
                                    una nueva contrase&ntilde;a si tiene la sensaci&oacute;n de que la
                                    seguridad de su cuenta pueda correr riesgos</font></font></font></p>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Actualice
                                    siempre su software anti-virus</font></font></font></p>
                                </ul>
                            </ol>
                        </ol>
                        <ol>
                            <ol>
                                <ul>
                                    <li/>
                        <p align="left" style="border: none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 0.58cm; orphans: 2; widows: 2">
                                    <font color="#3c3c3c"><font face="Verdana, arial"><font size="2" style="font-size: 10pt">Evita
                                    acceder a su cuenta de Mil Espect&aacute;culos desde ordenadores
                                    cuyo acceso sea p&uacute;blico (p.ej., en cibercaf&eacute;s)</font></font></font></p>
                                </ul>
                            </ol>
                        </ol>
                        <p style="margin-bottom: 0.26cm; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 100%; orphans: 2; widows: 2">
                        <br/>
                        <br/>

                        </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection