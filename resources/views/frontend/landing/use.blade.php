@extends('layouts.app')

@section('header')
    
    @include('layouts.partials.headerfrontend')

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="content-buttons">
                <div class="row justify-content-around">
                    <div class="p-2">
                        <a href="{{ url('posts/create') }}">
                            <img src="{{ asset('./img/buttons/publicar-anuncio.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ url('my-posts') }}">
                            <img src="{{ asset('./img/buttons/modificar-anuncio.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ route('favorites.index') }}">
                            <img src="{{ asset('./img/buttons/anuncios-favoritos.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ url('events') }}">
                            <img src="{{ asset('./img/buttons/arte-eventos.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ route('zone-vip.index') }}">
                            <img src="{{ asset('./img/buttons/zona-vip.png') }}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="row bg-light pb-5">
        <div class="col-md-12">
            <div class="col pb-4">
                <div class="card">
                    <div class="card-body text-justify">
                        <p><h1><center>Términos de uso de Mil Espectáculos</center></h1></p>
                        <p>
                            Mil Espectáculos pone a disposición de los usuarios de Internet (en adelante EL USUARIO) el Sitio
                        Web Milespectaculos.com (en adelante Mil Espectáculos) de forma gratuita.
                        </p>
                        <p>
                            Los presentes Términos de Uso regulan el acceso y uso de nuestro portal web.
                        </p>
                        <p>
                            La utilización de este portal Web implica la aceptación plena de los Términos de Uso, así como de
                        nuestras Condiciones Legales en el momento en que el Usuario acceda al mismo. Mil Espectáculos
                        podrá modificar estos Términos de Uso sin notificación previa, por tanto, el usuario deberá leerlos
                        atentamente cada vez que utilice este servicio. Estas condiciones estarán plenamente accesibles en el
                        Sitio web a través del enlace "Términos de Uso".
                        </p>
                        <p>
                            <h1><center>Objeto</center></h1>
                        </p>
                        <p>
                            La finalidad de Mil Espectáculos es la de ser un punto de referencia para los interesados en la
                        contratación de artistas , espectáculos , Empresas relacionadas con el mundo de la organización de
                        Eventos, así como para los profesionales del sector.
                        </p>
                        <p>
                            Ofrecemos los siguientes servicios a los Usuarios que se registren a través de nuestro formulario de
                        alta:
                        </p>
                        <p>
                            •Publicación de anuncios relacionados con el sector del espectáculo <br>
                        •Creación de una ficha artística personal del usuario <br>
                        •Promoción y difusión de la ficha artísticas del usuario a través de Internet <br>
                        •Bolsa de trabajo para los usuarios <br>
                        •Información y últimas novedades sobre el sector del espectáculo y artistas <br>
                        •Remisión de comunicaciones electrónicas promocionales e informativas sobre el sector del
                        espectáculo y otros sectores de su interés <br>
                        </p>
                        <p>
                            Mil Espectáculos facilitará los datos y/o información proporcionados por el Usuario a los demás
                        usuarios de la web siempre que dicha información no vulnere disposiciones legales o nuestras
                        Condiciones legales.
                        </p>
                        <p>
                            Mil Espectáculos se esforzará al máximo por mantener disponible el Sitio Web en todo momento, pero
                        el USUARIO reconoce que técnicamente es imposible mantener la disponibilidad al 100%. El servicio
                        puede presentar anomalías o la suspensión pasajera del mismo por razones de mantenimiento o
                        seguridad, así como por causas no imputables a Mil Espectáculos (cortes de electricidad, anomalías
                        en los servicios de comunicación, etc…)
                        </p>
                        <p>
                            Mil Espectáculos se reserva el derecho a eliminar del Sitio Web cualquier contenido ilegal sin previo
                        aviso.
                        </p>
                        <p>
                            <h1><center>Obligaciones de los Usuarios en Mil Espectáculos</center></h1>
                        </p>
                        <p>
                            Para el acceso a los servicios ofertados en este sitio web, el Usuario debe ser mayor de edad o
                        disponer de la autorización de sus padres o tutores legales.
                        </p>
                        <p>
                            El Usuario se compromete a usar la plataforma sujetándose a la Ley, a las buenas costumbres y a los
                        presentes Términos de Uso, manteniendo el debido respeto a los demás usuarios. Asimismo, el
                        USUARIO está obligado a respetar las leyes aplicables y los derechos de terceros al utilizar los
                        contenidos y servicios de Mil Espectáculos.
                        </p>
                        <p>
                            Queda prohibida la reproducción, transmisión, distribución, adaptación, o modificación de los
                        contenidos del Sitio Web (textos, diseños, gráficos, informaciones, bases de datos, archivos de sonido
                        y/o imagen, logos) y cualquier elemento de este Sitio Web, salvo autorización previa de los legítimos
                        titulares o cuando resulte así permitido por la ley
                        </p>
                        <p>
                            Al USUARIO le estará prohibido: utilizar contenidos injuriosos o calumniosos, utilizar contenidos
                        pornográficos o que vulneren la leyes de protección de menores así como distribuir productos
                        pornográficos o que vulneren las leyes de protección de menores, molestar a otros usuarios utilizando
                        SPAM o de cualquier otra forma, utilizar contenidos protegidos legalmente sin tener derecho a ello, o
                        hacer publicidad, ofrecer o distribuir bienes o servicios protegidos legalmente, así como realizar
                        acciones contrarias a la libre competencia, incluidas las encaminadas a la captación de clientes
                        progresiva.
                        </p>
                        <p>
                            Algunas funcionalidades o servicios del Sitio Web no exigen la previa suscripción o registro de los
                        usuarios. No obstante, la utilización de algunos servicios está condicionada a la previa
                        cumplimentación del registro de usuario, seleccionando el identificador y la contraseña que el usuario
                        se compromete a conservar y a usar con diligencia.
                        </p>
                        <p>
                            La contraseña de cada usuario es personal e intransferible. No se permite la cesión de la contraseña a
                        terceros, ni tan siquiera de manera temporal. En este sentido, cada usuario deberá adoptar las
                        medidas necesarias para la custodia de la contraseña seleccionada, evitando el uso de la misma por
                        terceros. el Usuario es el único responsable de la utilización que se haga con su contraseña, con
                        completa indemnidad para Mil Espectáculos. Si el usuario sospecha que su contraseña está siendo
                        utilizada por terceros deberá comunicarlo a Mil Espectáculos con la mayor brevedad.
                        </p>
                        <p>
                            El Usuario garantiza la autenticidad de todos aquellos datos que comunique como consecuencia de la
                        cumplimentación de los formularios necesarios para la utilización de las funcionalidades. Es
                        responsabilidad del usuario mantener toda la información permanentemente actualizada, de forma que
                        responda en cada momento a la situación real del usuario. En cualquier caso, el Usuario será el único
                        responsable de las manifestaciones falsas o inexactas que realice y de los perjuicios que cause a Mil
                        Espectáculos o a terceros por la información facilitada.
                        </p>
                        <p>
                            Según nuestra política anti-spamming de Mil Espectáculos, el Usuario está obligado a no recabar y
                        utilizar datos a partir de los datos de otros usuarios a los que se pueda acceder a través de los
                        contenidos y servicios ofrecidos en Mil Espectáculos para la realización de actividades con fines
                        promocionales o publicitarios así como de remitir comunicaciones comerciales de cualquier tipo y a
                        través de cualquier soporte no solicitado ni previamente consentido por Mil Espectáculos y/o los
                        interesados.
                        </p>
                        <p>
                            El usuario es consciente y acepta voluntariamente, que el servicio tiene lugar, en todo caso, bajo su
                        única responsabilidad. El usuario responderá de los daños y perjuicios de toda naturaleza que Mil 
                        Espectáculos pueda sufrir como consecuencia del incumplimiento intencional o culpablemente de
                        cualquiera de las obligaciones expuestas en estos Términos de Uso, en nuestra Información Legal o
                        dictadas por la ley en relación con este servicio.
                        </p>
                        <p>
                            <h1><center>Publicación de contenidos en MilEspectáculos</center></h1>
                        </p>
                        <p>
                            El Usuario se declara titular legítimo de los derechos de propiedad intelectual e industrial del contenido
                        para la reproducción, distribución y comunicación pública a través de cualquier medio electrónico para
                        todo el mundo y con tiempo ilimitado
                        </p>
                        <p>
                            Esta prohibida la publicación de contenidos que deterioren la calidad del servicio.
                        </p>
                        <p>
                            Esta prohibida la publicación de contenidos que: <br>
                        •Sean presuntamente ilícitos con la normativa nacional, comunitaria o internacional o que
                        realicen actividades presuntamente ilícitas o contravengan los principio de la buena fe. <br>
                        •Puedan dañar el buen nombre y reputación de La Mil Espectáculos. <br>
                        •No reúnan los parámetros de calidad establecidos por Mil Espectáculos. <br>
                        •Sean engañosos, de dudosa eficacia o que puedan causar perjuicios contra las personas. <br>
                        •Que apoyen o justifiquen el racismo, la violencia y el odio. <br>
                        •Que atenten contra los derechos fundamentales de las personas, puedan buscar la debilidad
                        del usuario, falten a la cortesía, molesten o puedan generar opiniones negativas en nuestros
                        usuarios o terceros. <br>
                        •Que contravengan los principios de legalidad, honradez, responsabilidad, protección de la
                        dignidad humana, protección de menores, protección de orden público, protección de la vida
                        privada, protección del consumidor y los derechos de la propiedad intelectual e industrial. <br>
                        </p>
                        <p>
                            El Usuario que publique contenidos en Mil Espectáculos se compromete a cumplir con los requisitos y
                        pautas detallados a continuación: <br>
                        •Los datos requeridos tanto en el alta de usuario como en la inserción de un anuncio deben ser
                        completados con información veraz. <br>
                        •Los espectáculos ofertados deben ser reales, y la información sobre ellos debe ser clara,
                        precisa y tiene que ajustarse a la realidad <br>
                        •No se aceptan anuncios de espectáculos que puedan ser clasificados como "sólo para
                        adultos" <br>
                        •Se debe utilizar un lenguaje profesional en la descripción del espectáculo. <br>
                        •Está prohibida la publicación de anuncios que puedan deteriorar la calidad de nuestro servicio.
                        Está prohibida la inserción de anuncios duplicados, con datos de contacto erróneos o con un
                        nombre de empresa irreal <br>
                        •Que atenten contra los derechos fundamentales de las personas, puedan buscar la debilidad
                        del usuario, falten a la cortesía, molesten o puedan generar opiniones negativas en nuestros
                        usuarios o terceros. <br>
                        •Si encuentras algún anuncio o contenido inapropiado en Mil Espectáculos, por favor contacta
                        con nosotros a través de nuestro formulario de contacto <br>
                        </p>
                        <p>
                            Mil Espectáculos se reserva el derecho a eliminar las fichas artísticas de miembros por los siguientes
                        motivos: <br>
                        •Comportamiento inadecuado o poco ético que generen quejas por parte de nuestros clientes
                        y/o usuarios. <br>
                        •Uso inadecuado o fraudulento de nuestro sistema de reservas y contratación de espectáculos
                        y servicios. <br>
                        •No cumplir los requisitos mínimos de calidad y profesionalidad, tanto en el contenido de las
                        fichas artísticas como en los eventos contratados a través de Mil Espectáculos. <br>
                        </p>
                        <p>
                            La eliminación de una cuenta por incumplimiento de cualquiera de nuestros términos de uso implica la
                        no devolución de los importes abonados en caso de tratarse de una cuenta de pago
                        </p>
                        <p>
                            La determinación de cualquiera de estos supuestos es responsabilidad exclusiva de Mil Espectáculos
                        atendiendo a su buen juicio a las demandas de terceras personas o instituciones
                        </p>
                        <p>
                            El incumplimiento de dicha norma puede significar la cancelación inmediata de la cuenta de usuario y
                        la destrucción de todos los contenidos asociados al mismo sin posibilidad de recuperación de los
                        mismos
                        </p>
                        <p>
                            Hacemos revisiones periódicas de los anuncios y demandas para asegurar que nuestros Términos de
                        Uso se cumplan pero no podemos controlar todos y cada uno de los anuncios que se publican, por
                        tanto no podemos asumir la responsabilidad de los contenidos.
                        </p>
                        <p>
                            Asimismo, Mil Espectáculos se reserva la potestad de retirar del sitio web aquellos contenidos que se
                        consideren no apropiados a las características y finalidades de milespectaculos.com
                        </p>
                        <p>
                            Cualquier Usuario que inserte un contenido que contravenga la legalidad vigente asumirá la
                        responsabilidad exclusiva de los perjuicios y consecuencias derivadas de la misma, eximiendo a Mil
                        Espectáculos de cualquier responsabilidad.
                        </p>
                        <p>
                            <h1><center>Exclusión de garantías y responsabilidades</center></h1>
                        </p>
                        <p>
                            Mil Espectáculos no garantiza la disponibilidad, continuidad ni la infalibilidad del funcionamiento del
                        Sitio Web, y en consecuencia, excluye, en la máxima medida permitida por la legislación vigente,
                        cualquier responsabilidad por los daños y perjuicios de toda naturaleza que puedan deberse a la falta
                        de disponibilidad o de continuidad del funcionamiento del Sitio Web y de los servicios habilitados en el
                        mismo, así como a los errores en el acceso a las distintas páginas web o aquellas desde las que, en su
                        caso, se presten dichos servicios.
                        </p>
                        <p>
                            Mil Espectáculos excluye cualquier responsabilidad por los daños y perjuicios de toda índole que
                        pudieran deberse a los servicios prestados por terceros a través de este Portal Web así como a los
                        medios que estos habilitan para gestionar las solicitudes de servicio, y concretamente, a modo
                        enunciativo y no limitativo: por los actos de competencia desleal y publicidad ilícita como consecuencia
                        de la prestación de servicios por terceros a través del Sitio Web, así como a la falta de veracidad,
                        exactitud, exhaustividad, vicios, defectos, pertinencia y/o actualidad de los contenidos transmitidos,
                        difundidos, almacenados, recibidos, obtenidos, puestos a disposición o accesibles mediante los
                        servicios prestados por terceros a través de este Sitio Web.
                        </p>
                        <p>
                            El usuario tiene garantizado el derecho a darse de baja del portal de manera simple y eficaz y a dejar
                        de recibir mensajes con origen en el mismo. Mil Espectáculos no puede garantizar que otros usuarios
                        que ya cuentan con la dirección de correo del usuario a través de su contacto en el site puedan volver
                        a contactar en el futuro.
                        </p>
                        <p>
                            <h1><center>Datos personales</center></h1>
                        </p>
                        <p>
                            Mil Espectáculos le informa que tratará los datos de carácter personal en los términos de la Política de
                        Protección de Datos de Carácter Personal del Sitio Web milespectaculos.com, que el usuario podrá
                        encontrar en la Información Legal del Sitio Web.
                        </p>
                        <p>
                            Asimismo, el usuario consiente expresamente que los contenidos que introduzca sean encontrados y
                        accesibles a través de los buscadores de Internet. Los Usuarios garantizan la veracidad, exactitud,
                        vigencia y autenticidad de los Datos Personales facilitados, y se comprometen a mantenerlos
                        debidamente actualizados.
                        </p>
                        <p>
                            <h1><center>Pago de comisiones o tasas</center></h1>
                        </p>
                        <p>
                            Mil Espectáculos ofrece su plataforma de promoción de artistas y profesionales del espectáculo de
                        forma totalmente gratuita. No obstante, existen planes de promoción que ofrecen ventajas a los
                        usuarios que paguen una cuota mensual, semestral o anual.
                        </p>
                        <p>
                            Así mismo, Mil Espectáculos admite El derecho a desistimiento del Usuario al amparo de lo regulado
                        en el articulo 68 del Real Decreto Legislativo 1/2007 de 16 de Noviembre por el que se aprueba el
                        texto refundido de la Ley General para la defensa del Consumidor y Usuario y otras Leyes
                        complementarias la devolución del pago de nuestros planes de promoción siempre y cuando sea en el
                        plazo de 14 días naturales desde la fecha de su contratación tal y como establece la ley sobre el
                        derecho a desistimiento.
                        </p>
                        <p>
                            <h1><center>Seguridad</center></h1>
                        </p>
                        <p>
                            Mil Espectáculos no puede garantizar que terceros, autorizados o no, puedan tener conocimiento de la
                        clase, condiciones, características y circunstancias del uso que los Usuarios hacen de las
                        funcionalidades o que puedan acceder y, en su caso, acceder, interceptar, eliminar, alterar, modificar o
                        manipular los mensajes, comunicaciones y contenidos de cualquier clase que los Usuarios difundan o
                        pongan a disposición de terceros a través de dichas funcionalidades, debido a que las medidas de
                        seguridad en Internet no son inexpugnables, respecto a la privacidad y seguridad.
                        </p>
                        <p>
                            <h1><center>Enlaces a páginas de terceros</center></h1>
                        </p>
                        <p>
                            Mil Espectáculos incluye dentro de sus contenidos enlaces con sitios pertenecientes y/o gestionados
                        por terceros con el objeto de facilitar el acceso a información disponible a través de Internet. Mil
                        Espectáculos no asume ninguna responsabilidad derivada de la existencia de enlaces entre los
                        contenidos de este sitio y contenidos situados fuera del mismo o de cualquier otra mención de
                        contenidos externos a este sitio. Tales enlaces o menciones tienen una finalidad exclusivamente
                        informativa y, en ningún caso, implican el apoyo, aprobación, comercialización o relación alguna entre 
                        Mil Espectáculos y las personas o entidades autoras y/o gestoras de tales contenidos o titulares de los
                        sitios donde se encuentren.
                        </p>
                        <p>
                            <h1><center>Legislación aplicable y Jurisdicción</center></h1>
                        </p>
                        <p>
                            Estas Condiciones Generales se rigen por la ley española. “Las partes se someten, a su elección, para
                        la resolución de los conflictos y con renuncia a cualquier otro fuero, a los juzgados y tribunales del
                        domicilio del usuario. Asimismo, como entidad adherida a CONFIANZA ONLINE y en los términos de
                        su Código Ético, en caso de controversias relativas a la contratación y publicidad Online, protección de
                        datos y protección de menores, el usuario podrá acudir al sistema de resolución extrajudicial de
                        controversias de CONFIANZA ONLINE (www.confianzaonline.es)
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection