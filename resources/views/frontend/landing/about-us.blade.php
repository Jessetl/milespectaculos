@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')


<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">QUIENES SOMOS.</h5>
        </header>

        <div class="col-md-12">
            <div class="col pb-4">
                <div class="card">
                    <div class="card-body text-justify">
                        <div class="content">
                            <h5 class="card-title">
                                ¿Que es Mil Espectáculos?
                            </h5>
                            <p class="font-weight-light text-justify">
                                Es una plataforma para facilitar la contratación de artistas y empresas relacionada con el mundo del espectáculo y organizadores los eventos . De forma sencilla e intuitiva con la mas amplia gama de Categorías y Subcategorías.
                            </p>
                        </div>
                        <div class="row p-5">

                            @foreach($categories as $key => $category)
                            
                            <div class="col-md-4">
                                <ul>
                                    <li class="text-uppercase">
                                        {{ $category->name }}
                                    </li>
                                    <ul>
                                        @foreach($category->category_type as $key => $type)
                                        <li>
                                            {{ $type->name }}
                                        </li>
                                        @endforeach
                                    </ul>
                                </ul>
                            </div>

                            @endforeach

                        </div>
                        <div class="content">
                            <h5 class="card-title">
                                ¿Que pretendemos?
                            </h5>
                            <p class="font-weight-light text-justify">
                                En Mil Espectáculos Pretendemos ser la herramienta mas Practica, sencilla y completa de Internet donde desde el usuario particular podrá organizar personalmente su boda, comunión cumpleaños, despedida de  soltero/a, etc. o contratar nuestras empresas de profesionales especializadas. Los ayuntamientos pueden organizar y contratar los artistas o a empresas especialistas para la organización de sus ferias, o empresas organizadoras que precisan contactar con otras empresas sea de catering, pirotecnia, azafatas, seguridad, Artistas, alquileres de autobuses o vehículos, Escenarios, etc. También la sala de conciertos o de fiestas, la discoteca, u hotel que desee contactar con artistas o grupos musicales de todo índole y estilo etc. 
                            </p>
                            <p class="font-weight-light text-justify">
                                Mil Espectáculos ofrece todas las herramientas necesarias para realizar las contrataciones de servicios o productos con todas las garantías, por que todos los usuarios pueden ver lo que contrata mediante todo tipo de videos, mp3, fotos, etc. 
                            </p>
                            <p class="font-weight-light text-justify">
                                Pero si tenemos que destacar algo en lo que no tenemos competencia es la creación de una plataforma paralelamente interna donde todos los miembros asociados interactúan a tiempo real entre Artistas y Empresas del sector. Ofreciéndoles una Red Social interna donde pueden interactuar desde el Management con el artista o la empresa organizadora de eventos con empresas de sonido e iluminación, Catering, etc. Cambien nuestros socios se benefician de descuentos internos que se ofrecen entre profesionales, ejemplo, un artista ofrece un 20% de descuento solo para socios por lo que a un representante le interesa para ser mas competitivo en precio. A un artista le interesa el descuento que ofrece un estudio de grabación o una tienda de instrumentos o equipos de sonido. A una organizadora de bodas le interesa el descuento que hace la empresa de Catering Y así sucesivamente. 
                            </p>
                            <p class="font-weight-light text-justify">
                                Otra característica que poseen los asociados es que las demandas que solicitan en la web solo se ven de forma interna, ejemplo, si alguien solicita cualquier tipo de servicio o espectáculo solo lo podrán ver los artistas y Empresas asociadas. De forma interna también se otorgan herramientas para compartir desde contratos en todo tipo de formatos, mp3, videos, Midis, fotos, partituras, galas, etc. y muchas características mas que de forma externa no se ven pero existen de forma interna para fomentar y facilitar la cooperación entre Artistas y Empresas tanto de espectáculos como organizadoras de Eventos.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection