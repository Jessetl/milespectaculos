@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">Preguntas y respuestas frecuentes.</h5>
        </header>
        <div class="col">
            <div class="card">
                <div class="card-body text-justify">
                	<h5 class="card-title text-uppercase">Respuestas para organizadores de fiestas y eventos</h5>
                	
                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo trabaja Mil Espectáculos?</h6>
                	<hr>
                	
                	<p class="text-justify">En Mil Espectáculos tenemos una base de datos con miles de artistas y profesionales del espectáculo preparados para actuar en cualquier tipo de fiesta o evento. Nuestra plataforma facilita el contacto directo entre artistas y organizadores de fiestas o eventos. Da igual si es la primera vez que organizas una fiesta o si eres un profesional de la organización de eventos. En Mil Espectáculos encontrarás todo lo que necesitas sin moverte del sofá.
                	</p>
                	<p class="text-justify">Haz clic en el botón de contacto de los profesionales que te interesen y pídeles presupuesto. Los artistas recibirán tu propuesta al instante y contactarán contigo para facilitarte un presupuesto o concretar cualquier detalle. Podrás ver todos los mensajes que te envíen los artistas y proveedores accediendo a tu panel de control.</p>
                	<p class="text-justify">También tenemos un equipo de especialistas en fiestas y eventos preparado para ayudarte a encontrar y contratar a los artistas y proveedores que necesitas. Contacta con nosotros directamente y te enviaremos presupuestos sin compromiso de artistas y proveedores de confianza, recomendados por Mil Espectáculos.</p>
                	<a href="{{ url('contact/us') }}" target="_blank" class="my-5">Solicitar un presupuesto</a>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo puedo contactar con los artistas y proveedores?</h6>
                	<hr>

                	<p class="text-justify">Puedes enviar un mensaje a cualquier artista o proveedor desde nuestros listados o accediendo a las fichas artísticas y haciendo clic sobre cualquier botón de “Póngase en contacto con el propietario de este anuncio”. Tu mensaje se enviará directamente a los artistas y proveedores y te responderán en cuanto vean el mensaje. Podrás ver todos los mensajes y gestionar tus eventos desde tu panel de control </p>

                	<h6 class="my-3 text-uppercase font-weight-light">Pero… ¿es realmente gratis solicitar presupuestos a los artistas?</h6>
                	<hr>

                	<p class="text-justify">Si, al 100%. No cobramos nada por contactar o solicitar presupuestos a cualquier profesional. Sin cuotas, sin comisiones… Nuestro objetivo es facilitar el contacto entre profesionales del espectáculo y organizadores de fiestas y eventos. Queremos que la contratación de espectáculo sea un asunto sencillo y transparente.</p>
                	<p class="text-justify">A aquellos clientes que quieren un servicio más personalizado y delegan en nuestras manos la contratación de los artistas y/o empresas que necesitan, les cobramos una comisión que oscila entre el 5% y el 10%, por los gastos de gestión.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">Necesito ayuda para organizar mi evento ¿Podéis ayudarme?</h6>
                	<hr>

                	<p class="text-justify">Cuando un cliente nos pide un presupuesto o que le ayudemos a organizar su evento, estudiamos la propuesta y buscamos todos artistas y profesionales que mejor se adapten al evento. Sólo incluimos artistas o empresas que cumplen unos requisitos de calidad y que garanticen el éxito de la fiesta y evento. Nos encargamos de comprobar la calidad de cada artista o profesional que recomendamos en cada propuesta</p>
                	<p class="text-justify">Así mismo, cuando cerramos un evento, creamos un contrato legal que protege a ambas partes ante cualquier problema o imprevisto y hacemos un seguimiento, dando soporte al cliente en todo momento para asegurarnos de que la fiesta y evento sea un éxito.</p>
                	<a href="{{ url('contact/us') }}" target="_blank" class="my-5">Contacta con nuestro equipo de especialistas</a>

                	<h5 class="card-title text-uppercase my-3">Respuestas para artistas y profesionales del espectáculo</h5>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo puedo crear una ficha artística?</h6>
                	<hr>

                	<p class="text-justify">Haz clic en este <a href="{{ route('register') }}" target="_blank">enlace</a>. En un par de minutos tendrás tu ficha artística on-line y podrás empezar a promocionar tu espectáculo o servicio.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Por qué debería hacerme miembro de Mil Espectáculos?</h6>
                	<hr>

                	<p class="text-justify">En estos tiempos, la mayoría de las personas busca información en Internet sobre cualquier producto o servicio que vaya a comprar o a contratar. Desde comida para perros hasta reserva de hoteles, la gente cada vez compra más en Internet. Con una ficha artística en Mil Espectáculos vas a ser mucho más visible en Internet y vas a tener muchas más oportunidades de encontrar clientes interesados en contratarte. Podrás crear un completo dossier online, con toda tu información, fotos, vídeos, audio, opiniones, tus Eventos,, etc… Las personas que visiten tu ficha artística podrán contactar contigo por teléfono (solo para miembros de pago) o a través de nuestro sistema de mensajería.</p>

                	<p class="text-justify">Cada día acceden miles de personas a nuestra web, buscando artistas y proveedores para sus fiestas y eventos (nuestro tráfico aumenta más de un 20% cada mes). Todos los días se cierran un montón de contratos para fiestas y eventos de toda España. Si eres un artista profesional o proveedor del mundo del espectáculo u Eventos, debes estar aquí.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Tengo que pagar alguna comisión si me contratan a través de Mil Espectáculos?</h6>
                	<hr>

                	<p class="text-justify">Mil espectáculos nunca te cobrará nada cuando alguien te contrate a través de nuestro portal. Este es sólo uno de los aspectos que nos diferencia de cualquier otra agencia de espectáculos. Puedes estar en nuestro catálogo gratuitamente y firmar bolos sin pagar ninguna comisión por el servicio.</p>
                	<p class="text-justify">Además, te ofrecemos algunos planes de promoción que son de pago, para artistas y proveedores profesionales que quieran obtener más ventajas y servicios de Mil Espectáculos. Estos planes son totalmente opcionales, pero mejoran bastante las posibilidades de conseguir clientes. Puedes ver nuestros planes de pago aquí:</p>
                	<a href="{{ route('prices') }}" target="_blank" class="my-5">Planes de promoción de Mil espectáculos</a>

                	<p class="text-justify">También ofrecemos un servicio personalizado a clientes que necesitan ayuda para organizar sus fiestas y eventos. En estos casos actuamos como agentes de reservas y nos encargamos de todas las gestiones del evento (contratos, facturas, cobros, etc…). Mil Espectáculos cobra una comisión de entre el 5% y el 10% (depende del tipo de evento) por realizar estas gestiones.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Puedo crear más de una Ficha Artística?</h6>
                	<hr>

                	<p class="text-justify">Sólo los miembros con cuenta VIP pueden crear más de una ficha artística. Accede a tu <a href="{{ route('login') }}" target="_blank">Panel de Control</a> y haz clic en la pestaña “Mi Cuenta”, después haz clic en “Añadir ficha”</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Puedo crear más de una Ficha Artística?</h6>
                	<hr>

                	<p class="text-justify">Mil Espectáculos ya ocupa los primeros lugares en el ranking de Google para búsquedas relacionadas con la contratación de artistas. Estamos constantemente refinando y ajustando las cosas para que nuestro portal esté todavía más optimizado. No solo nos basamos en los motores de búsqueda para promocionar el portal. También hacemos un montón de publicidad online e impresa y estamos en ferias, congresos y eventos. Además, tenemos una relación excepcional, tanto con artistas de gran talento como con organizadores de fiestas profesionales. Puedes tener la seguridad de que en Mil Espectáculos estamos trabajando duro para beneficiar tanto nuestros artistas y proveedores, como los organizadores de fiestas y eventos.</p>

                	<p class="text-justify">También en Mil Espectáculos ofrecemos las herramientas mas vanguardistas para promocionar tu perfil de Empresa u Artista, donde puedes promocionar tus eventos en tu perfil y compartirlo en las más importantes redes sociales con un solo clik. Dependerá también del buen uso y veces que actualices tu perfil, dado que así se posicionara en los primeros lugares de nuestra plataforma para los perfiles de Artistas y Empresas gratuitos a excepción de nuestros miembros VIP que no precisaran actualizar su perfil diariamente dado que se actualizara de forma automática cada día </p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Puedo crear más de una Ficha Artística?</h6>
                	<hr>

                	<p class="text-justify">Tendrás acceso a tu propio panel de control, donde puedes personalizar y gestionar tu ficha artística. Serás visto por organizadores de eventos de todo el país y muchos más clientes potenciales se fijarán en ti.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Mil Espectáculos es una agencia de representación o management de artistas?</h6>
                	<hr>

                	<p class="text-justify">No exactamente, nosotros trabajamos de otra manera: Damos a los profesionales del espectáculo las herramientas necesarias para crear su propia ficha artística y que ellos mismos puedan promocionarse en Internet y conseguir trabajo directamente sin intermediarios y sin pagar comisiones. A no ser que sea una empresa organizadora de fiestas u Eventos las que les contraten y el porcentaje de comisión dependerá del acuerdo al que ustedes lleguen.</p>

                	<p class="text-justify">También ofrecemos un servicio de “asesoría artística” para orientar y dar soporte a los clientes que necesitan ayuda o no quieren complicarse la vida con todo el proceso de búsqueda y reserva de artistas y proveedores para sus eventos. En estos casos si que percibimos comisiones por nuestra gestión.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo puedo aparecer en la página principal de Mil Espectáculos?</h6>
                	<hr>

                	<p class="text-justify">Estos lugares están reservados para miembros Premium o V.I.P. Los anuncios van cambiando aleatoriamente cada hora.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Puedo cambiar las provincias en las que puedo trabajar?</h6>
                	<hr>

                	<p class="text-justify">Sí,es muy fácil. Accede a tu <a href="{{ route('login') }}" target="_blank">Panel de Control</a> y haz clic en “Editar Ficha artística” luego haz clic en “Información general”.Un poco más abajo podrás ver “Tu zona de trabajo”. Selecciona las provincias que te interesen. Los miembros VIP pueden añadir más provincias y ampliar su radio de exposición.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Puedo editar o modificar una opinión que un cliente a escrito en mi perfil?</h6>
                	<hr>

                	<p class="text-justify">Para hacer cambios en las opiniones que la gente hace sobre tu servicio debes solicitarlo a la persona que escribió la opinión. Simplemente contacta con el cliente (desde el mismo correo electrónico que utiliza para publicar la opinión) indicando los detalles de los cambios que te gustaría hacer</p>

                	<p class="text-justify">Sabemos que esto puede ser un poco conflictivo, pero lo hacemos para defender la integridad de las críticas y opiniones de los usuarios de Mil Espectáculos. No obstante, si existe algún problema al respecto puedes <a href="{{ url('contact/us') }}" target="_blank" class="my-5">contactar con nosotros</a> e intentaremos ayudarte.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo puedo cambiar mi URL?</h6>
                	<hr>

                	<p class="text-justify">Nuestros miembros Premium y V.I.P pueden cambiar su URL de Mil Espectáculos (ejemplo: www.milespectaculos.com/migrupo). Si eres un miembro VIP y estás interesado en tener una URL personalizada, contacta con nosotros y nos ocuparemos de ello.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">No recibo ningún correo de Mil Especáculos ¿Hay algún problema?</h6>
                	<hr>

                	<p class="text-justify">Probablemente no recibas nuestros mails por que tu servicio de correo electrónico ha bloqueado nuestros correos como SPAM. Mira en la bandeja de correo no-deseado y añade nuestra dirección de mail a la bandeja de correos seguros. Si esto no lo resuelve, puedes ponerte en contacto con tu proveedor de Internet para que averigüen cual es el problema. <a href="{{ url('contact/us') }}" target="_blank" class="my-5">contacta con nosotros</a></p>

                	<h6 class="my-3 text-uppercase font-weight-light">Acabo de recibir una oferta de alguien que está organizando un evento ¿Cómo sé que me puedo fiar?</h6>
                	<hr>

                	<p class="text-justify">Ten mucho cuidado cuando se trate de peticiones que contengan cualquiera de los siguientes puntos:</p>

                	<ul>
                		<li> El cliente pide que sólo contactes con él a través del correo electrónico. </li>
                		<li> Te piden que realices cosas que no estén incluidas en los servicios que ofreces. </li>
                		<li> Mensajes de organizadores de eventos que estén fuera de España. </li>
                		<li> Mensajes de correo electrónico con una gramática excepcionalmente pobre. </li>
                		<li> Organizadores de fiestas y eventos que te piden dinero por adelantado por incluirte en su programación.</li>
                		<li> Consultas con un número de teléfono falso. </li>
                		<li> Consultas con datos falsos como la ubicación del evento. </li>
                	</ul>

                	<p class="text-justify">Una prueba sencilla es llamar al número de teléfono que ofrecen. Si se trata de un sistema de respuesta automática, un número falso o simplemente no hay número de teléfono, hay que ser muy prudente.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo puedo cancelar mi cuenta?</h6>
                	<hr>

                	<p class="text-justify">Basta con acceder a tu Panel de Control y seleccionar la pestaña “Mi cuenta”, luego haz clic en “Administrar”. Sigue las instrucciones para eliminar tu cuenta. Así de fácil.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo elimino mi anuncio gratuito en Mil Espectáculos?</h6>
                	<hr>

                	<p class="text-justify">¿Por qué quieres hacer eso? ¡si es gratis! Bueno... Si es necesario entra tu Panel de Control y selecciona la pestaña “Información general”, luego haz clic en “Preferencias”. Desplázate hacia abajo y verás una opción para desactivar tu ficha artística. Puedes regresar y habilitarlo cuando quieras.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">¿Cómo es posible que todavía no haya creado una cuenta en Mil Espectáculos?</h6>
                	<hr>

                	<p class="text-justify">Por que estabas leyendo esto, y eso nos parece perfecto. Pero ha llegado el momento de unirte a nosotros. ¡Haz clic <a href="{{ route('register') }}" target="_blank">aquí</a> para hacerte miembro!"</p>
                	<p class="text-justify">Sabemos que puedes tener más preguntas. Si no hemos resuelto todas tus dudas, <a href="{{ url('contact/us') }}" target="_blank" class="my-5">Contacta con nosotros.</a> Estaremos encantados de ayudarte.</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection