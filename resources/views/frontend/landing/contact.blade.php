@extends('layouts.app')

@section('logo')

    @include('layouts.partials.logo')

@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">Contacta con mil espectáculos.</h5>
        </header>

        <div class="col-md-7">

            @include('frontend.session.session')

            {!! Form::open(['route' => 'contact__us', 'method' => 'POST', 'name' => 'ContactForm']) !!}

            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <h4 class="card-title">
                                    Contacta con nostros si tienes alguna duda o problema.
                                </h4>
                                <span class="font-weight-light">Completa el siguiente formulario y te responderemos lo antes posible.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="departament"> Elige un departamento </label>
                                <select name="departament" class="form-control">
                                    <option value="Departamento comercial">Departamento comercial</option>
                                    <option value="Departamento de sugerencias">Departamento de sugerencias</option>
                                    <option value="Información y preguntas">Información y preguntas</option>
                                    <option value="Soporte técnico e incidencias web">Soporte técnico e incidencias Web</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name"> Nombre <span class="text-danger">(*)</span></label>
                                <input type="text" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} popover-dismiss" name="name" data-toggle="popover" value="{{ old('name') }}" data-placement="top" data-content="Escribe tu nombre completo" required>
                                @if ($errors->has('name'))
                                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="phone"> Teléfono <span class="text-danger">(*)</span></label>
                                <input type="text" id="phone" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }} popover-dismiss" name="phone" data-toggle="popover" value="{{ old('phone') }}" data-placement="top" data-content="Escribe tu número de teléfono"/>
                                @if ($errors->has('phone'))
                                    <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="email"> Tu e-mail <span class="text-danger">(*)</span></label>
                                <input type="text" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} popover-dismiss" name="email" data-toggle="popover" value="{{ old('email') }}" data-placement="top" data-content="Escribe tu Correo electronico" required>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="suggest"> Observaciones <span class="text-danger">(*)</span></label>
                                <textarea id="suggest" class="form-control {{ $errors->has('suggest') ? ' is-invalid' : '' }} popover-dismiss" name="suggest" data-toggle="popover" rows="5" data-placement="top" data-content="Escribe aquí tu consulta o comentario" required>
                                {{ old('suggest') }}"
                                </textarea>
                                @if ($errors->has('suggest'))
                                    <div class="invalid-feedback">{{ $errors->first('suggest') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {!! NoCaptcha::display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block text-danger" role="alert">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="politics" required> He leído y acepto el <a href="{{ url('legal') }}"> aviso legal y condiciones de uso y politica de privacidad y cookies. </a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Enviar">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            <div class="form-group">
                                <div class="bg-light p-2">
                                    <div class="font-weight-bold">
                                        Responsable: <span class="font-weight-light">Francisco Carvajal Pérez</span>
                                    </div>
                                    <div class="font-weight-bold">
                                        Finalidad: <span class="font-weight-light">Responder solicitudes y dar respuesta a las dudas relativas al funcionamiento del sitio web</span>
                                    </div>
                                    <div class="font-weight-bold">
                                        Legitimización: <span class="font-weight-light">Consentimiento del interesado</span>
                                    </div>
                                    <div class="font-weight-bold">
                                        Destinatarios: <span class="font-weight-light">Se cederán datos a terceros por obligación legal y para la ejecución del servicio que nos has solicitado</span>
                                    </div>
                                    <div class="font-weight-bold">
                                        Derechos: <span class="font-weight-light">Acceder, rectificar y suprimir los datos, asi como otros derechos, como se explica en la información adicional</span>
                                    </div>
                                    <div class="font-weight-bold">
                                        Información adicional: <span class="font-weight-light">Consulta la información completa en la <a href="">politica de privacidad.</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer p-2">
                    <span>Los campos marcados con (*) son obligatorios.</span>
                </div>
            </div>

            {!! Form::close() !!}

        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col">
                    <h4>
                        Estamos aquí para ayudarte
                    </h4>
                    <p class="text-justify">
                        Contacta con nosotros para resolver cualquier duda o consulta que tengas. Nos pondremos en contacto contigo a la mayor brevedad. Todos los datos personales que nos proporciones se guardarán de forma totalmente confidencial.
                    </p>
                    <h4 class="font-weight-bold">
                        Oficina Málaga:
                    </h4>
                    <p>
                        Camino de los Carabineros, 37 29004 MÁLAGA
                    </p>
                    <p>
                        <span class="text-primary d-block"><i class="fa fa-phone-square fa-2x"></i> (+ 34) 633 77 40 08</span>
                        <span class="text-primary d-block"><i class="fa fa-phone-square fa-2x"></i> (+ 34) 622 94 76 77</span>
                        <span class="text-primary d-block"><i class="fa fa-phone-square fa-2x"></i> (+ 34) 952 07 31 57</span>
                    </p>
                    <p>
                        <a href="https://web.whatsapp.com/send?phone=34633774008&text=" target="_blank" class=" d-block"><img src="{{ asset('img/Whatsapp_37229.png') }}"> Whatsapp</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
