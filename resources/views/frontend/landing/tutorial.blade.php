@extends('layouts.app')

@section('header')
    
    @include('layouts.partials.headerfrontend')

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="content-buttons">
                <div class="row justify-content-around">
                    <div class="p-2">
                        <a href="{{ url('posts/create') }}">
                            <img src="{{ asset('img/buttons/publicar-anuncio.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ url('my-posts') }}">
                            <img src="{{ asset('img/buttons/modificar-anuncio.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ route('favorites.index') }}">
                            <img src="{{ asset('img/buttons/anuncios-favoritos.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ url('events') }}">
                            <img src="{{ asset('img/buttons/arte-eventos.png') }}">
                        </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ route('zone-vip.index') }}">
                            <img src="{{ asset('img/buttons/zona-vip.png') }}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row bg-light pb-5">
        <div class="col-md-12">
            <div class="col pb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <p><h1><center>Tutorial de ¿Cómo insertar un vídeo de Youtube?</center></h1></p>
                        <p><h2>Paso #1.</h2></p>
                        <p><h2> Busca el vídeo en Youtube que quieres poner en tu página</h2></p>
                        <p>
                            <img src="{{ asset('img/youtube/img5.png') }}">
                        </p>
                        <p><h2>Paso #2.</h2></p>
                        <p><h2> Debajo del video hay un botón en el que pone “compartir”.</h2></p>
                        <p>
                            <img src="{{ asset('img/youtube/img1.png') }}">
                        </p>
                        <p><h2>Paso #3.</h2></p>
                        <p><h2>Pinchamos en el botón que dice "Incorporar" o "insertar", como podemos ver en la siguiente imagen</h2></p>
                        <p>
                            <img src="{{ asset('img/youtube/img2.png') }}">
                        </p>
                        <p><h2>Paso #4.</h2></p>
                        <p><h2>Copiamos el enlace.</h2></p>
                        <p><h2>Vamos a copiar uncicamente el enlace del video, como aparece resaltado en la imagen.</h2></p>
                        <p>
                            <img src="{{ asset('img/youtube/img3.png') }}">
                        </p>
                        <p><h2>Paso #5.</h2></p>
                        <p><h2>Insertamos el enlace.</h2></p>
                        <p><h2>Colocamos el enlace en nuestro formulario de Mil Espectaculos para que asi las personas interesadas puedan tener un mejor preview de tu trabajo, de lo que ofreces o de lo que buscas.</h2></p>
                        <p>
                            <img src="{{ asset('img/youtube/img4.png') }}">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection