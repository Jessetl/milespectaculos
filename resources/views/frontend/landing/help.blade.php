@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">Preguntas y respuestas frecuentes.</h5>
        </header>
        <div class="col">
            <div class="card">
                <div class="card-body text-justify">
                	<h5 class="card-title text-uppercase">Así funciona Mil Espectáculos</h5>
                	<hr>

                	<p class="text-justify">En Mil Espectáculos facilitamos los datos de artistas y/o profesionales del espectáculo a cualquier persona que esté interesada en organizar una fiesta o evento. Nuestra misión es hacer que la contratación de artistas sea algo sencillo y transparente, ofreciendo a los usuarios una gran variedad de opciones para sus fiestas y eventos. </p>

                	<h6 class="my-3 text-uppercase font-weight-light">Busca y compara artistas y profesionales delespectáculo</h6>
                	<hr>

                	<p class="text-justify">Bandas de Rock, orquestas, payasos, cómicos, escenarios, carpas, castillos hinchables...Catering, alquileres de Equipos de sonido y luces, Azafatas de congresos, Mobiliario para eventos, etc.¡Aquí tenemos de todo!. Hay miles de artistas y proveedores en Mil Espectáculos. Selecciona la categoría que te interese,indica el lugar del evento y...¡Hop!. Aparecerá la lista de artistas y proveedores que trabajan en tu zona. Puedes ver toda su información, fotos y vídeos, escuchar sus canciones, ver sus Próximos Eventos ver opiniones o agregarlo a tu lista de favoritos.</p>
                	<p class="text-justify">También puedes optar por solicitar mediante el panel de demanda de servicios lo que deseas contratar, para que tipo de evento y fecha y aparecerá en el apartado de demandas, donde las Empresas y Artistas interesados y disponibles para la fecha solicitada, se pondrán en contacto con usted para acordar presupuesto sin necesidad de tener que contactar con ellos.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">Comprueba los precios y la disponibilidad</h6>
                	<hr>

                	<p class="text-justify">Una vez seleccionados los posibles candidatos, el siguiente paso será ponerse en contacto. Haz clic en el botón de “Pedir presupuesto” para hacerles saber que estás interesado y necesitas información completa. Nos pondremos en contacto con el artista o proveedor y concretaremos la disponibilidad y los precios. También puedes hacerle cualquier otra pregunta que tengas sobre sus servicios.</p>

                	<h6 class="my-3 text-uppercase font-weight-light">Contrata a tu favorito y disfruta tu evento</h6>
                	<hr>

                	<p class="text-justify">Este paso es el mejor. Una vez que hayas elegido al mejor candidato para tu evento, Mil Espectáculos se encarga de todos los detalles del evento. Sólo tienes que esperar que llegue el día esperado y disfrutar al máximo de tu fiesta o evento.</p>
                	<p class="text-justify">Es fácil ¿Verdad? Si todo esto todavía te parece complicado,prueba a solicitar una <a href="{{ url('contact/us') }}" target="_blank">Propuesta personalizada GRATIS.</a> Te ayudamos a seleccionar los mejores artistas y/o proveedores disponibles analizando tus necesidades y tu presupuesto. <a href="{{ url('contact/us') }}" target="_blank">¿Quieres saber cómo lo hacemos?</a></p>

                </div>	
            </div>
        </div>
    </div>
</div>

@endsection