@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5" id="app">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">RED SOCIAL INTERNA.</h5>
        </header>

        <div class="col-md-9">
            
            {!! Form::open(['route' => 'advanced.search', 'method' => 'GET', 'autocomplete' => 'off']) !!}
            
                <div class="card border mb-3">
                    <div class="card-body">

                        @include('frontend.network.partials.fields-search')

                        <span class="text-black">Encontradas  publicaciones.</span>
                    
                        <div class="row pt-3">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-primary float-right" value="Buscar">
                            </div>
                        </div>
                    </div>
                </div>
                
            {!! Form::close() !!}

            @forelse($networks as $network)
                <publication :data="{{ $network }}" :user_id="{{ Auth::user()->id }}"></publication>
            @empty

            @endforelse
        </div>
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')
            
            <div class="my-2">
                <div class="card border">
                    <div class="card-body">
                        <a href="{{ route('my/network') }}" class="btn btn-primary text-white btn-block">
                            ENTRAR A MI RED
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>


@endsection