@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">MI RED SOCIAL</h5>
        </header>

        <div class="col-md-9">

            {!! Form::open(['route' => 'my/network_store', 'method' => 'POST', 'autocomplete' => 'off']) !!}

            <div class="card border">
                <div class="card-body">
                    <div class="card-network">
                        <div class="card-options">
                            {{-- <div class="dropdown">
                                <a href="javascript:void(0);" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-chevron-down carret" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu dropdown-menu-right"><li><a href="javascript:void(0);"><i class="fa fa-window-close" aria-hidden="true"></i><strong>Hide post</strong><br><small>See fewer posts like this</small></a></li><li><a href="javascript:void(0);"><i class="fa fa-window-close" aria-hidden="true"></i><strong>Hide all from Carlo Fontanos</strong><br><small>Stop seeing posts from this Page</small></a></li><li class="divider"></li><li><a href="javascript:void(0);"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Report post</a></li><li><a href="javascript:void(0);"><i class="fa fa-external-link-square" aria-hidden="true"></i> Save link</a></li><li><a href="javascript:void(0);"><i class="fa fa-globe" aria-hidden="true"></i> Turn on notifications for this post</a></li><li><a href="javascript:void(0);"><i class="fa fa-code" aria-hidden="true"></i> Embed</a></li><li class="divider"></li><li><a href="javascript:void(0);"><i class="fa fa-chevron-down" aria-hidden="true"></i> More options</a></li></ul>
                            </div> --}}
                        </div>
                        <div class="card-author-avatar">
                            <a href="javascript:void(0);">
                                @if ( empty($me->url) )
                                    <img src="{{ asset('img/icon.png') }}">
                                @else
                                    <img src="{{ $me->url }}">
                                @endif
                            </a>
                        </div>
                        <div class="card-author">
                            <a href="javascript:void(0);">
                                {{ $me->name .' '. $me->surname }}
                            </a>
                        </div>
                    </div>

                    <div class="card-excerpt">
                        <div class="row my-4">
                            <div class="col">
                                <select class="form-control text-uppercase" name="category" id="category">
                                    <option value="" selected disabled>CATEGORÍA</option>
                                    @forelse($categories as $key => $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @empty
                                        <option value="" selected disabled></option>
                                    @endforelse
                                </select> 
                            </div>
                            <div class="col">
                                <select class="form-control text-uppercase" name="subcategory" id="subcategory">
                                    <option value="">SUBCATEGORÍA</option>
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-control text-uppercase" name="districts">
                                    <option value="" selected>PROVINCIA</option>
                                    @forelse($districts as $key => $district)
                                        <option value="{{ $district->id }}">
                                            {{ $district->name }}</option>
                                    @empty
                                        <option value="" selected disabled></option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="row my-4">
                            <div class="col">
                                <select class="form-control" name="type">
                                    <option value="0">PARTICULAR</option>
                                    <option value="1">PROFESIONAL</option>
                                </select>
                            </div>
                            <div class="col">
                                <select id="type_amount" class="form-control {{ $errors->has('type_amount') ? ' is-invalid' : '' }} popover-dismiss" name="type_amount" required>
                                    <option value="A Convenir">A CONVENIR</option>
                                    <option value="A Consultar">A CONSULTAR</option>
                                    <option value="Otro">OTRO</option>
                                </select>
                                @if ($errors->has('type_amount'))
                                    <div class="invalid-feedback">{{ $errors->first('type_amount') }}</div>
                                @endif
                            </div>
                            <div class="col">
                                <input type="number" id="amount" class="form-control {{ $errors->has('amount') ? ' is-invalid' : '' }} popover-dismiss" step=".01" name="amount" value="{{ old('amount') }}" data-toggle="popover" data-placement="top" data-content="Coloca una suma aproximada del costo de tu servicio, agrega solo dos decimales en caso de necesitarlo. Ejemplo: 1485,02" disabled required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <textarea class="form-control" rows="5" name="message" placeholder="¿Qué éstas buscando, {{ $me->name }}?" required></textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-small btn-block">Publicar</button>
                </div>
            </div> 

            {!! Form::close() !!}

            @forelse($me->network as $network)
            
            <div class="card border my-2">
                <div class="card-body">
                    <div class="card-network">
                        <div class="card-options">
                            <div class="dropdown">
                                <a href="javascript:void(0);" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-chevron-down carret" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="javascript:void(0);"><i class="fa fa-window-close" aria-hidden="true"></i> Eliminar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-author-avatar">
                            <a href="javascript:void(0);">
                                @if ( empty($me->url) )
                                    <img src="{{ asset('img/icon.png') }}">
                                @else
                                    <img src="{{ $me->url }}">
                                @endif
                            </a>
                        </div>
                        <div class="card-author">
                            <a href="javascript:void(0);">
                                {{ $me->name .' '. $me->surname }}
                            </a>
                        </div>
                        <div class="card-time">{{ $network->updated_at }} <i class="fa fa-globe" aria-hidden="true"></i></div>
                        <div class="card-body">
                            <div class="card-excerpt">
                                <p>{{ $network->message }}</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="card-events mx-3">
                        <a href="javascript:void(0);">{{ count($network->messages) }} Comentarios</a>
                    </div>
                </div>
            </div> 
            @empty

            @endforelse
        </div>
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')
            
        </div> 
    </div>
</div>


@endsection