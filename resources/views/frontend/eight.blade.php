@extends('layouts.app')

@section('content')

<div class="container pt-5 pb-5">
    <div class="row bg-light">
        <header class="page-header">
            <h5>8 CONSEJOS IMPRESCINDIBLES.</h5>
        </header>
    </div>
    	
    <div class="row justify-content-start bg-light pb-5">
        <div class="advice red">
            <span class="title d-block">1.- Título</span>
            <span>El título es lo mas importante, Debe contener <strong>palabras que una persona buscaria</strong> para intentar encontrar lo que ofrece. Además debe ser tan <stong>atractivo</stong> que invite a leer el resto del anuncio. Las  visitas a un anuncio no sólo llegan desde milanuncios, buena parte lo haces desde Google y el título es el factor más importante para aparecer prímero en una búsqueda.</span>
        </div>
        <div class="advice yellow">
            <span class="title d-block">2.- Texto</span>
            <span>El texto debe contener <strong>vocabulario variado</strong> sobre la temática del anuncio. Cuantas más palabras tenga más facilidad para ser encontrado por alguna combinación de términos tanto en el buscador de milespectaculos como en Google.</span>
        </div>
        <div class="advice blue">
            <span class="title d-block">3.- Enlaza tu anuncio</span>
            <span>Para que un anuncio sea leído por más usuarios puedes enlazarlo desde foros, blogs, páginas personales,... No sólo se consigue tener más audiencia sino que <strong>Google da mejores posiciones</strong> a anuncios que están enlazados desde otras páginas. Si dispones de cuenta en <strong>facebook</strong> también puedes publicar tu anuncio haciendo clic sobre "enviar a amigos" que encontrarás junto a cada anuncio.</span>
        </div>
        <div class="advice green">
            <span class="title d-block">4.- No repetir</span>
            <span>Es muy importante no repetir el texto de un anuncio por varias razones: <strong>Google considera spam la repetición</strong> de contenidos y hará que no aparezcan bien posicionados en las búsquedas. Por otro lado, en milanuncios no se permite la repetición de anuncios y si se detectan pueden ser borrados y bloqueada la cuenta. Y finalmente, los usuarios desconfían de anuncios repetidos porque dan mala imagen al utilizar métodos parecidos a los estafadores. Mejor calidad que cantidad.</span>
        </div>
        <div class="advice yellowdark">
            <span class="title d-block">5.- Imágenes</span>
            <span>Es fundamental poner imágenes siempre que éstas aporten información de interés para los usuarios. Se puede decir que un coche está en perfecto estado pero si no se ve, no se cree.</span>
        </div>
        <div class="advice purple">
            <span class="title d-block">6.- Precio</span>
            <span>Un usuario interesado en un artículo no se molestará en ir llamando a los anuncios que no tienen precio sólo para saberlo.</span>
        </div>
        <div class="advice bluedark">
            <span class="title d-block">7.- Datos de contacto</span>
            <span>Es preferible dejar tanto email como teléfono porque hay usuarios que o sólo llaman o sólo mandan emails. El email es además necesario para poder gestionar el anuncio. El email no será mostrado a los usuarios para evitar el spam.</span>
        </div>
        <div class="advice brown">
            <span class="title d-block">Renovar</span>
            <span>Es recomendable renovar los anuncios porque volverán a aparecer al principio del listado. No se puede renovar más de una vez al día.</span>
        </div>
    </div>
</div>

@endsection