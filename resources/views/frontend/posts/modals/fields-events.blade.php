<!-- Modal -->
<div class="modal fade" id="registerEvents" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<Events :districts="{{ $districts }}"></Events>

		</div>
	</div>
</div>