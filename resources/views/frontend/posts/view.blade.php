@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">
                {{ $post->type_posts ? 'PUBLICACIÓN DE DEMANDA' : 'PUBLICACIÓN DE OFERTA' }}
            </h5>
        </header>

        <div class="col-md-9">
        	<div class="card border">
                <div class="card-header">
                    {{ $post->name }} <span class="float-right">{{ $post->created_at }}</span>
                </div>
        		
                @include('frontend.posts.partials.fields-views')
            
        	</div>
        </div>
        <div class="col-md-3">
            
            @include('frontend.posts.partials.menu-vertical')


            <div class="card border mt-3 mb-3">
                <div class="card-header">
                    DETALLES DE CONTACTO
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">

                            @if($post->type_posts == 0)

                            <span class="d-block">
                                <i class="fa fa-map-marker"></i> 
                                <strong class="details float-right">
                                    {{ $post->address }}
                                </strong>
                            </span> 
                            <span class="d-block">
                                <i class="fa fa-map-pin"></i> 
                                <strong class="details float-right">
                                    {{ $post->postal_code }}
                                </strong>
                            </span> 
                            <span class="d-block">
                                <i class="fa fa-envelope"></i>
                                <strong class="details float-right">
                                    {{ $post->email }}
                                </strong>
                            </span>
                            <span class="d-block">
                                <i class="fa fa-phone"></i>
                                <strong class="details float-right">
                                    {{ $post->phone1 }}
                                </strong>
                            </span>
                            <span class="d-block">
                                <i class="fa fa-globe"></i>
                                <strong class="details float-right">
                                    <a href="{{ $post->url }}" class="text-primary" target="_blank">
                                        {{ $post->url }}
                                    </a>
                                </strong>
                            </span>

                            @else

                            <div class="card">
                                <div class="card-body">
                                    <div class="content">Email: <strong class="d-block">{{ $post->email }}</strong></div>
                                    <div class="content">Telefóno 1: <strong class="d-block">{{ $post->phone1 }}</strong></div>
                                    <div class="content">Tele: <strong class="d-block">{{ $post->phone2 }}</strong></div>
                                </div>
                            </div>

                            @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection