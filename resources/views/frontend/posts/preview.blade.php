@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ANUNCIO PRELIMINAR</h5>
        </header>
        <div class="col-md-9">
            
            @include('frontend.session.session')

            <div class="card border p-2 mb-3">
                <div class="card-body">
                    <h4 class="font-weight-light">¿Deseas publicitar tu anuncio en más provincias?</h4>
                    <div class="ml-draft__info">
                        <h6 class="font-weight-bold">{{ $post->name }}</h6>
                        <p>
                            Iniciada desde {{ $post->created_at }}
                        </p>
                    </div>
                    <div class="ml-action__button">
                        <a href="{{ route('posts.shopping_cart', $post) }}" class="btn btn-border btn-primary">Sí</a>
                    </div>
                </div>
            </div>


            @include('frontend.posts.partials.fields-preview')
    
        </div>
  
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

            <div id="map" style="height:400px; margin-top: 10px;"></div>

            <div class="card border mt-3">
                <div class="card-header menu-vertical">
                    <div class="text-center">PROXIMOS EVENTOS</div>
                </div>
                <div class="card-body">
            
                    @forelse ( $user->events as $key => $event )

                        <p class="text-justify">
                            {{ $event->date }} - <span class="text-primary">{{ $event->address }}</span>
                        </p>

                    @empty  

                        <p class="text-justify">
                            No hay eventos proximos
                        </p>

                    @endforelse

                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="lat" value="{{ $post->lat }}">
<input type="hidden" name="lng" value="{{ $post->lng }}">

@endsection


@if($post->directions !== NULL)

    @section('scripts')

    <!-- Google Maps -->
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbapNm5FNXk2Db4pJ47Fr7lKXEsd8jtsA&callback=initMap">
    </script>

    <script>

    let lat = $('input[name=lat]').val();
    let lng = $('input[name=lng]').val();

    function initMap() {
            
        var myLatlng = new google.maps.LatLng(lat, lng);

        var mapOptions = {
            zoom: 12,
            center: myLatlng
        }

        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
        });
    }

    </script>

    @endsection

@endif