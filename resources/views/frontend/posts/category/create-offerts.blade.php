@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">CARACTERÍSTICAS DE SU SERVICIO Y DATOS DE CONTACTO.</h5>
        </header>
  
        <div class="col-md-9">

            @include('frontend.session.session')
            
            {!! Form::model($post, ['url' => ['posts', $post->id], 'method' => 'PUT', 'autocomplete' => 'off', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}
            <div class="card border mb-3">
                <div class="card-body">

                    @include('frontend.posts.partials.fields-offerts')

                    <div class="row mt-4">
                        <div class="col">
                            <input type="submit" class="btn btn-primary float-right" value="PUBLICAR ANUNCIO">
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>

        @include('frontend.posts.modals.fields-images')
        @include('frontend.posts.modals.fields-videos')
        @include('frontend.posts.modals.fields-audios')
        @include('frontend.posts.modals.fields-events')

    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    
Dropzone.options.myAwesomeDropzone  = {

    dictDefaultMessage: 'PINCHE AQUÍ PARA SUBIR ARCHIVOS',
    dictRemoveFile: 'Remover',
    dictMaxFilesExceeded: 'No puedes subir más archivos.',

    maxFilesize: 5,
    addRemoveLinks: true,
    acceptedFiles: ".jpg,.png",

    error: function(file, message) {
        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.message);
    }
};

</script>

<!-- Google Maps -->
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbapNm5FNXk2Db4pJ47Fr7lKXEsd8jtsA&callback=initMap">
</script>

<script>

var map;
var marker;
var coords;

function initMap() {
        
    var myLatlng = new google.maps.LatLng(-25.363882,131.044922);

    var mapOptions = {
        zoom: 12,
        center: myLatlng
    }

    var geocoder = new google.maps.Geocoder();

    document.getElementById('look').addEventListener('click', function() {
        geocodeAddress(geocoder, map);
    });
          
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Drag me!"
    });
        
    marker.addListener('click', function(){
        placeMarker(marker.getPosition());
    });

    marker.addListener('drag', handleEvent);
    marker.addListener('dragend', handleEvent);

}

function handleEvent(event) {

    document.getElementById('id_lat').value = event.latLng.lat();
    document.getElementById('id_lng').value = event.latLng.lng();
}

function placeMarker(cords) {
    
    marker.setPosition(cords);
    document.getElementById("id_lat").value = cords.lat();
    document.getElementById("id_lng").value = cords.lng();
}

function geocodeAddress(geocoder, resultsMap) {

    var address = document.getElementById('address').value;
    
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            placeMarker(results[0].geometry.location);
            
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
    });
}
    
</script>

@endsection