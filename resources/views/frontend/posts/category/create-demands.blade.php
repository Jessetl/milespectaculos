@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')


<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">DATOS DEL ANUNCIO <span class="text-danger">(PANEL DEMANDA DE SERVICIOS)</span></h5>
        </header>

        <div class="col-md-9">

            @include('frontend.session.session')
            
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="pt-3 text-primary">Si así lo desea puedes insertar fotos. <br> Los anuncios con fotos llaman más la atención de posibles compradores.</h5>
                    
                    <div id="dropzone">
                        
                        {!! Form::model($post, ['route' => ['posts.upload', $post->id], 'files' => 'true', 'id' => 'frmImageUpload', 'class' => 'dropzone', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}

                            <div class="fallback">
                                <input name="file" type="file" required/>
                            </div>

                        {!! Form::close() !!}

                    </div>
                   
                </div>
            </div>
        
            {!! Form::model($post, ['url' => ['posts', $post->id], 'method' => 'PUT', 'autocomplete' => 'off']) !!}
            <div class="card border">
                <div class="card-body">
                
                    @include('frontend.posts.partials.fields-demands')

                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Siguiente">
                        </div>
                    </div>
                </div>
                <div class="card-footer pl-3">
                    Los campos con <span class="text-danger">(*)</span> son obligatorios
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>


</div>

@endsection

@section('scripts')

<script type="text/javascript">

Dropzone.autoDiscover = true;

Dropzone.options.frmImageUpload = {

    dictDefaultMessage: 'PINCHE AQUÍ PARA SUBIR ARCHIVOS',
    dictRemoveFile: 'Remover',
    dictMaxFilesExceeded: 'No puedes subir más archivos.',

    maxFilesize: 5,
    addRemoveLinks: true,
    acceptedFiles: ".jpg,.png",

    error: function(file, message) {
        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.message);
    }
};

</script>

@endsection
