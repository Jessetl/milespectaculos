@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ANUNCIO PRELIMINAR</h5>
        </header>
        <div class="col-md-9">
            
            @include('frontend.session.session')

            <div class="card border p-2 mb-3">
                <div class="card-body">
                    <h4 class="font-weight-light">¿Deseas publicitar tu anuncio en más provincias?</h4>
                    <div class="ml-draft__info">
                        <h6 class="font-weight-bold">{{ $post->name }}</h6>
                        <p>
                            Iniciada desde {{ $post->created_at }}
                        </p>
                    </div>
                    <div class="ml-action__button">
                        <a href="{{ route('posts.shopping_cart', $post) }}" class="btn btn-border btn-primary">Sí</a>
                    </div>
                </div>
            </div>

            @include('frontend.posts.partials.fields-previewd')
    
        </div>
  
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

@endsection