@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">SELECCIONA LA ZONA Y TIPO DE ANUNCIO.</h5>
        </header>

        <div class="col-md-9">
            
            @include('frontend.session.session')

            {!! Form::open(['route' => 'posts.new__pts', 'method' => 'POST', 'autocomplete' => 'off']) !!}
            
            <div class="card border">
                <div class="card-header">
                    {{ $category->name }}
                </div>
                <div class="card-body">

        		    @include('frontend.posts.partials.fields-post_type')

                    @if( Auth::user()->premium == (FALSE) AND Auth::user()->role !== 3)

                        <div class="row">
                            <div class="col-md">
                                <div class="form-group">
                                    <div class="text-center">
                                        <span class="text-danger">
                                            Solo si eres socio VIP. Tienes permitido Publicar un Anuncio por cada Provincia.
                                        </span>
                                        <br>
                                        <span>
                                            <a href="{{ route('zone-vip.index') }}">
                                                ¿Deseas conocer todas las ventajas de ser VIP? 
                                            </a>
                                        </span>
                                        <br>
                                        @if(Auth::user()->role !== 3)
                                            <a href="{{ route('subscription') }}" class="btn btn-danger"> DESEO HACERME CLIENTE VIP </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif

                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary float-right" value="Siguiente">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer p-2">
                    <span>Los campos marcados con <span class="text-danger">(*)</span> son obligatorios.</span>
                </div>
            </div>

            {!! Form::close() !!}

        </div>

        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>
</div>

@endsection