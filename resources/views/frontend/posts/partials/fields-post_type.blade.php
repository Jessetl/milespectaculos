<div class="row">
    <div class="col-md-8">
        <label for="type_posts">Oferta/Demanda <span class="text-danger">(*)</span></label>
        <div class="form-group">
            <select id="type_posts" class="form-control {{ $errors->has('type_posts') ? ' is-invalid' : '' }}" name="type_posts" required>
                <option value="0">Ofrezco mis servicios como empresa o artista (Oferta)</option>
                <option value="1">Busco empresa o artista para contratar (Demanda)</option>
            </select>
            @if ($errors->has('type_posts'))
                <div class="invalid-feedback">{{ $errors->first('type_posts') }}</div>
            @endif
        </div>
    </div>
    @if(Auth::user()->premium)
    <div class="col d-none" id="lengthBlock">
        <div class="form-group">
            <label for="length"> Duración <span class="text-danger">(*)</span></label>
            <select id="length" class="form-control {{ $errors->has('length') ? ' is-invalid' : '' }} popover-dismiss" name="length" data-toggle="popover" data-placement="top" data-content="Selecciona la duración de tu anuncio.">
                    <option value="1">30 DÍAS</option>
                    <option value="2">60 DÍAS</option>
                    <option value="3">90 DÍAS</option>
            </select>
        </div>
    </div>
    @else
    <div class="col">
        <div class="form-group">
            <label for="length"> Duración <span class="text-danger">(*)</span></label>
            <select id="length" class="form-control {{ $errors->has('length') ? ' is-invalid' : '' }} popover-dismiss" name="length" data-toggle="popover" data-placement="top" data-content="Selecciona la duración de tu anuncio.">
                    <option value="1">30 DÍAS</option>
                    <option value="2">60 DÍAS</option>
                    <option value="3">90 DÍAS</option>
            </select>
        </div>
    </div>
    @endif
</div>

<input type="hidden" name="category" value="{{ $category->id }}">

@if(Auth::user()->premium)

@if ($errors->has('circuits'))
    <div class="row">
        <div class="col">
            <div class="form-group">
                <div class="text-danger">{{ $errors->first('circuits') }}</div>
            </div>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-md-6">
        <div class="well" style="max-height: 300px;overflow: auto;">
            <ul class="list-group checked-list-box">
                @foreach($districts as $key => $district)

                    <li class="list-group-item">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="district" value="{{ $district->id }}">
                            <label class="form-check-label" for="defaultCheck1">
                                {{ $district->name }}
                            </label>
                        </div>
                    </li>

                @endforeach
            </ul>
        </div>
    </div>
    <div class="col-md-6" id="ajax__dt"></div>
</div>

@else

<div class="row">
    <div class="col-md">
        <label for="district"> Provincia <span class="text-danger">(*)</span></label>
        <div class="form-group">
            <select id="district" class="form-control {{ $errors->has('district') ? ' is-invalid' : '' }}" name="district" required/>
                <option value="" disabled selected>Selecciona una opción</option>
                
                @foreach($districts as $key => $district)

                    <option value="{{ $district->id }}">{{ $district->name }}</option>

                @endforeach

            </select>   
        </div>
    </div>
    <div class="col-md">
        <label for="circuit"> Población <span class="text-danger">(*)</span></label>
        <div class="form-group">
            <input type="text" id="circuit" class="form-control {{ $errors->has('circuit') ? ' is-invalid' : '' }}" name="circuit" required/>
            @if ($errors->has('circuit'))
                <div class="invalid-feedback">{{ $errors->first('circuit') }}</div>
            @endif
        </div>
    </div>
</div>

@endif

<hr>

