<div class="row pt-2">
    <div class="col">
        <label for="title">Título <span class="text-danger">(*)</span></label>
        <input type="text" id="title" name="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }} 
        popover-dismiss" data-toggle="popover" data-placement="top" data-content="Escribe el titulo de tu demanda, Utiliza palabras que la describan facilmente" 
        value="{{ old('title') }}" required/>
        @if ($errors->has('title'))
            <div class="invalid-feedback">{{ $errors->first('title') }}</div>
        @endif
    </div>
    <div class="col">
        <label for="email">Email <span class="text-danger">(*)</span></label>
        <input type="email" id="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} popover-dismiss" 
        data-toggle="popover" data-placement="top" data-content="Escribe un E-Mail valido con el formato: ejemplo@ejemplo.com" 
        value="{{ old('email') }}" required/>
        @if ($errors->has('email'))
            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
        @endif
    </div> 
</div>
<div class="row pt-2">
    <div class="col">
        <label for="phone">Teléfono 1 <span class="text-danger">(*)</span></label>
        <input type="text" id="phone" name="phone" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }} popover-dismiss" 
        data-toggle="popover" data-placement="top" data-content="Escribe tu telefono principal sin espacios, guiones ni puntos. ejemplo: 916341234"
        value="{{ old('phone') }}" maxlength="15" required/>
        @if ($errors->has('phone'))
            <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
        @endif
    </div>
    <div class="col">
        <label for="phone_two">Teléfono 2</label>
        <input type="text" id="phone_two" name="phone_two" class="form-control {{ $errors->has('phone_two') ? ' is-invalid' : '' }} popover-dismiss" 
        data-toggle="popover" data-placement="top" data-content="Escribe tu telefono secundario sin espacios, guiones ni puntos. ejemplo: 916341234" 
        value="{{ old('phone_two') }}" maxlength="15"/>
        @if ($errors->has('phone_two'))
            <div class="invalid-feedback">{{ $errors->first('phone_two') }}</div>
        @endif
    </div>
</div>
<div class="row pt-2">
    <div class="col">
        <label for="content">Descripción <span class="text-danger">(*)</span></label>
        <textarea id="content" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }} popover-dismiss" name="content" data-toggle="popover" data-placement="top" data-content="Describe tu demanda de la manera mas clara y exacta posible." rows="3">{{ old('content') }}</textarea>
        @if ($errors->has('content'))
            <div class="invalid-feedback">{{ $errors->first('content') }}</div>
        @endif
    </div>
</div>
