<input type="hidden" value="{{ $post->id }}" name="post">

@if( Auth::user()->premium == (TRUE) )

<div class="card-title">
    <h6 class="text-primary">
        Seleccione la foto que desea que aparezca en su portada.
    </h6>
</div>

<hr>

<div class="row">
    <div class="col">
        <label for="file">Portada</label>
        <div class="form-group">
            <input id="file" type="file" class="form-control-input {{ $errors->has('file') ? ' is-invalid' : '' }}" name="file" value="{{ old('file') }}"/>
            @if ($errors->has('file'))
                <div class="invalid-feedback">{{ $errors->first('file') }}</div>
            @endif
            <small class="form-text my-2">
                La imagen debe ser rectangular de forma panorámica. 
            </small>
        </div>
    </div>
</div>
@endif

<div class="card-title">
    <h6 class="text-primary">
        Rellene sus datos de contacto y detalles del servicio que ofrece o demandas
    </h6>
</div>

<hr>

<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="title">Título <span class="text-danger">(*)</span></label>
            <input type="text" id="title" name="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }} popover-dismiss" 
            value="{{ old('title') }}" data-toggle="popover" data-placement="top" data-content="Agrega un titulo que facilite la búsqueda de tu anuncio." maxlength="75" required/>
            @if ($errors->has('title'))
                <div class="invalid-feedback">{{ $errors->first('title') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="content">Descripción <span class="text-danger">(*)</span></label>
            <textarea id="content" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }} popover-dismiss" name="content" rows="6" data-toggle="popover" data-placement="top" data-content="Describe tu oferta de la manera mas clara y exacta posible." required>{{ old('content') }}</textarea>
            @if ($errors->has('content'))
                <div class="invalid-feedback">{{ $errors->first('content') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="one">Teléfono 1 <span class="text-danger">(*)</span></label>
            <input type="text" id="one" class="form-control {{ $errors->has('one') ? ' is-invalid' : '' }} popover-dismiss" name="one" data-toggle="popover" data-placement="top" data-content="Escribe un telefóno de contacto sin espacios, guiones ni puntos. ejemplo: 916341234" 
            value="{{ old('one') }}" maxlength="15" required/>
            @if ($errors->has('one'))
                <div class="invalid-feedback">{{ $errors->first('one') }}</div>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="two">Teléfono 2</label>
            <input type="text" id="two" class="form-control {{ $errors->has('two') ? ' is-invalid' : '' }}" 
            name="tow" value="{{ old('two') }}" maxlength="15"/>
            @if ($errors->has('two'))
                <div class="invalid-feedback">{{ $errors->first('two') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="email">E-mail <span class="text-danger">(*)</span></label>
            <input type="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} popover-dismiss" name="email" data-toggle="popover" data-placement="top" data-content="Escribe un E-Mail valido con el formato: ejemplo@ejemplo.com" value="{{ old('email') }}" maxlength="75" required/>
            @if ($errors->has('email'))
                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="url">Sitio Web</label>
            <input type="text" class="form-control {{ $errors->has('url') ? ' is-invalid' : '' }} popover-dismiss" name="url" data-toggle="popover" data-placement="top" data-content="Si posees página Web, escribe aquí la URL de la página, Ejemplo: www.milespectaculos.com" value="{{ old('url') }}"/>
            @if ($errors->has('url'))
                <div class="invalid-feedback">{{ $errors->first('url') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <label for="address">Lugar</label>
        <div class="input-group">
            <input class="form-control" id="address" name="directions" value="{{ old('directions') }}" type="text"/>
            <span class="input-group-append">
                <input class="btn btn-primary" id="look" type="button" value="BUSCAR">
            </span>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col">  
        <div class="form-group">  
            <div id="map" style="height:400px;"></div>
            <input id="id_lat" type="text" style="display:none;" name="latInput" value="{{ old('latInput') }}"/>
            <input id="id_lng" type="text" style="display:none;" name="lngInput" value="{{ old('lngInput') }}"/>
        </div>
    </div>
</div>

<div class="card-title">
    <h6 class="text-primary">
        Cargue las imágenes que desea que aparezca en su anuncio.
    </h6>
</div>

<hr>

<h6 class="card-title"> 
    Nota: <span class="text-danger">(Los anuncios gratis solo permiten {{ $configuration['image']->meta_value }} fotos)</span>
</h6>

<div class="row">
    <div class="col">
        <div class="form-group">
            <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#uploadImage">CARGAR IMÁGENES</button>
        </div>
    </div>
</div>

<div class="card-title">
    <h6 class="text-primary">
        Cargue los enlaces de videos de YouTube que desea que aparezcan en su anuncio
    </h6>
</div>

<hr>

<h6 class="card-title"> Nota: <span class="text-danger">(Los anuncios gratis solo permiten {{ $configuration['video']->meta_value }} videos)</span></h6>

<div class="row">
    <div class="col">
        <div class="form-group">
            <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#uploadVideo">CARGAR VIDEOS</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group font-weight-bold">
           Sube tu video a <a href="{{ route('upload.video', $post) }}" target="_blank"> YouTube </a> con nostros. 
        </div>
    </div>
</div>

<div class="card-title">
    <h6 class="text-primary">
        Ponga precio a su servicio o seleccione entre; a convenir, a consultar o otros.
    </h6>
</div>

<hr>

<h6 class="card-title"> 
    Puede ajustar su precio cuando lo desee, incluso después de que se publique su lista.
</h6>

<div class="row pt-2">
    <div class="col">
        <div class="form-group">
            <label for="type_amount">Precio del servicio <span class="text-danger">(*)</span></label>
            <select id="type_amount" class="form-control {{ $errors->has('type_amount') ? ' is-invalid' : '' }} popover-dismiss" name="type_amount" required>
                <option value="A Convenir">A CONVENIR</option>
                <option value="A Consultar">A CONSULTAR</option>
                <option value="Otro">OTRO</option>
            </select>
            @if ($errors->has('type_amount'))
                <div class="invalid-feedback">{{ $errors->first('type_amount') }}</div>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="discount">Descuento Especial <span class="text-danger">%</span></label>
            <input type="number" id="discount" class="form-control {{ $errors->has('discount') ? ' is-invalid' : '' }} popover-dismiss" name="discount" data-toggle="popover" data-placement="top" data-content="Descuento especial para profesionales" value="{{ old('discount') }}"/>
            @if ($errors->has('discount'))
                <div class="invalid-feedback">{{ $errors->first('discount') }}</div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="amount">¿Cuánto quieres que se enumere? <span class="text-danger">(*)</span></label>
            <input type="number" id="amount" class="form-control {{ $errors->has('amount') ? ' is-invalid' : '' }} popover-dismiss" step=".01" name="amount" value="{{ old('amount') }}" data-toggle="popover" data-placement="top" data-content="Coloca una suma aproximada del costo de tu servicio, agrega solo dos decimales en caso de necesitarlo. Ejemplo: 1485,02" disabled required/>
            @if ($errors->has('amount'))
                <div class="invalid-feedback">{{ $errors->first('amount') }}</div>
            @endif
        </div>
    </div>
</div>

<div class="card-title">
    <h6 class="text-primary">
        Seleccione todos sus enlaces de MP3 de SoundCloud que desee que aparezcan
    </h6>
</div>

<hr>

<h6 class="card-title"> 
    Nota: <span class="text-danger">(Los anuncios gratis solo permiten {{ $configuration['audio']->meta_value }} audios)</span>
</h6>

<div class="row">
    <div class="col">
        <div class="form-group">
            <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#uploadAudios">CARGAR AUDIOS</button>
        </div>
    </div>
</div>  

<div class="card-title">
    <h6 class="text-primary">
        Añada sus proximos Eventos si lo desea
    </h6>
</div>

<hr>

<h6 class="card-title"> 
    Nota: <span class="text-danger">(Los anuncios gratuitos solo permiten 3 Eventos)</span>
</h6>

<div class="row">
    <div class="col">
        <div class="form-group">
            <button type="button" class="btn btn-border btn-primary" data-toggle="modal" data-target="#registerEvents">AÑADIR UN EVENTO</button>
        </div>
    </div>
</div>

<div class="card-title">
    <h6 class="text-primary">
        Comparte tu anuncio en tus redes sociales si lo deseas.
    </h6>
</div>

<hr>

<div>
    <strong>
        <a href="mailto:name@milespectaculos.com" class="text-primary"> 
            <h5><i class="fa fa-envelope-open-o"></i>
            Enviar por email</h5>
        </a>
    </strong>
</div>
<div>
    <strong>
        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
            <h5><i class="fa fa-facebook-official"></i>
            Facebook</h5>
        </a>
    </strong>
</div>
<div>
    <strong> 
        <a target="_blank" href="http://twitter.com/share?url=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
            <h5><i class="fa fa-twitter-square"></i>
            Twitter</h5>
        </a>
    </strong>
</div>
<div>
    <strong>
        <a target="_blank" href="http://pinterest.com/pin/create/button/?url=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
            <h5><i class="fa fa-pinterest-square"></i>
            Pinterest</h5>
        </a>
    </strong>
</div>
<div>
    <strong>
        <a target="_blank" href="https://plus.google.com/share?url=https://www.milespectaculos.com/{{ $post->slug }}/post" class="text-primary">
            <h5><i class="fa fa-google-plus-square "></i>
            Google+</h5>
        </a>
    </strong>
</div>