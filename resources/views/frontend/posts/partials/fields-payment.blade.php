<div class="row">
    <div class="col-md-6">
        <div class="well" style="max-height: 300px;overflow: auto;">
            <ul class="list-group checked-list-box">
                @foreach($districts as $key => $district)

                    <li class="list-group-item">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="district" value="{{ $district->id }}">
                            <label class="form-check-label" for="defaultCheck1">
                                {{ $district->name }}
                            </label>
                        </div>
                    </li>

                @endforeach
            </ul>
        </div>
    </div>
    <div class="col-md-6" id="ajax__dt"></div>
</div>
<div class="row">
    <div class="col">
        <h5 class="text-center text-primary pt-4">
            Ha seleccionado <span id="payment_ct">0</span> provincias por <span id="amount"></span> euro + 21 % IVA total = <span class="text-danger" id="total">0</span> euros mes
        </h5>
    </div>
</div>

<input type="hidden" name="post" value="{{ $post->slug }}">