<div class="card border mb-3">
    <div class="card-header">
        DESCRIPCIÓN
    </div>
    <div class="card-body">
        <p class="text-justify">
            {{ $post->content }}
        </p>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="p-3">
                <h3 class="text-danger"> OFERTA </h3>
            </div>
            @if($post->type_posts == 0)
            <div class="p-3">
                <h3 class="text-danger d-inline text-uppercase">
                    {{ 
                        strcmp($post->type_amount, 'Otro') ? 
                        $post->type_amount : $post->amount . 
                        ' € ' 
                    }} 
                </h3>
            </div>

            @endif
            <div class="p-3">
                @if ( $post->type )
                    <button class="btn btn-border btn-success"> 
                        PROFESIONAL 
                    </button>
                @else 
                    <button class="btn btn-border btn-warning text-white"> 
                        PARTICULAR 
                    </button>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="card border mb-3">
    <div class="card-header">
        GALERIA
    </div>
    <div class="card-body">
        <h1 class="card-title">
            <span class="font-weight-light">{{ $post->name }}</span>
        </h1>
        <h4>
            <small class="font-weight-light">
                <i class="fa fa-suitcase"></i> 
                <small class="text-primary">
                    {{ $post->category->name }}
                </small>
                <small class="font-weight-light">
                / {{ $post->created_at }} by {{ $user->name }}
                </small>
            </small>
        </h4>

        @if ( count($images) > 0)

            <div id="image" class="text-center p-2">
                <img src="{{ asset('/storage/posts/' . $images[0]) }}" width="600" height="400" class="img-fluid" />
            </div>
            <div class="content-slider">
                <ul id="GallerylightSlider" class="cS-hidden">
                    @foreach( $images as $key => $image )
                        <li class="lslide"> 
                            <a href="#" class="image" rel="{{ asset('/storage/posts/' . $image) }}">
                                <img src="{{ asset('/storage/posts/' . $image) }}" width="210" height="120"/>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

        @else

            <div id="image" class="text-center p-2">
                <img src="{{ asset('img/icon.png') }}" width="600" height="400"/>
            </div>

        @endif
            
    </div>
</div>

@if ( count($movies) > 0 )

<div class="card border mb-3">
    <div class="card-header">
        VIDEOS
    </div>
    <div class="card-body">
        <div class="row">
            @foreach ( $movies as $key => $movie )

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <iframe width="100%" height="240" src="{{ $movie }}" 
                                frameborder="0" allow="autoplay; encrypted-media" 
                                allowfullscreen>
                            </iframe>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
</div>

@endif

@if ( count($sounds) > 0 )

<div class="card border mt-3 mb-3">
    <div class="card-header">
        MP3
    </div>
    <div class="card-body">
        <div class="row">
            @foreach ( $sounds as $key => $sound )

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <iframe 
                            width="100%" 
                            scrolling="no" 
                            height="100" 
                            frameborder="no" 
                            src="https://w.soundcloud.com/player/?url={{ $sound }}">   
                            </iframe>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
</div>

@endif
