<div class="card border mb-4">
    <div class="card-header">
        {{ $post->name }} <span class="float-right">{{ $post->code }} - {{ $post->created_at }}</span>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <p class="text-justify title-description-ad">
                    {{ $post->content }}
                </p>
            </div>
            <div class="col-md-4 pb-5">
                <div class="image" style="background-image: url({{ postLoadImage($post) }})"></div>
            </div>

            @if($post->type_posts == 0)

            <div class="p-3">
                <h3 class="text-danger d-inline text-uppercase">
                    {{ strcmp($post->type_amount, 'Otro') ? $post->type_amount : $post->amount . ' € ' }} 
                </h3>
            </div>

            @endif
            <div class="p-3">
                @if ( $post->type )
                    <button class="btn btn-border btn-success"> PROFESIONAL </button>
                @else 
                    <button class="btn btn-border btn-warning text-white"> PARTICULAR </button>
                @endif
            </div>
            <div class="p-3">
                <button class="btn btn-border btn-warning text-white"><i class="fa fa-thumbs-up"></i> FANS {{ $post->countFollowers($post->user) }}</button>
            </div>
            
        </div>
    </div>
    <div class="card-footer">
        <div class="d-flex flex-wrap justify-content-start">
            
            <div class="p-1">
                <button type="button" class="btn btn-border btn-primary btn-sm" 
                    data-toggle="modal" data-target="" disabled>
                    <i class="fa fa-radius fa-volume-control-phone text-danger"></i> 
                    CONTACTAR 
                </button>
            </div>
            
            <div class="p-1">
                <button type="button" class="btn btn-border btn-primary btn-sm" 
                    data-toggle="modal" data-target="" disabled>
                    <i class="fa fa-radius fa-share-alt text-danger"></i> 
                    COMPARTIR 
                </button>
            </div>
           
           <div class="p-1">
                <button type="submit" class="btn btn-border btn-primary btn-sm js__favorite" id="{{ $post->slug }}" disabled>
                    <i data-ref="{{ $post->slug }}" class="fa fa-radius fa-heart {{ Auth::user()->isFavorite($post) ? 'text-danger' : '' }}"></i> 
                    FAVORITO 
                </button>
            </div>

            <div class="p-1">
                <a href="{{ route('reports.create', $post) }}" class="btn btn-border btn-primary btn-sm disabled" 
                    target="_blank">
                    <i class="fa fa-radius fa-bullhorn text-danger"></i> 
                    DENUNCIAR 
                </a>
            </div>

            <div class="p-1">
                <button type="button" class="btn btn-border btn-primary btn-sm" 
                    data-toggle="modal" data-target="" disabled>
                    MP3 
                    <i class="fa fa-camera fa-radius text-danger"></i>
                    <i class="fa fa-headphones fa-radius text-danger"></i> 
                </button>
            </div>

            <div class="p-1">
                <a href="javascript:void(0)" class="btn btn-border btn-primary btn-sm disabled">
                    <i class="fa fa-radius fa-user"></i>  VER PERFIL 
                </a>    
            </div>
        </div>
    </div>
</div>