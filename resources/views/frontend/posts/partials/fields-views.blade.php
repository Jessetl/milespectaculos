<div class="card-body">
	<div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <p class="text-justify">
                        {{ $post->content }}
                    </p>
                </div>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="clearfix" style="max-width:474px;">
                <ul id="imageslider" class="cS-hidden">
                    
                    @forelse ( $images as $key => $image )
                    <li> 
                        <img src="{{ asset('/storage/posts/' . $image) }}" width="340" height="240" />
                    </li>
                    @empty
                    <li> 
                        <img src="{{ asset('img/icon.png') }}"/>
                    </li>
                    @endforelse

                </ul>
            </div>
        </div>  

        @if( $post->type_posts == 0 )

            @if ( count($movies) > 0 )

                <div class="col-md-12">
                    <div class="card border mt-3 mb-3">
                        <div class="card-header">
                            VIDEOS
                        </div>
                        <div class="row">

                        @foreach ( $movies as $key => $movie )

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <iframe width="100%" height="240" src="{{ $movie }}" 
                                            frameborder="0" allow="autoplay; encrypted-media" 
                                            allowfullscreen>
                                        </iframe>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                        </div>
                    </div>
                </div>

            @endif

            @if ( count($sounds) > 0 )

            <div class="col-md-12">
                <div class="card border mt-3 mb-3">
                    <div class="card-header">MP3</div>
                    <div class="row">
                    
                    @foreach ( $sounds as $key => $sound )

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <iframe 
                                    width="100%" 
                                    scrolling="no" 
                                    height="100" 
                                    frameborder="no" 
                                    src="https://w.soundcloud.com/player/?url={{ $sound }}">   
                                    </iframe>
                                </div>
                            </div>
                        </div>

                    @endforeach
                    </div>
                </div>
            </div>

            @endif

        @endif

    </div>
</div>