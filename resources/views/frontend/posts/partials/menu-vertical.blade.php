<div class="card">
    <div class="card-header menu-vertical">
        <div class="text-center">MENU</div>
    </div>
    <div class="card-body">
        <div class="row justify-content-around">
            <div class="">
                <a href="{{ url('posts/create') }}">
                    <img src="{{ asset('/img/buttons/publicar-anuncio.png') }}" width="220" height="45">
                </a>
            </div>
            <div class="">
                <a href="{{ url('my-posts') }}">
                    <img src="{{ asset('/img/buttons/modificar-anuncio.png') }}" width="220" height="45">
                </a>
            </div>
            <div class="">
                @if(auth()->guest())
                    <a href="{{ route('my_favorites') }}">
                        <img src="{{ asset('./img/buttons/anuncios-favoritos.png') }}" width="220" height="45">
                    </a>
                @else
                    <a href="{{ url('favorites') }}">
                        <img src="{{ asset('/img/buttons/anuncios-favoritos.png') }}" width="220" height="45">
                    </a>
                @endif
            </div>
            <div class="">
                <a href="{{ url('events') }}">
                    <img src="{{ asset('/img/buttons/arte-eventos.png') }}" width="220" height="45">
                </a>
            </div>
            <div class="">
                <a href="{{ url('zone-vip') }}">
                    <img src="{{ asset('/img/buttons/zona-vip.png') }}" width="220" height="45">
                </a>
            </div>
        </div>
    </div>
</div>
<div class="card my-3">
    <div class="card-body">
        <div class="row">
            <div class="col text-center">
                <small class="d-block mb-2">
                    ¿Falta de tiempo para crear tu anuncio?
                </small>
                <a href="{{ route('budgets.ad') }}" class="btn btn-border btn-primary">
                    Déjalo de nuestra mano
                </a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col text-center">
                <small class="d-block mb-2">
                    ¿Falta de tiempo para organizar tu evento?
                </small>
                <a href="{{ route('budgets.event') }}" class="btn btn-border btn-primary">
                    Te lo organizamos
                </a>
            </div>
        </div>
    </div>
</div>