@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ESCOGE LA CATEGORÍA QUE MÁS SE ADAPTE AL ANUNCIO QUE DESEA PUBLICAR.</h5>
        </header>

        <div class="col-md-9">
            <div class="card border">
                <div class="card-body">
                	<h5 class="card-title text-center">
                        <a href="{{ route('advice') }}" target="_blank" class="text-danger"> 
                            <u> 8 CONSEJOS IMPRESCINDIBLES PARA OBTENER MEJORES RESULTADOS </u> 
                        </a>
                    </h5>
                	<span class="card-title">
                        En milespectaculos.com, Usted podrá poner su anuncio Gratis. Miles de personas visitan esta tabla de anuncios
                    </span>
                	<div class="categories my-2">
                		<ul class="category">
                		@foreach($categories as $key => $category)
                			<li class="liClose text-primary"><i class="fa fa-plus-square-o"></i> 
                				{{ $category->name }}
	                			<ul>
	                				@foreach($category->category_type as $key => $subcategories)
                                        @if ( $subcategories->name !== 'Otros' AND $subcategories->name !== 'otros' )
	                					<li>
                                            <a href="{{ route('posts.new__ptc', $subcategories) }}" class="text-primary">{{ $subcategories->name }}</a>
                                        </li>

                                        @endif
	                				@endforeach

                                    <li>
                                        <a href="{{ route('posts.new__ptc', 'otros') }}" class="text-primary">Otros</a>
                                    </li>
	                			</ul>
	                		</li>
                		@endforeach
                		</ul>
                	</div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>
</div>

@endsection