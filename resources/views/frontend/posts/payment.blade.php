@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

{!! Form::open(['route' => 'posts.payment', 'method' => 'POST', 'autocomplete' => 'off']) !!}

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">SELECCIONE LA COBERTURA DEL SERVICIO QUE OFRECE SU EMPRESA O ESPECTACULO</h5>
        </header>
        <div class="col-md-9">
        	<div class="card border">
        		<div class="card-body">

                    @include('frontend.posts.partials.fields-payment')

                </div>
        	</div>
        </div>
        <div class="col-md-3">
            
            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>

    {!! Form::close() !!}

</div>

@endsection