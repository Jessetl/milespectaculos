@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')


<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">SELECCIONE LA COBERTURA DEL SERVICIO QUE OFRECE SU EMPRESA O ESPECTACULO</h5>
        </header>
        
        <div class="col-md-9">
            
            @include('frontend.session.session')
            
            {!! Form::open(['route' => 'shopping.items', 'method' => 'POST', 'autocomplete' => 'off']) !!}
        
        	<div class="card border">
        		<div class="card-body">

                    <h5 class="card-title text-primary text-center pb-4">
                        (De esta forma ahorrará tiempo en exponer un anuncio por ciudad)
                    </h5>

                    @include('frontend.posts.partials.fields-payment')

                    <div class="row">
                        <div class="col pt-4">
                            <a href="{{ route('view.post', $post) }}" class="btn btn-border btn-primary text-danger">CANCELAR</a>
                            <button type="submit" class="btn btn-border btn-primary float-right">ACEPTAR COMPRA</button>
                        </div>
                    </div>
                </div>
        	</div>

            {!! Form::close() !!}

        </div>
        <div class="col-md-3">
            
            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>


</div>

@endsection