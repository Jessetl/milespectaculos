@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5>DENUNCIAR.</h5>
        </header>

        <div class="col-md-9">
            
            @include('frontend.session.session')

        	{!! Form::open(['route' => ['reports.store', $post], 'method' => 'POST', 'autocomplete' => 'off']) !!}
            <div class="card border">
            	<div class="card-body">
                    <h6 class="card-title">Ayúdanos marcando este anuncio si piensas que..</h6>

                    @include('frontend.reports.partials.fields')

                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Denunciar">
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>
</div>

@endsection