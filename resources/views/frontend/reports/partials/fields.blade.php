<div class="row pt-2">
    <div class="col">
        <label for="">¿Porque? <span class="text-danger">(*)</span></label>
        <select class="form-control" name="reason" required>
        	<option value="es una broma">.. es una broma</option>
        	<option value="simplemente, no me gusta">.. no me gusta</option>
        	<option value="está mal clasificado">.. está mal clasificado</option>
        	<option value="contiene palabras gancho">.. contiene palabras gancho</option>
        	<option value="está repetido">.. esta repetido</option>
        	<option value="es una estafa">.. es una estada</option>
        	<option value="el teléfono es incorrecto">.. el teléfono es incorrecto</option>
        	<option value="es profesional y dice ser particular">.. es profesional y dice ser particular</option>
        	<option value="obsoleto, ya está vendido">.. obsoleto, ya está vendido</option>
        </select>
    </div>
</div>
<div class="row pt-2">
	<div class="col">
		<label for="">Mensaje</label>
		<textarea class="form-control" name="content" rows="3"></textarea>
	</div>
</div>