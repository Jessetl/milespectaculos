@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">TUS NOTIFICACIONES RECIENTES</h5>
        </header>

        <div class="col-md-12">
        	
        	@include('frontend.session.session')

        	<table class="table table-bordered bg-white">
			  	<thead>
			    	<tr>
			    		<th>#</th>
			    		<th>Interesado</th>
			    		<th>Anuncio de interes</th>
			    		<th>Tipo de evento</th>
			    		<th>Día del Evento</th>
			    		<th>Acciones</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@forelse($mails as $key => $mail)
			    	<tr>
			    		<td>
			    			{{ $key + 1 }}
			    		</td>
			    		<td>
			    			{{ $mail->usr->name }}
			    		</td>
			    		<td>
			    			{{ $mail->pts->name }}
			    		</td>
			    		<td>
			    			{{ $mail->type }}
			    		</td>
			    		<td>
			    			{{ $mail->dayevent->format('d-m-Y') }}
			    		</td>
			    		<td class="text-center">
			    			<a href="{{ route('notify.show', $mail) }}" class="btn btn-primary">Ver más</a>
			    		</td>
			    	</tr>
			    	@empty
			    	<tr>
			    		<td colspan="6">No tienes notificaciones actualmente</td>
			    	</tr>
			    	@endforelse
			    </tbody>
			</table>
			{!! $mails->render() !!}
        </div>
    </div>
</div>

@endsection