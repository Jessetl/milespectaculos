@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">NOTIFICACIÓN</h5>
        </header>

        <div class="col-md-9">

        	@include('frontend.session.session')

        	<div class="card border">
        		<div class="card-body mx-3">
        			<p class="font-weight-normal text-justify my-2">
        				Estimados señores, un usuario esta interesado en la contratación de sus servicios para la fecha <strong class="text-uppercase"> {{ $notify->dayevent->format('l d, F Y') }}</strong>, Hora del evento a las <strong class="text-uppercase"> {{ $notify->hourevent }} hrs. </strong> Tipo de evento <strong class="text-uppercase"> {{ $notify->type }}</strong>, Nombre del interesado <strong class="text-uppercase"> {{ $notify->usr->name }}</strong>, Lugar del evento <strong class="text-uppercase"> {{ $notify->place }} </strong> Provincia <strong class="text-uppercase"> {{ $notify->country }}</strong>. 
        				@if( !\Auth::user()->premium AND !$notify->authOdata ) Si desea sus datos de contacto para ponerse en contacto con el usuario, milespectáculos le ofrece tres opciones. 
        				@elseif ($notify->authOdata) Puedes contactar a este usuario mediante su correo electrónico <strong> {{ $notify->usr->email }} </strong> 
	        				@if( !empty($notify->usr->phone) ) o su número de teléfono {{ $notify->usr->phone }} 
	        				@endif 

        				@else Puedes contactar a este usuario mediante su correo electrónico <strong> {{ $notify->usr->email }} </strong> 
	        				@if( !empty($notify->usr->phone) ) o su número de teléfono {{ $notify->usr->phone }} 
	        				@endif 
        				@endif
        			</p>

        			@if( !\Auth::user()->premium AND !$notify->authOdata )
        			<p class="font-weight-normal text-justify mt-5">
        				<ul>
        					<li>
        						3,00 € + 21% IVA = <strong class="text-danger"> 3,63 € </strong> mediante transferencia PAYPAL
        					</li>
        					<li>
        						Contratar nuestra promoción VIP anual <br>
        						OFERTA ESPECIAL SOLO ESTE MES <br>
        						12,00 € + 21% IVA = <strong class="text-danger"> 14,52 € </strong> anual. mediante transferencia PAYPAL
        					</li>
        					<li>
        						Nosotros gestionemos y fidelizamos la contratación con el cliente bajo un 20% de comisión de su cache en concepto de gestión de representación.
        					</li>
        				</ul>
        			</p>
        			@endif
        			<div class="row justify-content-center mt-5">
        				@if( !\Auth::user()->premium AND !$notify->authOdata )
	        			<div class="col-md text-center">
	        				<small class="text-primary">Accede a datos de contacto</small>
	        				<a href="{{ route('billings.notify', $notify) }}" class="btn btn-border btn-warning btn-block text-white font-weight-bold">por 3,63 €</a>
	        			</div>
	        			<div class="col-md text-center">
	        				<small class="text-primary">Deseo ser socio VIP</small>
	        				<a href="{{ route('subscription') }}" class="btn btn-border btn-warning btn-block text-white font-weight-bold">por 14,52 € ANUAL</a>
	        				<a href="{{ url('prices') }}" class="text-danger">Ventajas de ser miembro VIP</a>
	        			</div>
	        			@endif
	        			@if ( !$notify->manage )
	        			<div class="col-md-4 text-center">
	        				<small class="text-primary">Deseo que me lo gestionen</small>
	        				<a href="{{ route('notify.manage', $notify) }}" class="btn btn-border btn-warning btn-block text-white font-weight-bold">20% comisión</a>
	        			</div>
	        			@endif
        			</div>
        			@if ( $notify->manage AND !$notify->authOdata )
        			<div class="row justify-content-center mt-5">
        				<p class="font-weight-normal text-justify my-2 text-danger text-uppercase">
        					Nuestro equipo esta gestionando tu anuncio.
        				</p>
        			</div>
        			@endif
        		</div>
        	</div>
        </div>

        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')
            
        </div>
    </div>
</div>
@endsection