@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')


<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">NOTIFICACIÓN DE CONTACTO</h5>
        </header>

        <div class="col-md-9">
            <div class="card border mb-3">
                <div class="card-header">
                    <span class="font-weight-normal">De:</span>
                    {{ $mail->names }}
                </div>
                <div class="card-body">
                    <h6 class="card-title">
                        E-mail: <span class="font-weight-light">{{ $mail->email }}</span>
                    </h6>
                    <div class="row">
                        <div class="col">
                            <p class="font-weight-light">
                                Saludos,
                            </p>
                            <p class="font-weight-normal">
                                {{ $mail->message }}
                            </p>
                            
                            <p class="font-weight-light">
                                Enviado {{ $mail->created_at }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <a href="{{ route('mail.answer', $mail->id) }}" class="btn btn-primary btn-border">Responder</a>
                        </div>
                    </div>

                    <hr>

                    <h6 class="card-title">
                        Respuestas
                    </h6>
                    @forelse($mail->answers as $key => $answer)
                    <div class="row">
                        <div class="col">
                            <em>
                                - {{ $answer->answer }}
                            </em>
                        </div>
                    </div>
                    @empty
                    <div class="row">
                        <div class="col">
                            <p class="font-weight-normal">
                                No tienes ningúna respuesta.
                            </p>
                        </div>
                    </div>
                    @endforelse
            	</div>
            </div>
        </div>
         <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')
            
        </div>
    </div>
</div>

@endsection