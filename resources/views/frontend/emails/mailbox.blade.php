@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">CONTACTAR CON EL PROPIETARIO DEL ANUNCIO</h5>
        </header>

        <div class="col-md-6">
            
            @include('frontend.session.session')

            {!! Form::open(['route' => 'post.sendmail', 'method' => 'POST', 'autocomplete' => 'off']) !!}
            
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="nombre"> Nombre <span class="text-danger">(*)</span></label>
                                <input type="text" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} popover-dismiss" name="name" value="{{ old('name') }}" data-toggle="popover" data-placement="top" data-content="Escribe tu nombre completo" required>
                                @if ($errors->has('name'))
                                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="email"> Correo electronico <span class="text-danger">(*)</span></label>
                                <input type="text" autocomplete="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} popover-dismiss" name="email" value="{{ old('email') }}" data-toggle="popover" data-placement="top" data-content="Escribe tu Correo electronico" required>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="slug" value="{{ $post->slug }}">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="suggest"> Mensaje <span class="text-danger">(*)</span></label>
                                <textarea id="suggest" class="form-control {{ $errors->has('suggest') ? ' is-invalid' : '' }} popover-dismiss" name="suggest" value="{{ old('suggest') }}" data-toggle="popover" data-placement="top" data-content="Escribe tu mensaje de contacto detalladamente." required rows="5"></textarea>
                                @if($errors->has('suggest'))
                                    <div class="invalid-feedback">{{ $errors->first('suggest') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="submit" class="btn btn-primary float-right" value="Enviar">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    Los campos con <span class="text-danger">(*)</span> son obligatorios
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>


</div>

@endsection