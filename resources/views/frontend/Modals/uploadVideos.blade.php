<!-- Modal -->
<div class="modal fade" id="uploadVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">SUBIR VIDEOS</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

         	{!! Form::open(['method' => 'POST', 'name' => 'formMoviesCookies', 'id' => 'formMoviesCookies']) !!}

			<div class="modal-body">
				<div class="row">
					<div class="col-md">
						<div class="form-group">
	                		<input type="text" class="form-control popover-dismiss" name="video" data-toggle="popover" data-placement="top" data-content="Pega aquí la URL de tu video de Youtube" required/>
	                		<span id="response" class="text-danger"></span>
						</div>
					</div>
                </div>    
			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="uploadMovies">Enviar</button>
			</div>

            {!! Form::close() !!}
		</div>
	</div>
</div>