@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">TUS ANUNCIOS FAVORITOS</h5>
        </header>

        <div class="col-md-9">
        	
        	@include('frontend.favorites.partials.fields')

        </div>

        <div class="col-md-3">
	        
            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

@endsection