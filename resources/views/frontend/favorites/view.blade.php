@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pt-5 pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">{{ $post->name }}</h5>
        </header>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ $post->code }} <span class="float-right">{{ $post->created_at }}</span>
                </div>
                  
                @include('frontend.MyPosts.partials.fields-views')

            </div>
        </div>
    </div>
</div>

@endsection
