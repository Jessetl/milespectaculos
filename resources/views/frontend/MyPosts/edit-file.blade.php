@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">MIS IMAGENES DE ANUNCIO: {{ $post->name }}</h5>
        </header>

        <div class="col-md-12">
        	<div class="card border p-4">
        		<div id="dropzone">
                    
                    {!! Form::model($post, ['route' => ['posts.upload', $post->id], 'files' => 'true', 'id' => 'frmFileUpload', 'class' => 'dropzone', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}

                        <div class="fallback">
                            <input name="file" type="file" required/>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="card border mt-2">
                <div class="card-header">
                    IMAGENES
                </div>
                <div class="card-body">
                	<div class="row justify-content-between">
                        @foreach($post->images as $key => $image)
                            
                            {!! Form::open(['route' => ['meta.destroy', $image], 'method' => 'DELETE']) !!}
                            
                                <div class="card border mx-4 my-3" style="width: 18rem;">
                                    <img class="card-img-top" src="{{ asset('storage/posts/'. $image->meta_url) }}" height="180" alt="Cap"/>
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-danger btn-block"> Eliminar </button>
                                    </div>
                                </div>

                            {!! Form::close() !!}

                    	@endforeach
                    </div>
        	    </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
Dropzone.options.frmFileUpload = {

    dictDefaultMessage: 'PINCHE AQUÍ PARA SUBIR ARCHIVOS',
    dictRemoveFile: 'Remover',
    dictMaxFilesExceeded: 'No puedes subir más archivos.',

    maxFilesize: 5,
    addRemoveLinks: true,
    acceptedFiles: ".jpg,.png",

    error: function(file, message) {
        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.message);
    }
};
</script>

@endsection