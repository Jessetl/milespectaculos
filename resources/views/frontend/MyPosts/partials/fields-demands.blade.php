<div class="row pt-2">
    <div class="col">
        <label for="email">Email <span class="text-danger">(*)</span></label>
        <input type="email" id="email" name="email" value="{{ $post->email }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Dirección de correo electrónico" required/>
        @if ($errors->has('email'))
            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
        @endif
    </div> 
</div>
<div class="row pt-2">
    <div class="col">
        <label for="phone1">Teléfono 1</label>
        <input type="text" id="phone1" name="phone1" value="{{ $post->phone1 }}" class="form-control {{ $errors->has('phone1') ? ' is-invalid' : '' }}" placeholder="Télefono principal" required/>
        @if ($errors->has('phone1'))
            <div class="invalid-feedback">{{ $errors->first('phone1') }}</div>
        @endif
    </div>
    <div class="col">
        <label for="phone2">Teléfono 2</label>
        <input type="text" id="phone2" name="phone2" value="{{ $post->phone2 }}" class="form-control {{ $errors->has('phone2') ? ' is-invalid' : '' }}" placeholder="Télefono secundario"/>
        @if ($errors->has('phone2'))
            <div class="invalid-feedback">{{ $errors->first('phone2') }}</div>
        @endif
    </div>
</div>
<div class="row pt-2">
    <div class="col">
        <label for="content">Descripción <span class="text-danger">(*)</span></label>
        <textarea id="content" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" value="{{ $post->content }}" placeholder="Describe tu anuncio lo más claro posible." rows="3">{{ $post->content }}</textarea>
        @if ($errors->has('content'))
            <div class="invalid-feedback">{{ $errors->first('content') }}</div>
        @endif
    </div>
</div>