@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pt-5 pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">MIS ANUNCIOS PUBLICADOS</h5>
        </header>

        <div class="col-md-9">
        	
        	@include('frontend.session.session')
        	
        
        	@forelse ( $posts as $key => $post )
	        	<div class="card border mb-3">
	        		<div class="card-header">
				   		{{ $post->name }} <span class="text-danger">{{ $post->type_posts ? '(DEMANDA DE SERVICIO)' : '(OFERTA DE SERVICIO)' }}</span> <span class="float-right">{{ toDateString($post->updated_at) }}</span>
				  	</div>
	        		<div class="card-body">
	        			<div class="row">
		        			<div class="col-md-8">
		        				<p class="text-justify">
		        					{{ $post->content }}
		        				</p>
		        			</div>
		        			<div class="col-md-4">
		        				<div class="image" style="background-image: url({{ postLoadImage($post) }})"></div>
		        			</div>
					       	<div class="col-md-12"> 
					       		<div class="pt-4">
					       			<a href="{{ route('my-posts.show', $post) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Vista preliminar del anuncio">Ver</a>
					       			<a href="{{ route('my-posts.edit', $post) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title=" Editar el contenido de este anuncio.">Editar</a>
					       			<a href="{{ route('my-posts.gallery', $post) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Agregar, eliminar o sustituir imagenes de este anuncio.">Galería</a>
					       			<button type="button" class="btn btn-primary float-right renewPostsJs" data-post="{{ $post->slug }}">Renovar</button>
					       		</div>
					       	</div>
					       	<div class="col-md-12">
					       		{!! Form::open(['route' => ['my-posts.destroy', $post->id], 'method' => 'DELETE', 'class' => 'inline']) !!}
					       			<button type="submit" class="btn btn-primary mt-4" data-toggle="tooltip" data-placement="right" title="Elimina por completo este anuncio">Eliminar</button>
				       			{!! Form::close() !!}
					       	</div>
	        			</div>
	        		</div>
	        	</div>

	        	@empty

	        		<div class="card">
		    			<div class="card-body">
	        				<h5 class="card-title">No tienes anuncios creados.</h5>
	        				<a href="{{ url('posts/create') }}" class="btn btn-primary">Crear Anuncios</a>
	        			</div>
	            	</div>

	        	@endforelse
	        <span>
        		{!! $posts->render() !!}
    		</span>
        </div>

        @include('frontend.MyPosts.modals.renew')

        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

@endsection