@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">TU ANUNCIO DE OFERTA DE DEMANDA DE SERVICIOS</h5>
        </header>
        <div class="col-md-9">
            
            @include('frontend.session.session')

            @include('frontend.posts.partials.fields-previewd')
    
        </div>
  
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

@endsection