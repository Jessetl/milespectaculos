@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">EDITA TU ANUNCIO</h5>
        </header>

        <div class="col-md-9">
        	
            {!! Form::model($post, ['route' => ['my-posts.update', $post->id], 'method' => 'PUT', 'autocomplete' => 'off', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}

            <div class="card">
        		
        		<div class="card-body">

                    @include('frontend.MyPosts.partials.fields-offerts')

                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Editar">
                        </div>
                    </div>

        		</div>

            </div>
        	
            {!! Form::close() !!}

            @include('frontend.posts.modals.fields-videos')
            @include('frontend.posts.modals.fields-audios')

        </div>
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

<input type="hidden" name="lat" value="{{ $post->lat }}">
<input type="hidden" name="lng" value="{{ $post->lng }}">

@endsection

@section('scripts')
<!-- Google Maps -->
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbapNm5FNXk2Db4pJ47Fr7lKXEsd8jtsA&callback=initMap">
</script>


<script>

var map;
var marker;
var coords;

let lat = $('input[name=lat]').val();
let lng = $('input[name=lng]').val();

function initMap() {
        
    var myLatlng = new google.maps.LatLng(lat,lng);

    var mapOptions = {
        zoom: 12,
        center: myLatlng
    }

    var geocoder = new google.maps.Geocoder();

    document.getElementById('look').addEventListener('click', function() {
        geocodeAddress(geocoder, map);
    });
          
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Drag me!"
    });
        
    marker.addListener('click', function(){
        placeMarker(marker.getPosition());
    });

    marker.addListener('drag', handleEvent);
    marker.addListener('dragend', handleEvent);

}

function handleEvent(event) {

    document.getElementById('id_lat').value = event.latLng.lat();
    document.getElementById('id_lng').value = event.latLng.lng();
}

function placeMarker(cords) {
    
    marker.setPosition(cords);
    document.getElementById("id_lat").value = cords.lat();
    document.getElementById("id_lng").value = cords.lng();
}

function geocodeAddress(geocoder, resultsMap) {

    var address = document.getElementById('address').value;
    
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            placeMarker(results[0].geometry.location);
            
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
    });
}
    
</script>


@endsection