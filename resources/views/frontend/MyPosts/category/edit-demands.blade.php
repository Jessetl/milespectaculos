@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">EDITA TU ANUNCIO</h5>
        </header>

        <div class="col-md-9">
        	<div class="card">
        	{!! Form::model($post, ['route' => ['my-posts.update', $post->id], 'method' => 'PUT', 'autocomplete' => 'off']) !!}
        		
        		<div class="card-body">

                    @include('frontend.MyPosts.partials.fields-demands')

                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Editar">
                        </div>
                    </div>

        		</div>

        	{!! Form::close() !!}
        	</div>
        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>
</div>

@endsection