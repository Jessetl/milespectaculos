@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">EVENTOS PUBLICADOS <a href="{{ route('my-events.index') }}" class="btn float-right text-white">MIS EVENTOS</a></h5>
        </header>

        <div class="col-md-9">
            
            @include('frontend.session.session')
            
        	@forelse ( $events as $key => $event )
        	
	        	<div class="card border mb-3">
	        		<div class="card-header">
				   		{{ empty($event->title) ? 'SIN TÍTULO' : $event->title }} <span class="float-right">{{ $event->created_at }}</span>
				  	</div>
	        		<div class="card-body">
	        			<div class="row">
		        			<div class="col-md-12">
                                <p>
                                    {{ $event->detail }}
                                </p>
		        				<p class="text-justify">
		        					{{ $event->date }} - <span class="text-primary">{{ $event->hour }}</span>
		        				</p>
		        			</div>
		        			<div class="col-md-12"> 
		        				<div class="float-right">
		        					<h4>{{ $event->country }}</h4>
		        				</div>
		        			</div>
	        			</div>
	        		</div>
	        	</div>

        	@empty

        	<div class="card">
    			<div class="card-body">
    				<h5 class="card-title">No hay eventos publicados.</h5>
    			</div>
            </div>        	

        	@endforelse

            <span>
                {!! $events->render() !!}
            </span>

        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>
</div>

@endsection