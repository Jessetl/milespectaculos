@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pt-5 pb-5">
    <div class="row bg-light">
        <header class="page-header">
            <h5 class="text-uppercase">SELECCIONA LA COBERTURA DEL SERVICIO QUE OFRECE SU EMPRESA O ESPECTACULO</h5>
        </header>
    </div>

    {!! Form::open(['route' => ['post.postPayment', $post->id], 'method' => 'POST', 'autocomplete' => 'off']) !!}

    <div class="row justify-content-between bg-light pb-5">
        <div class="col-md-9">
            <div class="card border">
                <div class="card-body">
                	<button type="button" id="add" class="btn btn-success"><i class="fa fa-plus"></i> AGREGAR</button>
                	<button type="button" id="remove" class="btn btn-danger"><i class="fa fa-minus"></i> ELIMINAR</button>
                </div>
	            <div class="card-body">
					<div class="row mt-2" id="div_clone">
						<div class="col">
							<label for="districts">Provincia <span class="text-danger">(*)</span></label>
							<select class="form-control" id="districts" name="districts[]">
								@foreach($districts as $key => $district)
									<option value="{{ $district->id }}">{{ $district->name }}</option>
								@endforeach
							</select>
		            	</div>
		            	<div class="col">
							<label for="circuit">Localidad <span class="text-danger">(*)</span></label>
							<input type="text" name="circuit" class="form-control" required/>
		            	</div>
		            </div>
	            	<div id="clone"></div>
	            	<div class="row pt-3">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary float-right" value="Siguiente">
                        </div>
                    </div>
	            </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('frontend.posts.partials.menu-vertical')
        </div>
    </div>

    {!! Form::close() !!}
</div>

@endsection