    @extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ZONA DE SOCIOS</h5>
        </header>

        <div class="col-md-12">
            
            @include('frontend.session.session')

        </div>
        <div class="col-md-6">
            <div class="col pb-4">
            	<div class="card">
            		<div class="card-body">
                        <p>
                            <img src="{{ asset('/img/member/red-social.png') }}" class="float-left">
                            <h4 class="d-inline card-title text-primary"> ACCEDE A NUESTRA RED SOCIAL </h4>
                        </p>
            			<p class="text-justify">Podrás estableces contacto con Artistas y Empresas del sector, intercambiar experiencias, ideas, documentos, midis, karaokes, partituras, etc. Buscar sustitutos para tus galas, hacer negocios, comprar y vender lo que ya no necesitas, etc.</p>
                        <p>
                            @if (!Auth::guest())
                                <h3 class="text-primary">ACCEDE PINCHANDO <a href="{{ route('netw') }}" class="text-danger">AQUÍ</a></h3>
                            @endif
                        </p>
            		</div>
            	</div>
            </div>
            <div class="col pb-4">
            	<div class="card">
            		<div class="card-body">
                        <p>
                            <img src="{{ asset('/img/member/arte-y-eventos.png') }}" class="float-left">
                        </p>
            			<p class="text-justify">Presenta tus eventos en tu perfil, a muchos clientes les gusta ver antes de Contratar la Actuación o Servicio, Más aún si organizas conciertos, ferias, Exposiciones, etc. Tambien sirve para que automaticamente se comparta a tus fans de redes sociales y a nuestro departamento de Marketing para promosionaros con otras tecnicas a nivel Online.</p>
            		</div>
            	</div>
            </div>
            <div class="col pb-4">
                <div class="card">
                    <div class="card-body">
                        <p>
                            <img src="{{ asset('/img/member/mejor-posicionamiento.png') }}" class="float-left">
                            <h4 class="d-inline card-title text-primary"> MEJOR POSICIONAMIENTO </h4>
                        </p>
                        <p class="text-justify">Nuestros Socios Vip gozan de un mejor posicionamiento no solo en su categoría y en el panel superior de la página principal, donde aparecen de forma animada, por orden de perfiles mas actualizados. Cada vez que actualizas tu perfil, te posicionas en primer lugar y sales en el panel superior de destacados.</p>
                    </div>
                </div>
            </div>
            <div class="col pb-4">
                <div class="card">
                    <div class="card-body">
                        <p>
                            <img src="{{ asset('/img/member/zona-negocios.png') }}" class="float-left">
                            <h3 class="d-inline card-title text-primary"> ZONA DE NEGOCIOS </h3>
                        </p>
                        <p class="text-justify">MIL ESPECTÁCULOS no solo es un portal de ofertas, donde nuetros profesionales exponen sus servicios, tambien somo un portal de demanda de servicios donde Particulares y Empresas, demandan profesionales, pero estas demandas solo aparecen en el panel interno, donde tan solo tienen acceso nuestros asociados VIP</p>
                        <p class="text-center">

                        @if(Auth::guest())
                            <h3 class="text-primary">ACCEDE PINCHANDO <a href="{{ url('login') }}" class="text-danger">AQUÍ</a></h3>
                        @else
                            @if(Auth::user()->premium)
                                <h3 class="text-primary">ACCEDE PINCHANDO <a href="{{ url(Auth::user()->username.'/page') }}" class="text-danger">AQUÍ</a></h3>
                            @else
                                @if(Auth::user()->role != 3)
                                    <h3 class="text-primary">SUSCRIBETE <a href="{{ route('subscription') }}" class="text-danger">AQUÍ</a></h3>
                                @endif
                            @endif
                        @endif

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6"> 
            <div class="col pb-4">
                <div class="card">
                    <div class="card-body">
                        <p>
                            <img src="{{ asset('/img/member/descuentos-especiales.png') }}" class="float-left">
                        </p>
                        <p class="text-justify">El lugar donde podras presentar tus Ofertas especiales y te podras beneficiar de los descuentos de las empresas Asociadas, estas Ofertas y Descuentos es solo para Profesionales Asociados.</p>
                    </div>
                </div>
            </div>
            <div class="col pb-4">
            	<div class="card">
            		<div class="card-body">
                        <p>
                            <img src="{{ asset('/img/member/facebook-fans.png') }}" class="float-left">
                            <h4 class="d-inline card-title text-primary"> DIFUNDE TU PERFIL EN LAS REDES SOCIALES </h4>
                        </p>
            			<p class="text-justify">Para que tus contactos se hagan tus FANS. A más fans y seguidores aparezcan en tu perfil, mayor Garantía percibiran los Clientes sobre tu empresa o Espectáculo y tambien cada vez que expongas un evento en tu perfil les llegará a su perfil o móvil haciendote una Gran publicidad de forma automática.</p>
            		</div>
            	</div>
            </div>
            <div class="col pb-4">
            	<div class="card">
            		<div class="card-body">
                        <P>
                            <img src="{{ asset('/img/member/marketing-online.png') }}" class="float-left">
            			    <h4 class="d-inline card-title text-primary">MARKETING ONLINE</h4>
                        </p>
            			<p class="text-justify">Tener a vuestra disposición el mejor Equipo de Marketing que se encargara no solo a través de MIL ESPECTÁCULOS si no también mediante otras técnicas como, E -mail Marketing, Tele Marketing, Social Network, Social Media, etc. para llegar a organismos públicos y privados, como ayuntamientos, agencias, representantes, Hoteles, etc. y así ser lo mas efectivos y rentables a nuestros socios Vip.</p>
            		</div>
            	</div>
            </div>
            <div class="col pb-4">
            	<div class="card">
            		<div class="card-body">
                        <p>
            			    <img src="{{ asset('/img/member/mas-beneficios.png') }}" class="float-left">
                            <h3 class="d-inline card-title text-primary"> MAS BENEFICIOS VIP </h3>
                        </p>
            			<p class="text-justify">Nuestros socios No poseen limites; al insertar fotos, videos, MP3, anuncios de ofertas y demandar, sin limites exponiendo todos los servicios que ofrece y en cada provincia de España. Esta ventaja es ideal para representantes artisticos qu poseen muchos Artistas. o Empresas y Espectáculos con cobertura Nacional. URL propia dentro de Mil Espectáculos, perfil web dentro de Mil Espectáculos. etc...</p>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>

@endsection