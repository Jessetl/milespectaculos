{!! Form::open(['route' => ['notify', $post], 'method' => 'POST', 'autocomplete' => 'off']) !!}
    @guest
    <div class="row">
        <div class="col-md">
            <div class="form-group"> 
                <a href="{{ route('register') }}">¿Ya estas registrado?</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="names">Nombre</label>
            <div class="form-group">
                <input type="text" id="names" class="form-control form-control-sm {{ $errors->has('names') ? ' is-invalid' : '' }}" name="names" maxlength="45" value="{{ old('names') }}" required/>
                @if ($errors->has('names'))
                    <div class="invalid-feedback">{{ $errors->first('names') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="email">Tu e-mail</label>
            <div class="form-group">
                <input type="text" id="email" class="form-control form-control-sm {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" maxlength="45" value="{{ old('email') }}" required/>
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="phone">Tu teléfono</label>
            <div class="form-group">
                <input type="text" id="phone" class="form-control form-control-sm {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" maxlength="25" value="{{ old('phone') }}" required/>
                @if ($errors->has('phone'))
                    <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="password">Contraseña</label>
            <div class="form-group">
                <input type="password" id="password" class="form-control form-control-sm {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" maxlength="16" value="{{ old('password') }}" required/>
                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="password_confirmation">Repite tu contraseña</label>
            <div class="form-group">
                <input type="password" id="password_confirmation" class="form-control form-control-sm {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password" maxlength="16" value="{{ old('password_confirmation') }}" required/>
                @if ($errors->has('password_confirmation'))
                    <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
                @endif
            </div>
        </div>
    </div>
    @endguest
    <div class="row">
        <div class="col-md">
            <label for="category">Tipo de evento</label>
            <div class="form-group">
                <select id="category" name="type" class="form-control form-control-sm {{ $errors->has('type') ? ' is-invalid' : '' }}" required>
                    <option value="" selected disabled>Elige un tipo de evento</option>
                    <optgroup label="FIESTAS POPULARES">
                        <option value="Verbenas">Verbenas</option>
                        <option value="Fallas">Fallas</option>
                        <option value="Ferias">Ferias</option>
                        <option value="Fiesta del Pilar">Fiesta del Pilar</option>
                        <option value="Fiestas locales y patronales">Fiestas locales y patronales</option>
                        <option value="Hoguera de San Juan">Hoguera de San Juan</option>
                        <option value="Moros y Cristianos">Moros y Cristianos</option>
                        <option value="San Fermín">San Fermín</option>
                        <option value="San Isidro">San Isidro</option>
                        <option value="Sant Jordi">Sant Jordi</option>
                        <option value="Semana Grande de Bilbao">Semana Grande de Bilbao</option>
                        <option value="Semana Santa">Semana Santa</option>
                        <option value="Tomatina de Buñol">Tomatina de Buñol</option>
                    </optgroup>
                    <optgroup label="BODAS">
                        <option value="Aniversario de Bodas">Aniversario de Bodas</option>
                        <option value="Banquetes de Boda">Banquetes de Boda</option>
                        <option value="Boda Civil">Boda Civil</option>
                        <option value="Boda Religiosa">Boda Religiosa</option>
                        <option value="Bodas">Bodas</option>
                        <option value="Bodas de Oro">Bodas de Oro</option>
                        <option value="Bodas de Plata">Bodas de Plata</option>
                        <option value="Bodas en la Playa">Bodas en la Playa</option>
                        <option value="Ceremonia de Bodas">Ceremonia de Bodas</option>
                        <option value="Cóctel de Bienvenida">Cóctel de Bienvenida</option>
                        <option value="Despedida de Soltero">Despedida de Soltero</option>
                        <option value="Despedida de Soltera">Despedida de Soltera</option>
                        <option value="Recepción de Bodas">Recepción de Bodas</option>
                    </optgroup>
                    <optgroup label="CUMPLEAÑOS Y ANIVERSARIOS">
                        <option value="Aniversario de Boda">Aniversario de Boda</option>
                        <option value="Cumpleaños">Cumpleaños</option>
                        <option value="Cumpleaños Adulto">Cumpleaños Adulto</option>
                        <option value="Cumpleaños Infantil">Cumpleaños Infantil</option>
                        <option value="Fiesta de Aniversario">Fiesta de Aniversario</option>
                        <option value="Fiesta de Jubilación">Fiesta de Jubilación</option>
                    </optgroup>
                    <optgroup label="EVENTOS">
                        <option value="Cabalgatas de Reyes">Cabalgatas de Reyes</option>
                        <option value="Cenas">Cenas</option>
                        <option value="Certámenes">Certámenes</option>
                        <option value="Cóctel">Cóctel</option>
                        <option value="Concentración de Coches">Concentración de Coches</option>
                        <option value="Concentración de Motos">Concentración de Motos</option>
                        <option value="Conciertos">Conciertos</option>
                        <option value="Convenciones">Convenciones</option>
                        <option value="Cruceros">Cruceros</option>
                        <option value="Entrega de Premios">Entrega de Premios</option>
                        <option value="Evento Corporativo">Evento Corporativo</option>
                        <option value="Exposiciones">Exposiciones</option>
                        <option value="Festivales">Festivales</option>
                        <option value="Inauguraciones">Inauguraciones</option>
                        <option value="Pasacalles">Pasacalles</option>
                        <option value="Quedadas">Quedadas</option>
                        <option value="Reuniones Familiares">Reuniones Familiares</option>
                        <option value="Seminarios">Seminarios</option>
                    </optgroup>
                    <optgroup label="DÍAS ESPECIALES">
                        <option value="Año Nuevo Chino">Año Nuevo Chino</option>
                        <option value="Bautizos">Bautizos</option>
                        <option value="Carnavales">Carnavales</option>
                        <option value="Comuniones">Comuniones</option>
                        <option value="Día de la Madre">Día de la Madre</option>
                        <option value="Día de todos los Santos">Día de todos los Santos</option>
                        <option value="Día del Padre">Día del Padre</option>
                        <option value="Día del Trabajador">Día del Trabajador</option>
                        <option value="Funerales">Funerales</option>
                        <option value="Noche Buena">Noche Buena</option>
                        <option value="Noche Vieja">Noche Vieja</option>
                        <option value="Pedidas de Mano">Pedidas de Mano</option>
                        <option value="San Valentín">San Valentín</option>
                    </optgroup>
                    <optgroup label="FIESTAS">
                        <option value="Fiesta de Disfraces">Fiesta de Disfraces</option>
                        <option value="Fiesta de Empresas">Fiesta de Empresas</option>
                        <option value="Fiesta de Fin de Curso">Fiesta de Fin de Curso</option>
                        <option value="Fiesta de Graduación">Fiesta de Graduación</option>
                        <option value="Fiesta de halloween">Fiesta de halloween</option>
                        <option value="Fiesta de los 50">Fiesta de los 50</option>
                        <option value="Fiesta de los 60">Fiesta de los 60</option>
                        <option value="Fiesta de los 70">Fiesta de los 70</option>
                        <option value="Fiesta de los 80">Fiesta de los 80</option>
                        <option value="Fiesta en Barco">Fiesta en Barco</option>
                        <option value="Fiesta en la Playa">Fiesta en la Playa</option>
                        <option value="Fiesta Gay">Fiesta Gay</option>
                        <option value="Fiestas Hawaianas">Fiestas Hawaianas</option>
                        <option value="Fiesta Étnicas">Fiesta Étnicas</option>
                        <option value="Fiesta Fin de Año">Fiesta Fin de Año</option>
                        <option value="Fiestas Navideñas">Fiestas Navideñas</option>
                        <option value="Fiesta para la 3ª Edad">Fiesta para la 3ª Edad</option>
                        <option value="Fiesta Privadas">Fiesta Privadas</option>
                        <option value="Fiestas Single">Fiestas Single</option>
                        <option value="Fiesta Sorpresa">Fiesta Sorpresa</option>
                        <option value="Fiesta Temáticas">Fiesta Temáticas</option>
                        <option value="Ectoberfest">Ectoberfest</option>
                        <option value="Otros">Otros</option>
                    </optgroup>
                </select>
                @if ($errors->has('type'))
                    <div class="invalid-feedback">{{ $errors->first('type') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="country">Ciudad o localidad</label>
            <div class="form-group">
                <input type="text" id="country" class="form-control form-control-sm {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" placeholder="Empieza a escribir la ciudad..." value="{{ old('country') }}" required/>
                @if ($errors->has('country'))
                    <div class="invalid-feedback">{{ $errors->first('country') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="postal_code"> Código Postal </label>
            <div class="form-group">
                <input type="text" id="postal_code" class="form-control form-control-sm {{ $errors->has('postal_code') ? ' is-invalid' : '' }}" name="postal_code" value="{{ old('postal_code') }}"/>
                @if ($errors->has('postal_code'))
                    <div class="invalid-feedback">{{ $errors->first('postal_code') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="day">Fecha del evento</label>
            <div class="form-group">
                <input type="text" id="day" class="form-control form-control-sm {{ $errors->has('day') ? ' is-invalid' : '' }} datepicker" name="day" data-required placeholder="Click para elegir" value="{{ old('day') }}" autocomplete="off" required/>
                @if ($errors->has('day'))
                    <div class="invalid-feedback">{{ $errors->first('day') }}</div>
                @endif
            </div>
            <div class="form-group">
                <select id="hour" name="hour" data-required="" data-error="Debes especificar la hora aproximada de inicio del evento" class="form-control form-control-sm {{ $errors->has('hour') ? ' is-invalid' : '' }}" required>
                    <option value="" selected disabled>Hora</option>
                    <option value="00:00">0:00</option>
                    <option value="00:30">0:30</option>
                    <option value="01:00">1:00</option>
                    <option value="01:30">1:30</option>
                    <option value="01:30">2:00</option>
                    <option value="02:00">2:30</option>
                    <option value="03:00">3:00</option>
                    <option value="03:30">3:30</option>
                    <option value="04:00">4:00</option>
                    <option value="04:30">4:30</option>
                    <option value="05:00">5:00</option>
                    <option value="05:30">5:30</option>
                    <option value="06:00">6:00</option>
                    <option value="06:30">6:30</option>
                    <option value="07:00">7:00</option>
                    <option value="07:30">7:30</option>
                    <option value="08:00">8:00</option>
                    <option value="08:30">8:30</option>
                    <option value="09:00">9:00</option>
                    <option value="09:30">9:30</option>
                    <option value="10:00">10:00</option>
                    <option value="10:30">10:30</option>
                    <option value="11:00">11:00</option>
                    <option value="11:30">11:30</option>
                    <option value="12:00">12:00</option>
                    <option value="12:30">12:30</option>
                    <option value="13:00">13:00</option>
                    <option value="13:30">13:30</option>
                    <option value="14:00">14:00</option>
                    <option value="14:30">14:30</option>
                    <option value="15:00">15:00</option>
                    <option value="15:30">15:30</option>
                    <option value="16:00">16:00</option>
                    <option value="16:30">16:30</option>
                    <option value="17:00">17:00</option>
                    <option value="17:30">17:30</option>
                    <option value="18:00">18:00</option>
                    <option value="18:30">18:30</option>
                    <option value="19:00">19:00</option>
                    <option value="19:30">19:30</option>
                    <option value="20:00">20:00</option>
                    <option value="20:30">20:30</option>
                    <option value="21:00">21:00</option>
                    <option value="21:30">21:30</option>
                    <option value="22:00">22:00</option>
                    <option value="22:30">22:30</option>
                    <option value="23:00">23:00</option>
                    <option value="23:30">23:30</option>
                </select>
                @if ($errors->has('hour'))
                    <div class="invalid-feedback">{{ $errors->first('hour') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="place">Lugar del evento</label>
            <div class="form-group">
                <input type="text" id="place" maxlength="75" name="place" class="form-control form-control-sm {{ $errors->has('place') ? ' is-invalid' : '' }}" value="{{ old('place') }}" required/>
                @if ($errors->has('place'))
                    <div class="invalid-feedback">{{ $errors->first('place') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <label for="detail">Detalles y comentarios</label>
            <div class="form-group"> 
                <textarea id="detail" class="form-control form-control-sm {{ $errors->has('detail') ? ' is-invalid' : '' }}" name="detail" required rows="5">{{ old('detail') }}</textarea>
                @if($errors->has('detail'))
                    <div class="invalid-feedback">{{ $errors->first('detail') }}</div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <div class="form-check"> 
                <div class="form-group">   
                    <input type="checkbox" class="form-check-input position-static" name="ar-rss"/> <small>¿Quieres contactar con otros artístas afines para poder comparar?</small>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <div class="form-check">
                <div class="form-group">
                    <input type="checkbox" class="form-check-input position-static" name="politics" required/> <small> He leído y acepto el <a href="{{ url('legal') }}" target="_blank"> aviso legal </a> y la <a href="{{ url('legal') }}" target="_blank"> política de privacidad.</a> </small>
                </div>    
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <button type="submit" class="btn btn-primary btn-block">
                CONTACTAR AHORA
            </button>
        </div>
    </div>

{!! Form::close() !!}