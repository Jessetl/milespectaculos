@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">PERFIL: {{ $user->name }}</h5>
        </header>
        <div class="col-md-9">
            
            @include('frontend.session.session')

            @include('frontend.premiums.partials.fields-view')
    
        </div>
  
        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

            <div id="map" style="height:400px; margin-top: 10px;"></div>

            <div class="row my-3">
                <div class="col-md">
                    <div class="card border">
                        <div class="card-header">
                            CONTACTAR AHORA:
                        </div>
                        <div class="card-body">
                            
                            @include('frontend.premiums.partials.notify')

                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-3 text-center">
                @if(Auth::guest())
                    <a href="{{ route('login') }}" class="btn btn-warning text-white" style="width: 100%;">
                        <i class="fa fa-thumbs-o-up"></i> HAZTE FAN 
                        <span class="text-danger d-block">{{ $user->followers()->count() }}</span>
                    </a>
                @else
                    @if(Auth::user()->isFollowing($user))
                        
                        {!! Form::open(
                            [
                                'route' => [
                                    'user.unfollow', $user
                                ], 
                                'method' => 'POST', 
                                'name' => 'form'
                            ]) 
                        !!}
                        <button type="submit" class="btn btn-warning text-white" style="width: 100%;">
                            <i class="fa fa-thumbs-o-up"></i> DEJAR DE SEGUIR
                            <span class="text-danger d-block">{{ $user->followers()->count() }}</span>
                        </button>
                        {!! Form::close() !!}
                

                    @else

                        {!! Form::open(
                            [
                                'route' => [
                                    'user.follow', $user
                                ], 
                                'method' => 'POST', 
                                'name' => 'form'
                            ]) 
                        !!}
                        <button type="submit" class="btn btn-warning text-white" style="width: 100%;">
                            <i class="fa fa-thumbs-o-up"></i> HAZTE FAN 
                            <span class="text-danger d-block">{{ $user->followers()->count() }}</span>
                        </button>
                        {!! Form::close() !!}

                    @endif
                @endif
            </div>
            <div class="card border mt-3">
                <div class="card-header menu-vertical">
                    <div class="text-center">PROXIMOS EVENTOS</div>
                </div>
                <div class="card-body">
            
                    @forelse ( $user->events as $key => $event )

                        <p class="text-justify">
                            {{ $event->date }} - <span class="text-primary">{{ $event->address }}</span>
                        </p>

                    @empty  

                        <p class="text-justify">
                            No hay eventos proximos
                        </p>

                    @endforelse

                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="lat" value="{{ $post->lat }}">
<input type="hidden" name="lng" value="{{ $post->lng }}">

@endsection


@if($post->directions !== NULL)

    @section('scripts')

    <!-- Google Maps -->
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbapNm5FNXk2Db4pJ47Fr7lKXEsd8jtsA&callback=initMap">
    </script>

    <script>

    let lat = $('input[name=lat]').val();
    let lng = $('input[name=lng]').val();

    function initMap() {
            
        var myLatlng = new google.maps.LatLng(lat, lng);

        var mapOptions = {
            zoom: 12,
            center: myLatlng
        }

        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
        });
    }

    </script>

    @endsection

@endif