@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">MI PERFIL</h5>
        </header>

	    <div class="col-md-3">
	    	<div class="card border mb-3">
	    		<div class="card-body text-center">
	    			@if ( empty($me->url) )
	    				<img src="{{ asset('img/icon.png') }}" class="img-fluid" width="220" height="220">
	    			@else
	    				<img src="{{ $me->url }}">
	    			@endif
	    		</div>
	    	</div>

	{{--	<div class="card border mb-3">
	    		<div class="card-body">
	    			<h5 class="card-title">
	    				Mis Créditos
	    			</h5>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<input type="text" class="form-control" value="0" readonly>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<a href="{{ route('credits.create') }}" class="btn btn-primary">Recargar creditos</a>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div> --}}

	    	<div class="card border mb-3">
	    		<div class="card-body">
	    			<h5 class="card-title">
	    				Mi suscripción
	    			</h5>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<input type="text" class="form-control" value="{{ $me->premium ? 'ACTIVA' : 'INACTIVA' }}" readonly>
	    					</div>
	    				</div>
	    			</div>
	    			@if( $me->premium == (true) )
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="trial_ends_at">Finaliza</label>
	    						<input type="text" class="form-control" value="{{ toDateTimeString($me->trial_ends_at) }}" readonly>
	    					</div>
	    				</div>
	    			</div>
	    			@else
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalCode">Código gratuito</button>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						@if(Auth::user()->role !== 3)
                                    <a href="{{ route('subscription') }}" class="btn btn-red my-2"> Hacerme cliente VIP </a>
                                @endif
                                <a href="{{ route('prices') }}" target="_blank" class="btn btn-green limit">Ver Precios</a>
	    					</div>
	    				</div>
	    			</div>
	    			@endif
	    		</div>
	    	</div>
	    </div>

	    <div class="col-md-6">

	    	@include('frontend.session.session')

	    	{!! Form::open(['route' => 'profile.save', 'method' => 'POST', 'name' => 'profile']) !!}

	    	<div class="card border">
	    		<div class="card-body">
	    			<h5 class="card-title">
	    				MIS DATOS
	    			</h5>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="username">Username</label>
	    						<input type="text" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ $me->username }}"/>
	    						@if ($errors->has('username'))
					                <div class="invalid-feedback">{{ $errors->first('username') }}</div>
					            @endif
	    					</div>
	    				</div>
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="email">E-mail</label>
	    						<input type="text" class="form-control" name="email" value="{{ $me->email }}" readonly/>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="name">Nombres</label>
	    						<input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $me->name }}" required/>
		    					@if ($errors->has('name'))
					                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
					            @endif
	    					</div>
	    				</div>
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="surname">Apellidos</label>
	    						<input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="surname"  value="{{ $me->surname }}" required/>
		    					@if ($errors->has('surname'))
					                <div class="invalid-feedback">{{ $errors->first('surname') }}</div>
					            @endif
	    					</div>
	    				</div>
	    			</div>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group ">
	    						<label for="subscriptions">Suscripción</label>
	    						<input type="text" class="form-control {{ $errors->has('subscriptions') ? ' is-invalid' : '' }}" value="{{ $me->premium ? 'ACTIVA' : 'INACTIVA' }}" readonly/>
		    					@if ($errors->has('subscriptions'))
					                <div class="invalid-feedback">{{ $errors->first('subscriptions') }}</div>
					            @endif
	    					</div>
	    				</div>
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="confirmation">E-mail confirmado</label>
	    						<input type="text" class="form-control" value="{{ $me->confirmed ? 'SI' : 'NO' }}" readonly/>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="password">Contraseña</label>
	    						<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
		    					@if ($errors->has('password'))
					                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
					            @endif
	    					</div>
	    				</div>
	    				<div class="col">
	    					<div class="form-group">
	    						<label for="password_confirmation">Confirmar contraseña</label>
	    						<input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation">
		    					@if ($errors->has('password_confirmation'))
					                <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
					            @endif
	    					</div>
	    				</div>
	    			</div>
	    			<div class="row">
	    				<div class="col">
	    					<div class="form-group">
	    						<button type="submit" class="btn btn-primary float-right">Guardar</button>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>

	    	{!! Form::close() !!}

	    </div>
	</div>
</div>

<div class="modal fade" id="ModalCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h6>Introduce tu código de suscripción</h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				{!! Form::open(['route' => 'subscriptions.code', 'method' => 'POST']) !!}
				<div class="row">
	    			<div class="col">
	    				<div class="form-group">
							<input type="text" class="form-control" name="code" maxlength="20" required/>
						</div>
					</div>
				</div>
				<div class="row">
    				<div class="col">
    					<div class="form-group">
    						<button type="submit" class="btn btn-primary">Enviar</button>
    					</div>
    				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection