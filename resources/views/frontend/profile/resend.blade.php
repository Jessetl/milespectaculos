@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">REENVIAR CONFIRMACIÓN</h5>
        </header>


	    <div class="col-md-5">

	    	{!! Form::open(['route' => 'resend.email', 'method' => 'POST', 'name' => 'resend']) !!}

	    	<div class="card border mb-3">
		    	<div class="card-body">
		    		<div class="row">
		    			<div class="col">
		    				<div class="form-group">
		    					<label for="email">E-mail <span class="text-danger">(*)</span></label>
		    					<input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required>
		    					@if ($errors->has('email'))
					                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
					            @endif
		    				</div>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col">
		    				<div class="form-group">
		    					<button type="submit" class="btn btn-primary">Enviar</button>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    {!! Form::close() !!}

	    </div>
	</div>
</div>

@endsection