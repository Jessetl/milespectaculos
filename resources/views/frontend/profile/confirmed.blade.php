@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">E-MAIL CONFIRMADO</h5>
        </header>

        <div class="col-md-6">
            <div class="card border">
                <div class="card-body">
                    <h4 class="font-weight-light">E-mail confirmado</h4>
                    @if(\Auth::user()->role !== 3)
                    <div class="ml-draft__info">
                        <p>
                            ¿Deseas hacer publicaciones como usuario VIP?
                        </p>
                    </div>
                    <div class="ml-action__button">
                        <a class="btn btn-primary limit" href="{{ url('posts/create') }}">Anunciarme gratis</a>
                        <a class="btn btn-danger limit" href="{{ route('subscription') }}">Hacerme VIP</a>
                        <a class="btn btn-green limit" target="_blank" href="{{ route('prices') }}">Ver precios</a>
                    </div>
                    @else
                    <div class="ml-action__button">
                        <a class="btn btn-primary limit" href="{{ url('posts/create') }}">Anunciarme gratis</a>
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>
@endsection