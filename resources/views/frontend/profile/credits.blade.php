@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-between bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">RECARGAR CREDITOS</h5>
        </header>

        <div class="col-md-9">

        	{!! Form::open(['route' => 'credits.purchase', 'method' => 'POST', 'name' => 'profile']) !!}

        		<div class="card border">
        			<div class="card-body">
        				<div class="row">
        					<div class="col">
        						<div class="form-group">
        							<label for="credits">Creditos</label>
        							<input type="number" class="form-control" name="credits" value="{{ old('credits') }}" min="5" required/>
        						</div>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col">
        						<div class="form-group">
        							<p class="text-danger">Monto mínimo de recarga 5€</p>
        						</div>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col">
        						<div class="form-group">
        							<button type="submit" class="btn btn-primary float-right">Comprar</button>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>

        	{!! Form::close() !!}

        </div>

        <div class="col-md-3">

            @include('frontend.posts.partials.menu-vertical')

        </div>
    </div>
</div>

@endsection