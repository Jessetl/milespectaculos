@if (session('error'))
    <div class="alert alert-danger" role="alert">
        {{ session('error') }}
    </div>
@endif

@if (session('limit'))
    <div class="alert alert-danger" role="alert">
        {{ session('limit') }} <a href="{{ route('zone-vip.index') }}" target="_blank" class="btn btn-primary limit">Ver ventajas VIP</a> <a href="{{ route('prices') }}" target="_blank" class="btn btn-green limit">Ver Precios</a>
    </div>
@endif

@if (session('email'))
    <div class="alert alert-warning" role="alert">
        {{ session('email') }} <a href="{{ route('resend') }}" class="btn btn-primary">Reenviar verificación</a>
    </div>
@endif

@if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif

@if (session('warning'))
    <div class="alert alert-warning" role="alert">
        {{ session('warning') }}
    </div>
@endif
