@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container pb-5">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">CREA TU CUENTA</h5>
        </header>

        <div class="col-md-6">

        {!! Form::open(['route' => 'register.facebook', 'method' => 'POST']) !!}

            <div class="card border">
                <div class="card-body">
                    <div class="row mb-4">
                        <img src="{{ $user->avatar }}" class="rounded mx-auto d-block" alt="Avatar">
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label for="name" class="control-label">Nombres</label>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" 
                                name="name" placeholder="Tus nombres" value="{{ $user->name }}" required/>

                            @if ($errors->has('name'))
                                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="col">
                            <label for="surname" class="control-label">Apellidos</label>
                            <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" 
                                name="surname" placeholder="Tus apellidos" value="{{ old('surname') }}" required/>

                            @if ($errors->has('surname'))
                                <div class="invalid-feedback">{{ $errors->first('surname') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label for="email" class="control-label">Email</label>
                            <input id="email" type="email" class="form-control" 
                                name="email" placeholder="Correo Electrónico" value="{{ $user->email }}" readonly/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label for="password-confirm" class="control-label">Identifícate, Qué eres?</label>
                            <select class="form-control" name="role" required>
                                <option value="3">Usuario Particular</option>
                                <option value="4">Artista</option>
                                <option value="5">Empresa</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">
                                Registrarme
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    {!! Form::close() !!}
    
    </div>


</div>

@endsection
