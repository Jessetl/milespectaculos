@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center bg-light">
        <header class="page-header">
            <h5 class="text-uppercase">CREA TU CUENTA</h5>
        </header>

        <div class="col-md-5 my-4">

        {!! Form::open(['route' => 'register', 'method' => 'POST']) !!}

            <div class="card border">
                <div class="card-body">
                    <!--<div class="form-group text-center">
                        <a href="{{ route('auth.facebook') }}" class="btn btn-blue btn-block">
                            Registrarme con Facebook
                        </a>
                    </div> -->
                    <!--<div class="form-group text-center">
                        <span class="font-weight-light">o</span>
                    </div>-->
                    <div class="row">
                        <div class="col-md">
                            <label for="username">Nombre artístico</label>
                            <div class="form-group">
                                <input type="text" id="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" 
                                    name="username" value="{{ old('username') }}" required/>
                                @if ($errors->has('username'))
                                    <div class="invalid-feedback">{{ $errors->first('username') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="name">Nombre</label>
                            <div class="form-group">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" 
                                    name="name" value="{{ old('name') }}" required/>
                                @if ($errors->has('name'))
                                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md">
                            <label for="surname">Apellidos</label>
                            <div class="form-group">
                                <input type="text" id="surname" class="form-control {{ $errors->has('surname') ? ' is-invalid' : '' }}" 
                                    name="surname" value="{{ old('surname') }}" required/>
                                @if ($errors->has('surname'))
                                    <div class="invalid-feedback">{{ $errors->first('surname') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="phone">Tu teléfono</label>
                            <div class="form-group">
                                <input type="text" id="phone" maxlength="16" pattern="^[0-9]+" name="phone" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" required/>
                                @if ($errors->has('phone'))
                                    <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="email">Tu email</label>
                            <div class="form-group">
                                <input type="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" 
                                    name="email" value="{{ old('email') }}" required/>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="password">Crea una contraseña</label>
                            <div class="form-group">
                                <input type="password" id="password" maxlength="16" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" 
                                    name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md">
                            <label for="password_confirmation" >Repite la contraseña</label>
                            <div class="form-group">
                                <input type="password" id="password_confirmation" maxlength="16" class="form-control" name="password_confirmation" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="role">Identifícate, Qué eres?</label>
                                <select class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" required>
                                    <option value="" selected disabled>Selecciona una opción</option>
                                    <option value="artist" {{ old('role') == 4 ? 'selected' : '' }}>Artista</option>
                                    <option value="company" {{ old('role') == 5 ? 'selected' : '' }}>Empresa</option>
                                    <option value="user" {{ old('role') == 3 ? 'selected' : '' }}>Particular</option>
                                </select>
                                @if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-check"> 
                                <div class="form-group">  
                                    <input type="checkbox" class="form-check-input position-static" name="politics" required/> <span class="font-weight-light"> He leído y acepto el <a href="{{ url('legal#aviso') }}"> aviso legal </a> y la <a href="{{ url('legal#politica') }}"> política de privacidad </a> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary font-weight-light">
                                    Registrarme
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        {!! Form::close() !!}

    </div>
</div>

@endsection
