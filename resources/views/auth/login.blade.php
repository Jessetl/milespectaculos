@extends('layouts.app')

@section('logo')
    @include('layouts.partials.logo')
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center bg-light pb-5">
        <header class="page-header">
            <h5 class="text-uppercase">ACCEDE A TU CUENTA</h5>
        </header>

        <div class="col-md-5 my-4">
        
        {!! Form::open(['route' => 'login', 'method' => 'POST']) !!}

            @include('frontend.session.session')

            <div class="card border">
                <div class="card-body">
                    <div class="form-group text-center">
                        <a href="{{ route('auth.facebook') }}" class="btn btn-blue btn-block">
                            Iniciar con Facebook
                        </a>
                    </div>
                    <div class="form-group text-center">
                        <span class="text-center">o</span>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="email">Dirección de correo electrónico</label>
                            <div class="form-group">
                                <input type="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" 
                                        name="email" value="{{ old('email') }}" required autofocus/>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <label for="password">Contraseña</label>
                            <div class="form-group">
                                <input type="password" id="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" 
                                        name="password" required/>
                                @if ($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-check"> 
                                <div class="form-group">  
                                    <input type="checkbox" name="remember" class="form-check-input position-static" {{ old('remember') ? 'checked' : '' }}> <span class="font-weight-light"> Recuérdame </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary mr-2">
                            Iniciar sesión
                        </button>
                        <a href="{{ route('password.request') }}" class="text-primary">¿Has olvidado tu contraseña?</a>
                    </div>
                    <div class="form-group">
                        ¿Ya estas registrado? Registrate para publicar tus anuncios pinchando <a href="{{ route('register') }}" class="text-primary"> aquí</a>.
                    </div>
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection
