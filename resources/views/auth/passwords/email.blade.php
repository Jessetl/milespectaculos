@extends('layouts.app')

@section('content')

<div class="container my-5">
    <div class="row justify-content-center bg-light">
        <header class="page-header">
            <h5>RECUPERAR CONTRASEÑA</h5>
        </header>


        <div class="col-md-6 my-2">
            @if (Session::has('status'))
                <div class="alert alert-success">
                {{ Session::get('status') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Los datos introducidos en el formulario son incorrectos.
                </div>
            @endif
            <div class="card p-2">
                <div class="card-body">

                    {!! Form::open(['route' => 'password.email', 'method' => 'POST', 'autocomplete' => 'off']) !!}

                        <div class="form-group{{ $errors->has('email') ? ' is-invalid' : '' }}">
                            <label for="email" class="control-label">E-Mail</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required/>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Restablecer contraseña
                            </button>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
