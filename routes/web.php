<?php

## Rutas de prueba
Route::get('test', 'HomeController@test')->name('test.image');

## Ruta de inicio de sesión
Route::get('/', 'HomeController@index')->name('home')->middleware('confirm');

## Ruta ajax para videos y mp3
Route::get('post/{slug}/files', 'SearchController@files')->name('files');
Route::get('post/{slug}/images', 'SearchController@images')->name('files__images');

## Rutas de Buscadores
Route::get('{slug}/posts', 'SearchController@searchDistrict')->name('district.posts');
Route::get('posts-lists/advanced', 'SearchController@searchAdvanced')->name('advanced.search');
Route::get('posts-search', 'SearchController@searchIndex')->name('search.index');
Route::get('category/{slug}', 'SearchController@redirect')->name('category.return');
Route::get('posts-lists', 'SearchController@search')->name('category.search');

## Rutas ajax para distritos
Route::get('get/districts', 'HomeController@dt__set')->name('dt__set');
Route::get('get/{id}/district', 'HomeController@dt_get')->name('dt__get');

## Rutas ajax para categorias
Route::get('get/{id}/subcategory', 'HomeController@ct_get')->name('ct__get');

## Ruta para ver el post de oferta de servicios
Route::get('{slug}/post', 'HomeController@getPost')->name('view.post');
## Ruta para ver el post de demanda de servicios

## Ruta para ver el monto de provincias
Route::get('get/payment', 'HomeController@getPayment')->name('get__payment');

## Rutas Frontend acerca de la empresa.
Route::get('get/{subcategory}', 'HomeController@getCategories')->name('subcategory.get');
Route::get('legal', function () { return view('frontend.landing.legal'); });
Route::get('use', function () { return view('frontend.landing.use'); });
Route::get('about/us', 'HomeController@about__us')->name('about_us');
Route::get('questions', 'HomeController@questions')->name('questions');
Route::get('help', 'HomeController@help')->name('help');

## Contactanos y soporte técnico
Route::get('contact/us', 'HomeController@contacme')->name('contactme');
Route::post('contactus', 'SendEmails@contact__us')->name('contact__us');

## Contacto Mailbox Users
Route::get('send/{slug}/mail', 'SendEmails@pt__mail')->name('post.mail');
Route::post('send/email', 'SendEmails@send__email')->name('post.sendmail');
Route::get('send/{id}/answer', 'SendEmails@send__answer')->name('mail__answer');
Route::post('send/{id}/answers', 'SendEmails@send__answers')->name('mail__answers');

## Rutas para verificar y eliminar ## ATENCIÓN
Route::get('mailbox', function () { return view('frontend.landing.mailbox'); });
Route::post('mailbox/store', 'SendEmails@suggestions')->name('mailbox.store');

## Ruta para ver los precios de los usuarios VIP
Route::get('prices', 'HomeController@get__prices')->name('prices');

## Ruta de confirmación de email
Route::get('confirmed/{code}', 'HomeController@confirmed')->name('profile.confirmed');
Route::get('resend', 'HomeController@resend')->name('resend');
Route::post('resend/email', 'HomeController@resend__email')->name('resend.email');

Auth::routes();

Route::namespace('Frontend')->group(function () {
	## Ruta para organizar los eventos
	Route::get('budgets/events', 'BudgetsController@event')->name('budgets.event');
	Route::get('budgets', 'BudgetsController@ad')->name('budgets.ad')->middleware('auth');
	Route::post('budgets/event', 'BudgetsController@storeEvent')->name('budgets.storeEvent');
	Route::get('budgets/show', 'BudgetsController@showPost')->name('budgets.showPost');

	## Ruta para guardar post
	Route::post('budgets/post', 'BudgetsController@storePost')->name('budgets.storePost');

	## Ruta para notificaciones de contacto
	Route::post('notify/{post}', 'NotifyController@notify')->name('notify');
	Route::get('notify/{notify}', 'NotifyController@show')->name('notify.show')->middleware('auth');
});

## Rutas para eventos guardados en cookies
Route::middleware('guest')->group(function () {

	Route::get('my-favorites', 'FavoritesController@my_favorites')->name('my_favorites');
	Route::post('my-favorites/store', 'FavoritesController@my_favorites__save')->name('store.my_favorites');
	Route::delete('my-favorites/delete', 'FavoritesController@my_favorites__delete')->name('delete.my_favorites');
});

## Controllers Within The "App\Http\Controllers\Auth" Namespace
Route::namespace('Auth')->group(function () {

	## Autentificación con Facebook
	Route::get('auth/facebook', 'SocialAuthController@facebook')->name('auth.facebook');
	Route::get('auth/facebook/callback', 'SocialAuthController@callback')->name('auth.callback');
	Route::post('auth/facebook/register', 'SocialAuthController@register')->name('register.facebook');

});

## Controllers Within The "App\Http\Controllers\Payments" Namespace
Route::namespace('Payments')->group(function () {

	Route::get('budgets/payment', 'PaymentsBudgets@PostPayment')->name('payment.budgetPostPayment');
	Route::get('payments/{post}/budgets', 'PaymentsBudgets@payment')->name('payment.budgetPost');
	Route::get('payments/budgets/approved', 'PaymentsBudgets@approved')->name('payments.budgetApproved');

	Route::get('subscriptions', 'PaymentsSubscriptions@subscription')->name('subscription');
	Route::get('subscriptions/{paypal}', 'PaymentsSubscriptions@payment')->name('subscription.payment');
	Route::get('subscriptions/paypal/return', 'PaymentsSubscriptions@approved')->name('subscription.approved');
});

## Controllers Within The "App\Http\Controllers\Frontend" Namespace
Route::namespace('Frontend')->group(function () {
	
	## Zona para usuarios de pago
	Route::get('{slug}/profile', 'ZoneVIPController@profile')->name('user.profile');

	## Rutas de eventos y usuarios de pago
	Route::resource('events', 'EventsController', ['only' => ['index', 'show']]);
	Route::resource('zone-vip', 'ZoneVIPController');
});

## Controllers Within The "App\Http\Controllers\Frontend" Namespace
Route::namespace('Frontend')->middleware(['auth', 'confirm'])->group(function () {

	## Ruta para subir imagenes en cache
	Route::post('upload/files', 'BudgetsController@uploadFiles')->name('uploadFiles');
	Route::post('upload/videos', 'BudgetsController@uploadVideos')->name('uploadVideos');
	Route::post('upload/audios', 'BudgetsController@uploadAudios')->name('uploadAudios');

	## Ruta para cobrar por anuncios con presupuestos
	Route::get('payment/post', 'BillingsController@billingPost')->name('billing.post');

	## Ruta para los mails que se envian los usuarios
	Route::get('mails', 'MailsController@index')->name('mails.index');
	Route::get('mails/{id}', 'MailsController@show')->name('mail.show');
	Route::get('mails/{id}/answer', 'MailsController@answer')->name('mail.answer');
	Route::post('mails/answer', 'MailsController@pt__answer')->name('mail.answer__pt');

	## Ruta para suscribirse mediante cupones
	Route::post('subscriptions/code', 'EnterSuscriptionsController@save')->name('subscriptions.code');

	## Ruta para pagar por datos de anuncio
	Route::get('notify/{notify}/billing', 'BillingsController@billing')->name('billings.notify');
	Route::get('notify/{notify}/approved', 'BillingsController@approved')->name('billings.approved');
	Route::get('notify/{notify}/manage', 'NotifyController@manage')->name('notify.manage');

	## Rutas para añadir un nuevo anuncio
	Route::get('posts/draft', 'PostsController@draft')->name('posts.draft');
	Route::get('posts/new', 'PostsController@new')->name('posts.new');
	Route::get('posts/new/{category}', 'PostsController@new__ptc')->name('posts.new__ptc');
	Route::get('posts/hedge', 'PostsController@pt__hedge')->name('posts.hedge');
	Route::get('posts/keep', 'PostsController@pt__keep')->name('posts.keep');
	Route::get('posts/{slug}/preview', 'PostsController@pt__preview')->name('posts.preview');
	Route::get('posts/{slug}/shopping_cart', 'PostsController@shopping__cart')->name('posts.shopping_cart');

	## Rutas para anuncios en estado de borrador.
	Route::get('posts/return', 'PostsController@return')->name('posts.return');
	Route::post('posts/new', 'PostsController@new__pts')->name('posts.new__pts');

	## Rutas para los avisos de la web
	Route::get('posts/create/images', 'PostsController@postImages')->name('posts.create.image');
	Route::get('posts-ads/advice', 'PostsController@getAdvice')->name('advice');

	## Rutas para pagos de provincias
	Route::get('payment', 'ShoppingCartController@shoppingReturn')->name('shopping.return');
	Route::post('posts/payments', 'ShoppingCartController@shoppingItems')->name('shopping.items');
	
	## Ruta de pagos
	Route::get('posts/payments', 'PostsController@getPayment')->name('get.payment');

	## Rutas para cargar imagenes, videos y sonidos de los anuncios
	Route::put('posts/{id}/images', 'UploadsController@upload')->name('posts.upload');
	Route::put('posts/{id}/movies', 'UploadsController@uploadMovies')->name('upload.movies');
	Route::put('posts/{id}/sounds', 'UploadsController@uploadSounds')->name('upload.sounds');
	Route::delete('meta/{id}', 'PostsController@drop')->name('meta.destroy');

	## Ruta para subir video.
	Route::get('upload/{slug}/video', 'UploadsController@upload__video')->name('upload.video');
	Route::put('upload/video/{slug}', 'UploadsController@upload_youtube')->name('upload.youtube');

	/*Route::post('{slug}/publish', 'SubscriptionsController@postPayment')->name('post.postPayment');
	Route::get('payment/status', 'SubscriptionsController@getPaymentStatus')->name('payment.status');*/

	## Ruta de posts creados
	Route::get('{slug}/publish/my-posts', 'MyPostsController@publish')->name('my-posts.publish');
	Route::get('my-posts/gallery/{slug}', 'MyPostsController@gallery')->name('my-posts.gallery');
	Route::get('my-posts/renew/{slug}', 'MyPostsController@renew')->name('my-posts.renew');

	## Rutas de seguidores
	Route::post('{username}/follow', 'FollowersController@follow')->name('user.follow');
	Route::post('{username}/unfollow', 'FollowersController@unfollow')->name('user.unfollow');
	Route::get('{username}/followers', 'FollowersController@followers')->name('user.followers');

	## Ruta para añadir favoritos
	Route::post('favorite/add', 'FavoritesController@favorite')->name('post.favorite');

	## Ruta de reporte de anuncios
	Route::get('reports/{slug}/create', 'ReportsController@create')->name('reports.create');
	Route::post('{slug}/reports', 'ReportsController@store')->name('reports.store');

	## Ruta de perfil para usuarios de pago
	Route::get('{username}/page', 'PremiumsController@index')->name('username.index')->middleware('premium');

	## Ruta de demanda de servicios
	Route::get('services/demands', 'DemandsController@index')->name('services.demands')->middleware('premium');

	## Rutas de opiniones para los perfiles de usuarios de pago
	Route::post('messages', 'MessagesController@message')->name('messages.message');
	Route::post('messages/response', 'MessagesController@response')->name('messages.response');
	
	## Ruta de perfil de usuario
	Route::get('profile', 'ProfileController@index')->name('user.profile');
	Route::post('profile/save', 'ProfileController@update')->name('profile.save');

	## Ruta para comprar creditos
	Route::get('credits', 'CreditsController@create')->name('credits.create');
	Route::post('credits/purchase', 'CreditsController@credits__purchase')->name('credits.purchase');

	## Ruta para la red social interna
	Route::get('network', 'NetwController@index')->name('netw');
	Route::post('network/comment', 'NetwController@comment');
	Route::post('network/like', 'NetwController@like');
	Route::get('my/network', 'NetwController@my_network')->name('my/network');
	Route::post('my/network', 'NetwController@mynet_store')->name('my/network_store');

	## Rutas de anuncios, anuncios creados, eventos y favoritos.
	Route::resource('posts', 'PostsController', ['except' => ['index', 'edit', 'show']]);
	Route::resource('my-posts', 'MyPostsController', ['except' => ['create', 'store']]);
	Route::resource('my-events', 'MyEventsController');
	Route::resource('favorites', 'FavoritesController');
});

// Controllers Within The "App\Http\Controllers\Backend" Namespace
Route::namespace('Backend')->prefix('admin')->middleware(['auth', 'master'])->group(function () {

	## Ruta inicial del admin
	Route::get('/', 'MasterController@index')->name('master');

	## Ruta para gestionar anuncios
	Route::get('budgets/post', 'BudgetsPostController@index')->name('budgetsPost.index');
	Route::get('budgets/post/{post}', 'BudgetsPostController@edit')->name('budgetsPost.edit');
	Route::put('budgets/post/{post}/update', 'BudgetsPostController@update')->name('budgetsPost.update');

	## Ruta para gestionar videos, imagenes y audios
	Route::get('budgets/post/{post}/gallery', 'BudgetsPostController@gallery')->name('budgetsPost.gallery');
	Route::get('budgets/post/{post}/movies', 'BudgetsPostController@movies')->name('budgetsPost.movies');
	Route::get('budgets/post/{post}/sounds', 'BudgetsPostController@sounds')->name('budgetsPost.sounds');
	## Rutas para guardar los videos, imagenes y audios
	Route::put('budgets/post/{post}/gallery', 'BudgetsPostController@updateGallery')->name('budgetsPost.updateGallery');
	Route::put('budgets/post/{post}/movies', 'BudgetsPostController@updateMovies')->name('budgetsPost.updateMovies');
	Route::put('budgets/post/{post}/sounds', 'BudgetsPostController@updateSounds')->name('budgetsPost.updateSounds');
	## Rutas para eliminar los videos, imagenes y audios
	Route::get('budgets/delete/{id}/gallery', 'BudgetsPostController@deleteGallery')->name('budgetsPost.deleteGallery');
	Route::get('budgets/delete/{id}/movies', 'BudgetsPostController@deleteMovies')->name('budgetsPost.deleteMovies');
	Route::get('budgets/delete/{id}/sounds', 'BudgetsPostController@deleteSounds')->name('budgetsPost.deleteSounds');

	## Ruta para presupuestos de eventos
	Route::get('budgets', 'BudgetsController@index')->name('budgets.index');
	Route::get('budgets/{id}', 'BudgetsController@show')->name('budgets.show');
	Route::get('budgets/{id}/success', 'BudgetsController@success')->name('budgets.success');
	Route::delete('budgets/{id}', 'BudgetsController@destroy')->name('budgets.destroy');


	## Ruta para notificaciones
	Route::get('manages', 'ManageController@index')->name('manages.index');

	## Ruta para modificar el perfil del Usuario
	Route::get('my-profile', 'ProfileController@index')->name('profile.index');
	Route::post('my-profile', 'ProfileController@update')->name('profile.update');

	## Ruta para generar códigos de suscripciones
	Route::get('encrypted', 'EncryptedController@index')->name('encrypted.index');
	Route::get('encrypted/create', 'EncryptedController@create')->name('encrypted.create');
	Route::post('encrypted', 'EncryptedController@store')->name('encrypted.store');
	Route::delete('encrypted/{id}', 'EncryptedController@delete')->name('encrypted.destroy');
	
	## Rutas para header de paginas
	Route::get('headers/pages', 'HeadersController@headers_pages')->name('headers_pages');
	Route::get('headers/{id}/pages', 'HeadersController@edit_headers_pages')->name('edit.headers_pages');
	Route::put('headers/{id}/update-pages', 'HeadersController@update_headers_pages')->name('update.headers_pages');
	Route::delete('headers/{id}/header-delete_page', 'HeadersController@delete_headers_page')->name('delete.headers_page');

	## Rutas para pagos paypal
	Route::get('paypal/{slug}/edit', 'PaypalController@edit')->name('paypal.edit');
	Route::get('paypal', 'PaypalController@index')->name('paypal.index');
	Route::put('paypal/{slug}/update', 'PaypalController@update')->name('paypal.update');

	## Rutas para headers del frontend
	Route::get('headers/category', 'HeadersController@header__category')->name('headers.index');
	Route::get('headers/subcategory', 'HeadersController@header__category_type')->name('headers.categories-type');
	Route::get('headers/{slug}/edit', 'HeadersController@edit')->name('headers.edit');
	Route::get('headers/{slug}/edit-category_type', 'HeadersController@edit_category__type')->name('headers.edit-category_type');
	Route::put('headers/{slug}/update', 'HeadersController@update')->name('headers.update');
	Route::put('headers/{slug}/update-category_type', 'HeadersController@update_category__type')->name('headers.update-category_type');
	Route::delete('headers/{slug}/category', 'HeadersController@delete_category')->name('headers.delete-category');
	Route::delete('headers/{slug}/category-type', 'HeadersController@delete_category__type')->name('headers.delete-category__type');

	## Ruta de carousel
	Route::get('carousel', 'CarouselController@index')->name('carousel.index');
	Route::post('carousel/{id}', 'CarouselController@store')->name('carousel.store');

	## Rutas de facturaciones de usuarios
	Route::get('subscriptions/users', 'SubscriptionsController@index')->name('subscriptions.index');
	Route::get('subscriptions/{payment}', 'SubscriptionsController@show')->name('subscriptions.show');

	## Rutas de facturaciones de anuncios
	Route::get('advertisements/posts', 'AdvertisementsController@index')->name('advertisements.index');
	Route::get('advertisements/{payment}', 'AdvertisementsController@show')->name('advertisements.show');

	## Rutas para transferir anuncios
	Route::get('{slug}/to', 'PostsController@toTransfer')->name('toTransfer');
	Route::get('search/user/{email}', 'PostsController@search')->name('search.user');
	Route::post('to/transfer', 'PostsController@update')->name('toTransfer.update');

	## Rutas generales de la administración
	Route::resource('mailbox', 'MailboxController');
	Route::resource('categories', 'CategoriesController');
	Route::resource('categories-type', 'CategoriesTypeController');
	Route::resource('published', 'PostsController', ['only' => ['index', 'destroy']]);
	Route::resource('users', 'UsersController', ['except' => ['show']]);
	Route::resource('complaints', 'ComplaintsController', ['only' => ['index', 'destroy']]);
	Route::resource('config', 'ConfigurationsController', ['only' => ['index', 'edit', 'update', 'destroy']]);
	Route::resource('events', 'EventsController');
});