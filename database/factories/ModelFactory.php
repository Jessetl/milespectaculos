<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->unique()->userName,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'role' => App\Role::all()->random()->id,
        'premium' => $faker->randomElement($array = array('1','0')),
    ];
});

$factory->define(App\Post::class, function (Faker\Generator $faker) {

	$word = $faker->unique()->word;

    return [
        'category_type' => App\CategoryType::all()->random()->id,
        'user' => App\User::all()->random()->id,
        'name' => $word,
        'slug' => str_replace(' ', '-', strtolower($word)),
        'code' => 'r'.$faker->unique()->randomNumber($nbDigits = 9),
        'content' => $faker->text,
        'type' => $faker->randomElement($array = array('1','0')),
        'type_posts' => $faker->randomElement($array = array('1','0')),
        'type_amount' => $faker->randomElement($array = array('Otro', 'A Consultar', 'A Convenir')),
        'amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 700),
        'address' => $faker->streetAddress,
        'email' => $faker->safeEmail,
        'postal_code' => $faker->postcode,
        'status' => 'published',
    ];
});

$factory->define(App\PostMeta::class, function (Faker\Generator $faker) {

    return [
        'post' => App\Post::all()->random()->id,
        'meta_key' => 'Image',
        'meta_url' => 'icon.png',
    ];
});

$factory->define(App\Event::class, function (Faker\Generator $faker) {

    $word = $faker->unique()->word;

    return [
        'user' => App\User::all()->random()->id,
        'district' => App\District::all()->random()->id,
        'circuit' => $faker->citySuffix,
        'title' => $word,
        'slug' => str_replace(' ', '-', strtolower($word)),
        'date' => $faker->dateTimeBetween('+1 week', '+1 month'),
        'address' => $faker->streetAddress,
    ];
});