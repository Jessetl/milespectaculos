<?php

use Illuminate\Database\Seeder;

class CarouselTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('carousel')->insert([
            [
                'id' => 1, 
                'name' => 'Premium',    
                'status' =>  1,          
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 2, 
                'name' => 'Free',    
                'status' =>  0,          
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ]
        ]);
    }
}
