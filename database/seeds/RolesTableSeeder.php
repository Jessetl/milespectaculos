<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([ 
        	[
                'id' => 1, 
                'name' => 'Softevolution', 
                'slug' => 'softevolution', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 2, 'name' => 
                'Administrador', 
                'slug' => 'administrador', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 3, 
                'name' => 'Usuario', 
                'slug' => 'user', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 4, 
                'name' => 'Artista', 
                'slug' => 'artist', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 5, 
                'name' => 'Empresa', 
                'slug' => 'company', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ]
        ]);
    }
}