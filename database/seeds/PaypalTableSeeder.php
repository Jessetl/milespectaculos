<?php

use Illuminate\Database\Seeder;

class PaypalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('paypal')->insert([
        	[
                'id' => 1, 
                'name' => 'Artista Premium', 
                'slug' => 'paymentsar-premium',
                'description' => 'Pago de servicios para artistas premium.',  
                'amount' => 12.00, 
                'status' => 'ACTIVO',
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
            [
                'id' => 2, 
                'name' => 'Empresa Premium', 
                'slug' => 'paymentsem-premium',
                'description' => 'Pago de servicios para empresas premium.',  
                'amount' => 150.00, 
                'status' => 'ACTIVO',
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
            [
                'id' => 3, 
                'name' => 'Provincias Individuales', 
                'slug' => 'paymentspro-provincias',
                'description' => 'Pago por provincias adicionales.',  
                'amount' => 1.00, 
                'status' => 'ACTIVO',
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ]
        ]);
    }
}
