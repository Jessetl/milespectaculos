<?php

use Illuminate\Database\Seeder;

class HeadersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('headers_page')->insert([
        	[
                'id' => 1, 
                'url' => NULL,
                'page' => 'home', 
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
            [
                'id' => 2, 
                'url' => NULL,
                'page' => 'about', 
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
            [
                'id' => 3, 
                'url' => NULL,
                'page' => 'contact', 
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
            [
                'id' => 4, 
                'url' => NULL,
                'page' => 'legal_warning', 
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
            [
                'id' => 5, 
                'url' => NULL,
                'page' => 'support', 
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
            [
                'id' => 6, 
                'url' => NULL,
                'page' => 'blog', 
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ]        
        ]);
    }
}
