<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CategoriesTypeTableSeeder::class);
        $this->call(ConfigurationTableSeeder::class);
        $this->call(PaypalTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CarouselTableSeeder::class);

        /*$users = factory(\App\User::class, 50)->create(); 
        
        $users->each(function (App\User $user) use ($users) {

            $user->follows()->sync(
                $users->random(10)
            );
        });

        $posts = factory(\App\Post::class, 100)->create(); 


        $posts->each(function (App\Post $post) use ($users) {

            $post->users()->sync(
                $users->random(10)
            );
        });
        
        factory(\App\PostMeta::class, 200)->create();
        factory(\App\Event::class, 50)->create(); */ 
    }
}
