<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([ 
            'id' => 1, 
            'username' => 'franklatino',
            'name' => 'Francisco',
            'surname' => 'Carvajal',
            'email' => 'info@milespectaculos.com', 
            'password' => bcrypt('secret'), 
            'role' => 2, 
            'premium' => 1,
            'created_at' => new DateTime, 
            'updated_at' => new DateTime
        ]);
    }
}
