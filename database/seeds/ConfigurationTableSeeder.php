<?php

use Illuminate\Database\Seeder;

class ConfigurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('configurations')->insert([
        	[
                'id' => 1, 
                'meta_key' => 'Fotos', 
                'slug' => 'fotos-gratis',  
                'meta_value' => 4, 
                'created_at' => new DateTime, 
                'updated_at' => new datetime
            ],
        	[
                'id' => 2, 
                'meta_key' => 'Videos', 
                'slug' => 'videos-gratis', 
                'meta_value' => 1,
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 3, 
                'meta_key' => 'Audios',  
                'slug' => 'audios-gratis', 
                'meta_value' => 2, 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ]
        ]);
    }
}
