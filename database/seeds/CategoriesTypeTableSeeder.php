<?php

use Illuminate\Database\Seeder;

class CategoriesTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories_type')->insert([
        	[
                'id' => 1, 
                'category' => 1, 
                'name' => 'Bandas Municipales', 			
                'slug' => 'bandas-municipales', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 2, 
                'category' => 1, 
                'name' => 'Batucadas', 					
                'slug' => 'batucadas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 3, 
                'category' => 1, 
                'name' => 'Canción Folcrórica Española', 	
                'slug' => 'cancion-folkrorica-espanola', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 4, 
                'category' => 1, 
                'name' => 'Coros Rocieros, Rumbas y Sevillanas', 	       
                'slug' => 'coros-rocieros-rumbas-y-sevillanas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 5, 
                'category' => 1, 
                'name' => 'Disc Jockey (DJ)', 			
                'slug' => 'disk-jockey-dj', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 6, 'category' => 1, 
                'name' => 'Grupos De Baile (Dúos, Tríos, Cuartetos', 	   
                'slug' => 'grupos-de-baile', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 7, 
                'category' => 1, 
                'name' => 'Grupos Flamencos', 			
                'slug' => 'grupos-flamencos', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 8, 
                'category' => 1, 
                'name' => 'Grupos Mariachis', 			
                'slug' => 'grupos-mariachis', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 9, 
                'category' => 1, 
                'name' => 'Grupos de Música Clásica', 	
                'slug' => 'grupos-musica-clasica', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 10, 
                'category' => 1, 
                'name' => 'Grupos De Musisa Jazz, Blues Soul, Gospel',  
                'slug' => 'grupos-de-musica-jazz-blues-gospel', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 11, 
                'category' => 1, 
                'name' => 'Grupos De Música Latina', 	
                'slug' => 'grupos-de-musica-latina', 
                'created_at' => new DateTime, 'updated_at' => new DateTime
            ],
        	[
                'id' => 12, 
                'category' => 1, 
                'name' => 'Grupos de Música Pop, Rock, Reggae', 	       
                'slug' => 'grupos-de-musica-pop-rock-reggae', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 13, 
                'category' => 1, 
                'name' => 'Grupos de Música Rock Duro, Heavy Metal',    
                'slug' => 'grupos-de-musica-rock-heavy', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 14, 
                'category' => 1, 
                'name' => 'Música Celta', 				
                'slug' => 'musica-celta', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 15, 
                'category' => 1, 
                'name' => 'Música Étnica', 				
                'slug' => 'musica-etnica', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 16, 
                'category' => 1, 
                'name' => 'Música Folk', 				
                'slug' => 'musica-folk', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 17, 
                'category' => 1, 
                'name' => 'Música Hip Hop, Rap, Disco, Dance', 	        
                'slug' => 'musica-hip-hop-rap-disco-dance', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 18, 
                'category' => 1, 
                'name' => 'Orquestas De Baile', 		    
                'slug' => 'orquestas-de-baile', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        	[
                'id' => 19, 
                'category' => 1, 
                'name' => 'Tunas', 	
                'slug' => 'tunas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 20, 
                'category' => 1, 
                'name' => 'Otros',    
                'slug' => 'otros', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 21, 
                'category' => 2, 
                'name' => 'Alquiler De Atracciones Mecánicas',            
                'slug' => 'alquiler-de-atracciones-mecanicas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 22, 
                'category' => 2, 
                'name' => 'Alquiler De Autobuses',       
                'slug' => 'alquiler-de-autobuses', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 23, 
                'category' => 2, 
                'name' => 'Alquiler De Carpas',          
                'slug' => 'alquiler-de-carpas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 24, 
                'category' => 2, 
                'name' => 'Alquiler De Coches De Caballos',               
                'slug' => 'alquiler-de-coches-de-caballos', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 25, 
                'category' => 2, 
                'name' => 'Alquiler De Disfraces',       
                'slug' => 'alquiler-de-disfraces', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 26, 
                'category' => 2, 
                'name' => 'Alquiler De Escenarios',      
                'slug' => 'alquiler-de-escenarios', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 27, 
                'category' => 2, 
                'name' => 'Alquiler De Karaoke',         
                'slug' => 'alquiler-de-karaoke', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 28, 
                'category' => 2, 
                'name' => 'Alquiler De Limusinas Y Coches Clásicos',      
                'slug' => 'alquiler-de-limusinas-y-coches-clasicos', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 29, 
                'category' => 2, 
                'name' => 'Alquiler De Mesas Y Sillas',   
                'slug' => 'alquiler-de-mesas-y-sillas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 30, 
                'category' => 2, 
                'name' => 'Alquiler De Sanitarios',       
                'slug' => 'alquiler-de-sanitarios', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 31, 
                'category' => 2, 
                'name' => 'Alquiler De Sonido E Iluminación',            
                'slug' => 'alquiler-de-sonido-e-iluminacion', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 32, 
                'category' => 2, 
                'name' => 'Alquiler de Tarimas y Escenarios',            
                'slug' => 'alquiler-de-tarimas-y-escenarios', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 33, 
                'category' => 2, 
                'name' => 'Alquiler Maquina Fiestas De La Espuma',       
                'slug' => 'alquiler-de-maquina-de-espuma', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 34, 
                'category' => 2, 
                'name' => 'Catering',                    
                'slug' => 'catering', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 35, 
                'category' => 2, 
                'name' => 'Discoteca Móvil',             
                'slug' => 'discoteca-movil', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 36, 
                'category' => 2, 
                'name' => 'Distribución De Carteles Y Publicidad',       
                'slug' => 'distribucion-de-carteles-y-publicidad', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 37, 
                'category' => 2, 
                'name' => 'Fuegos Artificiales',         
                'slug' => 'fuegos-artificiales', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 38, 
                'category' => 2, 
                'name' => 'Globos Aerostaticos',         
                'slug' => 'globos-aerostaticos', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'id' => 40, 
                'category' => 2, 
                'name' => 'Reportajes De Fotografía Y Vídeo',            
                'slug' => 'reportajes-de-fotrografia-y-video', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 104, 
                'category' => 2, 
                'name' => 'Servicios De Seguridad',      
                'slug' => 'servicios-de-seguridad', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 112, 
                'category' => 2, 
                'name' => 'Otros',                       
                'slug' => 'otros', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 41, 
                'category' => 3, 
                'name' => 'Acróbatas Y Malabaristas',   
                'slug' => 'acrobatas-y-malabaristas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 42, 
                'category' => 3, 
                'name' => 'Actores Y Actrices',          
                'slug' => 'actores-y-acrobatas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 43, 
                'category' => 3, 
                'name' => 'Cabalgatas',                  
                'slug' => 'cabalgatas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 44, 
                'category' => 3, 
                'name' => 'Compañías De Teatro',         
                'slug' => 'compañias-de-teatro', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 45, 
                'category' => 3, 
                'name' => 'Especialistas De Circo',      
                'slug' => 'especialistas-de-circo', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 46, 
                'category' => 3, 
                'name' => 'Fakires',                     
                'slug' => 'fakires', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 47, 
                'category' => 3, 
                'name' => 'Humoristas',                  
                'slug' => 'humoristas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 48, 
                'category' => 3, 
                'name' => 'Magos Ilusionistas',          
                'slug' => 'magos-ilusionistas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 49, 
                'category' => 3, 
                'name' => 'Mimos',                       
                'slug' => 'mimos', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 51, 
                'category' => 3, 
                'name' => 'Performers',                  
                'slug' => 'performers', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 52, 
                'category' => 3, 
                'name' => 'Revista, Cabaret',            
                'slug' => 'revista-cabaret', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 53, 
                'category' => 3, 
                'name' => 'Showman',                     
                'slug' => 'showman', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 54, 
                'category' => 3, 
                'name' => 'Ventriloquia',                
                'slug' => 'ventriloquia', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            [
                'id' => 111, 
                'category' => 3, 
                'name' => 'Otros',                       
                'slug' => 'Otros', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            ['id' => '105', 'category' => '4', 'name' => 'Arpistas',                    'slug' => 'arpistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '106', 'category' => '4', 'name' => 'Bajistas Y Contrabajistas',   'slug' => 'bajistas-y-contrabajistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '114', 'category' => '4', 'name' => 'Cantantes Femeninas',         'slug' => 'cantantes-femeninas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '108', 'category' => '4', 'name' => 'Cantantes Masculinos',        'slug' => 'cantantes-masculinos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '55', 'category' => '4', 'name' => 'Flautistas',                  'slug' => 'flautistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '56', 'category' => '4', 'name' => 'Guitarristas',                'slug' => 'guitarristas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '57', 'category' => '4', 'name' => 'Organistas',                  'slug' => 'organistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '59', 'category' => '4', 'name' => 'Percusionistas',              'slug' => 'percusionistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '60', 'category' => '4', 'name' => 'Pianistas',                   'slug' => 'pianistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '61', 'category' => '4', 'name' => 'Saxofonistas',                'slug' => 'saxofonistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '62', 'category' => '4', 'name' => 'Trompetistas',                'slug' => 'trompetistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '63', 'category' => '4', 'name' => 'Violinistas Y Violonchelistas',   'slug' => 'violinistas-y-violonchelistas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '115', 'category' => '4', 'name' => 'Otros',                       'slug' => 'otros', 'created_at' => new DateTime, 'updated_at' => new DateTime],


            ['id' => '64', 'category' => '5', 'name' => 'Academias De Danza',          'slug' => 'academias-de-danza', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '65', 'category' => '5', 'name' => 'Danza Clásica',               'slug' => 'danza-clasica', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '66', 'category' => '5', 'name' => 'Danza Étnica',                'slug' => 'danza-etnica', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '67', 'category' => '5', 'name' => 'Danza Flamenco',              'slug' => 'danza-flamenco', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '68', 'category' => '5', 'name' => 'Danza Folclórica',            'slug' => 'danza-folclorica', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '69', 'category' => '5', 'name' => 'Danza Latina',                'slug' => 'danza-latina', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '70', 'category' => '5', 'name' => 'Danza Moderna',               'slug' => 'danza-moderna', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '71', 'category' => '5', 'name' => 'Danza Oriental',              'slug' => 'danza-oriental', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '72', 'category' => '5', 'name' => 'Otros',                       'slug' => 'otros', 'created_at' => new DateTime, 'updated_at' => new DateTime],


            ['id' => '73', 'category' => '6', 'name' => 'Agencia De Azafatas Y Modelos',   'slug' => 'agencia-de-asafatas-y-modelos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '74', 'category' => '6', 'name' => 'Agencias de Espectáculos',    'slug' => 'agencias-de-espectaculos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '75', 'category' => '6', 'name' => 'Managers Y Representantes',   'slug' => 'managers-y-representantes', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '76', 'category' => '6', 'name' => 'Organizadores De Bodas',      'slug' => 'organizadores-de-bodas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '77', 'category' => '6', 'name' => 'Organizadores De Congresos',  'slug' => 'organizadores-de-congresos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '78', 'category' => '6', 'name' => 'Organizadores De Despedidas De Solteros/as',   'slug' => 'organizadores-de-despedidas-de-solteros', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '79', 'category' => '6', 'name' => 'Organizadores De Conciertos',  'slug' => 'organizadores-de-conciertos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '80', 'category' => '6', 'name' => 'Otros',   'slug' => 'otros', 'created_at' => new DateTime, 'updated_at' => new DateTime],


            ['id' => '81', 'category' => '7', 'name' => 'Animadores Infantiles',       'slug' => 'animadores-infantiles', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '82', 'category' => '7', 'name' => 'Castillos Hinchables',        'slug' => 'castillos-hinchables', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '83', 'category' => '7', 'name' => 'Cuenta Cuentos',              'slug' => 'cuenta-cuentos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '84', 'category' => '7', 'name' => 'Globoflexia',                 'slug' => 'globoflexia', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '85', 'category' => '7', 'name' => 'Maquilladores',               'slug' => 'maquilladores', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '86', 'category' => '7', 'name' => 'Orquestas Infantiles',        'slug' => 'orquestas infantiles', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '88', 'category' => '7', 'name' => 'Payasos',                     'slug' => 'payasos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '89', 'category' => '7', 'name' => 'Teatro De Marionetas',        'slug' => 'teatro-de-marionetas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '113', 'category' => '7', 'name' => 'Otros',                       'slug' => 'otros', 'created_at' => new DateTime, 'updated_at' => new DateTime],

            ['id' => '98', 'category' => '8', 'name' => 'Estudios De Grabación',       'slug' => 'estudios-de-grabacion', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '99', 'category' => '8', 'name' => 'Oferta De Empleo, Demanda De Empleo',     'slug' => 'oferta-de-empleo-demanda-de-empleo', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '101', 'category' => '8', 'name' => 'Venta De Accesorios Para El Espectáculo',   'slug' => 'venta-de-accesorios-para-el-espectaculo', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '102', 'category' => '8', 'name' => 'Venta De Equipos De Sonido Y Luces',     'slug' => 'venta-de-equipos-de-sonido-y-luces', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '103', 'category' => '8', 'name' => 'Venta De Instrumentos Musicales',        'slug' => 'venta-de-instrumentos-musicales', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '110', 'category' => '8', 'name' => 'Otros',                      'slug' => 'otros', 'created_at' => new DateTime, 'updated_at' => new DateTime],

            ['id' => '117', 'category' => '9', 'name' => 'Cine De Verano',                      'slug' => 'cine-de-verano', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '118', 'category' => '9', 'name' => 'Conciertos',                      'slug' => 'conciertos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '119', 'category' => '9', 'name' => 'Congresos',                      'slug' => 'congresos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '120', 'category' => '9', 'name' => 'Estrenos',                      'slug' => 'estrenos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '121', 'category' => '9', 'name' => 'Espectáculos Infantiles',                      'slug' => 'espectaculos-infantiles', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '122', 'category' => '9', 'name' => 'Excursiones',                      'slug' => 'excursiones', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '123', 'category' => '9', 'name' => 'Ferias',                      'slug' => 'ferias', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '124', 'category' => '9', 'name' => 'Fiestas Culturales',                      'slug' => 'fiestas-culturales', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '125', 'category' => '9', 'name' => 'Fiestas Gastronómicas',                      'slug' => 'fiestas-gastronomicas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '126', 'category' => '9', 'name' => 'Fiestas Patronales',                      'slug' => 'fiestas-patronales', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '127', 'category' => '9', 'name' => 'Fiestas Tematicas',                      'slug' => 'fiestas-tematicas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '128', 'category' => '9', 'name' => 'Museos',                      'slug' => 'museos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '129', 'category' => '9', 'name' => 'Rutas Organizadas',                      'slug' => 'rutas-organizadas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '130', 'category' => '9', 'name' => 'Teatro',                      'slug' => 'teatro', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '131', 'category' => '9', 'name' => 'Otros',                      'slug' => 'otros', 'created_at' => new DateTime, 'updated_at' => new DateTime],

            ['id' => '132', 'category' => '10', 'name' => 'Complementos Para El Espectáculo',                      'slug' => 'complementos-para-el-espectaculo', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '133', 'category' => '10', 'name' => 'Estudios De Grabación',                      'slug' => 'estudios-de-grabacion', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '134', 'category' => '10', 'name' => 'Oferta Y Demanda De Empleo',                      'slug' => 'oferta-y-demanda-de-empleo', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '135', 'category' => '10', 'name' => 'Tiendas De Instrumentos Musicales',                      'slug' => 'tiendas-de-instrumentos-musicales', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '136', 'category' => '10', 'name' => 'Tiendas De Sonido Y Luces',                      'slug' => 'tiendas-de-sonido-y-luces', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '137', 'category' => '10', 'name' => 'otros',                      'slug' => 'otros', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        ]);
    }
}