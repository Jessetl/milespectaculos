<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->insert([
            
            [
                'id' => 1, 
                'name' => 'Espectáculos Musicales',    
                'url' =>  NULL,          
                'slug' => 'espectaculos-musicales', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 2, 
                'name' => 'Servicios Adicionales Para Eventos',   
                'url'  => NULL,
                'slug' => 'servicios-adicionales-para-eventos', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
            
            [
                'id' => 3, 
                'name' => 'Espectáculos Teatro Y Variedades', 
                'url'  => NULL,   
                'slug' => 'espectaculos-teatro-y-variedades', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 4, 
                'name' => 'Músicos Solistas',   
                'url'  => NULL,                  
                'slug' => 'musicos-solistas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 5, 
                'name' => 'Espectáculos De Danza', 
                'url' => NULL,           
                'slug' => 'espectaculos-de-danza', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 6, 
                'name' => 'Organizadores de fiestas y eventos', 
                'url'  => NULL,   
                'slug' => 'organizadores-de-fiestas-y-eventos', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 7, 
                'name' => 'Espectáculos Infantiles', 
                'url'  => NULL,            
                'slug' => 'espectaculos-infantiles', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

        	[
                'id' => 8, 
                'name' => 'Anuncios Oferta Y demanda', 	
                'url'  => NULL,		
                'slug' => 'anuncios-oferta-y-demanda', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 9, 
                'name' => 'Arte y eventos',  
                'url'  => NULL,        
                'slug' => 'arte-y-eventos', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],

            [
                'id' => 10, 
                'name' => 'Servicios para artistas',  
                'url'  => NULL,        
                'slug' => 'servicios-para-artistas', 
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ],
        ]);
    }
}
