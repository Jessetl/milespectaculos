<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('districts')->insert([
            ['id' => '1', 'name' => 'Albacete', 	'slug' => 'albacete', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '2', 'name' => 'Ciudad Real', 	'slug' => 'ciudad-real', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '3', 'name' => 'Cuenca', 		'slug' => 'cuenca', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '4', 'name' => 'Guadalajara', 	'slug' => 'guadalajara', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '5', 'name' => 'Toledo', 		'slug' => 'toledo', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '6', 'name' => 'Huesca', 		'slug' => 'huesca', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '7', 'name' => 'Teruel', 		'slug' => 'teruel', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '8', 'name' => 'Zaragoza', 	'slug' => 'zaragoza', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '9', 'name' => 'Ceuta', 		'slug' => 'ceuta', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '10', 'name' => 'Madrid', 		'slug' => 'madrid', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '11', 'name' => 'Murcia', 		'slug' => 'murcia', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '12', 'name' => 'Melilla', 	'slug' => 'melilla', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '13', 'name' => 'Navarra', 	'slug' => 'navarra', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '14', 'name' => 'Almería', 	'slug' => 'almeria', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '15', 'name' => 'Cádiz', 		'slug' => 'cadiz', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '16', 'name' => 'Córdoba', 	'slug' => 'cordoba', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '17', 'name' => 'Granada', 	'slug' => 'granada', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '18', 'name' => 'Huelva', 		'slug' => 'huelva', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '19', 'name' => 'Jaén', 		'slug' => 'jaen', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '20', 'name' => 'Málaga', 		'slug' => 'malaga', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '21', 'name' => 'Sevilla', 	'slug' => 'sevilla', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '22', 'name' => 'Asturias', 	'slug' => 'asturias', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '23', 'name' => 'Cantabria', 	'slug' => 'cantabria', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '24', 'name' => 'Ávila', 		'slug' => 'avila', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '25', 'name' => 'Burgos', 		'slug' => 'burgos', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '26', 'name' => 'León', 		'slug' => 'leon', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '27', 'name' => 'Palencia', 	'slug' => 'palencia', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '28', 'name' => 'Salamanca', 	'slug' => 'salamanca', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '29', 'name' => 'Segovia', 	'slug' => 'segovia', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '30', 'name' => 'Soria', 		'slug' => 'soria', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '31', 'name' => 'Valladolid', 	'slug' => 'valladolid', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '32', 'name' => 'Zamora', 		'slug' => 'zamora', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '33', 'name' => 'Barcelona', 	'slug' => 'barcelona', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '34', 'name' => 'Girona', 		'slug' => 'girona', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '35', 'name' => 'LLeida', 		'slug' => 'llerida', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '36', 'name' => 'Tarragona', 	'slug' => 'tarragona', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '37', 'name' => 'Badajoz', 	'slug' => 'badajoz', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '38', 'name' => 'Cáceres', 	'slug' => 'caceres', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '39', 'name' => 'A Coruña', 	'slug' => 'a-coruna', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '40', 'name' => 'Lugo', 		'slug' => 'lugo', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '41', 'name' => 'Ourense', 		'slug' => 'ourense', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '42', 'name' => 'Pontevedra', 	'slug' => 'pontevedra', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '43', 'name' => 'La Rioja', 	'slug' => 'la-rioja', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '44', 'name' => 'Baleares', 'slug' => 'baleares', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '45', 'name' => 'Álava', 		'slug' => 'alava', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '46', 'name' => 'Guipúzcoa', 	'slug' => 'guipuzcoa', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '47', 'name' => 'Vizcaya', 	'slug' => 'vizcaya', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '48', 'name' => 'Las Palmas', 'slug' => 'las-palmas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '49', 'name' => 'Santa Cruz de Tenerife ', 'slug' => 'santa-cruz-de-tenerife', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '50', 'name' => 'Alicante', 	'slug' => 'alicante', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '51', 'name' => 'Castellón', 	'slug' => 'castellon', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => '52', 'name' => 'Valencia', 	'slug' => 'valencia', 'created_at' => new DateTime, 'updated_at' => new DateTime]
        ]);
    }
}
