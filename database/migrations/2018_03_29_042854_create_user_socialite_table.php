<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSocialiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_socialite', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->nullable()->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('Cascade');
            $table->enum('red', array('Facebook', 'Twitter', 'Instragram'));
            $table->string('socialite', 255);
            $table->string('profile', 255);
            $table->string('avatar', 255);
            $table->string('gender', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_socialite');
    }
}
