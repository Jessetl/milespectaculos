<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsCircuitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_circuits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post')->unsigned();
            $table->foreign('post')->references('id')->on('posts')->onDelete('Cascade');
            $table->integer('district')->unsigned();
            $table->foreign('district')->references('id')->on('districts')->onDelete('Cascade');
            $table->string('circuit', 255)->nullable();
            //$table->enum('status', array('approved', 'waiting'))->default('approved');
            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_circuits');
    }
}
