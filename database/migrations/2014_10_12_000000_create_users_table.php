<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 255)->nullable();
            $table->string('username', 35);
            $table->string('name', 45)->nullable();
            $table->string('surname', 45)->nullable();
            $table->string('email', 45)->unique();
            $table->string('password', 255);
            $table->integer('role')->unsigned()->default(3);
            $table->foreign('role')->references('id')->on('roles')->onDelete('Cascade');
            $table->boolean('confirmed')->default(1);
            $table->string('confirmation_code', 255)->nullable();
            $table->boolean('premium')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
