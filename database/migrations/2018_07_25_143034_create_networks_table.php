<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('networks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('Cascade');
            $table->integer('category_type')->unsigned()->nullable();
            $table->foreign('category_type')->references('id')->on('categories_type')->onDelete('Cascade');
            $table->integer('district')->unsigned()->nullable();
            $table->foreign('district')->references('id')->on('users')->onDelete('Cascade');
            $table->boolean('type_posts');
            $table->enum('type_amount', array('A Convenir', 'A Consultar', 'Otro'))->default('Otro');
            $table->float('amount', 15,2)->nullable();
            $table->longText('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('networks');
    }
}
