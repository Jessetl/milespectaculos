<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('post');
            $table->foreign('post')->references('id')->on('posts')->onDelete('Cascade');
            $table->string('payment')->nullable();
            $table->string('name', 255)->nullable();
            $table->float('price', 15, 2);
            $table->float('tax', 15, 2);
            $table->float('total', 15, 2);
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_posts');
    }
}
