<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadersPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headers_page', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 255)->nullable();
            $table->enum('page', array('home', 'about', 'contact', 'legal_warning', 'support', 'blog'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headers_page');
    }
}
