<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('Cascade');
            $table->string('title', 115)->nullable();
            $table->string('slug', 155)->nullable();
            $table->longText('address');
            $table->integer('district')->unsigned();
            $table->foreign('district')->references('id')->on('districts')->onDelete('Cascade');
            $table->string('circuit', 115);
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
