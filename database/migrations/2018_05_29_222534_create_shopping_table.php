<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post')->unsigned();
            $table->foreign('post')->references('id')->on('posts')->onDelete('Cascade');
            $table->enum('type_payment', array('paypal', 'stripe', 'other'))->default('paypal');
            $table->boolean('type');
            $table->string('description', 255);
            $table->string('paypal_payment_id', 255)->nullable();
            $table->float('amount_total', 15,2)->nullable();
            $table->float('iva', 15,2)->nullable();
            $table->enum('status', array('rejected', 'approved'))->default('rejected');
            $table->timestamp('ends_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping');
    }
}
