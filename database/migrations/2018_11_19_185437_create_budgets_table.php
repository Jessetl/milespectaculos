<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('type', 170);
            $table->string('country', 75);
            $table->string('postal', 75)->nullable();
            $table->date('dayevent');
            $table->string('duration', 45);
            $table->string('hourevent');
            $table->string('budget', 115)->nullable();
            $table->string('place', 75)->nullable();
            $table->longText('detail');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
