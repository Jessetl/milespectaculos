<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('header', 255)->nullable();
            $table->integer('category_type')->unsigned();
            $table->foreign('category_type')->references('id')->on('categories_type')->onDelete('Cascade');
            $table->integer('user')->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('Cascade');
            $table->string('name', 75)->nullable();
            $table->string('slug', 115)->nullable();
            $table->string('code', 115)->unique()->nullable();
            $table->longText('content')->nullable();
            $table->boolean('type')->nullable(); // Particular (0) o profesional (1)
            $table->boolean('type_posts')->nullable(); // Oferta (0) o demanda (1)
            $table->longText('address')->nullable();
            $table->string('directions', 255)->nullable(); // longitud y latitud en json
            $table->bigInteger('phone1')->nullable();
            $table->bigInteger('phone2')->nullable();
            $table->string('email', 75)->nullable();
            $table->string('postal_code')->nullable();
            $table->string('url', 255)->nullable();
            $table->enum('type_amount', array('Otro', 'A Consultar', 'A Convenir'))->default('Otro');
            $table->float('amount', 15,2)->nullable(); // Precio
            $table->integer('discount')->nullable();
            $table->date('length')->nullable(); // Duración de anuncio
            $table->string('status', 25)->default('draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
