<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notify', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('post');
            $table->foreign('post')->references('id')->on('posts')->onDelete('Cascade');
            $table->unsignedInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('Cascade');
            $table->string('type', 170);
            $table->string('country', 75);
            $table->string('postal', 75)->nullable();
            $table->date('dayevent');
            $table->string('hourevent');
            $table->string('place', 75)->nullable();
            $table->longText('detail');
            $table->boolean('authOdata')->default(0);
            $table->boolean('manage')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notify');
    }
}
