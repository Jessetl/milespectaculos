<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 75)->unique();
            $table->string('slug', 115)->unique();
            $table->longText('description')->nullable();
            $table->float('amount', 25,2)->default(0);
            $table->enum('type', array('F', 'T'))->default('F'); ## FIJO Y TEMPORAL
            $table->enum('status', array('ACTIVO', 'INACTIVO'))->default('INACTIVO');
            $table->timestamp('ends_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal');
    }
}
