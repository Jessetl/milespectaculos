<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopping extends Model
{
    protected $table = 'shopping';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post', 'type_payment', 'type', 'description', 'paypal_payment_id', 
        'amount_total', 'iva', 'status', 'ends_at'
    ];

    public function posts()
    {
    	return $this->belongsTo('App\Post', 'post');
    }

    public function items()
    {
    	return $this->hasMany('App\ShoppingCart', 'shopping', 'id');
    }
}
