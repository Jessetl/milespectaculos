<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PaymentSubscription extends Model
{
    protected $table = 'payment_subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'payment', 'name', 'price', 'tax', 'total', 'status',
    ];

    public function users()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function getCreatedAtAttribute($created_at)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $created_at)->format('d-m-Y h:i:s');
    }
}
