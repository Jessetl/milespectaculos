<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailAnswer extends Model
{
    protected $table = 'mails_answers';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mail', 'answer'
    ];

    public function getCreatedAtAttribute($date)
	{
		$newDate = Date::parse($date)->diffForHumans();

		return $newDate;
	}

    public function mails()
    {
        return $this->belongsTo('App\Mail', 'mail');
    }
}
