<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class Event extends Model
{
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'title', 'slug', 'address', 'district', 'circuit', 'date'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function getCreatedAtAttribute($date)
    {
        $newDate = Date::parse($date)->diffForHumans();

        return $newDate;
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->toFormattedDateString();
    }

    public function user_event()
    {
    	return $this->belongsTo('App\User', 'user');
    }   

    public function districts()
    {
        return $this->belongsTo('App\District', 'district');
    } 
}

