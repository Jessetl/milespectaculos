<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CategoryType extends Model
{
    protected $table = 'categories_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header', 'category', 'name', 'slug'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->toFormattedDateString();
    }

    public function categorie()
    {
    	return $this->belongsTo('App\Category', 'category', 'id');
    }

    public function posts()
    {
        return $this->hasMany('App\Post', 'category_type');
    }
}