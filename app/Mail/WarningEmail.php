<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WarningEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $post;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($post, $user)
    {
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return 
        $this->markdown('emails.expire')
        ->subject($this->user->name . ', Tu anuncio esta apunto de vencer.')
        ->from('info@milespectaculos.com')
        ->with([
            'user' => $this->user,
            'post' => $this->post,
        ]);
    }
}
