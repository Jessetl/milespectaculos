<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notify extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $notify;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notify)
    {
        $this->notify = $notify;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.notify')->subject('Nueva pregunta en ' . ucfirst(strtolower($this->notify->pts->name)))->from('info@milespectaculos.com', 'Milespectáculos')->with('notify', $this->notify);
    }
}
