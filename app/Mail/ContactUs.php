<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    public $mailbox;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailbox)
    {
        $this->mailbox = $mailbox;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.contactus.observations')
                ->subject('Mil espectáculos, tienes una nueva notificación de contacto.')
                ->from('info@milespectaculos.com')
                ->with('mail', $this->mailbox);
    }
}
