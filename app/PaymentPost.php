<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PaymentPost extends Model
{
    protected $table = 'payment_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post', 'payment', 'name', 'price', 'tax', 'total', 'status',
    ];

    public function posts()
    {
        return $this->belongsTo('App\Post', 'post');
    }

    public function getCreatedAtAttribute($created_at)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $created_at)->format('d-m-Y h:i:s');
    }
}
