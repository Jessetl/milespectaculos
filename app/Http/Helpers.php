<?php


use App\Post as Post;
use App\User as User;
use Jenssegers\Date\Date;
use Carbon\Carbon;

function postLoadImage($post)
{
	$meta = $post->posts_meta()->where('meta_key', 'Image')->first();
	
	if ( !is_null($meta) ) {

		return '/storage/posts/'. $meta->meta_url;	

	} else {

		return '/img/icon.png';	
	}
}

function is_premium($user)
{
	return User::find($user)->premium;
}

function isFavorite($user, Post $post) 
{
	return $user->favorites->contains($post);
}

function postImage($images)
{
	return $images->posts_meta()->where('meta_key', 'Image')->get();
}

function toDateString($date) 
{
	$newDate = Date::parse($date)->diffForHumans();

	return $newDate;
}

function toDateTimeString($date) 
{
	$newDate = new Carbon($date);

	return $newDate->toFormattedDateString();
}

function isFavoriteInCookies($post)
{
	
}