<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class RedirectIfPremium
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->premium) {

            return $next($request);

        }

        Session::flash('error', 'Disculpe usted no tiene acceso al usuario VIP, actualiza tu cuenta para obtener los beneficios');
        
        return redirect('zone-vip');
    }
}
