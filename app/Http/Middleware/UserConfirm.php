<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;

class UserConfirm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() !== null) {
            if (Auth::user()->confirmed) {
                return $next($request);
            }

            auth()->logout();

            Session::flash('email', 'Confirma tu dirección de correo electrónico, asegúrate de revisar tu bandeja de entrada y tus correos no deseados');

            return redirect('login');
        }

        return $next($request);
    }
}
