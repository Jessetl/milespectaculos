<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;

class RedirectIfAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Role::findOrFail(2);
        $role_st = Role::findOrFail(1);
        $user = \Auth::user();

        if (strcmp($role['id'], $user['role']) == 0 
            OR strcmp($role_st['id'], $user['role']) == 0) {

            return $next($request);
        }
        
        return redirect('/');
    }
}
