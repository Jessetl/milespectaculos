<?php

namespace App\Http\Controllers\Payments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// use to process billing agreements
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Agreement;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

use App\Post as Post;
use App\PaymentPost as Payout;
use Session;

class PaymentsBudgets extends Controller
{
    
    // Create a new instance with our paypal credentials
    public function __construct()
    {
        $this->middleware('auth');

        if (config('paypal.settings.mode') == 'live') {

            $this->client_id = config('paypal.live_client_id');
            $this->secret = config('paypal.live_secret');

        } else {

            $this->client_id = config('paypal.sandbox_client_id');
            $this->secret = config('paypal.sandbox_secret');

        }
        
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $this->apiContext->setConfig(config('paypal.settings'));
    }

    public function PostPayment(Request $request)
    {
        $post = $request->session()->get('post');

        return view('frontend.budgets.PostPayment', [
            'post' => $post
        ]);
    }

    public function payment(Request $request, Post $post)
    {
    	$payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $payname = 'Gestión de anuncio ' . $post->name;
        $payprice = 5;

        $tax = $payprice * 0.21;

        $item = new Item();
        $item->setName($payname)
            ->setCurrency('EUR')
            ->setDescription($payname)
            ->setQuantity(1)
            ->setPrice($payprice);

        $item_list = new ItemList();
        $item_list->setItems(array($item));

        $details = new Details();
        $details->setSubtotal($payprice)
            ->setTax($tax);

        $total = $payprice + $tax;

        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($total)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription($payname);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::route('payments.budgetApproved'))
            ->setCancelUrl(\URL::route('budgets.ad'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {

        	$payment->create($this->apiContext);
        	
        	$payout = new Payout();
        	$payout->post = $post->id;
        	$payout->payment = $payment->getId();
        	$payout->name = $payname;
        	$payout->price = $payprice;
        	$payout->tax = $tax;
        	$payout->total = $total;
        	$payout->save();

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

        	Session::flash('error', 'Hubo un error al procesar el pago, intente nuevamente o contacte con el soporte técnico.');

            return redirect()->back();
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        ## add payment ID to session
        $request->session()->put('paypal_payment_id', $payment->getId());
        $request->session()->put('payment', $payout);

        if(isset($redirect_url)) {
            ## redirect to paypal
            return \Redirect::away($redirect_url);
        }

        return redirect()->back()->with('error', 'Error desconocido, intentelo mas tarde.');
    }

    public function approved(Request $request)
    {
        // Get the payment ID before session clear
        $payment_id = $request->session()->get('paypal_payment_id');
        $payout = $request->session()->get('payment');

        $payerId = $request['PayerID'];
        $token = $request['token'];

        if (empty($payerId) OR empty($token) OR empty($payment_id)) {

            return redirect()->route('budgets.index')
                ->with('message', 'Hubo un problema al intentar pagar con Paypal');
        }

        try {

            $payment = Payment::get($payment_id, $this->apiContext);

            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            $result = $payment->execute($execution, $this->apiContext);

            if ($result->getState() == 'approved') {

                ## Modificamos el paypal_id como código unico de transacción.
                $payout->status = 1;
                $payout->save();

                // clear the session payment ID
                $request->session()->forget('paypal_payment_id');
                $request->session()->forget('payment');
                
                Session::flash('success', 'Tu anuncio sera publicado en las proximas 24h.');
                
                return redirect()->route('budgets.ad'); 
            }

        } catch (\Exception $e) {
            return redirect('budgets')->with('error', 'Error al procesar solicitud, intentelo mas tarde');
        }
    }
}
