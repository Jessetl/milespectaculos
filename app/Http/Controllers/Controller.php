<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function replace($string) {

        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        
        $slug = "$slug-" . mt_rand(10000, 99999);

        return strtolower($slug);
    }
}
