<?php

namespace App\Http\Controllers;

use App\MailAnswer as Answer;
use App\Mail as SendMail;
use App\Mailbox as Mailbox;
use App\Mail\AnswerMailAnswer;
use App\Mail\ContactUs;
use App\Mail\ContactUser;
use App\Post as Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;
use Session;

class SendEmails extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:75',
            'email' => 'required|email',
            'suggest' => 'required',
        ]);
    }

    public function send__email(Request $request)
    {
        $this->validator($request->all())->validate();

        $post = Post::whereSlug($request['slug'])->firstOrFail();

        try {

            $mail = new SendMail();
            $mail->post = $post['id'];
            $mail->names = $request['name'];
            $mail->email = $request['email'];
            $mail->message = $request['suggest'];
            $mail->save();

            Mail::to($post->email)->send(new ContactUser($mail, $post));

            return redirect()->back()->with('success', 'Hemos enviado tu email, gracias por contactar con Milespectáculos');

        } catch (\Exception $e) {

            $this->saveLogInfo($e);

            $mail->delete();

            Session::flash('error', 'Error al intentar enviar email, revisa tu conexión a internet y vuelve a intentarlo.');

            return redirect()->back()->withInput($request->all());
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateMessages(array $data)
    {
        return Validator::make($data, [
            'departament' => 'required',
            'name' => 'required|max:255',
            'phone' => 'numeric|digits_between:5,15',
            'email' => 'required|email',
            'suggest' => 'required|max:255',
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }
    public function contact__us(Request $request)
    {
        $this->validateMessages($request->all())->validate();

        try {

            $mailbox = new Mailbox();
            $mailbox->departament = $request['departament'];
            $mailbox->name = $request['name'];
            $mailbox->phone = $request['phone'];
            $mailbox->email = $request['email'];
            $mailbox->suggest = $request['suggest'];
            $mailbox->save();

            Mail::to('info@milespectaculos.com')->send(new ContactUs($mailbox));

        } catch (\Exception $e) {

            $mailbox->delete();

            $this->saveLogInfo($e);

            Session::flash('error', 'Error al intentar enviar email, revisa tu conexión a internet y vuelve a intentarlo.');

            return redirect()->back()->withInput($request->all());
        }

        return redirect()->back()->with('success', 'Hemos recibido tus observaciones gracias por contactar con Mil Espectáculos.');
    }

    public function send__answers(Request $request, $id)
    {
        $mail = SendMail::findOrFail($id);

        try {

            $answer = new Answer(['answer' => $request['suggest']]);
            $mail->answers()->save($answer);

            Mail::to($mail->email)->send(new AnswerMailAnswer($answer));

            Session::flash('success', 'Respuesta enviada');

        } catch (\Exception $e) {

            $mail->answers()->get()->last()->delete();

            Session::flash('error', 'Error al intentar enviar email, revisa tu conexión a internet y vuelve a intentarlo.');

            return redirect()->back();
        }

        return redirect()->back();
    }

    public function send__answer($id)
    {
        $mail = SendMail::with('answers')->findOrFail($id);

        return view('frontend.emails.answer', [
            'mail' => $mail,
        ]);
    }

    public function pt__mail($slug)
    {
        $post = Post::with('usr')->whereSlug($slug)->firstOrFail();

        return view('frontend.emails.mailbox', [
            'post' => $post,
        ]);
    }

    public function saveLogInfo($e)
    {
        // Almacenamos la información del error.
        return \Log::debug('Test fails' . $e);
    }
}
