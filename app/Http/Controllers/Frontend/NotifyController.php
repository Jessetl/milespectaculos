<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Mail\UserConfirmation;
use App\Mail\Notify as Email;
use App\Post;
use App\Notify;
use Mail;
use Carbon\Carbon;

class NotifyController extends Controller
{
	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'type' => 'required',
            'country' => 'required',
            'day' => 'required',
            'hour' => 'required',
            'place' => 'required',
            'detail' => 'required'
        ]);
    }

    public function notify(Request $request, Post $post)
    {
    	$this->validator($request->all())->validate();

    	$email = $post->email;

   		if ( empty($request->user()) ) {

            Validator::make($request->all(), [
                'name' => 'required|max:45',
                'email' => 'required|string|confirmed|email|max:45|unique:users',
                'password' => 'required|string|min:4|confirmed'
            ])->validate();

            $user = new User();
            $user->username = str_random(20);
            $user->name = $request['name'];
            $user->email = $request['email'];
            $user->phone = $request['phone'];
            $user->password = bcrypt($request['password']);
            $user->confirmed = 0;
            $user->confirmation_code = str_random(25);
            $user->save();

            Mail::to($user->email)->send(new UserConfirmation($user));
        }

     	$me = empty($request->user()) ? $user : $request->user();

        $notify = new Notify();
        $notify->post = $post->id;
        $notify->user = $me->id;
        $notify->type = $request['type'];
        $notify->country = $request['country'];
        $notify->postal = $request['postal_code'];
        $notify->dayevent = Carbon::parse($request['day'])->format('Y-m-d');
        $notify->hourevent = $request['hour'];
        $notify->place = $request['place'];
        $notify->detail = $request['detail'];
        $notify->save();

        $notify->load('pts', 'usr');
     
        Mail::to($this->email($post))->send(new Email($notify));
        
        $request->session()->flash('success', 'Hemos enviado tu mensaje, gracias por contactar con Milespectáculos');

    	return redirect()->back();
    }

    public function show(Notify $notify)
    {
        $notify->load('pts', 'usr');
        
        return view('frontend.notifications.show', [
            'notify' => $notify
        ]);
    }

    public function manage(Request $request, Notify $notify)
    {
        $notify->manage = 1;
        $notify->save();

        $request->session()->flash('success', 'Nos encargaremos de ello.');

        return redirect()->route('notify.show', $notify);
    }

    protected function email($post) 
    {	
    	return $post->email;
    }	
}
