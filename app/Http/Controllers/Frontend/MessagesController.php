<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Message as Message;
use App\Conversation as Response;
use App\Post as Post;
use Auth;

class MessagesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'message' => 'required|max:255'
        ]);
    }

    public function message(Request $request)
    {
        $this->validator($request->all())->validate();

    	$user = Auth::user();
    	$post = Post::whereSlug($request['post'])->firstOrFail();

    	$message = new Message();
    	$message->user = $user['id'];
    	$message->post = $post['id'];
    	$message->content = $request['message'];
    	$message->save();

    	return redirect()->back()->with('success', 'Compartiste un mensaje para esta publicación.');
    }
    
    public function response(Request $request)
    {
    	# code...
    }

}
