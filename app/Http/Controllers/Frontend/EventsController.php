<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event as Event;
use App\District as District;
use Auth;

class EventsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth',  ['only' => 'index', 'show']);
    }
    
    public function index()
    {
    	$events = Event::with('user_event')->orderBy('date', 'ASC')->paginate(10);
        
    	return view('frontend.events.index', [
            'events' => $events
        ]);
    }
}
