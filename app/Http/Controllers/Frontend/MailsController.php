<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Mail\AnswerMail;
use App\Mail as Mail;
use App\Post as Post;
use App\MailAnswer as Answer;
use Mail as SendMail;
use Session;

class MailsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $me = \Auth::user();
        $mails = array();

        foreach ($me->posts as $key => $post) 
        {
            ## Cargamos el post de ese email, para ver el código en la vista.
            foreach ($post->emails()->with('pts', 'usr')->orderBy('id', 'DESC')->get() as $key => $mail) 
            {
                $mails[] = $mail;
            }
        }

        $mails = $this->paginate($mails);

        return view('frontend.notifications.index', [
            'mails' => $mails
        ]);
    }

    public function show($id)
    {
    	$mail = Mail::with('answers')->findOrFail($id);

    	return view('frontend.notifications.view', [
    		'mail' => $mail
    	]);
    }

    public function answer($id)
    {
        $mail = Mail::findOrFail($id);

        return view('frontend.notifications.answer', [
            'mail' => $mail
        ]);
    }

    public function pt__answer(Request $request)
    {
        $mail = Mail::with('posts')->findOrFail($request['mail']);

        try {
            
            $answer = new Answer(['answer' => $request['suggest']]);
            $mail->answers()->save($answer);

            SendMail::to($mail->email)->send(new AnswerMail($answer));
            
            Session::flash('success', 'Respuesta enviada');

        } catch (\Exception $e) {
 
            $answer->delete();

            Session::flash('error', 'Error al intentar enviar email, revisa tu conexión a internet y vuelve a intentarlo.');

            return redirect()->back();
        }
        

        return redirect('mails');

    }

    protected function paginate($posts, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $posts = $posts instanceof Collection ? $posts : Collection::make($posts);

        return new LengthAwarePaginator($posts->forPage($page, $perPage), $posts->count(), $perPage, $page, ['path' => Paginator::resolveCurrentPath()]);
    }
}
