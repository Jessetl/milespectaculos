<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post as Post;
use App\PostMeta as Meta;
use App\Configuration as Configuration;
use Carbon\Carbon;
use Youtube;
use Auth;
use Session;
use Image;
use Storage;

class UploadsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function upload(Request $request, $id)
    {
        $configuration = Configuration::where('meta_key', 'Fotos')->firstOrFail();
        $post = Post::with('posts_meta')->findOrFail($id);
        $user = $request->user();

        $maxFile = $user['premium'] ? 99 : $configuration['meta_value'];

        $countFile = $post->posts_meta()->where('meta_key', 'Image')->get()->count();

        if ( $countFile >= $maxFile ) {

            $message = 'El límite para usuarios gratuitos es de ' . $configuration['meta_value'] . ' imagenes.';

            return response()->json([
                'message' => $message
            ], 423);

        } else {
            
            $path = $request->file('file')->store('public/posts');
            $fileName = collect(explode('/', $path))->last();
            
            $image = Image::make(Storage::get($path));

            $image->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('png', 80));

            $meta = new Meta(['meta_key' => 'Image', 'meta_url' => $fileName]);
            $post->posts_meta()->save($meta);

            return response()->json([
                'message' => 'Imagen guardada correctamente.'
            ]);
        }
    }

    public function uploadMovies(Request $request, $id) 
    {
        $configuration = Configuration::where('meta_key', 'Videos')->firstOrFail();
        $post = Post::with('posts_meta')->findOrFail($id);
        $user = $request->user();

        $maxFile = $user['premium'] ? 99 : $configuration['meta_value'];

        $countFile = $post->posts_meta()->where('meta_key', 'Video')->get()->count();

        if ( $countFile >= $maxFile ) {

            $message = 'El límite para usuarios gratuitos es de ' . $configuration['meta_value'] . ' videos.';

            return response()->json([
                'errors' => $message
            ]);

        } else {

            $videoEmbed = $this->converterVideoInEmbed($request['video']);

            $meta = new Meta(['meta_key' => 'Video', 'meta_url' => $videoEmbed]);
            $post->posts_meta()->save($meta);

            return response()->json([
                'success' => 'Video guardado correctamente.'
            ]);
        }
    }

    public function uploadSounds(Request $request, $id) 
    {
        $configuration = Configuration::where('meta_key', 'Audios')->firstOrFail();
        $post = Post::with('posts_meta')->findOrFail($id);
        $user = $request->user();

        $maxFile = $user['premium'] ? 99 : $configuration['meta_value'];

        $countFile = $post->posts_meta()->where('meta_key', 'Audio')->get()->count();

        if ( $countFile >= $maxFile ) {

            $message = 'El límite para usuarios gratuitos es de ' . $configuration['meta_value'] . ' audios.';

            return response()->json([
                'error' => $message
            ]);

        } else {

            $meta = new Meta(['meta_key' => 'Audio', 'meta_url' => $request['audio']]);
            $post->posts_meta()->save($meta);

            return response()->json([
                'success' => 'Audio guardado correctamente.'
            ]);
        }
    }

    public function upload__video($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        return view('frontend.upload.create', [
            'post' => $post
        ]);
    }

    public function upload_youtube(Request $request, $slug)
    {   
        $configuration = Configuration::where('meta_key', 'Videos')->firstOrFail();

        $post = Post::whereSlug($slug)->firstOrFail();
        $user = $request->user();

        $file = $request->file('file');

        $maxFile = $user['premium'] ? 99 : $configuration['meta_value'];

        $countFile = $post->posts_meta()->where('meta_key', 'Video')->get()->count();

        if ( $countFile >= $maxFile ) {

            Session::flash('errors', 'El límite para usuarios gratuitos es de ' . $configuration['meta_value'] . ' videos.');

            return redirect()->back();
        } 

        try {
            
            $video = Youtube::upload($file, [
                'title'       => $request['title'],
                'description' => $request['description'],
                'category_id' => $request['category']
            ]);

            $meta = new Meta(['meta_key' => 'Video', 'meta_url' => "https://www.youtube.com/embed/".$video->getVideoId()]);
            $post->posts_meta()->save($meta);

            Session::flash('success', 'Video cargado, hemos anexado tu video a tu anuncio, tambien puedes visualizarlo mediante tu url "https://www.youtube.com/watch?v=' . $video->getVideoId() . '" gracias por usar Mil Espectáculos');

            return redirect()->back();

        } catch (\Exception $e) {
            
            Session::flash('error', 'Hubo un error al intentar subir tu video, intenta nuevamente.');

            return redirect()->back()->withInput($request->all());
        }
    }

    protected function fileName($name)
    {
        return str_random(20);
    }

    protected function fileVideoName($name)
    {
        return str_random(20);
    }

    protected function converterVideoInEmbed($url)
    {
        // return data
        $finalUrl = '';

        if(strpos($url, 'youtube.com/') !== false) {
            
            //it is Youtube video
            $videoId = explode("v=",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }
        
            $finalUrl.='https://www.youtube.com/embed/'.$videoId;

        }else if(strpos($url, 'youtu.be/') !== false){
            
            //it is Youtube video
            $videoId = explode("youtu.be/",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }

            $finalUrl.='https://www.youtube.com/embed/'.$videoId;
        }

        return $finalUrl;
    }
}
