<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Event as Event;
use App\District as District;
use App\Configuration as Configuration;
use Auth;

class MyEventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $user->load('events');

        $events = $user->events()->orderBy('date', 'ASC')->paginate(10);

        return view('frontend.MyEvents.index', [
            'user' => $user,
            'events' => $events
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $user->load('events');
        $districts = District::orderBy('name', 'ASC')->get();

        $maxEvent = 3;  
        $events = $user['premium'] ? 999 : $maxEvent;

        if ( $user->events->count() >= $events ) {

            return redirect()->back()->with('error', 'Máximo de eventos gratuitos alcanzados.');
            
        } else {

            return view('frontend.MyEvents.create', [
                'districts' => $districts
            ]);

        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|max:75',
            'dayevent' => 'required|date|after:today',
            'address' => 'required',
            'district' => 'required',
            'circuit' => 'required|max:75'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $request->user();
        $user->load('events');
        
        $maxEvent = 3;  
        $events = $user['premium'] ? 999 : $maxEvent;
        
        if ( $user->events->count() >= $events ) {

            return response()->json([
                'limit' => 'El límite para usuarios gratuitos es de 3 eventos'
            ]);

        } else {

            $event = new Event();
            $event->user = Auth::user()->id;
            $event->title = $request['title'];
            $event->slug = $this->replace($request['title']); 
            $event->date = $request['dayevent'];
            $event->district = $request['district'];
            $event->circuit = $request['circuit'];
            $event->address = $request['address'];
            $event->save();

            if ($request->ajax()) {
                return response()->json([
                    'success' => 'Evento guardado correctamente.'
                ]);
            }

            return redirect('/my-events');
        }

        return redirect('/my-events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
