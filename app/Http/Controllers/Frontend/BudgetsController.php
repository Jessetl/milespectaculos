<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Mail\UserConfirmation;
use App\Budget;
use App\Category;
use App\District;
use App\User;
use App\Post;
use App\PostMeta as Meta;

use Carbon\Carbon;
use Mail;
use Image;
use Storage;

use Session;

class BudgetsController extends Controller
{
    public function __construct() 
    {
        if (!\Session::has('files')) \Session::put('files', array());
    }

    protected function strKey() 
    {
        return 'r'.mt_rand(100000000, 999999999);
    }

    public function event(Request $request)
    {
    	return view('frontend.budgets.create-events');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function valid(array $data)
    {
        return Validator::make($data, [
            'country' => 'required',
            'day' => 'required',
            'detail' => 'required',
            'duration' => 'required',
            'hour' => 'required',
            'postal_code' => 'required',
            'type' => 'required'        
        ]);
    }

    public function storeEvent(Request $request)
    {
    	$this->valid($request->all())->validate();

        if ( empty($request->user()) ) {

            Validator::make($request->all(), [
                'name' => 'required|max:45',
                'surname' => 'required|max:45',
                'email' => 'required|string|email|max:45|unique:users',
                'password' => 'required|string|min:4|confirmed',
                'phone' => 'required|numeric|digits_between:5,15',
                'role' => 'required|in:user,artist,company'
            ])->validate();

            $user = new User();
            $user->username = str_random(20);
            $user->name = $request['name'];
            $user->surname = $request['surname'];
            $user->email = $request['email'];
            $user->phone = $request['phone'];
            $user->password = bcrypt($request['password']);
            $user->company = $request['company'];
            $user->confirmed = 0;
            $user->confirmation_code = str_random(25);
            $user->save();

            Mail::to($user->email)->send(new UserConfirmation($user));
        }

        $me = empty($request->user()) ? $user : $request->user();

    	$event = new Budget();
    	$event->user = $me->id;
    	$event->type = $request['type'];
    	$event->country = $request['country'];
    	$event->postal = $request['postal_code'];
    	$event->dayevent = Carbon::parse($request['day'])->format('Y-m-d');
    	$event->hourevent = $request['hour'];
        $event->duration = $request['duration'];
        $event->budget = $request['budget'];
        $event->place = $request['place'];
        $event->detail = $request['detail'];
    	$event->status = 0;
    	$event->save();

    	$request->session()->flash('success', 'Tu evento ha sido enviado.');

    	return redirect()->back();
    }

    public function ad(Request $request)
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        $districts = District::orderBy('name', 'ASC')->get();

        $array = [
            'categories' => $categories,
            'districts' => $districts
        ];

        return view('frontend.budgets.create-posts', $array);
    }

    public function uploadFiles(Request $request)
    {
        $files = \Session::get('files');

        $path = $request->file('file')->store('public/temp');
        $fileName = collect(explode('/', $path))->last();

        $image = Image::make(Storage::get($path));

        $image->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put($path, (string) $image->encode('png', 80));

        $files[] = $fileName;
        \Session::put('files', $files);

        return response()->json([
            'message' => 'Imagen guardada correctamente.'
        ]);
    }

    public function uploadVideos(Request $request)
    {
        if (!\Session::has('videos')) \Session::put('videos', array());

        $videoEmbed = $this->converterVideoInEmbed($request['video']);

        $videos = \Session::get('videos');
        $videos[] = $videoEmbed;
        \Session::put('videos', $videos);

        return response()->json([
            'success' => 'Video guardado correctamente.'
        ]);
    }   

    public function uploadAudios(Request $request)
    {
        if (!\Session::has('audios')) \Session::put('audios', array());

        $audios = \Session::get('audios');
        $audios[] = $request['audio'];

        return response()->json([
            'success' => 'Audio guardado correctamente.'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {    
        return Validator::make($data, [
            'title' => 'required|max:75',
            'content' => 'required|min:50',
            'one' => 'required|digits_between:5,15',
            'two' => 'nullable|digits_between:5,15',
            'email' => 'required|string|email|max:255',
            'address' => 'nullable',
            'amount' => 'nullable'
        ]);
    }

    public function storePost(Request $request)
    {
        ## Validaciones para anuncio de oferta de servicios.
        $this->validator($request->all())->validate();
        
        $str = $this->strKey();
        $me = $request->user();

        $post = new Post();

        if ( !empty($request->file('file')) ) {

            $path = $request->file('file')->store('public/headers');
            $fileName = collect(explode('/', $path))->last();
            
            $image = Image::make(Storage::get($path));

            $image->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('png', 100));

            $post->header = $fileName;
        }

        $lngLt = json_encode(array('lat' => $request['latInput'], 'lng' => $request['lngInput']));

        $post->name = $request['title'];
        $post->category_type = $request['subcategory'];
        $post->user = $me->id;
        $post->code = $str;
        $post->slug = $str;
        $post->type = 1; 
        $post->type_posts = 0;
        $post->content = $request['content'];
        $post->phone1 = $request['one'];
        $post->phone2 = $request['two'];
        $post->email = $request['email'];
        $post->address = $request['directions'];
        $post->directions = $lngLt;
        $post->postal_code = $request['postal_code'];
        $post->url = $request['url'];
        $post->type_amount = $request['type_amount'];
        $post->status = 'draft';
        $post->amount = $request['amount'];
        $post->discount = $request['discount'];
        $post->budget = 1;
        $post->save();

        if ( \Session::has('files') ) {
            foreach (\Session::get('files') as $key => $file) {
                $meta = new Meta(['meta_key' => 'Image', 'meta_url' => $file]);
                $post->posts_meta()->save($meta);

                \Storage::move('public/temp/' . $file, 'public/posts/'. $file);
            }

            $request->session()->forget('files');
        }

        if ( \Session::has('videos') ) {
            foreach (\Session::get('videos') as $key => $video) {
                $meta = new Meta(['meta_key' => 'Video', 'meta_url' => $video]);
                $post->posts_meta()->save($meta);
            }

            $request->session()->forget('videos');
        }

        if ( \Session::has('audios') ) {
            foreach (\Session::get('audios') as $key => $audio) {
                $meta = new Meta(['meta_key' => 'Audio', 'meta_url' => $audio]);
                $post->posts_meta()->save($meta);
            }

            $request->session()->forget('audios');
        }   

        $request->session()->flash('post', $post);

        return redirect()->route('payment.budgetPostPayment');
    }

    public function showPost(Request $request)
    {
        Session::flash('success', 'Tu anuncio sera publicado en las proximas 24hrs');

        return redirect()->route('budgets.ad');
    }

    protected function converterVideoInEmbed($url)
    {
        // return data
        $finalUrl = '';

        if(strpos($url, 'youtube.com/') !== false) {
            
            //it is Youtube video
            $videoId = explode("v=",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }
        
            $finalUrl.='https://www.youtube.com/embed/'.$videoId;

        }else if(strpos($url, 'youtu.be/') !== false){
            
            //it is Youtube video
            $videoId = explode("youtu.be/",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }

            $finalUrl.='https://www.youtube.com/embed/'.$videoId;
        }

        return $finalUrl;
    }
}
