<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Auth;
use Session;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
    	$me = $request->user();
    	$me->load('socialite');

    	$facebook = null;

    	foreach ($me->socialite as $key => $socialite) {
    		
    		if ( $socialite->red == 'Facebook' ) {
    			$facebook = $socialite;
    		}
    	}
  
    	return view('frontend.profile.index', [
    		'me' => $me,
    		'facebook' => $facebook
    	]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $user)
    {
        return Validator::make($data, [
            'username' => 'required|alpha_dash|max:45', Rule::unique('users')->ignore($user->id),
            'name' => 'required|alpha_spaces|max:45',
            'surname' => 'required|alpha_spaces|max:45'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorPassword(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|string|min:4|confirmed'
        ]);
    }

    public function update(Request $request)
    {
        $me = $request->user();

        $this->validator($request->all(), $me)->validate();
        
        $me->username = $request['username'];
        $me->name = $request['name'];
        $me->surname = $request['surname'];

        if ( !is_null($request['password']) AND !is_null($request['password_confirmed']) ) {
  
            $this->validatorPassword($request->all())->validate();

            $me->password = bcrypt($request['password']);
        }

        $me->save();

        Session::flash('success', 'Perfil actualizado');

        return redirect()->back();
    }
}
