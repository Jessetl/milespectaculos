<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User as User;

class FollowersController extends Controller
{
    public function follow($username, Request $request)
    {
    	$user = User::whereUsername($username)->firstOrFail();
    	
    	$me = $request->user();
    	
    	$me->follows()->attach($user);

        return redirect()->back();
    }

    public function unfollow($username, Request $request)
    {
    	$user = User::whereUsername($username)->firstOrFail();

    	$me = $request->user();

    	$me->follows()->detach($user);

        return redirect()->back();
    }

    public function followers($username, Request $request)
    {
        $user = User::whereUsername($username)->firstOrFail();

        $me = $request->user();

        if ( $me->isFollowing($user) ) {

            $me->follows()->detach($user);

            $response = 'detach';

        } else {

            $me->follows()->attach($user);

            $response = 'attach';
        }

        return response()->json($response);
    }
}
