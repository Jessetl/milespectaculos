<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Post as Post;
use Auth;

class FavoritesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $me = $request->user();
        $me->load('favorites');
        
        $myfavorites = $me->favorites;
        $favorites = array();

        foreach ($myfavorites as $key => $favorite) 
        {
            $favorites[] = $favorite;
            $favorite->load('usr', 'posts_meta', 'districts');
        }

        $favorites = $this->paginate($favorites);
        
        return view('frontend.favorites.index', [
            'user' => $me,
            'favorites' => $favorites
        ]);
    }

    public function favorite(Request $request)
    {
        $post = Post::whereSlug($request['slug'])->firstOrFail();
        $me = $request->user();

        if ( $me->isFavorite($post) ) {

            $me->favorites()->detach($post);

            return response()->json([
                'success' => 'Removido de mis favoritos'
            ]);

        } else {
            
            $me->favorites()->attach($post);

            return response()->json([
                'success' => 'Agregado a mis favoritos'
            ]);
        }
    }

    public function unfavorite($slug, Request $request)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        $me = $request->user();

        $me->favorites()->detach($post);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->load('posts_meta');

        $images = array();
        $sounds = array();  
        $movies = array();
        
        foreach ($post->posts_meta as $key => $meta) 
        {
            if ( $meta->meta_key == 'Image' ) 
            {
                if ( !empty ( $meta->meta_url ) ) {
                    $images[] = $meta->meta_url;
                }

            } elseif ( $meta->meta_key == 'Video' ) {

                if ( !empty ( $meta->meta_url ) ) {
                    $movies[] = $meta->meta_url;
                }
            } elseif ( $meta->meta_key == 'Audio' ) {

                if ( !empty ( $meta->meta_url ) ) {
                    $sounds[] = $meta->meta_url;
                }
            }
        }

        return view('frontend.MyPosts.view', [
            'post' => $post,
            'images' => $images,
            'sounds' => $sounds,
            'movies' => $movies
        ]);
    }

    public function paginate($posts, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $posts = $posts instanceof Collection ? $posts : Collection::make($posts);

        return new LengthAwarePaginator($posts->forPage($page, $perPage), $posts->count(), $perPage, $page, ['path' => Paginator::resolveCurrentPath()]);
    }
}
