<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Post as Post;
use App\Reported as Reported;
use Session;

class ReportsController extends Controller
{
    public function create($slug)
    {
    	$post = $this->findBySlug($slug);

    	return view('frontend.reports.create', [
    		'post' => $post
    	]);
    }

    public function store($slug, Request $request)
    {
    	$post = $this->findBySlug($slug);

    	$reported = new Reported();
    	$reported->post = $post['id'];
    	$reported->reason = $request['reason'];
    	$reported->content = $request['content'];
    	$reported->save();

        Session::flash('success', 'Hemos recibido tu solicitud de reporte, tomaremos las medidas necesarias.');

    	return redirect()->back();
    }

    protected function findBySlug($slug) 
    {
    	return Post::whereSlug($slug)->firstOrFail();
    }
}
