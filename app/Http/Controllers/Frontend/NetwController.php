<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\District as District;
use App\Category as Category;
use App\Network;
use App\Like;
use Session;

class NetwController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'premium']);
    }

    public function index()
    {
        $user = auth()->user();
    	$districts = $this->getByDistrict();
    	$categories = $this->getByCategory();

        $networks = Network::with('messages.usr', 'usr', 'likes')->orderBy('id', 'DESC')->get();

        $networks->each(function($network) use ($user){
            $network->liked = $network->usersLikes->contains($user);
        });

    	return view('frontend.network.index', [
    		'districts' => $districts,
    		'categories' => $categories,
            'networks' => $networks
    	]);
    }

    public function my_network(Request $request)
    {
        $me = $request->user();
        $me->load('network');

        $districts = $this->getByDistrict();
        $categories = $this->getByCategory();

        return view('frontend.MyNetwork.index', [
            'me' => $me,
            'districts' => $districts,
            'categories' => $categories
        ]);
    }

    public function mynet_store(Request $request)
    {
        $me = $request->user();

        $network = new Network();
        $network->user = $me->id;
        $network->category_type = $request['subcategory'];
        $network->district = $request['district'];
        $network->type_posts = $request['type'];
        $network->type_amount = $request['type_amount'];
        $network->amount = $request['amount'];
        $network->message = $request['message'];
        $network->save();

        Session::flash('success', 'Haz publicado un nuevo contenido.');

        return redirect()->back();
    }

    protected function getByCategory() 
    {
        return Category::with('category_type')->orderBy('name', 'ASC')->get();
    }

    protected function getByDistrict() 
    {
        return District::orderBy('name', 'ASC')->get();
    }

    protected function comment(Request $request)
    {
        if(!$request->message) {
            return response()->json([
                'err' => 'Debe escribir su comentario'
            ], 422);
        }

        $network = Network::find($request->network);

        $message = $network->messages()->create([
            'user' => auth()->user()->id,
            'message' => $request->message
        ]);

        $message->load('usr');

        return $message;
    }

    public function like(Request $request)
    {
        $user = auth()->user();
        
        if($request->liked) {
            $like = Like::where('network', $request->network)
                        ->where('user', $user->id)
                        ->first();
            $like->delete();
        }else {
           $network = Network::find($request->network);
           $network->likes()->create([
              'user' => $user->id
           ]);
        }

        return $user;
    }
}
