<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post as Post;
use Auth;

class DemandsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'premium']);
    }

    public function index()
    {
    	$posts = Post::with('usr')->where([['type_posts', 1], ['status', 'published']])->orderBy('updated_at', 'DESC')->paginate(10);

    	return view('frontend.MyPage.index', [
    		'posts' => $posts
    	]);
    }
}
