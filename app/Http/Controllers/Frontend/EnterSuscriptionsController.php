<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Encrypted as Encrypted;
use Session;
use Carbon\Carbon;

class EnterSuscriptionsController extends Controller
{
    public function save(Request $request)
    {
    	$encrypted = Encrypted::where([ [ 'code', $request['code'] ], [ 'enabled', 1] ])->first();
    	
    	if (empty($encrypted)) {

    		Session::flash('error', 'Código inválido');

    		return redirect()->back();
    	}

    	$me = $request->user();
    	$me->premium = 1;
    	$me->trial_ends_at = Carbon::now()->addYear();
    	$me->save();	

    	$encrypted->enabled = 0;
    	$encrypted->save();

    	Session::flash('success', 'Suscripción activada');

    	return redirect()->back();
    }
}
