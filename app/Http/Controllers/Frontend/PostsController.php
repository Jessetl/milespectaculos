<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Category as Category;
use App\CategoryType as Type;
use App\District as District;
use App\Configuration as Configuration;
use App\Post as Post;
use App\PostMeta as Meta;
use App\Paypal as Paypal;
use Auth;
use Session;
use Image;
use Storage;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function strKey() 
    {
        return 'r'.mt_rand(100000000, 999999999);
    }

    public function new(Request $request)
    {
        $categories = Category::orderBy('id', 'ASC')->get();
        
        if ($request->session()->has('post')) {
            
            $post = $request->session()->get('post');
            $post->delete();

            $request->session()->forget('post');
        }

        return view('frontend.posts.create', [
            'categories' => $categories
        ]);
    }

    public function new__ptc($category)
    {
        $category = Type::whereSlug($category)->firstOrFail();
        $districts = District::orderBy('name', 'ASC')->get();

        return view('frontend.posts.create-type', [
            'category' => $category,
            'districts' => $districts
        ]);
    }

    public function new__pts(Request $request)
    {
        if ( $request['type_posts'] == 0 ) {
            if ( $this->validateMaxPublications(Auth::user()) == 'denied') {
                return redirect()->back()->with('limit', 'Alcanzó el límite máximo de publicaciones gratuitas.');
            }
        }

        $month = $this->lengthRelease($request->all());

        ## Definimos el tiempo que tendra el anuncio para ser borrado, los anuncios de usuarios VIP tienen una duración de un año.
        $today = Carbon::now();
        $today->addMonth($month);
        $convertToString = $today->toDateString();

        ## Buscamos la categoría y verificamos.
        $category = Type::findOrFail($request['category']);
        $str = $this->strKey();

        ## Guardamos los datos primordials del post.
        $post = new Post();
        $post->category_type = $category['id'];
        $post->code = $str;
        $post->slug = $str;
        $post->user = Auth::user()->id;
        $post->type_posts = $request['type_posts'];
        $post->length = $convertToString;
        $post->save();
        
        ## Incluimos el post dentro de una variable de sessión para su posterioridad.
        $request->session()->put('post', $post);

        ## Si el usuario tiene una cuenta paga guardo las poblaciones y provincias.
        if ( $request->user()->premium ) {

            empty($request->circuits) ? $request['circuits'] = array() : '';
    
            Validator::make($request->all(), [
                'circuits' => 'array|min:1'
            ])->validate();

            foreach ($request['circuits'] as $key => $circuits) {

                $dt_find = $this->getOnlyDistrict($key);

                foreach ($circuits as $circuit) {
                    $post->districts()->attach($key, ['circuit' => $circuit]);
                }
            }
        ## Si no, guardo solamente una provincia y población.
        } else {

            $dt_find = $this->getOnlyDistrict($request['district']);
            $post->districts()->attach($dt_find->id, ['circuit' => $request['circuit']]);
        }

        ## Redireccionamos el posts para continuar el proceso de creación de anuncio.
        return redirect()->route('posts.keep');
    }

    public function pt__hedge(Request $request)
    {
        $post = $request->session()->get('post');
        $post->load('districts');

        $districts = District::orderBy('name', 'ASC')->get();

        return view('frontend.posts.payment', [
            'post' => $post,
            'districts' => $districts
        ]);  
    }

    public function pt__keep(Request $request)
    {
        ## Tomamos el anuncio enviado por la sesión.
        $post = $request->session()->get('post');
        $districts = District::orderBy('name', 'ASC')->get();

        ## Definimos la vista del anuncio para rellenar el formulario.
        if ( $post->type_posts ) {

            return view('frontend.posts.category.create-demands', [
                'post' => $post
            ]);

        } else {
            
            return view('frontend.posts.category.create-offerts', [
                'post' => $post,
                'districts' => $districts
            ]);
        }
    }

    public function create(Request $request)
    {
        ## Seleccionamos el usuario en sesión y cargamos los anuncios.
        $user = $request->user();
        $user->load('posts');

        ## Verificamos el último anuncio publicado para saber, si esta como borrador.
        $last_post = $user->posts()->get()->last();

        ## Comprobamos si tiene último anuncio y si esta como borrador, es decir diferente de publicado.
        if ( $last_post !== NULL AND $last_post->status !== 'published' ) {
            
            ## Agregamos a la sesión el último anuncio creado y de estatus borrador.
            $request->session()->put('post', $last_post);

            ## Redireccionamos para ver si el usuario, desea continuar con su post o hacer uno nuevo.
            return redirect()->route('posts.draft');
        }

        $categories = Category::orderBy('id', 'ASC')->get();

        return view('frontend.posts.create', [
            'categories' => $categories
        ]);
    }

    public function draft(Request $request)
    {
        ## Verificamos que la variable del anuncio este en la sesión. 
        if ($request->session()->has('post')) {
            ## Guardamos la variable de sesión y cargamos la categoría.
            $post = $request->session()->get('post');
            $post->load('category');

            return view('frontend.posts.draft', [
                'post' => $post
            ]);
        ## Si no hay variable en sesión redireccionamos a crear el anuncio.
        } else {

            return redirect()->route('posts.new');
        }
    }

    public function return(Request $request)
    {
        ## Seleccionamos la variable en sesión y procedemos a continuar con el formulario para el anuncio.
        $post = $request->session()->get('post');
        $districts = District::orderBy('name', 'ASC')->get();

        if ( $post->type_posts ) {

            return view('frontend.posts.category.create-demands', [
                'post' => $post
            ]);

        } else {
            
            return view('frontend.posts.category.create-offerts', [
                'post' => $post,
                'districts' => $districts
            ]);
        }
    }

    public function store(Request $request)
    {   
        $type = $post->type_posts ? 'demand' : 'offert';

        session()->put('post', $post);

        return redirect()->route('posts.create.type', ['slug' => $post->category, 'type' => $type]);
    }

    public function getAdvice()
    {
        return view('frontend.eight');
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateTypeFalse(array $data)
    {
        return Validator::make($data, [
            'file' => 'nullable|mimes:jpeg,png,jpg',
            'title' => 'required|max:75',
            'content' => 'required|min:10',
            'one' => 'required|numeric|digits_between:5,15',
            'two' => 'nullable|digits_between:5,15',
            'email' => 'required|string|email|max:255',
            'postal_code' => 'nullable|numeric|min:4',
            'address' => 'nullable',
            'amount' => 'nullable'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateTypeTrue(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|max:75',
            'content' => 'required|min:50',
            'phone' => 'required|numeric|digits_between:5,15',
            'phone_two' => 'numeric|nullable|digits_between:5,15',
            'email' => 'required|string|email|max:255',
        ]);
    }

    public function update(Request $request, $id)
    {
        ## Buscamos el id del anuncio enviado por el metodo post.
        $post = Post::findOrFail($id);

        if ( $post->type_posts ) {
            ## Validaciones para anuncio de demanda de servicios.
            $this->validateTypeTrue($request->all())->validate();
            
            if ( $post->posts_meta()->where('meta_key', 'Image')->get()->count() === 0 ) {

                Session::flash('error', 'El anuncio debe contener al menos 1 imagen');

                return redirect()->back()->withInput($request->all());
            } 

            $post->name = $request['title'];
            $post->type = 0;
            $post->content = $request['content'];
            $post->phone1 = $request['phone'];
            $post->phone2 = $request['phone_two'];
            $post->email = $request['email'];
            $post->status = 'published';
            $post->save();

            Session::flash('success', 'Tu anuncio ha sido publicado');
            ## Eliminamos la variable en sesión ya que no la utilizaremos más.
            $request->session()->forget('post');
            
            if ( $request->user()->premium ) {
                return redirect('my-posts');
            }

            return redirect()->route('posts.preview', $post);

        } else {
            ## Validaciones para anuncio de oferta de servicios.
            $this->validateTypeFalse($request->all())->validate();

            if ( $post->posts_meta()->where('meta_key', 'Image')->get()->count() === 0 ) {

                Session::flash('error', 'El anuncio debe contener al menos 1 imagen');

                return redirect()->back()->withInput($request->all());
            } 
            
            $lngLt = json_encode(array('lat' => $request['latInput'], 'lng' => $request['lngInput']));
        
            $fileName = NULL;

            if ( !empty($request->file('file')) ) {

                $path = $request->file('file')->store('public/headers');
                $fileName = collect(explode('/', $path))->last();
                
                $image = Image::make(Storage::get($path));

                $image->resize(1280, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                Storage::put($path, (string) $image->encode('jpg', 100));

                $post->header = $fileName;
            }

            $post->name = $request['title'];
            $post->type = 1; 
            $post->content = $request['content'];
            $post->phone1 = $request['one'];
            $post->phone2 = $request['two'];
            $post->email = $request['email'];
            $post->address = $request['directions'];
            $post->directions = $lngLt;
            $post->postal_code = $request['postal_code'];
            $post->url = $request['url'];
            $post->type_amount = $request['type_amount'];
            $post->status = 'published';
            $post->amount = $request['amount'];
            $post->discount = $request['discount'];
            $post->save();

            Session::flash('success', 'Tu anuncio ha sido publicado');
            ## Eliminamos la variable en sesión ya que no la utilizaremos más.
            $request->session()->forget('post');
            
            ## Si el usuario es premium redireccionamos a ver su perfil directamente.
            if ( $request->user()->premium ) {
                return redirect()->route('view.post', $post); 
            }

            ## Hacemos un preview si el usuario es gratuito, para que vea su anuncio y se motive a publicitarlo en mas de una provincia.
            return redirect()->route('posts.preview', $post);
        }
    }

    public function pt__preview($slug)
    {
        ## Previsualización del anuncio, para usuarios gratuitos.
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->load('posts_meta', 'usr', 'category', 'messages');

        $header = $post->header;

        ## Variables a utilizar
        $images = array();
        $sounds = array();  
        $movies = array();

        $user = $post->usr; 

        ## Definimos la vista para la categoría del anuncio
        if ( $post->type_posts ) 
        {
            ## Dividimos imagenes.
            foreach ($post->posts_meta as $key => $meta) 
            {
                if ( $meta->meta_key == 'Image' ) 
                {
                    if ( !empty ( $meta->meta_url ) ) {
                        $images[] = $meta->meta_url;
                    }
                }
            }

            return view('frontend.posts.previewd', [
                'user' => $user,
                'post' => $post,
                'images' => $images,
            ]);
            
        } else {
            
            $json = json_decode($post->directions);

            if ( $json !== null )
            {
                $post->lat = $json->lat;
                $post->lng = $json->lng;
            }

            ## Dividimos imagenes, sonidos y videos para mostrarlo en la vista.
            foreach ($post->posts_meta as $key => $meta) 
            {
                if ( $meta->meta_key == 'Image' ) 
                {
                    if ( !empty ( $meta->meta_url ) ) {
                        $images[] = $meta->meta_url;
                    }

                } elseif ( $meta->meta_key == 'Video' ) {

                    if ( !empty ( $meta->meta_url ) ) {
                        $movies[] = $meta->meta_url;
                    }
                } elseif ( $meta->meta_key == 'Audio' ) {

                    if ( !empty ( $meta->meta_url ) ) {
                        $sounds[] = $meta->meta_url;
                    }
                }
            }

            return view('frontend.posts.preview', [
                'user' => $user,
                'post' => $post,
                'header' => $header,
                'images' => $images,
                'sounds' => $sounds,
                'movies' => $movies
            ]);
        }
    }

    public function shopping__cart($slug)
    {
        ## Vista para publicitar el anuncio creado.
        $post = Post::whereSlug($slug)->firstOrFail();
        $districts = District::orderBy('name', 'ASC')->get();

        return view('frontend.posts.shopping_cart', [
            'post' => $post,
            'districts' => $districts
        ]);
    }

    public function drop($id)
    {
        ## Eliminamos imagenes del anuncio. 
        $meta = Meta::findOrFail($id);
        $meta->delete();
        
        ## Eliminamos las imagenes de la carpeta storage.
        $uri = $this->uri();
        $delete = \File::Delete($uri.$meta->meta_url);

        return redirect()->back();
    }

    protected function fileName($name)
    {
        return str_random(20);
    }

    protected function uri()
    {
        ## Url de imagenes de los anuncios.
        return public_path().'/storage/posts/';
    }

    protected function validateMaxPublications ($user) 
    {
        $number_posts = $user->posts()->where('type_posts', 0)->count();

        if ( $user->premium == 0 AND $number_posts >= 1) {
            return 'denied';
        }
    }

    protected function getOnlyDistrict($id)
    {
        ## Busqueda de provincias
        return District::findOrFail($id);
    }

    protected function lengthRelease($request) 
    {
        ## Verificando la duración del anuncio por categoría de usuario.
        if ( Auth::user()->premium ) 
        {
            $month = $request['type_posts'] == 0 ? 12 : $request['length'];

        } else {
            
            $month = $request['length'];
        }

        return $month;
    }
}
