<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Configuration as Configuration;
use Auth;

class ConfigurationsController extends Controller
{

	public function user()
	{
		$user = Auth::user();
		$configuration = Configuration::where('meta_key', 'Fotos')->first();


		return response()->json([
			'user' => $user,
			'configuration' => $configuration
		]);
	}
}
