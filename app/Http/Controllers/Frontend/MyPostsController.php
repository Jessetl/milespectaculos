<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Post as Post;
use App\PostMeta as Meta;
use App\Configuration as Configuration;
use App\District as District;
use Carbon\Carbon;
use Session;
use Auth;

use Image;
use Storage;

class MyPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {    
        $me = $request->user();

        $posts = $me->posts()->with('districts', 'posts_meta')->where('status', 'published')->orderBy('updated_at', 'DESC')->paginate(10);

        return view('frontend.MyPosts.index', [
            'user' => $me,
            'posts' => $posts
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        ## Variable para deshabilitar logo
        $logo = null;

        ## Previsualización del anuncio, para usuarios gratuitos.
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->load('posts_meta', 'usr', 'category', 'messages');

        $header = $post->header;

        ## Variables a utilizar
        $images = array();
        $sounds = array();  
        $movies = array();

        $user = $post->usr; 

        ## Definimos la vista para la categoría del anuncio
        if ( $post->type_posts ) 
        {
            ## Dividimos imagenes.
            foreach ($post->posts_meta as $key => $meta) 
            {
                if ( $meta->meta_key == 'Image' ) 
                {
                    if ( !empty ( $meta->meta_url ) ) {
                        $images[] = $meta->meta_url;
                    }
                }
            }

            return view('frontend.MyPosts.viewd', [
                'user' => $user,
                'post' => $post,
                'images' => $images,
            ]);
            
        } else {
            
            $json = json_decode($post->directions);

            if ( $json !== null )
            {
                $post->lat = $json->lat;
                $post->lng = $json->lng;
            }

            ## Dividimos imagenes, sonidos y videos para mostrarlo en la vista.
            foreach ($post->posts_meta as $key => $meta) 
            {
                if ( $meta->meta_key == 'Image' ) 
                {
                    if ( !empty ( $meta->meta_url ) ) {
                        $images[] = $meta->meta_url;
                    }

                } elseif ( $meta->meta_key == 'Video' ) {

                    if ( !empty ( $meta->meta_url ) ) {
                        $movies[] = $meta->meta_url;
                    }
                } elseif ( $meta->meta_key == 'Audio' ) {

                    if ( !empty ( $meta->meta_url ) ) {
                        $sounds[] = $meta->meta_url;
                    }
                }
            }

            return view('frontend.MyPosts.view', [
                'user' => $user,
                'post' => $post,
                'logo' => $logo,
                'images' => $images,
                'header' => $header,
                'sounds' => $sounds,
                'movies' => $movies
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {   
        $post = Post::with('posts_meta')->whereSlug($slug)->firstOrFail();
            
        $header = $post->header;

        $post->videos = $post->posts_meta()->where('meta_key', 'Video')->get();
        $post->audios = $post->posts_meta()->where('meta_key', 'Audio')->get();

        $sounds = array();  
        $movies = array();

        foreach ($post->posts_meta as $key => $meta) 
        {
            if ( $meta->meta_key == 'Video' ) {

                if ( !empty ( $meta->meta_url ) ) {
                    $movies[$meta->id] = $meta->meta_url;
                }
            } elseif ( $meta->meta_key == 'Audio' ) {

                if ( !empty ( $meta->meta_url ) ) {
                    $sounds[$meta->id] = $meta->meta_url;
                }
            }
        }

        if ( $post->type_posts ) {

            return view('frontend.MyPosts.category.edit-demands', [
                'post' => $post,
            ]);

        } else {

            $json = json_decode($post->directions);

            $post->lat = $json->lat;
            $post->lng = $json->lng;

            return view('frontend.MyPosts.category.edit-offerts', [
                'post' => $post,
                'header' => $header,
                'sounds' => $sounds,
                'movies' => $movies
            ]);
        }   
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateTypeTrue(array $data)
    {
        return Validator::make($data, [
            'content' => 'required|min:50',
            'phone1' => 'required|numeric',
            'phone2' => 'nullable|numeric',
            'email' => 'required|string|email|max:255',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateTypeFalse(array $data)
    {    
        return Validator::make($data, [
            'file' => 'nullable|mimes:jpg,png,jpeg',
            'title' => 'required|max:75',
            'content' => 'required|min:50',
            'one' => 'required|digits_between:5,15',
            'two' => 'nullable|digits_between:5,15',
            'email' => 'required|string|email|max:255',
            'address' => 'nullable',
            'amount' => 'nullable'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        if ( $post->type_posts ) {
            
            $this->validateTypeTrue($request->all())->validate();

            $post->type = 1;
            $post->content = $request['content'];
            $post->phone1 = $request['phone1'];
            $post->phone2 = $request['phone2'];
            $post->email = $request['email'];
            $post->save();

        } else {

            $this->validateTypeFalse($request->all(), $post)->validate();

            if ( !empty($request['latInput']) AND !empty($request['lngInput']) ) {
                $lngLt = json_encode(array('lat' => $request['latInput'], 'lng' => $request['lngInput']));
                $post->directions = $lngLt;
            }

            $fileName = NULL;

            if ( !empty($request->file('file')) ) {

                ## Eliminamos las imagenes de la carpeta storage.
                $uri = $this->uri();
                \File::Delete($uri.$post->header);

                $path = $request->file('file')->store('public/headers');
                $fileName = collect(explode('/', $path))->last();
                
                $image = Image::make(Storage::get($path));

                $image->resize(1280, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                Storage::put($path, (string) $image->encode('jpg', 100));

                $post->header = $fileName;
            }

            $post->name = $request['title'];
            $post->content = $request['content'];
            $post->phone1 = $request['one'];
            $post->phone2 = $request['two'];
            $post->email = $request['email'];
            $post->address = $request['directions'];
            $post->url = $request['url'];
            $post->type_amount = $request['type_amount'];
            $post->amount = $request['amount'];
            $post->discount = $request['discount'];
            $post->save();

            if ( isset($request['video']) ) {
                
                foreach ($request['video'] as $key => $video) {

                    $videoEmbed = $this->converterVideoInEmbed($video[0]);

                    $meta = Meta::findOrFail($key);
                    $meta->meta_url = $video[0];
                    $meta->save();
                }
            }

            if ( isset($request['audio']) ) {
                
                foreach ($request['audio'] as $key => $audio) {
                        
                    $meta = Meta::findOrFail($key);
                    $meta->meta_url = $audio[0];
                    $meta->save();
                }
            }
        }

        Session::flash('success', 'Anuncio modificado');

        return redirect()->route('my-posts.show', $post);
    }

    public function gallery($slug) 
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->load('posts_meta');

        $post->images = $post->posts_meta()->where('meta_key', 'Image')->get();

        return view('frontend.MyPosts.edit-file', [
            'post' => $post
        ]);
    }

    public function renew(Request $request, $slug)
    {
        if ($request->ajax()) 
        {
            $post = Post::whereSlug($slug)->firstOrFail();
            $today = Carbon::now()->toDateString();
            $postRenew = Carbon::parse($post->updated_at)->toDateString();

            if ( $today == $postRenew ) {

                return response()->json([
                    'error' => 'Renovación gratuita agotada.'
                ], 422);
            }

            $post->updated_at = Carbon::now();
            $post->save();

            Session::flash('success', 'Anuncio renovado');

            return redirect()->back();
        } 

        Session::flash('error', 'Error en llamar el metodo, intente de otra forma.');

        return redirect()->back();

    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::with('posts_meta')->findOrFail($id);

        foreach ($post->posts_meta as $key => $meta) {
            if ( $meta->meta_key == 'Image') {
                ## Eliminamos las imagenes de la carpeta storage.
                $uri = $this->urimeta();
                \File::Delete($uri.$meta->meta_url);
            }   
        }

        $post->delete();

        Session::flash('success', 'Anuncio ' . $post->code . ' eliminado permanentemente');
        
        return redirect()->back();
    }

    protected function fileName($name)
    {
        return str_random(20);
    }

    protected function uri()
    {
        ## Url de imagenes de los anuncios.
        return public_path().'/storage/headers/';
    }

    protected function urimeta()
    {
        ## Url de imagenes de los anuncios.
        return public_path().'/storage/posts/';
    }

    protected function converterVideoInEmbed($url)
    {
        // return data
        $finalUrl = '';
    
        if (strpos($url, 'https://www.youtube.com/embed/') !== false){

            return $url;

        } elseif (strpos($url, 'youtube.com/') !== false) {

            //it is Youtube video
            $videoId = explode("v=",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }
        
            $finalUrl.='https://www.youtube.com/embed/'.$videoId;

        } elseif (strpos($url, 'youtu.be/') !== false){
            
            //it is Youtube video
            $videoId = explode("youtu.be/",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }

            $finalUrl.='https://www.youtube.com/embed/'.$videoId;

        }

        return $finalUrl;
    }
}
