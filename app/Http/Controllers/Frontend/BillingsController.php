<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// use to process billing agreements
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Agreement;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

use App\Notify;
use App\Payment as Payout;
use Session;

class BillingsController extends Controller
{
    // Create a new instance with our paypal credentials
    public function __construct()
    {
        // Detect if we are running in live mode or sandbox
        if (config('paypal.settings.mode') == 'live') {

            $this->client_id = config('paypal.live_client_id');
            $this->secret = config('paypal.live_secret');

        } else {

            $this->client_id = config('paypal.sandbox_client_id');
            $this->secret = config('paypal.sandbox_secret');

        }
        
        // Set the Paypal API Context/Credentials
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $this->apiContext->setConfig(config('paypal.settings'));
    }

    public function billing(Request $request, Notify $notify)
    {
    	## Creamos el nuevo metodo de pago.
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        ## Creamos las variables a usar para paypal.
        $payname = 'Autorización a acceso de datos';
        $payprice = 3;

        $tax = $payprice * 0.21;
        
        $item = new Item();
        $item->setName($payname)
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice($payprice);

        ## Creamos una lista de los datos a pagar.
        $item_list = new ItemList();
        $item_list->setItems(array($item));

        ## Detalles de la compra, iva y precio.
        $details = new Details();
        $details->setSubtotal($payprice)
            ->setTax($tax);

        $total = $payprice + $tax;

         ## Agregamos los detalles de la compra.
        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($total)
            ->setDetails($details);

        ## Descripción y lista de items a agregar al anuncio.
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Autorización a acceso de datos');

        ## Redirecciones para aprobar o cancelar el metodo de pago.
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::route('billings.approved', $notify->id))
            ->setCancelUrl(\URL::route('notify.show', $notify->id));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {

        	$payment->create($this->apiContext);
        	
        	$payout = new Payout();
            $payout->user = $request->user()->id;
        	$payout->payment = $payment->getId();
            $payout->name = $payname;
        	$payout->price = $payprice;
        	$payout->tax = $tax;
        	$payout->total = $total;
        	$payout->save();

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

        	Session::flash('error', 'Hubo un error al procesar el pago, intente nuevamente o contacte con el soporte técnico.');

            return redirect()->back();
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        ## add payment ID to session
        $request->session()->put('paypal_payment_id', $payment->getId());
        $request->session()->put('payment', $payout);

        if(isset($redirect_url)) {
            ## redirect to paypal
            return \Redirect::away($redirect_url);
        }

        return redirect()->back()->with('error', 'Error desconocido, intentelo mas tarde.');
    }

	public function approved(Request $request, Notify $notify)
	{
		// Get the payment ID before session clear
        $payment_id = $request->session()->get('paypal_payment_id');
        $payout = $request->session()->get('payment');

        $payerId = $request['PayerID'];
        $token = $request['token'];

        if (empty($payerId) OR empty($token) OR empty($payment_id)) {

            return redirect()->route('subscription')
                ->with('message', 'Hubo un problema al intentar pagar con Paypal');
        }

        try {

            $payment = Payment::get($payment_id, $this->apiContext);

            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            $result = $payment->execute($execution, $this->apiContext);

            if ($result->getState() == 'approved') {

                ## Modificamos el paypal_id como código unico de transacción.
                $payout->status = 1;
                $payout->save();

                $notify->authOdata = 1;
                $notify->save();

                // clear the session payment ID
                $request->session()->forget('paypal_payment_id');
                $request->session()->forget('payment');
                
                Session::flash('success', 'Ya puedes acceder a los datos del usuario.');

                return redirect()->route('notify.show', $notify); 
            }

        } catch (\Exception $e) {
            return redirect('notify.show')->with('error', 'Error al procesar solicitud, intentelo mas tarde');
        }
	}
}
