<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paypal as Paypal;
use App\District as District;
use App\Post as Post;
use App\Shopping as Shopping;
use App\ShoppingCart as Cart;
use Carbon\Carbon;
use Auth;
use Session;

// use to process billing agreements
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Agreement;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

class ShoppingCartController extends Controller
{
	private $apiContext;
    private $mode;
    private $client_id;
    private $secret;

    // Create a new instance with our paypal credentials
    public function __construct()
    {
        // Detect if we are running in live mode or sandbox
        if (config('paypal.settings.mode') == 'live') {

            $this->client_id = config('paypal.live_client_id');
            $this->secret = config('paypal.live_secret');

        } else {

            $this->client_id = config('paypal.sandbox_client_id');
            $this->secret = config('paypal.sandbox_secret');

        }
        
        // Set the Paypal API Context/Credentials
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $this->apiContext->setConfig(config('paypal.settings'));
    }

    public function ShoppingItems(Request $request)
    {
        ## Buscamos el id del registro de paypal para verificar la cantidad a cobrar.
        $paypal = Paypal::findOrFail(3);

        ## Creamos el nuevo metodo de pago.
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        ## Creamos las variables a usar para paypal.
        $items = array();
        $subtotal = 0;
        $iva = 0;
        $iva_stack = 0.21;

        # Creamos el registro de las provincias y publicaciones.
        $shopping = $this->addshopping($request->all());

        ## Buscamos el anuncio enviado del metodo post.
        $post = $this->getOnlyPost($request['post']);

        foreach ($request['circuits'] as $key => $circuit) 
        {
            ## Buscamos la provincia en la base de datos.
            $district = $this->getOnlyDist($key);

            ## Añadimos por cada ciclo el la provincia a carrito de compras.
            $this->addshoppingcart($shopping, $district, $paypal->amount);

            // Agregamos cada uno de los items al carrito.
            $item = new Item();
            $item->setName($district->name)
                ->setCurrency('EUR')
                ->setDescription($paypal->description)
                ->setQuantity(1)
                ->setPrice($paypal->amount);

            $items[] = $item;
            $subtotal += $paypal->amount * 1;
            $iva += 0.21;
        }

        ## Creamos una lista de todas las provincias a pagar.
        $item_list = new ItemList();
        $item_list->setItems($items);

        ## Detalles de la compra, iva y subtotal.
        $details = new Details();
        $details->setSubtotal($subtotal)
            ->setTax($iva);

        ## Guardamos la suma total de toda la compra en una variable.
        $total = $subtotal + $iva;

        ## Guardamos el subtotal de todal la compra
        $shopping->amount_total = $total;
        $shopping->iva = $iva;
        $shopping->save();

        ## Agregamos los detalles de la compra.
        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($total)
            ->setDetails($details);

        ## Descripción y lista de items a agregar al anuncio.
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Publicidad de anuncio en diferentes provincias.');

        ## Redirecciones para aprovar o cancelar el metodo de pago.
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::route('shopping.return'))
            ->setCancelUrl(\URL::route('view.post', $post->slug));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        
        try {

            $payment->create($this->apiContext);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            
            Session::flash('error', 'Hubo un error al procesar el pago, intente nuevamente o contacte con el soporte técnico.');

            return redirect()->back();
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        ## add payment ID to session
        $request->session()->put('paypal_payment_id', $payment->getId());
        $request->session()->put('districts', $request['circuits']);
        $request->session()->put('shopping', $shopping);
        $request->session()->put('post', $post);
        
        if(isset($redirect_url)) {
            ## redirect to paypal
            return \Redirect::away($redirect_url);
        }
    
        return redirect()->back()->with('error', 'Error desconocido, intentelo mas tarde.');
    }

    public function shoppingReturn(Request $request)
    {
        // Get the payment ID before session clear
        $payment_id = $request->session()->get('paypal_payment_id');
        $shopping = $request->session()->get('shopping');
        $post = $request->session()->get('post');
        $districts = $request->session()->get('districts');

        // clear the session payment ID
        $request->session()->forget('paypal_payment_id');
        $request->session()->forget('shopping');
        $request->session()->forget('post');
        $request->session()->forget('districts');

        $payerId = $request['PayerID'];
        $token = $request['token'];

        if (empty($payerId) OR empty($token) OR empty($payment_id)) {

            return redirect()->route('my-posts.publish', ['slug', $post])
                ->with('message', 'Hubo un problema al intentar pagar con Paypal');
        }

        try {
            
            $payment = Payment::get($payment_id, $this->apiContext);
            
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);
     
            $result = $payment->execute($execution, $this->apiContext);
     
            if ($result->getState() == 'approved') {

                ## Modificamos el paypal_id como código unico de transacción.
                $shopping->paypal_payment_id = $payment_id;
                $shopping->status = 'approved';
                $shopping->ends_at = $this->convertToString();
                $shopping->save();

                ## Guardamos las provincias y poblaciones que fueron pagadas.
                $this->savePayments($post, $shopping, $districts);
                
                Session::flash('success', 'Se han añadido tus provincias, disfruta de tu servicio.');
                
                return redirect('my-posts');
            }

            return redirect('/')->with('error', 'Paypal ha cancelado esta compra, intentelo mas tarde');

        } catch (\Exception $e) {
            
            return redirect('my-posts')->with('error', 'Error al procesar solicitud, intentelo mas tarde');
        }
    }

    protected function addshopping($request)
    {
        $post = $this->getOnlyPost($request['post']);

        $shopping = new Shopping();
        $shopping->post = $post['id'];
        $shopping->type_payment = 'paypal';
        $shopping->type = 1;
        $shopping->description = 'Publicitar anuncio en diferentes provincias.';
        $shopping->paypal_payment_id = null;
        $shopping->amount_total = null;
        $shopping->iva = null;
        $shopping->status = 'rejected';
        $shopping->ends_at = null;
        $shopping->save();

        return $shopping;
    }

    protected function addshoppingcart($shopping, $district, $amount)
    {
        $cart = new Cart();
        $cart->shopping = $shopping['id'];
        $cart->item = $district->name;
        $cart->amount = $amount;
        $cart->iva = 0.21;
        $cart->save();
    }

    protected function getOnlyPost($slug)
    {
        return Post::whereSlug($slug)->firstOrFail();
    }

    protected function getOnlyDist($id)
    {
        return District::findOrFail($id);
    }

    protected function savePayments($post, $shopping, $districts)
    {
        $shopping->load('items');

        ## Añadimos la duración del anuncio 1 mes.
        $convertToString = $this->convertToString();

        ## Recorremos las provincias para guardarlas en la relación del post.
        foreach ($districts as $key => $district) 
        {
            ## Buscamos la provincia
            $district_find = District::findOrFail($key);

            $post->districts()->attach($district_find->id, ['circuit' => $district[0], 'trial_ends_at' => $convertToString]);
        }
    }

    protected function convertToString()
    {
        $today = Carbon::now();
        $today->addMonth(1);

        return $today->toDateString();
    }
}
