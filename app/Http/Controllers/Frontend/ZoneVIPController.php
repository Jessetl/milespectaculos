<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User as User;
use App\Post as Post;
use Auth;

class ZoneVIPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.premiums.index');
    }

    public function profile($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->load('posts_meta', 'usr', 'category', 'messages');

        $user = $post->usr;
        $messages = $post->messages;
        $messages->load('response', 'users');

        $directions = json_decode($post->directions);
        $post->lat = $directions->lat;
        $post->lng = $directions->lng;

        $images = array();
        $sounds = array();  
        $movies = array();

        foreach ($post->posts_meta as $key => $meta) 
        {
            if ( $meta->meta_key == 'Image' ) 
            {
                if ( !empty ( $meta->meta_url ) ) {
                    $images[] = $meta->meta_url;
                }

            } elseif ( $meta->meta_key == 'Video' ) {

                if ( !empty ( $meta->meta_url ) ) {
                    $movies[] = $meta->meta_url;
                }
            } elseif ( $meta->meta_key == 'Audio' ) {

                if ( !empty ( $meta->meta_url ) ) {
                    $sounds[] = $meta->meta_url;
                }
            }
        }

        return view('frontend.premiums.profile', [
            'user' => $user,
            'post' => $post,
            'messages' => $messages,
            'images' => $images,
            'sounds' => $sounds,
            'movies' => $movies
        ]);
    }
}
