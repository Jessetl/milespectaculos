<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Post as Post;
use Cookie;

class FavoritesController extends Controller
{
    public function my_favorites(Request $request)
    {
    	$cookie_data = Cookie::get('favorites');
    	$cookies = unserialize($cookie_data);

    	$posts = array();

    	if ( $cookies !== FALSE ) {

	    	foreach ($cookies as $key => $slug) {

	    		$posts[$key] = Post::whereSlug($slug)->firstOrFail();
	    	}
    	}

        $favorites = $this->paginate($posts);

        return view('frontend.favorites.index', [
            'favorites' => $favorites
        ]);
    }

    public function my_favorites__save(Request $request)
    {
    	if ( $cookie_data = Cookie::get('favorites') ) {

    		$cookies = unserialize($cookie_data);


			if (($key = array_search($request->slug, $cookies)) !== FALSE) {
	
				unset($cookies[$key]);

				$setCookie = Cookie::queue('favorites', serialize($cookies), 2628000);
				$message = 'Removido de mis favoritos';

			} else {

	    		array_push($cookies, $request->slug);

	    		$setCookie = Cookie::queue('favorites', serialize($cookies), 2628000);
	    		$message = 'Agregado a mis favoritos';
			}

			return response()->json([
                'success' => $message
            ]);

    	} else {
    		
    		$cookies[] = $request->slug;

    		$setCookie = Cookie::queue('favorites', serialize($cookies), 2628000);

 			return response()->json([
                'success' => 'Agregado a mis favoritos'
            ]);
    	}
    }

    public function paginate($posts, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $posts = $posts instanceof Collection ? $posts : Collection::make($posts);

        return new LengthAwarePaginator($posts->forPage($page, $perPage), $posts->count(), $perPage, $page, ['path' => Paginator::resolveCurrentPath()]);
    }
}
