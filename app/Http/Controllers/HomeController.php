<?php

namespace App\Http\Controllers;

use App\Category as Category;
use App\CategoryType as Type;
use App\District as District;
use App\Mail\UserConfirmation;
use App\Page as Page;
use App\Paypal as Paypal;
use App\Post as Post;
use App\User as User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Mail;
use Session;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('category_type')->get();
        $header = Page::where('page', 'home')->first();

        $header = $header['url'];

        return view('welcome', [
            'categories' => $categories,
            'header' => $header,
        ]);
    }

    public function contacme()
    {
        $header = Page::where('page', 'contact')->first();

        $header = $header['url'];
        $logo = 'INHABILITAR';

        return view('frontend.landing.contact', [
            'header' => $header,
            'logo' => $logo,
        ]);
    }

    public function getUser($username)
    {
        $user = User::whereUsername($username)->firstOrFail();

        return view('frontend.search.partials.contact-mailbox', [
            'user' => $user,
        ]);
    }

    public function getDistricts($slug)
    {
        $posts_districts = District::whereSlug($slug)->firstOrFail();
        $posts_districts->load('posts');

        $posts = array();

        $query_posts = $posts_districts->posts()->where('posts.status', 'published')->get();

        $this->deleteByUser($query_posts);

        foreach ($query_posts as $key => $post) {

            $post->load('usr', 'posts_meta', 'districts');
            $posts[] = $post;
        }

        $posts = new Paginator($posts, 10);

        return view('frontend.search.index-map', [
            'posts' => $posts,
        ]);
    }

    public function getPost($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->load('posts_meta', 'usr', 'category', 'messages');

        ## Variable para deshabilitar logo
        $logo = 'INHABILITAR';

        $header = $post->header;

        $json = json_decode($post->directions);

        if ($json !== null) {
            $post->lat = $json->lat;
            $post->lng = $json->lng;
        }

        $user = $post->usr;
        $messages = $post->messages;
        $messages->load('response', 'users');

        $images = array();
        $sounds = array();
        $movies = array();

        foreach ($post->posts_meta as $key => $meta) {
            if ($meta->meta_key == 'Image') {
                if (!empty($meta->meta_url)) {
                    $images[] = $meta->meta_url;
                }

            } elseif ($meta->meta_key == 'Video') {

                if (!empty($meta->meta_url)) {
                    $movies[] = $meta->meta_url;
                }
            } elseif ($meta->meta_key == 'Audio') {

                if (!empty($meta->meta_url)) {
                    $sounds[] = $meta->meta_url;
                }
            }
        }

        return view('frontend.premiums.profile', [
            'user' => $user,
            'post' => $post,
            'header' => $header,
            'logo' => $logo,
            'messages' => $messages,
            'images' => $images,
            'sounds' => $sounds,
            'movies' => $movies,
        ]);
    }

    public function get__prices()
    {
        $paypal['ar'] = Paypal::findOrFail(1);
        $paypal['em'] = Paypal::findOrFail(2);
        $paypal['pr'] = Paypal::findOrFail(3);

        return view('frontend.subscriptions.index', [
            'payments' => $paypal,
        ]);
    }

    public function confirmed($code)
    {
        if (\Auth::guest()) {

            $user = User::where('confirmation_code', $code)->firstOrFail();

            if ($user->confirmation_code !== null) {
                $user->confirmed = 1;
                $user->confirmation_code = null;
                $user->save();
            }

            auth()->login($user);

        } else {

            $user = \Auth::user();
        }

        return view('frontend.profile.confirmed', [
            'user' => $user,
        ]);
    }

    public function resend()
    {
        return view('frontend.profile.resend');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|exists:users',
        ]);
    }

    public function resend__email(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = User::where('email', $request['email'])->firstOrFail();

        try {

            Mail::to($user->email)->send(new UserConfirmation($user));

            Session::flash('success', 'Te hemos enviado un correo de verificación, revisa tu buzon de entrada o tus correos no deseados');

            return redirect('login');

        } catch (\Exception $e) {

            Session::flash('email', 'Huebo un problema al enviar tu correo de confirmación, contacta con nosotros o pulsa aquí para reenviar.');

            return redirect('login');
        }
    }

    public function getCategories($subcategory, Request $request)
    {
        $subcategories = Type::where('category', $subcategory)->get();

        return $subcategories;
    }

    public function getPayment()
    {
        return Paypal::findOrFail(3);
    }

    public function about__us()
    {
        $categories = Category::with('category_type')->get();

        return view('frontend.landing.about-us', [
            'categories' => $categories,
        ]);
    }

    public function questions()
    {
        return view('frontend.landing.questions');
    }

    public function help()
    {
        return view('frontend.landing.help');
    }

    protected function deleteByUser($posts)
    {
        foreach ($posts as $key => $post) {

            if ($post->usr->role == 3 and $post->type_posts == 1) {

                unset($posts[$key]);
            }
        }

        return $posts;
    }

    protected function getByCategory()
    {
        return Category::orderBy('name', 'ASC')->get();
    }

    protected function getByDistrict()
    {
        return District::orderBy('name', 'ASC')->get();
    }

    public function dt__set(Request $request)
    {
        if ($request->ajax()) {
            return $this->getByDistrict();
        }
    }

    protected function getOnlyDistrict($id)
    {
        return District::findOrFail($id);
    }

    public function dt_get(Request $request, $id)
    {
        if ($request->ajax()) {
            return $this->getOnlyDistrict($id);
        }
    }

    public function ct_get($id)
    {
        return Type::where('category', $id)->get();
    }

    public function test()
    {
        header("Content-type: image/jpeg");

        // Imagen base
        $estampa = imagecreatefromjpeg("http://www.maestrosdelweb.com/images/2009/08/crayones_jpg.jpg");
        // Imagen a superponer
        $im = imagecreatefromjpeg("http://datoweb.com/user/avatar/no_avatar.jpg");

        // Establecer los márgenes para la estampa y obtener el alto/ancho de la imagen de la estampa
        $margen_dcho = 10;
        $margen_inf = 10;
        $sx = imagesx($estampa);
        $sy = imagesy($estampa);

        // Copiar la imagen de la estampa sobre nuestra foto usando los índices de márgen y el
        // ancho de la foto para calcular la posición de la estampa.
        imagecopy($im, $estampa, imagesx($im) - $sx - $margen_dcho, imagesy($im) - $sy - $margen_inf, 0, 0, imagesx($estampa), imagesy($estampa));

        imagepng($im);
        imagedestroy($im);

    }
}
