<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\UserSocialite as UserSocialite;
use App\User as User;
use App\Role as Role;
use Socialite;
use Session;

class SocialAuthController extends Controller
{
    public function facebook()
    {
    	return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {
        try {
            
        	$user = Socialite::driver('facebook')->user();

            $existsInApplication = User::where('email', $user->email)->first();
            $existsInFacebook = User::whereIn('users.id', function($query) use ($user) {
                                $query->from('user_socialite')
                                    ->select('user_socialite.user')
                                    ->where('user_socialite.socialite', $user->id);
                            })->first();
    
            $redirect = redirect('/');

            if ($existsInFacebook !== null) {
              
                auth()->login($existsInFacebook);

                return $redirect;

            } elseif ($existsInApplication != null) {

                auth()->login($existsInApplication);

                return $redirect;
            }

        	session()->put('facebookUser', $user);

        	return view('auth.facebook.register', [
        		'user' => $user
        	]);

        } catch (\Exception $e) {
            
            $this->saveLogInfo($e);

            Session::flash('error', 'Error al conectar con Facebook, intentalo nuevamente.');

            return redirect()->back();
        }
    }

    public function register(Request $request)
    {
    	$role = Role::findOrFail($request['role']);

    	$data = $request->session()->get('facebookUser');

        $username = is_null($data->nickname) ? str_random(10) : $data->nickname;

    	$user = new User();
        $user->username = $username;
    	$user->url = $data->avatar;
    	$user->name = $request['name'];
    	$user->surname = $request['surname'];
    	$user->email = $request['email'];
    	$user->password = bcrypt(str_random(16));
    	$user->role = $role['id'];
    	$user->save();

    	$profile = new UserSocialite();
    	$profile->user = $user['id'];
    	$profile->red = 'Facebook';
    	$profile->socialite = $data->id;
    	$profile->profile = $data->profileUrl;
    	$profile->avatar = $data->avatar;
    	$profile->gender = $data['gender'];
    	$profile->save();

    	session()->forget('facebookUser');

    	auth()->login($user);

    	return redirect('posts/create');
    }

    public function saveLogInfo($e)
    {
        // Almacenamos la información del error.
        return \Log::debug('Test fails' . $e);
    }
}
