<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\UserConfirmation;
use App\Role as Role;
use App\User as User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;
use Session;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/posts/create';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:45|unique:users', 'regex:/^[a-zA-Z0-9]*([a-zA-Z][0-9]|[0-9][a-zA-Z])[a-zA-Z0-9]*$/',
            'name' => 'required|alpha_spaces|max:45',
            'surname' => 'required|alpha_spaces|max:45',
            'phone' => 'required|digits_between:8,16',
            'email' => 'required|string|email|max:45|unique:users',
            'password' => 'required|string|min:4|confirmed',
            'role' => 'required|in:user,artist,company',
        ]);
    }

    protected function create(array $data)
    {
        $this->validator($data)->validate();

        $role = Role::whereSlug($data['role'])->firstOrFail();
        $data['confirmation_code'] = str_random(25);

        return User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'surname' => $data['surname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmed' => 0,
            'confirmation_code' => str_random(25),
            'role' => $role['id'],
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        try {

            Mail::to($user->email)->send(new UserConfirmation($user));

            Session::flash('success', 'Te hemos enviado un correo de verificación, revisa tu buzón de entrada o tus correos no deseados');

            return redirect('login');

        } catch (\Exception $e) {

            Session::flash('email', 'Hubo un problema al enviar tu correo de confirmación, contacta con nosotros o pulsa aquí para reenviar.');

            return redirect('login');
        }
    }
}
