<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\District as District;
use App\Category as Category;
use App\CategoryType as Type;
use App\Post as Post;

class SearchController extends Controller
{
    public function searchDistrict($slug)
    {
        $posts_districts = District::with('posts')->whereSlug($slug)->firstOrFail();

    	$posts = array();

    	$districts = $this->getByDistrict();
        $categories = $this->getByCategory();
      
        $query_posts = $posts_districts->posts()->with('usr', 'posts_meta', 'districts')->where([['posts.status', 'published'], ['posts.type_posts', 0]])->orderBy('updated_at', 'DESC')->get();

        $posts = $this->paginate($query_posts);

        return view('frontend.search.index-map', [
        	'posts' => $posts,
        	'districts' => $districts,
        	'categories' => $categories,
        	'slug' => $slug,
        ]);
    }

    public function searchAdvanced(Request $request)
    {
    	$districts = $this->getByDistrict();
        $categories = $this->getByCategory();
        $header = null;

        if ( !empty($request['subcategory']) ) {

            $category = Type::findOrFail($request['subcategory']);
            $header = $category->header;

        } elseif ( !empty($request['category']) ) {

            $category = Category::findOrFail($request['category']);
            $header = $category->header;
        }

        $query = Post::with([
            'usr', 
            'posts_meta', 
            'districts'
        ])
        ->where([
            ['status', 'published'],
            ['type_posts', 0]
        ]);

        $query
            ->when($request->has('subcategory'), function($query) use ($request) {
                $query->where('category_type', $request->subcategory);
            })
            ->when($request->has('search'), function ($query) use ($request) {
                $query->where('name', 'LIKE', '%' . $request->search . '%');
            })
            ->when($request->has('type'), function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->when($request->has('amount_min') && $request->has('amount_max'), function ($query) use ($request) {
                $query->whereBetween('amount', [$request->amount_min, $request->amount_max]);
            })
            ->when($request->has('districts'), function($query) use ($request) {
                $query->whereHas('districts', function ($query) use ($request) {
                    $query->where('district', $request->districts);
                });
            })
            ->when($request->has('date'), function ($query) use ($request) {
                $query->orderBy('updated_at', $request->date);
            });
        
        $posts = $query->paginate(5);
       
		return view('frontend.search.index', [
			'posts' => $posts,
			'districts' => $districts,
			'categories' => $categories,
            'type' => $request['type'],
            'search' => $request['search'],
            'header' => $header
		]);
    }

    public function searchIndex(Request $request)
    {
    	$districts = $this->getByDistrict();
        $categories = $this->getByCategory();

        $posts = Post::with('usr', 'posts_meta', 'districts')->callback($request->all())->paginate(5);

        return view('frontend.search.index-search', [
            'posts' => $posts,
            'districts' => $districts,
            'categories' => $categories
        ]);
    }

    public function search(Request $request)
    {
        $districts = $this->getByDistrict();
        $categories = $this->getByCategory();
        $subcategories = Type::get();
        $header = null;

        $category = Category::with('category_type')->whereSlug($request['subcategory'])->first();
    
        if ( !empty($category) ) {
            
            $category_type = $category->category_type()->with('categorie', 'posts')->get();
            $header = $category->header;
            $query_posts = array();
            $posts = array();
            

            foreach ($category_type as $key => $category) {

                if ( $category->posts->count() > 0 ) {
                    
                    $posts[] = $category->posts()->with('usr', 'posts_meta', 'districts')->where([['type_posts', 0],['status', 'published']])->get();
                }
            }
            
            foreach ($posts as $key => $post) {
                foreach ($post as $key => $pt) {
                    $query_posts[] = $pt;
                }
            }

            $posts = $this->paginate($query_posts);

        } else {

            $category_type = Type::whereSlug($request['subcategory'])->firstOrFail();
            $category_type->load('categorie', 'posts');
            
            $posts = array();
            $header = $category_type->header;

            $query_posts = $category_type->posts()->with('usr', 'posts_meta', 'districts')->where([['type_posts', 0],['status', 'published']])->get();
       
            $posts = $this->paginate($query_posts);
        }

        return view('frontend.search.index', [
            'posts' => $posts,
            'districts' => $districts,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'header' => $header
        ]);
    }

    public function files(Request $request, $slug)
    {
        $post = Post::with('posts_meta')->whereSlug($slug)->firstOrFail();
        $posts_meta = $post->posts_meta()->get();

        return response()->json([
            'files' => $posts_meta
        ]);
       
    }

    public function images(Request $request, $slug)
    {
        $post = Post::with('posts_meta')->whereSlug($slug)->firstOrFail();
        $posts_meta = $post->posts_meta()->where('meta_key', 'Image')->get();

        return response()->json([
            'files' => $posts_meta
        ]);
       
    }

    public function redirect($slug)
    {
        return redirect()->route('category.search', ['subcategory' => $slug]);
    }

    protected function deleteByUser($posts) 
    {
        foreach ($posts as $key => $post) {

            if ($post->usr->role == 3 AND $post->type_posts == 1) {

                unset($posts[$key]);
            }
        }

        return $posts;
    }

    protected function getByCategory() 
    {
        return Category::with('category_type')->orderBy('name', 'ASC')->get();
    }

    protected function getByDistrict() 
    {
        return District::orderBy('name', 'ASC')->get();
    }

    protected function paginate($posts, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $posts = $posts instanceof Collection ? $posts : Collection::make($posts);

        return new LengthAwarePaginator($posts->forPage($page, $perPage), $posts->count(), $perPage, $page, ['path' => Paginator::resolveCurrentPath()]);
    }
}
