<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category as Category;
use App\Page as Page;
use App\CategoryType as Type;
use App\Header as Header;
use Carbon\Carbon;

use Session;
use Image;
use Storage;

class HeadersController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth', 'master']);
    }

    public function headers_pages()
    {
        $pages = Page::orderBy('id', 'ASC')->get();

        return view('backend.headers.page', [
            'pages' => $pages
        ]);
    }

    public function edit_headers_pages($id)
    {
        $page = Page::findOrFail($id);

        return view('backend.headers.edit-page', [
            'page' => $page
        ]);
    }

    public function update_headers_pages(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        if ( !empty($request->file('file')) ) {

            $path = $request->file('file')->store('public/headers');
            $fileName = collect(explode('/', $path))->last();

            $image = Image::make(Storage::get($path));

            $image->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('jpg', 100));

            Session::flash('success', 'Cambio de header para la página ' . $page->page . ' correctamente.');
 
        } else {

            Session::flash('errors', 'No ha seleccionado ningun header para la página ' . $page->page . '.');
        }

        if ( empty($page->url) ) {

            $page->url = $fileName;
            $page->save();

        } else {

            $uri = $this->uri();
            $delete = \File::Delete($uri.$page->url);

            $page->url = $fileName; 
            $page->save();
        }

        return redirect()->back();
    }

    public function delete_headers_page($id)
    {
        ## Eliminamos imagenes del anuncio. 
        $page = Page::findOrFail($id);
        ## Eliminamos las imagenes de la carpeta storage.
        $uri = $this->uri();
        $delete = \File::Delete($uri.$page->url);
        
        $page->url = NULL;
        $page->save();
        
        Session::flash('success', 'Header de página eliminado.');

        return redirect()->back();
    }

    public function header__category()
    {
    	$categories = Category::orderBy('id', 'ASC')->get();
  
    	return view('backend.headers.category', [
            'categories' => $categories
        ]);
    }

    public function header__category_type()
    {
        $categories = Type::orderBy('id', 'ASC')->get();
        
        return view('backend.headers.category-type', [
            'categories' => $categories
        ]);
    }

    public function edit_category__type($slug)
    {
        $category = Type::whereSlug($slug)->firstOrFail();

        return view('backend.headers.edit-category_type', [
            'category' => $category
        ]);
    }

    public function edit($slug)
    {
    	$category = Category::whereSlug($slug)->firstOrFail();

        return view('backend.headers.edit', [
            'category' => $category
        ]);
    }

    public function update(Request $request, $id)
    {
    	$category = Category::findOrFail($id);
        
        if ( !empty($request->file('file')) ) {

            $path = $request->file('file')->store('public/headers');
            $fileName = collect(explode('/', $path))->last();

            $image = Image::make(Storage::get($path));

            $image->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('jpg', 60));

            Session::flash('success', 'El header de la categoría ' . $category->name . ' se ha editado.');
 
        } else {

            Session::flash('errors', 'No ha seleccionado ningun header para la categoría ' . $category->name . '.');
        }

        if ( empty($category->header) ) {

            $category->header = $fileName;
            $category->save();

        } else {

            $uri = $this->uri();
            $delete = \File::Delete($uri.$category->header);

            $category->header = $fileName; 
            $category->save();
        }

        return redirect()->back();
    }

    public function update_category__type(Request $request, $id)
    {
        $category = Type::findOrFail($id);
        
        if ( !empty($request->file('file')) ) {

            $path = $request->file('file')->store('public/headers');
            $fileName = collect(explode('/', $path))->last();

            $image = Image::make(Storage::get($path));

            $image->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('jpg', 60));

            Session::flash('success', 'Cambio de header para la categoría ' . $category->name . ' correctamente.');
 
        } else {

            Session::flash('errors', 'No ha seleccionado ningun header para la categoría ' . $category->name . '.');
        }

        if ( empty($category->header) ) {

            $category->header = $fileName;
            $category->save();

        } else {

            $uri = $this->uri();
            $delete = \File::Delete($uri.$category->header);

            $category->header = $fileName; 
            $category->save();
        }

        return redirect()->back();
    }

    public function delete_category($id)
    {
        ## Eliminamos imagenes del anuncio. 
        $category = Category::findOrFail($id);
        ## Eliminamos las imagenes de la carpeta storage.
        $uri = $this->uri();
        $delete = \File::Delete($uri.$category->header);
        
        $category->header = NULL;
        $category->save();

        
        Session::flash('success', 'Header de categoría eliminado.');

        return redirect()->back();
    }

    public function delete_category__type($id)
    {
        ## Eliminamos imagenes del anuncio. 
        $category = Type::findOrFail($id);
        ## Eliminamos las imagenes de la carpeta storage.
        $uri = $this->uri();
        $delete = \File::Delete($uri.$category->header);

        $category->header = NULL;
        $category->save();


        Session::flash('success', 'Header de categoría eliminado.');

        return redirect()->back();
    }

    protected function fileName($name)
    {
        return str_random(20);
    }

    protected function uri()
    {
        return public_path().'/storage/headers/';
    }
}
