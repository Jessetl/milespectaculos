<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Carousel as Carousel;
use Session;

class CarouselController extends Controller
{
    public function index()
    {
    	$carousels = Carousel::orderBY('id', 'DESC')->get();

    	return view('backend.carousel.index', [
    		'carousels' => $carousels
    	]); 
    }

    public function store(Request $request, $id)
    {
    	$carousel = Carousel::findOrFail($id);
    	$carousel->status = $carousel->status ? 0 : 1;
    	$carousel->save();

        $status = $carousel->status ? 'ACTIVO' : ' INACTIVO';

    	Session::flash('success', 'Carrusel ' . $carousel->name . ' modo ' . $status);

    	return redirect()->back();	
    }
}
