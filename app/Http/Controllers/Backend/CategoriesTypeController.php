<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\CategoryType as Type;
use App\Category as Category;
use Carbon\Carbon;
use Session;

class CategoriesTypeController extends Controller
{   
    public function __construct()
    {
        $this->middleware(['auth', 'master']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories_type = Type::with('categorie')->orderBy('category', 'ASC')->get();

        return view('backend.categoriesType.index', [
            'categories_type' => $categories_type
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('id', 'ASC')->get();

        return view('backend.categoriesType.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'category' => 'required',
            'name' => 'required|max:75|unique:categories_type',
            'slug' => 'required|alpha_dash|max:115|unique:categories_type'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $category = Category::findOrFail($request['category']);
        $request['slug'] = $this->streplace($request['slug']);

        $categories_type = new Type();
        $categories_type->category = $category['id'];
        $categories_type->name = $request['name'];
        $categories_type->slug = $request['slug'];
        $categories_type->save();

        Session::flash('success', 'Nueva Subcategoría agregada');

        return redirect('/admin/categories-type');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories_type = Type::findOrFail($id);

        $categories = Category::orderBy('id', 'ASC')->get();

        return view('backend.categoriesType.edit', [
            'categories_type' => $categories_type,
            'categories' => $categories
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'category' => 'required',
            'name' => 'required|alpha_spaces|max:75', Rule::unique('categories_type', 'name')->ignore($id),
            'slug' => 'required|alpha_dash|max:115', Rule::unique('categories_type', 'slug')->ignore($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validatorUpdate($request->all(), $id)->validate();

        $categories_type = Type::findOrFail($id);
        $categories = Category::findOrFail($request['category']);

        $request['slug'] = $this->streplace($request['slug']);

        $categories_type->name = $request['name'];
        $categories_type->slug = $request['slug'];
        $categories_type->save();

        Session::flash('success', 'La subcategoría ' . $categories_type->name . ' fue editada.');

        return redirect('/admin/categories-type');
    }

    public function destroy($id)
    {
        $category_type = Type::findOrFail($id);
        $category_type->delete();

        Session::flash('success', 'La subcategoría ' . $category_type->name . ' ha sido borrada.');

        return redirect()->back();
    }

    protected function uri()
    {
        return public_path().'/storage/categories/';
    }

    protected function streplace($string) 
    {
        $tofind = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $replac = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        $string = utf8_decode($string);
        $string = strtr($string,utf8_decode($tofind),$replac);
        $string = strtolower($string);

        return utf8_encode($string);
    }
}
