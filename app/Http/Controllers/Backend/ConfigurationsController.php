<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Configuration as Configuration;
use Session;

class ConfigurationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('master');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configurations = Configuration::orderBy('meta_key', 'ASC')->get();

        return view('backend.configurations.index', [
            'configurations' => $configurations
        ]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $configuration = Configuration::whereSlug($id)->firstOrFail();

        return view('backend.configurations.edit', [
            'configuration' => $configuration
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $configuration = Configuration::findOrFail($id);
        $configuration->meta_value = $request['meta_value'];
        $configuration->save();

        Session::flash('success', 'Configuración de ' . $configuration->meta_key . ' actualizada');

        return redirect('/admin/config');
    }
}
