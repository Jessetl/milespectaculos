<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\PaymentSubscription as Payment;

class SubscriptionsController extends Controller
{
    public function index()
    {
        $payments = Payment::with('users')->orderBy('id', 'DESC')->get();

        return view('backend.subscriptions.index', [
            'payments' => $payments,
        ]);
    }

    public function show(Payment $payment)
    {
        $payment->load('users');

        return view('backend.subscriptions.show', [
            'payment' => $payment,
        ]);
    }
}
