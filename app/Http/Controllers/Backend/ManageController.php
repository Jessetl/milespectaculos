<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notify;

class ManageController extends Controller
{
    public function index()
    {
    	$notifications = Notify::with('pts', 'usr')->where('manage', 1)->orderBy('id', 'DESC')->get();

    	return view('backend.notifications.index', [
    		'notifications' => $notifications
    	]);
    }
}
