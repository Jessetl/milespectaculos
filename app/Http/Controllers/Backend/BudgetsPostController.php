<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Post;
use App\PostMeta as Meta;

use Image;
use Storage;

use Session;

class BudgetsPostController extends Controller
{
    public function index()
    {
    	$posts = Post::with('usr', 'category')->where('budget', 1)->orderBy('id', 'DESC')->get(); 

    	return view('backend.budgets.posts', [
    		'posts' => $posts
    	]);
    }

    public function edit(Post $post)
    {
    	$post->load('usr', 'category', 'posts_meta');

    	$json = json_decode($post->directions);

        $post->lat = $json->lat;
        $post->lng = $json->lng;

    	return view('backend.budgets.editPost', [
    		'post' => $post
    	]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'file' => 'nullable|mimes:jpeg,png,jpg',
            'title' => 'required|max:75',
            'content' => 'required|min:10',
            'one' => 'required|numeric|digits_between:5,15',
            'two' => 'nullable|digits_between:5,15',
            'email' => 'required|string|email|max:255',
            'postal_code' => 'nullable|numeric|min:4',
            'address' => 'nullable',
            'amount' => 'nullable'
        ]);
    }

    public function update(Request $request, Post $post)
    {
    	## Validaciones para anuncio de oferta de servicios.
        $this->validator($request->all())->validate();
    	
    	$lngLt = json_encode(array('lat' => $request['latInput'], 'lng' => $request['lngInput']));

    	if ( !empty($request->file('file')) ) {
            
    		## Eliminamos las imagenes de la carpeta storage.
            $uri = $this->uri();
            \File::Delete($uri.$post->header);

            $path = $request->file('file')->store('public/headers');
            $fileName = collect(explode('/', $path))->last();
            
            $image = Image::make(Storage::get($path));

            $image->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('jpg', 100));

            $post->header = $fileName;
        }

        $post->name = $request['title'];
	    $post->content = $request['content'];
	    $post->phone1 = $request['one'];
	    $post->phone2 = $request['two'];
	    $post->email = $request['email'];
	    $post->address = $request['directions'];
	    $post->directions = $lngLt;
	    $post->url = $request['url'];
	    $post->type_amount = $request['type_amount'];
	    $post->amount = $request['amount'];
	    $post->status = $request['status'];
	    $post->discount = $request['discount'];
	    $post->save();

	    Session::flash('success', 'Anuncio modificado');

	    return redirect()->back();
    }

    public function gallery(Post $post)
    {
    	$post->load('posts_meta');

        $post->images = $post->posts_meta()->where('meta_key', 'Image')->get();

        return view('backend.budgets.sections.gallery', [
        	'post' => $post
        ]);
    }

    public function movies(Post $post)
    {
    	$post->load('posts_meta');

        $post->videos = $post->posts_meta()->where('meta_key', 'Video')->get();

        return view('backend.budgets.sections.videos', [
        	'post' => $post
        ]);
    }

    public function sounds(Post $post)
    {
    	$post->load('posts_meta');

        $post->audios = $post->posts_meta()->where('meta_key', 'Audio')->get();

        return view('backend.budgets.sections.audios', [
        	'post' => $post
        ]);
    }

    public function updateGallery(Request $request, $id)
    {
    	$post = Post::with('posts_meta')->findOrFail($id);

    	$path = $request->file('file')->store('public/posts');
        $fileName = collect(explode('/', $path))->last();
        
        $image = Image::make(Storage::get($path));

        $image->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::put($path, (string) $image->encode('png', 80));

        $meta = new Meta(['meta_key' => 'Image', 'meta_url' => $fileName]);
        $post->posts_meta()->save($meta);

        return response()->json([
            'message' => 'Imagen guardada correctamente.'
        ]);
    }

    public function updateMovies(Request $request, $id)
    {
    	$post = Post::with('posts_meta')->findOrFail($id);

    	$videoEmbed = $this->converterVideoInEmbed($request['video']);

    	$meta = new Meta(['meta_key' => 'Video', 'meta_url' => $videoEmbed]);
        $post->posts_meta()->save($meta);

        return redirect()->back();
    }

    public function updateSounds(Request $request, $id)
    {
    	$post = Post::with('posts_meta')->findOrFail($id);

    	$meta = new Meta(['meta_key' => 'Audio', 'meta_url' => $request['audio']]);
        $post->posts_meta()->save($meta);

        return redirect()->back();
    }

    public function deleteGallery($id)
    {
    	$meta = Meta::findOrFail($id);
    	\File::Delete( public_path().'/storage/posts/' . $meta->meta_url);

    	$meta->delete();

    	return redirect()->back();
    }

    public function deleteMovies($id)
    {
    	$meta = Meta::findOrFail($id);
    	$meta->delete();

    	return redirect()->back();
    }

    public function deleteSounds($id)
    {
    	$meta = Meta::findOrFail($id);
    	$meta->delete();

    	return redirect()->back();
    }

    protected function uri()
    {
        ## Url de imagenes de los anuncios.
        return public_path().'/storage/headers/';
    }

    protected function converterVideoInEmbed($url)
    {
        // return data
        $finalUrl = '';

        if(strpos($url, 'youtube.com/') !== false) {
            
            //it is Youtube video
            $videoId = explode("v=",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }
        
            $finalUrl.='https://www.youtube.com/embed/'.$videoId;

        }else if(strpos($url, 'youtu.be/') !== false){
            
            //it is Youtube video
            $videoId = explode("youtu.be/",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }

            $finalUrl.='https://www.youtube.com/embed/'.$videoId;
        }

        return $finalUrl;
    }
}
