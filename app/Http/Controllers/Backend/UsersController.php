<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\User as User;
use App\Role as Role;
use Carbon\Carbon;
use Session;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('master');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'DESC')->get();

        return view('backend.users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::orderBy('name', 'ASC')->get();

        return view('backend.users.create', [
            'roles' => $roles
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'role' => 'required',
            'username' => 'required|max:35|unique:users',
            'name' => 'required|alpha_spaces|min:1|max:45',
            'surname' => 'required|alpha_spaces|min:1|max:45',
            'email' => 'required|email|max:45|unique:users',
            'password' => 'required|string|min:4|confirmed',
            'confirmed' => 'required|boolean',
            'premium' => 'required|boolean'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $roles = Role::findOrFail($request['role']);
        
        $user = new User();
        $user->username = $request['username'];
        $user->name = $request['name'];
        $user->surname = $request['surname'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->role = $roles['id'];
        $user->confirmed = $request['confirmed'];
        $user->premium = $request['premium'];
        $user->save();

        Session::flash('success', 'Nuevo usuario ' . $user->email . ' agregado');

        return redirect('/admin/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        $user = User::whereUsername($username)->firstOrFail();
        $roles = Role::orderBy('name', 'ASC')->get();

        return view('backend.users.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'role' => 'required',
            'name' => 'required|alpha_spaces|max:45',
            'surname' => 'required|alpha_spaces|max:45',
            'email' => 'required|email|min:5|max:45|unique:users,email,'.$id,
            'confirmed' => 'required|boolean',
            'premium' => 'required|boolean'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validatorUpdate($request->all(), $id)->validate();

        $user = User::findOrFail($id);
        $roles = Role::findOrFail($request['role']);

        $user->name = $request['name'];
        $user->surname = $request['surname'];
        $user->email = $request['email'];
        $user->role = $roles['id'];
        $user->confirmed = $request['confirmed'];
        $user->premium = $request['premium'];
        $user->save();

        Session::flash('success', 'Usuario ' . $user->name . ' modificado');

        return redirect('/admin/users'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        Session::flash('success', 'Usuario ' . $user->name . ' eliminada');

        return redirect()->back();
    }
}
