<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Paypal as Paypal;
use Session;

class PaypalController extends Controller
{

    public function index()
    {
        $paypals = Paypal::orderBy('name', 'ASC')->get();

        return view('backend.paypal.index', [
            'paypals' => $paypals
        ]);
    }

    public function edit($slug)
    {
        $paypal = Paypal::whereSlug($slug)->firstorFail();

        return view('backend.paypal.edit', [
            'paypal' => $paypal
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'name' => 'required|max:50', Rule::unique('paypal', 'name')->ignore($id),
            'slug' => 'required|alpha_dash|max:75', Rule::unique('paypal', 'slug')->ignore($id),
            'status' => 'required|in:ACTIVO,INACTIVO',
            'amount' => 'required',
            'description' => 'nullable',
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validatorUpdate($request->all(), $id)->validate();

        $paypal = Paypal::findOrFail($id);
        $paypal->name = $request['name'];
        $paypal->status = $request['status'];
        $paypal->slug = $this->streplace($request['slug']);
        $paypal->amount = $request['amount'];
        $paypal->description = $request['description'];
        $paypal->save();

        Session::flash('success', 'La suscripción ' . $paypal->name . ' ha sido editada.');
        
        return redirect('admin/paypal');
    }

    protected function streplace($string) 
    {
        $tofind = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $replac = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        $string = utf8_decode($string);
        $string = strtr($string,utf8_decode($tofind),$replac);
        $string = strtolower($string);

        return utf8_encode($string);
    }
}
