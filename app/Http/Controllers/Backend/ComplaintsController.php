<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reported as Reported;
use App\Post as Post;
use Session;

class ComplaintsController extends Controller
{
    public function __construct()
    {
        $this->middleware('master');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Reported::with('posts')->orderBy('id', 'DESC')->get();

        return view('backend.posts_reported.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrfail($id);
        $post->delete();

        Session::flash('success', 'Anuncio ' . $post->name . ' ha sido eliminado');

        return redirect()->back();
    }
}
