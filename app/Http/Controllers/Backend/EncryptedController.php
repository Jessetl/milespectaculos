<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Encrypted as Encrypted;
use Session;

class EncryptedController extends Controller
{
    public function index()
    {
    	$encrypteds = Encrypted::orderBy('id', 'DESC')->where('enabled', 1)->get(); 

    	return view('backend.encrypted.index', [
    		'encrypteds' => $encrypteds
    	]);
    }

    public function create()
    {
    	return view('backend.encrypted.create');
    }

    public function store(Request $request)
    {
    	for ($i = 0; $i <= $request['number']; $i++) {

    		$str_code = '';

    		$encrypted = new Encrypted();
    		$encrypted->ends_at = $request['ends_at'];

    		for ($n = 0; $n < 3; $n++) {
    			$str_code .= $this->getRandomCode() . '-';
    			$myString = substr($str_code, 0, -1);
    		}

    		$encrypted->code = $myString;
    		$encrypted->save();	
    	}
    	
    	Session::flash('success', 'Nuevos códigos generados');

        return redirect('/admin/encrypted');
    }

    public function delete($id)
    {
    	$encrypted = Encrypted::findOrFail($id);
    	$encrypted->delete();

    	Session::flash('success', 'Código ' . $encrypted->code . ' eliminado');

        return redirect()->back();
    }

    public function getRandomCode() {

	    $an = "0123456789";
	    $su = strlen($an) - 1;

	    return substr($an, rand(0, $su), 1) .
	            substr($an, rand(0, $su), 1) .
	            substr($an, rand(0, $su), 1) .
	            substr($an, rand(0, $su), 1) .
	            substr($an, rand(0, $su), 1) .
	            substr($an, rand(0, $su), 1);
	}
}
