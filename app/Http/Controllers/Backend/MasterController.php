<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
	public function __construct()
	{
		$this->middleware('master');
	}
	
    public function index()
    {
    	return view('home');
    }
}
