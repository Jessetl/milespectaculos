<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post as Post;
use App\PostMeta as Meta;
use App\User as User;
use Session;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('master');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['category', 'usr'])->where('status', 'published')->orderBy('id', 'DESC')->get();

        return view('backend.posts.index', [
            'posts' => $posts
        ]);
    }   

    public function toTransfer($slug)
    {
        $post = Post::with(['category', 'usr'])->whereSlug($slug)->firstOrFail();

        return view('backend.posts.transfer', [
            'post' => $post
        ]);
    }

    public function search($email)
    {
        $user = User::where('email', $email)->first();

        $array = [
            'user' => $user
        ];

        return response()->json($array);   
    }

    public function update(Request $request)
    {
        $user = $request['user'];
        $json = $request['post'];
        
        $post = Post::findOrFail($json['id']);
        $post->user = $user['id'];
        $post->save();

        $array = [
            'post' => $post
        ];

        return response()->json($array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        Session::flash('success', 'El anuncio ' . $post->code . ' fue borrado');
        
        return redirect()->back();
    }
}
