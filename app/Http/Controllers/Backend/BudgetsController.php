<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Budget;
use Session;

class BudgetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budgets = Budget::with('usr')->orderBy('id', 'DESC')->get();

        return view('backend.budgets.events', [
            'budgets' => $budgets
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $budget = Budget::with('usr')->findOrFail($id);

        return view('backend.budgets.showevent', [
            'budget' => $budget
        ]);
    }

    public function success($id)
    {
        $budget = Budget::findOrFail($id);
        $budget->status = 1;
        $budget->save();

        Session::flash('success', 'El evento ha sido cambiado de estado.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $budget = Budget::findOrFail($id);
        $budget->delete();

        Session::flash('success', 'El evento fue eliminado.');

        return redirect()->back();
    }
}
