<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\PaymentPost as Payment;

class AdvertisementsController extends Controller
{
    public function index()
    {
        $payments = Payment::with([
            'posts',
            'posts.usr',
        ])->orderBy('id', 'DESC')->get();

        return view('backend.advertisements.index', [
            'payments' => $payments,
        ]);
    }

    public function show(Payment $payment)
    {
        $payment->load('posts');

        return view('backend.advertisements.show', [
            'payment' => $payment,
        ]);
    }
}
