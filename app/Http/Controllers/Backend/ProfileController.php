<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
	public function index(Request $request)
	{
		$me = $request->user();

		return view('backend.profile.index', [
			'me' => $me
		]);
	}

	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:45',
            'surname' => 'required|max:45',
            'file' => 'nullable|image|mimes:jpeg,png'
        ]);
    }

    public function update(Request $request)
    {
    	$this->validator($request->all())->validate();

    	$me = $request->user();
    	$me->name = $request['name'];
    	$me->surname = $request['surname'];
    	
    	if ( !empty($request->file('file')) ) {

    		if ( !empty($me->url) ) \Storage::disk('public')->delete($me->url);
    		
	        $fileName = $request->file('file')->store('avatars', 'public');
	    	$me->url = $fileName;
    	}

    	if ( !empty($request['password']) ) {

    		Validator::make($request->all(), [
                'password' => 'required|string|min:4|confirmed',
                'password_confirmation' => 'required'
            ])->validate();

            $me->password = \Hash::make($request['password']);
    	}

    	$me->save();

    	$array = [
            'user' => $me
        ];

    	$request->session()->flash('success', 'Tu perfil se ha editado.');

    	return response()->json($array);
    }
}
