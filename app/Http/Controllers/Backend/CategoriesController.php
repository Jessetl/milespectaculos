<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Category as Category;
use App\CategoryType as Type;
use Carbon\Carbon;
use Session;
use Image;
use Storage;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'master']);
    }
    
    public function index()
    {
    	$categories = Category::with('category_type')->orderBy('id', 'ASC')->get();

    	return view('backend.categories.index', [
            'categories' => $categories
        ]);
    }

    public function create()
    {
        $categories = Category::orderBy('id', 'ASC')->get();

        return view('backend.categories.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:75|unique:categories',
            'slug' => 'required|alpha_dash|max:115|unique:categories',
            'file' => 'nullable|image'
        ]);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $request['slug'] = $this->streplace($request['slug']);
        $fileName = null;
        
        $categories = new Category();
        $categories->name = $request['name'];
        $categories->slug = $request['slug'];

        if ( !empty($request->file('file')) ) {
            
            $path = $request->file('file')->store('public/categories');
            $fileName = collect(explode('/', $path))->last();

            $image = Image::make(Storage::get($path));

            $image->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('jpg', 75));

            $categories->url = $fileName;
        }
        
        $categories->save();

        $type = new Type();
        $type->header = null;
        $type->category = $categories['id'];
        $type->name = 'Otros';
        $type->slug = 'otros';
        $type->save();

        Session::flash('success', 'Nueva categoría agregada');

        return redirect('/admin/categories');
    }    

    public function edit($slug)
    { 
    	$category = Category::whereSlug($slug)->firstOrFail();

        return view('backend.categories.edit', [
            'category' => $category
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorUpdate(array $data, $id)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_spaces|max:75', Rule::unique('categories', 'name')->ignore($id),
            'slug' => 'required|alpha_dash|max:75', Rule::unique('categories', 'slug')->ignore($id),
            'file' => 'nullable|image'
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validatorUpdate($request->all(), $id)->validate();

        $category = Category::findOrFail($id);
       
        $request['slug'] = $this->streplace($request['slug']);
        $fileName = $category->url;

        if ( !empty($request->file('file')) ) {

            $path = $request->file('file')->store('public/categories');
            $fileName = collect(explode('/', $path))->last();

            $image = Image::make(Storage::get($path));

            $image->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::put($path, (string) $image->encode('jpg', 75));

            if ( $category->url !== null ) {
                \File::Delete($this->uri().$category->url);
            }
            
            $category->url = $fileName;
        }

        $category->name = $request['name'];
        $category->slug = $request['slug'];
        $category->save();

        Session::flash('success', 'Categoría ' . $category->name . ' modificada');

        return redirect('/admin/categories');
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        \File::Delete($this->uri().$category->url);

        Session::flash('success', 'Categoría ' . $category->name . ' eliminada');

        return redirect()->back();
    }

    protected function fileName($name)
    {
        return 'IMG-ME'.mt_rand(10000, 99999).Carbon::now()->toDateString();
    }

    protected function uri()
    {
        return public_path().'/storage/categories/';
    }

    protected function streplace($string) 
    {
        $tofind = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $replac = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        $string = utf8_decode($string);
        $string = strtr($string,utf8_decode($tofind),$replac);
        $string = strtolower($string);

        return utf8_encode($string);
    }
}