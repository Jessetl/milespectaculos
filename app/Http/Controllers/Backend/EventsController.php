<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event as Event;
use Session;

class EventsController extends Controller
{
    public function index()
    {
        $events = Event::with('districts', 'user_event')->orderBy('date', 'ASC')->get();

        return view('backend.events.index', [
        	'events' => $events
        ]);
    }

    public function destroy($id)
    {
    	$event = Event::findOrFail($id);
    	$event->delete();

    	Session::flash('success', 'Evento emilinado');

    	return redirect()->back();
    }
}
