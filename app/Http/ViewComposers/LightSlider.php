<?php 

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\User as User;
use App\Post as Post;
use App\Carousel as Carousel;

class LightSlider 
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $premium = Carousel::findOrFail(1);
        $free = Carousel::findOrFail(2);

        $posts_users = Post::with('usr', 'posts_meta')->where([['type_posts', 0], ['status', 'published']])->orderBy('updated_at', 'DESC')->inRandomOrder()->limit(10)->get();

        $postsPremium = array();
        $postsFree = array();
        $posts = array();

        foreach ($posts_users as $key => $post) 
        {
            $post->image = $post->posts_meta()->where('meta_key', 'Image')->first();

            if ( !empty($post->image) ) {
                
                $post->image = $post->posts_meta()->where('meta_key', 'Image')->first()->meta_url;

                if ( $post->usr->premium ) {

                    if ( $premium->status ) {
                        $postsPremium[] = $post; 
                    }

                } else {

                    if ( $free->status ) {
                        $postsFree[] = $post;
                    }
                }   
            }
        }
           
        $posts = array_merge($postsPremium, $postsFree);

        $view->with(['posts' => $posts]);
    }
}