<?php 

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Configuration as Configuration;

class Configurations
{
	/**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
    	$configuration['image'] = Configuration::where('meta_key', 'Fotos')->firstOrFail();
        $configuration['audio'] = Configuration::where('meta_key', 'Audios')->firstOrFail();
        $configuration['video'] = Configuration::where('meta_key', 'Videos')->firstOrFail();

    	$view->with(['configuration' => $configuration]);
    }
}