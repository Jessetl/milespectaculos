<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $table = 'shopping_cart';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shopping', 'item', 'amount', 'iva'
    ];

    public function shoppings()
    {
    	return $this->belongsTo('App\Shopping', 'shopping');
    }

    public function districts()
    {
    	return $this->belongsTo('App\District', 'district');
    }
}
