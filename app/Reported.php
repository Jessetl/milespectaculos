<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reported extends Model
{
    protected $table = 'posts_reported';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post', 'reason', 'content'
    ];

    public function posts()
    {
        return $this->belongsTo('App\Post', 'post');
    }
}
