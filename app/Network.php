<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    protected $table = 'networks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'category_type', 'district', 'type_posts', 'type_amount', 'amount', 'message'
    ];

    public function usr()
    {
    	return $this->belongsTo('App\User', 'user');
    }

    public function category()
    {
    	return $this->belongsTo('App\CategoryType', 'category_type');
    }

    public function districts()
    {
    	return $this->belongsTo('App\District', 'district');
    }

    public function messages()
    {
    	return $this->hasMany('App\NetworkMessage', 'network', 'id');
    }

    public function likes()
    {
        return $this->hasMany(Like::class, 'network', 'id');
    }

    public function usersLikes()
    {
        return $this->belongsToMany(User::class, 'likes', 'network', 'user');
    }
}
