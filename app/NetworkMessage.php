<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NetworkMessage extends Model
{
    protected $table = 'networks_messages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'network', 'user', 'message'
    ];

    public function networks()
    {
    	return $this->belongsTo('App\Network', 'network');
    }

    public function usr()
    {
    	return $this->belongsTo('App\User', 'user', 'id');
    }
}
