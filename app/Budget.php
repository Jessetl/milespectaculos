<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $table = 'budgets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'type', 'country', 'postal', 'dayevent', 'duration', 'hourevent', 'budget', 'place', 'detail', 'status'
    ];

    public function usr()
    {
    	return $this->belongsTo('App\User', 'user');
    }
}
