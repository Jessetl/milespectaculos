<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table = 'conversations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'post', 'content'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user');
    }

    public function message()
    {
    	return $this->belongsTo('App\Message', 'Message');
    }
}
