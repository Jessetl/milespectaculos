<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class Message extends Model
{
    protected $table = 'messages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'post', 'content'
    ];

    public function getCreatedAtAttribute($date)
    {
        $newDate = Date::parse($date)->diffForHumans();

        return $newDate;
    }

    public function users()
    {
    	return $this->belongsTo('App\User', 'user');
    }

    public function post()
    {
    	return $this->belongsTo('App\Post', 'post');
    }

    public function response()
    {
        return $this->hasMany('App\Conversation', 'message', 'id');
    }
}
