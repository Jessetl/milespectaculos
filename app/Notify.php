<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class Notify extends Model
{
    protected $table = 'notify';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post', 'user', 'type', 'country', 'postal', 'dayevent', 'hourevent', 'place', 'detail'
    ];

    public function getDayEventAttribute($dayevent)
    {
        return new Date($dayevent);
    }

    public function pts()
    {
        return $this->belongsTo('App\Post', 'post');
    }

    public function usr()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function answers()
    {
        return $this->hasMany('App\MailAnswer', 'mail', 'id');
    }
}
