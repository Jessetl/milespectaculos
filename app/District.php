<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug'
    ];

    public function event()
    {
    	return $this->hasMany('App\Event', 'district', 'id');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post', 'posts_circuits', 'district', 'post')->withPivot('circuit')->withTimestamps();
    }

    public function items()
    {
        return $this->hasMany('App\ShoppingCart', 'district', 'id');
    }
}
