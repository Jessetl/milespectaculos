<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use App\Notifications\MyResetPassword;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'surname', 'email', 'password', 'role', 'confirmed', 
        'confirmation_code', 'phone', 'company', 'reset', 'premium_ends_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role'
    ];

    public function getRouteKeyName()
    {
        return 'username';
    }
    
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->toFormattedDateString();
    }

    public function events()
    {
        return $this->hasMany('App\Event', 'user', 'id');
    }

    public function posts()
    {
        return $this->hasMany('App\Post', 'user', 'id');
    }

    public function rol()
    {
        return $this->belongsTo('App\Role', 'role');
    }

    public function favorites()
    {
        return $this->belongsToMany('App\Post', 'favorites', 'user', 'post')->withTimestamps();
    }
    
    public function follows()
    {
        return $this->belongsToMany('App\User', 'followers', 'user', 'followed')->withTimestamps();
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'followers', 'followed', 'user')->withTimestamps();
    }

    public function socialite()
    {
        return $this->hasMany('App\UserSocialite', 'user', 'id');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment', 'user', 'id');
    }

    public function network()
    {
        return $this->hasMany('App\Network', 'user', 'id');
    }

    public function netw_message()
    {
        return $this->hasMany('App\NetworkMessage', 'user', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }

    public function isFavorite(Post $post)
    {
        return $this->favorites->contains($post);
    }

    public function isFollowing(User $user)
    {
        return $this->follows->contains($user);
    }
}
