<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocialite extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_socialite';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'red', 'socialite_id', 'profile_url', 'avatar', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];


    public function users()
    {
    	return $this->belongsTo('App\User', 'user');
    }
}
