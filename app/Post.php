<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Post extends Model
{
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header', 'category_type', 'user', 'name', 'slug', 'code',
        'type', 'type_posts', 'address', 'directions', 'phone1', 'phone2', 'email',
        'postal_code', 'url', 'type_amount', 'amount', 'length', 'discount', 'status', 'frequency', 'night_mode', 'budget',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getCreatedAtAttribute($date)
    {
        $newDate = Date::parse($date)->diffForHumans();

        return $newDate;
    }

    public function posts_meta()
    {
        return $this->hasMany('App\PostMeta', 'post', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\CategoryType', 'category_type');
    }

    public function usr()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function reported()
    {
        return $this->hasMany('App\Reported', 'post', 'id');
    }

    public function districts()
    {
        return $this->belongsToMany('App\District', 'posts_circuits', 'post', 'district')->withPivot('circuit')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'favorites', 'post', 'user')->withTimestamps();
    }

    public function messages()
    {
        return $this->hasMany('App\Message', 'post', 'id');
    }

    public function shopping()
    {
        return $this->hasOne('App\Shopping', 'shopping', 'id');
    }

    public function emails()
    {
        return $this->hasMany('App\Notify', 'post', 'id');
    }

    public function isPremium($user)
    {
        $me = User::findOrFail($user);

        return $me->premium;
    }

    public function countFollowers($user)
    {
        $me = User::findOrFail($user);

        return $me->followers()->get()->count();
    }

    public function scopeSearch($query, $request)
    {
        if (empty($request['districts'])) {
            if (empty($request['category'])) {

                return
                $query->where([['type_posts', 0], ['status', 'published']]);
                if (empty($request['amount_max']) and empty($request['amount_min'])) {
                    $query->where('type_amount', 'A Convenir');
                    $query->orWhere('type_amount', 'A Consultar');
                } else {
                    $query->whereBetween('amount', [$request->amount_min, $request->amount_max]);
                }
                if (!empty($request['search'])) {
                    $query->where('name', '%' . $request->search . '%');
                }
                if (!empty($request['search'])) {
                    $query->orWhere('code', '%' . $request->search . '%');
                }
                if (empty($request['type'])) {
                    $query->where([['type', 1], ['type', 0]]);
                }
                if (empty($request['date'])) {
                    $query->orderBy('updated_at', 'DESC');
                } else {
                    $query->orderBy('updated_at', $request->date);
                }

            } else {

                return
                $query->where([['type_posts', 0], ['status', 'published'], ['category_type', $request['subcategory']]]);
                if (empty($request['amount_max']) and empty($request['amount_min'])) {
                    $query->where('type_amount', 'A Convenir');
                    $query->orWhere('type_amount', 'A Consultar');
                } else {
                    $query->whereBetween('amount', [$request->amount_min, $request->amount_max]);
                }
                if (!empty($request['search'])) {
                    $query->where('name', '%' . $request->search . '%');
                }
                if (!empty($request['search'])) {
                    $query->orWhere('code', '%' . $request->search . '%');
                }
                if (empty($request['type'])) {
                    $query->where([['type', 1], ['type', 0]]);
                }
                if (empty($request['date'])) {
                    $query->orderBy('updated_at', 'ASC');
                } else {
                    $query->orderBy('updated_at', $request->date);
                }
            }

        } else {

            $pivot = $this->districts()->getTable();

            if (empty($request['category'])) {

                return
                $query->WhereHas('districts', function ($q) use ($request, $pivot) {
                    $q->where("{$pivot}.district", $request['districts']);
                    $q->where([['type_posts', 0], ['status', 'published']]);
                    if (empty($request['amount_max']) and empty($request['amount_min'])) {
                        $q->where('type_amount', 'A Convenir');
                        $q->orWhere('type_amount', 'A Consultar');
                    } else {
                        $q->whereBetween('amount', [$request->amount_min, $request->amount_max]);
                    }
                    if (!empty($request['search'])) {
                        $q->where('name', '%' . $request->search . '%');
                    }
                    if (!empty($request['search'])) {
                        $q->orWhere('code', '%' . $request->search . '%');
                    }
                    if (empty($request['type'])) {
                        $q->where([['type', 1], ['type', 0]]);
                    }
                    if (empty($request['date'])) {
                        $q->orderBy('posts.updated_at', 'ASC');
                    } else {
                        $q->orderBy('posts.updated_at', $request->date);
                    }
                });

            } else {

                $posts =
                $query->WhereHas('districts', function ($q) use ($request, $pivot) {
                    $q->where("{$pivot}.district", $request['districts']);
                    $q->where([['type_posts', 0], ['status', 'published']]);
                    $q->Where('category_type', $request['subcategory']);
                    if (empty($request['amount_max']) and empty($request['amount_min'])) {
                        $q->where('type_amount', 'A Convenir');
                        $q->orWhere('type_amount', 'A Consultar');
                    } else {
                        $q->whereBetween('amount', [$request->amount_min, $request->amount_max]);
                    }
                    if (!empty($request['search'])) {
                        $q->where('name', '%' . $request->search . '%');
                    }
                    if (!empty($request['search'])) {
                        $q->orWhere('code', '%' . $request->search . '%');
                    }
                    if (empty($request['type'])) {
                        $q->where([['type', 1], ['type', 0]]);
                    }
                    if (empty($request['date'])) {
                        $q->orderBy('posts.updated_at', 'ASC');
                    } else {
                        $q->orderBy('posts.updated_at', $request->date);
                    }
                });
            }
        }
    }

    public function scopeCallback($query, $request)
    {
        if (empty($request['search'])) {
            return
            $query->Where([['type_posts', 0], ['status', 'published']])
                ->orderBy('updated_at', 'DESC');
        }

        return
        $query->Where([['type_posts', 0], ['status', 'published'], ['name', 'like', '%' . $request['search'] . '%']])
            ->orWhere([['type_posts', 0], ['status', 'published'], ['code', 'like', '%' . $request['search'] . '%']])
            ->orWhere([['type_posts', 0], ['status', 'published'], ['content', 'like', '%' . $request['search'] . '%']])
            ->orderBy('updated_at', 'DESC');
    }
}
