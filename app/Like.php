<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'network'
    ];
    /*
    *   Relationships
    */
    public function usr()
    {
        return $this->belongsTo(User::class, 'user', 'id');
    }
}
