<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encrypted extends Model
{
    protected $table = 'codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'enabled', 'ends_at'
    ];
}
