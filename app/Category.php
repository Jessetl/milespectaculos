<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Category extends Model
{
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header', 'name', 'slug'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->toFormattedDateString();
    }

    public function category_type()
    {
    	return $this->hasMany('App\CategoryType', 'category', 'id');
    }
}