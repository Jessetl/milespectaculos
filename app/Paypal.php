<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Paypal extends Model
{
    protected $table = 'paypal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       	'name', 'slug', 'description', 'amount', 'type', 'status', 'ends_at'
    ];

    /**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
	    return 'slug';
	}

	public function getCreatedAtAttribute($date)
	{
		return Carbon::parse($date)->toFormattedDateString();
	}
}
