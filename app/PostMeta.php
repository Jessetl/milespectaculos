<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    protected $table = 'posts_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post', 'meta_key', 'meta_url'
    ];

    public function posts()
    {
        return $this->belongsTo('App\Post', 'post');
    }
}
