<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(155);

        // Using class based composers...
        View::composer(
            'welcome', 'App\Http\ViewComposers\LightSlider'
        );

        View::composer(
            [
                'frontend.posts.category.create-demands', 
                'frontend.posts.category.create-offerts', 
                'frontend.MyPosts.category.edit-demands', 
                'frontend.MyPosts.category.edit-offerts'
            ], 
            'App\Http\ViewComposers\Configurations'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*$this->app->bind('path.public', function() {
            return base_path('public_html');
        });*/
    }
}
