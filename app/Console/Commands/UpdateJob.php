<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Post as Post;
use App\User as User;

class UpdateJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Update:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update ads automatically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::with('posts')->where('premium', 1)->get();

        foreach ($users as $key => $user) {
            
            $posts = $user->posts;

            foreach ($posts as $key => $post) {
                $post->updated_at = Carbon::now();
                $post->save();
            }
        }

        $this->info('Post successfully updated!');
    }
}
