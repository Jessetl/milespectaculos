<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Mail\WarningEmail;
use App\Post as Post;
use App\User as User;
use Mail;

class CronEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Update:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $day = Carbon::now();
        $addDays = $day->addDays(7);
        $strdate = $addDays->toDateString();

        $posts = Post::with('usr')->where('length', $strdate)->get();

        foreach ($posts as $key => $post) {
            
            Mail::to($post->usr->email)->queue(new WarningEmail($post, $post->usr));

        }

        $this->info('Emails sent successfully!');
    }
}
