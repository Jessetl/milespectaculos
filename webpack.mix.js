let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

//mix.copy('node_modules/metisMenu/dist/metisMenu.js', 'resources/assets/js/metisMenu');
//mix.copy('node_modules/dropzone/dist/dropzone.js', 'resources/assets/js/dropzone');
//mix.copy('node_modules/dropzone/dist/dropzone.css', 'resources/assets/sass/frontend');
//mix.copy('node_modules/kendo-ui-core/js/kendo.core.js', 'resources/assets/js/kendo');
//mix.copy('node_modules/kendo-ui-core/js/kendo.userevents.js', 'resources/assets/js/kendo');